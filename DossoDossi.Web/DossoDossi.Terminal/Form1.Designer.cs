﻿
namespace DossoDossi.Terminal
{
    partial class frm_Giris
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Giris));
            this.btn_ayarlar = new System.Windows.Forms.Button();
            this.btn_cikis = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btn_siparis = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_ayarlar
            // 
            this.btn_ayarlar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_ayarlar.BackColor = System.Drawing.SystemColors.GrayText;
            this.btn_ayarlar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_ayarlar.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Bold);
            this.btn_ayarlar.ForeColor = System.Drawing.Color.Black;
            this.btn_ayarlar.Location = new System.Drawing.Point(2, 150);
            this.btn_ayarlar.Name = "btn_ayarlar";
            this.btn_ayarlar.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_ayarlar.Size = new System.Drawing.Size(230, 53);
            this.btn_ayarlar.TabIndex = 3;
            this.btn_ayarlar.Text = "AYARLAR";
            this.btn_ayarlar.UseVisualStyleBackColor = false;
            this.btn_ayarlar.Click += new System.EventHandler(this.btn_ayarlar_Click);
            // 
            // btn_cikis
            // 
            this.btn_cikis.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_cikis.BackColor = System.Drawing.Color.DarkGray;
            this.btn_cikis.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_cikis.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Bold);
            this.btn_cikis.ForeColor = System.Drawing.Color.Black;
            this.btn_cikis.Location = new System.Drawing.Point(0, 209);
            this.btn_cikis.Name = "btn_cikis";
            this.btn_cikis.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_cikis.Size = new System.Drawing.Size(230, 50);
            this.btn_cikis.TabIndex = 4;
            this.btn_cikis.Text = "ÇIKIŞ";
            this.btn_cikis.UseVisualStyleBackColor = false;
            this.btn_cikis.Click += new System.EventHandler(this.btn_cikis_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.SizeNS;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(2, -27);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(220, 112);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // btn_siparis
            // 
            this.btn_siparis.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_siparis.AutoSize = true;
            this.btn_siparis.BackColor = System.Drawing.SystemColors.GrayText;
            this.btn_siparis.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_siparis.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Bold);
            this.btn_siparis.ForeColor = System.Drawing.Color.Black;
            this.btn_siparis.Location = new System.Drawing.Point(0, 91);
            this.btn_siparis.Name = "btn_siparis";
            this.btn_siparis.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_siparis.Size = new System.Drawing.Size(230, 53);
            this.btn_siparis.TabIndex = 6;
            this.btn_siparis.Text = "YENi SİPARİŞ";
            this.btn_siparis.UseVisualStyleBackColor = false;
            this.btn_siparis.Click += new System.EventHandler(this.btn_siparis_Click);
            // 
            // frm_Giris
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(242, 299);
            this.Controls.Add(this.btn_siparis);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btn_cikis);
            this.Controls.Add(this.btn_ayarlar);
            this.HelpButton = true;
            this.Name = "frm_Giris";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Load += new System.EventHandler(this.frm_Giris_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_ayarlar;
        private System.Windows.Forms.Button btn_cikis;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btn_siparis;
    }
}

