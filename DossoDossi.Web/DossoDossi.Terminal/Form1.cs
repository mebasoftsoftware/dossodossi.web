﻿using DossoDossi.Data.Helper;
using DossoDossi.Terminal.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DossoDossi.Terminal
{
    public partial class frm_Giris : Form
    {
        public frm_Giris()
        {
            InitializeComponent();
        }

        private const int WS_SYSMENU = 0x80000;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.Style &= ~WS_SYSMENU;
                return cp;
            }
        }

        private void frm_Giris_Load_1(object sender, EventArgs e)
        {
            pictureBox1.BorderStyle = BorderStyle.None;

            //if (CacheModel.Projelers==null || !CacheModel.Projelers.Any())
            //{
            //    var confirmResult = MessageBox.Show("Hoşgeldiniz. Hafıza yenilensin mi? ","Sistem Uyarı",MessageBoxButtons.YesNo);

            //    if (confirmResult == DialogResult.Yes)
            //    {
            //        var sonu = DeviceHelper.RunSyncCahceModel();

            //       MessageBox.Show(sonu ? "İşlem yapıldı. Teşekkürler" : "Hata oluştu lütfen sonra deneyiniz.");
            //    }
            //}
        }

        private void btn_cikis_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        private void btn_ayarlar_Click(object sender, EventArgs e)
        {
            this.Hide();
            new frm_Ayarlar().Show();
        }

        private void btn_siparis_Click(object sender, EventArgs e)
        {
            this.Hide();
            new frm_Order().Show();
        }
    }
}
