﻿using DossoDossi.Data.Helper;
using DossoDossi.Data.Models;
using DossoDossi.Terminal.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace DossoDossi.Terminal
{
    public partial class frm_Order : Form
    {
        public frm_Order()
        {
            InitializeComponent();
        }

        private const int WS_SYSMENU = 0x80000;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.Style &= ~WS_SYSMENU;
                return cp;
            }
        }

        private void frm_Order_Load(object sender, EventArgs e)
        {
            groupBox1.Visible = true;

            groupBox2.Visible = false;

            this.FormBorderStyle = FormBorderStyle.None;

            var projeKodu = Properties.Settings.Default["ApiActiveProjeCode"].ToString();

            if (projeKodu.ToControl())
            {
                MessageBox.Show("Lütfen ayarlardan doğru ip port değerlerini yazınız.");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new frm_Giris().Show();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var no = textBox1.Text;

            if (no.ToControl())
            {
                MessageBox.Show("Lütfen müşteri no alanı giriniz.");
                return;
            }

            var userResp = DeviceHelper.GetUserBySicil(no.Trim());

            if (userResp==null ||!userResp.Status)
            {
                MessageBox.Show("Kayıtlı olmayan kullanıcı girdiniz. Sipariş verilemez.");
                return;
            }

            groupBox2.Visible = true;
            groupBox1.Visible = false;

            label2.Text = "Müşteri : " +  userResp.Data.Name;

            CacheModel.SeciliMusteri = userResp.Data;

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            var txt = textBox2.Text;

            if (txt.ToControl())
            {
                return;
            }

            var findApiUrun = DeviceHelper.GetFindProductByBarcode(txt);

            if(findApiUrun==null || !findApiUrun.Status)
            {
                MessageBox.Show("Hatalı barkod. Ürün bulunamadı.");
                return;
            }

            var urun = findApiUrun.Data;

            lbTotText.Text ="Total :" + urun.SatisFiyat.ToString("N2") + " " + urun.Doviz;
            labelSeriText.Text = "Seri : " + urun.SeriAdedi;

            labelUrunText.Visible = true;
            labelUrunText.Text = urun.StokAdi + " Kod : " + urun.StokKodu;

            textBox2.Text = string.Empty;

            var cari = CacheModel.SeciliMusteri;

            urun.Aciklama2 = "";
            
            var resp = DeviceHelper.CreateOfferOne(cari.MusteriId, urun.Id);

            //var body = new CreateOfferApiModel
            //{
            //    MusteriId = cari.Id,
            //    ProjeKodu = Properties.Settings.Default["ApiActiveProjeCode"].ToString(),
            //    Urunlers = new List<Data.Entity.ProjeTedarikciUrunler> { urun }
            //};

            //var resp = DeviceHelper.CreateOffer(body);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var sepetlers = CacheModel.Sepetlers;
            if (sepetlers == null)
            {
                sepetlers = new List<MusteriSepetItemListDto>();
            }

            if (sepetlers.Count == 0)
            {
                MessageBox.Show("Sepet boş");
                return;
            }
            groupBox1.Visible = false;
            groupBox2.Visible = false;

            //sepet listesi
            lbTotText.Text = "Toplam : " + sepetlers.Sum(a => a.Urun.SeriAdedi ?? 0) + " adet ürün";

            labelSeriText.Text = sepetlers.Sum(a => (a.Urun.SeriAdedi ?? 0) * a.Urun.SatisFiyat).ToString("N2") +" " + sepetlers.FirstOrDefault().Urun.Doviz+ " tutar";

        }

        private void button6_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = false;
            groupBox2.Visible = true;


        }

        private void button7_Click(object sender, EventArgs e)
        {
            //geri dön.
            groupBox1.Visible = true;
            groupBox2.Visible = false;

        }

        private void button5_Click(object sender, EventArgs e)
        {
            //sepeti boşalt

            var confirmResult = MessageBox.Show("Emin misiniz?", "Uyarı", MessageBoxButtons.YesNo);

            if (confirmResult == DialogResult.Yes)
            {
                var sepetlers = CacheModel.Sepetlers;
                if (sepetlers == null)
                {
                    sepetlers = new List<MusteriSepetItemListDto>();
                }

                var cari = CacheModel.SeciliMusteri;
                if (cari != null)
                {
                    CacheModel.Sepetlers = sepetlers.Where(a => a.Musteri.MusteriId != cari.MusteriId).ToList();
                }

                MessageBox.Show("Sepet boşaltıldı");

                groupBox1.Visible = false;
                groupBox2.Visible = true;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //siparişi tamamla.

            groupBox1.Visible = true;
            groupBox2.Visible = false;

            textBox1.Text = "";

            //var sepetlers = CacheModel.Sepetlers;
            //if (sepetlers == null)
            //{
            //    sepetlers = new List<MusteriSepetItemListDto>();
            //}

            //var cari = CacheModel.SeciliMusteri;

            //if (sepetlers.Count(a=>a.Musteri.Id== cari.Id) == 0)
            //{
            //    MessageBox.Show("En az bir ürün seçili olmalıdır.");
            //    return;
            //}

            //var list = sepetlers.Where(a => a.Musteri.Id == cari.Id).ToList();

            //var confirmResult = MessageBox.Show("Kaydetmek istediğinize emin misiniz?", "Bilgi", MessageBoxButtons.YesNo);

            //if (confirmResult == DialogResult.Yes)
            //{
            //    var body = new CreateOfferApiModel
            //    {
            //        MusteriId = cari.Id,
            //        ProjeKodu = CacheModel.ActiveProjeCode,
            //        Urunlers = list.Select(a => a.Urun).ToList()
            //    };

            //    var resp = DeviceHelper.CreateOffer(body);

            //    if (resp == null)
            //    {
            //        MessageBox.Show("Erişim sorunu yaşandı.Lütfen sonra tekrar deneyiniz.");
            //        return;
            //    }

            //    MessageBox.Show(resp.Message);

            //    if (resp.Status)
            //    {
            //        //sepet revize edildi.
            //        CacheModel.Sepetlers = sepetlers.Where(a => a.Musteri.Id != cari.Id).ToList();

            //        groupBox1.Visible = false;
            //        groupBox2.Visible = true;
            //    }
            //}
        }
    }
}
