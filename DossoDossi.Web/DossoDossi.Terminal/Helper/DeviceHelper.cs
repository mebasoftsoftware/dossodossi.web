﻿using DossoDossi.Data.Entity;
using DossoDossi.Data.Helper;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DossoDossi.Terminal.Helper
{
    public class DeviceHelper
    {
        /// <summary>
        /// Cache i günceller.
        /// </summary>
        /// <returns></returns>
        public static bool RunSyncCahceModel()
        {
            try
            {
                var respFuar = TerminalApiRequestHandler.Execute<ResponseMessage<List<ActiveFuarListDto>>>("GetActiveFuars", Method.GET, null, null);

                if (respFuar != null && respFuar.Data != null)
                {
                    CacheModel.Projelers = respFuar.Data;
                }

                if (CacheModel.Projelers.Any() && CacheModel.ActiveProjeCode.ToControl())
                {
                    CacheModel.ActiveProjeCode = CacheModel.Projelers.OrderByDescending(a=>a.ProjeId).FirstOrDefault().ProjeKodu;
                }

                var respUrunler = TerminalApiRequestHandler.Execute<ResponseMessage<List<ProjeTedarikciUrunler>>>("GetFuarProducts?code=" + CacheModel.ActiveProjeCode , Method.GET, null, null);

                if (respUrunler != null && respUrunler.Data != null)
                {
                    CacheModel.Urunlers = respUrunler.Data;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static ResponseMessage<Musteriler> GetUserBySicil(string no)
        {
            return TerminalApiRequestHandler.Execute<ResponseMessage<Musteriler>>("GetUserBySicilNo?code=" + Properties.Settings.Default["ApiActiveProjeCode"].ToString() + "&userNo=" + no, Method.GET, null, null);
        }

        public static ResponseMessage<string> CreateOffer(object body)
        {
            var parameters = new Dictionary<string, object> {  };
            
            parameters.Add("Application/Json", body);

            return TerminalApiRequestHandler.Execute<ResponseMessage<string>>("CreateOffer", Method.POST, null, parameters);
        }
        public static ResponseMessage<string> CreateOfferOne(Guid musteriId,Guid urunId)
        {
            return TerminalApiRequestHandler.Execute<ResponseMessage<string>>("CreateOfferOne?projeKodu=" + Properties.Settings.Default["ApiActiveProjeCode"].ToString()+"&musteriId="+musteriId +"&urunId="+urunId, Method.GET, null, null);
        }

        public static ResponseMessage<ProjeTedarikciUrunler> GetFindProductByBarcode(string txt)
        {
            var projeKodu = Properties.Settings.Default["ApiActiveProjeCode"].ToString();

            return TerminalApiRequestHandler.Execute<ResponseMessage<ProjeTedarikciUrunler>>("GetUrunByBarkod?code=" + projeKodu + "&barkod=" + txt, Method.GET, null, null);
        }

        public static ResponseMessage<string> GetSettingByCode(string code)
        {
            return TerminalApiRequestHandler.Execute<ResponseMessage<string>>("GetSettingByCode?code=" + code, Method.GET, null, null);
           
        }
    }
}
