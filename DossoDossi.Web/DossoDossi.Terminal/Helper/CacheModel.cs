﻿using DossoDossi.Data.Entity;
using System.Collections.Generic;

namespace DossoDossi.Terminal.Helper
{
    public static class CacheModel
    {
        public static string ActiveProjeCode { get; set; }
        public static List<ActiveFuarListDto> Projelers { get; set; }
        public static List<ProjeTedarikciUrunler> Urunlers{ get; set; }
        
        public static Musteriler SeciliMusteri { get; set; }
        public static List<MusteriSepetItemListDto> Sepetlers { get; set; }
    }

    public class ActiveFuarListDto
    {
        public int ProjeId { get; set; }
        public string ProjeKodu { get; set; }
        public string ProjeAdi { get; set; }
    }

    public class MusteriSepetItemListDto
    {
        public Musteriler Musteri{ get; set; }
        public ProjeTedarikciUrunler Urun{ get; set; }

    }

}
