﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using RestSharp;
using System;
using System.Collections.Generic;
using DossoDossi.Data.Helper;
using System.Net;

namespace DossoDossi.Terminal.Helper
{
    public class TerminalApiRequestHandler
    {
        //private readonly IRestClient _client;
        //public static string _baseUrl = "Api.Url".AppSettingsVal();
        public static string _token = string.Empty;

        //public TerminalApiRequestHandler() : this(new RestClient(_baseUrl))
        //{

        //}

        //public TerminalApiRequestHandler(IRestClient restClient)
        //{
        //    _client = restClient;
        //}

        /// <summary>
        /// Normal execute method
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="resource"></param>
        /// <param name="method"></param>
        /// <param name="objects"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static T Execute<T>(string resource, Method method, object objects, Dictionary<string, object> parameters,
            bool hasAuth = false, bool isBearerAuth = false)
        {
            //_client.Authenticator = new HttpBasicAuthenticator("sapuser", "ta123456");
            try
            {
                var settingApi = Properties.Settings.Default["ApiIpAddress"];
                var port = Properties.Settings.Default["ApiPortAddress"];

                var apiLink = "https://" + settingApi + ":" + port + "/api/";

                var client = new RestClient(apiLink);

                var request = new RestRequest
                {
                    Method = method,
                    Resource = resource,
                    RequestFormat = DataFormat.Json,
                    DateFormat = "yyyy-MM-ddTHH:mm:ssZ",

                    //JsonSerializer = new RestSharpJsonNetSerializer() { DateFormat = "yyyy-MM-ddTHH:mm:ssZ" }
                };

                if (hasAuth)
                    if (isBearerAuth)
                        request.AddHeader("Authorization", $"Bearer {_token}");
                    else
                        request.AddHeader("Api-Token", _token);

                if (parameters != null)
                {
                    request.AddHeader("Content-Type", "application/json");
                    request.AddHeader("Content-Length", "1500");
                    request.AddHeader("Host", "TerminalDevice");
                    request.AddHeader("Accept", "*/*");

                    foreach (var parameter in parameters)
                    {
                        var parameterValue = parameter.Value;
                        if (parameterValue is DateTime)
                            parameterValue = DateTime.Parse(parameterValue.ToString()).ToString("yyyy-MM-dd");
                        if (method == Method.PUT)
                            request.AddParameter(parameter.Key, parameterValue, ParameterType.UrlSegment);
                        else
                            request.AddParameter(parameter.Key, parameterValue, ParameterType.RequestBody);
                    }
                }

                if (objects != null)
                {
                    request.AddHeader("Content-Type", "application/json");
                    request.AddHeader("Content-Length", (JsonConvert.SerializeObject(objects).Length + 5000).ToString());
                    request.AddHeader("Host", (JsonConvert.SerializeObject(objects).Length + 5000).ToString());
                    request.AddHeader("Accept", "*/*");

                    request.AddBody(objects);
                }

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                var response = client.Execute(request);
                var result = response.Content;
                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    Formatting = Formatting.Indented,
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                };
                var model = JsonConvert.DeserializeObject<T>(result, jsonSerializerSettings);

                return model;
            }

            catch (Exception e)
            {
                return default(T);
            }
        }

    }
}
