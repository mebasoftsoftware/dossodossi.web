﻿using DossoDossi.Data.Helper;
using DossoDossi.Terminal.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DossoDossi.Terminal
{
    public partial class frm_Ayarlar : Form
    {
        public frm_Ayarlar()
        {
            InitializeComponent();
        }

        private void frm_Ayarlar_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.None;
            label1.Text ="Aktif proje : " + Properties.Settings.Default["ApiActiveProjeCode"].ToString();

            textBox1.Text = Properties.Settings.Default["ApiIpAddress"].ToString();
            textBox2.Text = Properties.Settings.Default["ApiPortAddress"].ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            new frm_Giris().Show();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //var selectedValue = (string)comboBox1.SelectedValue;
            //var selectedText = (string)comboBox1.SelectedText;
            //CacheModel.ActiveProjeCode = selectedValue;

            Properties.Settings.Default["ApiIpAddress"] = textBox1.Text;
            Properties.Settings.Default["ApiPortAddress"] = textBox2.Text;

            Properties.Settings.Default.Save();

            //MessageBox.Show("Kayıt başarılı");

            var getProjeCode = DeviceHelper.GetSettingByCode("ActiveFuarNo");

            if (getProjeCode == null)
            {
                MessageBox.Show("Girdiğiniz ip adresi veya port ile sunucuya erişemiyor. Aktif proje kodu çekilemedi.");
            }
            else
            {
                Properties.Settings.Default["ApiActiveProjeCode"] = getProjeCode.Data;
                Properties.Settings.Default.Save();

                new frm_Giris().Show();
                this.Close();
            }
        }
    }
}
