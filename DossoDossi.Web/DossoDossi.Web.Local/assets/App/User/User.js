﻿var User = {
    GetList: function (currentPage, filter, order, takeCount) {
        Erp.U.SaveFilterParam(currentPage, filter, order, takeCount, 'Id|DESC');
        var form = $("#FilterForm");
        //  $.Block.Open({ target: ".portlet" });
        Erp.U.A.Req.F(form, function (data) {
            //console.log(JSON.stringify(data));
            if (data != null && data.Data != null && data.Data.length > 0) {
                //  alert(JSON.stringify(data.Data));
                var count = data.Pager.CurrentPage === 0 ? 1 : (data.Pager.CurrentPage * data.Pager.TakeCount) + 1;
                $.each(data.Data, function (i, item) {
                    item.Index = count;

                    item.json = JSON.stringify(item);

                    if (item.Tarih != null) {
                        item.Tarih = Erp.U.TimeFormat(item.Tarih);
                    }

                    if (item.FisTarih != null) {
                        item.FisTarih = Erp.U.TimeFormat(item.FisTarih);
                    }

                    if (item.StartDate != null) {
                        item.StartDate = Erp.U.TimeFormat(item.StartDate);
                    }
                    if (item.CreateDate != null) {
                        item.CreateDate = Erp.U.TimeFormat(item.CreateDate);
                    }
                    if (item.StarDateProgram != null) {
                        item.StarDateProgram = Erp.U.TimeFormat(item.StarDateProgram);
                    }

                    if (item.EndDate != null) {
                        item.EndDate = Erp.U.TimeFormat(item.EndDate);
                    }
                    if (item.QuestionDate != null) {
                        item.QuestionDate = Erp.U.TimeFormat(item.QuestionDate);
                    }
                    if (item.Date != null) {
                        item.Date = Erp.U.TimeFormat(item.Date);
                    }

                    if (item.OrderDate != null) {
                        item.OrderDateFormat = Erp.U.TimeFormat(item.OrderDate);
                    }


                    if (item.DeliveryDate != null) {
                        item.DeliveryDateFormat = Erp.U.TimeFormat(item.DeliveryDate);
                    }

                    if (item.Status != null) {
                        //darft
                        if (item.Status == 0) {
                            item.SendToManager = true;
                        }

                        //approved
                        if (item.Status == 2) {
                            item.SendToOrder = true;
                        }

                        if (item.Status < 4) {
                            item.IsEdit = true;
                        }
                    }

                    if (item.Kapali != null) {

                        if (item.Kapali == "A") {
                            item.LabelColor = "#e0e0e0";
                            item.LabelText = "Pending"
                            item.IsEdit = true;

                        } else if (item.Kapali == "K") {
                            item.LabelColor = "#26a69a";
                            item.LabelText = "All Shipped"
                        } else {
                            item.LabelColor = "#ffc107";
                            item.LabelText = "Partial Shipped"
                        }
                    }


                    if (item.AcikSiparisAdedi != null && item.MinimumStok != null && item.MaxStok != null) {

                        if (item.AcilmasiGerekenStok != null) {
                            if (item.AcilmasiGerekenStok < 0) {
                                item.TextColor = "white";
                                item.BackColor = "green";
                            } else if (item.AcilmasiGerekenStok > 0 && item.AcilmasiGerekenStok >= item.MinimumStok) {
                                item.TextColor = "white";
                                item.BackColor = "red";
                            }
                        }

                        item.OngorulenStok = item.MerkezStok + item.AcikSiparisAdedi;
                    }

                    if (item.SonTarih != null) {
                        item.SonTarih = Erp.U.TimeFormat(item.SonTarih);
                    }

                    count++;
                });
            }

            var innerHtml = Mustache.to_html($('#Template').html(), { DATA: data.Data });
            $("#List").empty().append(innerHtml);
            Erp.U.GetPager(data.Pager, data.Total, "#Pager");
            $("#FilterResult_Total_Count").val(data.Total);
            $("#countTabTotal").text(data.Total);

            $('#Pager ul li a').click(function () {
                User.GetList($(this).attr("data-page"), data.Pager.Filter, data.Pager.Order);
            });

            Erp.U.ActiveTooltip();
        });
    },
    Event: function () {

        User.GetList(null, null, "Id|DESC", $("#TakeNumberList").val());

        $("#TakeNumberList").on('change', function () {
            var t = $(this).val();
            $("#TakeCount").val(t);
            User.GetList();
        });

        $("#Refresh").click(function () {
            $("#Filter").val('');
            $("#FiltreInput").val('');
            $(".clearprop").val('');
            $(".clearpropselect").val('-1');

            User.GetList(null, null, null, 10);
            $("#TakeNumberList").val('10');
            $("#AppendedItemLiSt").empty();
            $(".addtobaskeymultiplebutton").hide();

        });
        $("#FiltreButton").click(function () {

            $('#Filter').val($('#FiltreInput').val());
            var take = $("#TakeNumberList").val();
            var page = $("#CurrentPage").val();
            User.GetList(page, null, null, take);
        });

        Erp.U.KeyPressActive('#FiltreInput', '#FiltreButton');

        Erp.U.SortOrderColumn(function (orderResult) {
            User.GetList(null, null, orderResult, 0);
        });
    },
    Detail: function (id) {
        Erp.U.P.Modal.Open("/User/Detail", { id: id }, function () {

        });
    },
    Delete: function (id) {

        bootbox.confirm("Silmek istediğinize emin misiniz ?",
            function (result) {
                if (result === true) {
                    Business.U.A.Req.C("POST", "/Customer/Delete", { id: id }, function (data) {

                        if (!data.Status) {
                            Business.U.P.T.Warning("Uyarı !  : ", data.Message);
                            return false;
                        }

                        Business.U.P.T.Success("İşlem Sonucu : ", data.Message);
                        Target.GetList(null, null, "Id|DESC", 10);
                    });
                }
            });
    },
}
