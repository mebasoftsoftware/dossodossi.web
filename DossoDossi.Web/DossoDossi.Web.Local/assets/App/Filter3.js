﻿var Filter3 = {
    GetList: function (currentPage, filter, order, takeCount) {
        Erp.U.SaveFilterParamDifferent("3",currentPage, filter, order, takeCount, 'ID|DESC');
        var form = $("#FilterForm3");
        //  $.Block.Open({ target: ".portlet" });
        Erp.U.A.Req.F(form, function (data) {
            //console.log(JSON.stringify(data));
            if (data != null && data.Data != null && data.Data.length > 0) {
                //  alert(JSON.stringify(data.Data));
                var count = data.Pager.CurrentPage === 0 ? 1 : (data.Pager.CurrentPage * data.Pager.TakeCount) + 1;
                $.each(data.Data, function (i, item) {
                    item.Index = count;
                    
                    item.json = JSON.stringify(item);

                    count++;
                });
            }
            
            var innerHtml = Mustache.to_html($('#Template3').html(), { DATA: data.Data });
            $("#List3").empty().append(innerHtml);
            Erp.U.GetPager(data.Pager, data.Total, "#Pager3");
            $("#FilterResult_Total_Count3").val(data.Total);
            $("#countTabTotal3").text(data.Total);

            $('#Pager3 ul li a').click(function () {
                Filter3.GetList($(this).attr("data-page"), data.Pager.Filter, data.Pager.Order);
            });
            

            Erp.U.ActiveTooltip();
        });
    },
    Event: function () {

        Filter3.GetList(null, null, "ID|DESC", 10);

        $("#TakeNumberList3").on('change', function () {
            var t = $(this).val();
            $("#TakeCount3").val(t);
            Filter3.GetList();
        });

        $("#Refresh3").click(function () {
            $("#Filter3").val('');
            $("#FiltreInput3").val('');
            $(".clearprop3").val('');
            $(".clearpropselect3").val('-1');
            
            Filter3.GetList(null, null, null, 10);
            $("#TakeNumberList").val('10');
            $("#AppendedItemLiSt").empty();
            $(".addtobaskeymultiplebutton").hide();

        });
        $("#FiltreButton3").click(function () {

            $('#Filter3').val($('#FiltreInput3').val());
            var take = $("#TakeNumberList3").val();
            var page = $("#CurrentPage3").val();
            Filter3.GetList(page,null,null,take);
        });

        Erp.U.KeyPressActive('#FiltreInput3', '#FiltreButton3');

        Erp.U.SortOrderColumnDif('tablecontent3', function (orderResult) {
            Filter3.GetList(null, null, orderResult, 0);
        });
    }
    
}
