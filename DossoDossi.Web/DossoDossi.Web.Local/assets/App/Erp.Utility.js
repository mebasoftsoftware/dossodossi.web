﻿/// <reference path="Erp.Core.js" />
/// <reference path="../vendor/jquery/plugin/toastr.min.js" />
/// <reference path="../vendor/jquery/jquery-1.8.3.js" />

//Paginator için eklendi. -Menzeher-.Değiştirilebilir.

var ToPagerV2 = function (option) {
    var _options = $.extend(true, {}, option);
    var pages = Math.ceil(_options.count / (_options.pageCount));

    if (_options.currentPage > pages || _options.count < _options.pageCount || _options.count <= 3) {
        jQuery(_options.target).empty();
        return false;
    }
    var i = _options.currentPage + 1 >= _options.maxView ? _options.currentPage - 2 : 0;
    var ekle = i + _options.maxView;
    if (_options.maxView < pages) {
        if (pages - _options.currentPage === 1) {
            ekle = ekle - 2;
            i = i - 2;
        } else if (pages - _options.currentPage === 2) {
            ekle = ekle - 1;
            i = i - 1;
        }
    }
    var length = _options.currentPage + 1 >= _options.maxView ? ekle : _options.maxView > pages ? pages : _options.maxView;
    var pagination = new $.StringBuilder();

    // Left Pagination Begin
    pagination.Append('<ul class="pagination pagination-sm pull-right">');

    // Left Button Begin
    if (_options.currentPage === 0) {
        pagination.Append('<li class="paginate_button page-item  disabled"><a href="javascript:;" class="page-link"><i class="fa fa-angle-left"></i></a></li>');
    } else {
        pagination.Append($.Format('<li class="paginate_button page-item "><a href="javascript:;" data-page="{0}" class="page-link"><i class="fa fa-angle-left"></i></a></li>', (_options.currentPage - 1)));
    }
    // Left Button End

    // Numeric Button Begin
    for (i; i < length; i++) {
        if (i === _options.currentPage) {
            pagination.Append($.Format('<li class="disabled paginate_button page-item "><a class="page-link" href="javascript:;">{0}</a></li>', i + 1));
        } else {
            pagination.Append($.Format('<li class="paginate_button page-item "><a class="page-link" href="javascript:;" data-page="{0}">{1}</a></li>', i, i + 1));
        }
    }
    // Numeric Button End

    // Right Button Begin
    if (_options.currentPage === (pages - 1)) {
        pagination.Append('<li class="disabled paginate_button page-item "><a class="page-link" href="javascript:;"><i class="fa fa-angle-right"></i></a></li>');
    } else {
        pagination.Append($.Format('<li class="paginate_button page-item "><a class="page-link" href="javascript:;" data-page="{0}"><i class="fa fa-angle-right"></i></a></li>', _options.currentPage + 1));
    }
    // Right Button End

    pagination.Append('</ul>');
    // Left Pagination End

    // Right Pagination Begin
    pagination.Append('<ul class="pagination pagination-sm pull-left">');

    // First Button Begin
    if (_options.currentPage === 0) {
        pagination.Append('<li class="paginate_button page-item  previous disabled"><a class="page-link" href="javascript:;">&larr;&nbsp;'+'First'+'</a></li>');
    } else {
        pagination.Append('<li class="paginate_button page-item  previous"><a class="page-link" href="javascript:;" data-page="0">&larr;&nbsp;' + 'First'+'</a></li>');
    }
    // First Button End

    pagination.Append($.Format('<li class="paginate_button page-item "><span class="page-link text-muted" style="cursor:default;">{0} Page, {1} '+'Result'+'</span></li>', pages, _options.count));

    // Last Button Begin
    if (_options.currentPage === (pages - 1)) {
        pagination.Append('<li class="paginate_button page-item  next disabled"><a class="page-link" href="javascript:;">'+'Last'+'&nbsp;&rarr;</a></li>');
    } else {
        pagination.Append($.Format('<li class="paginate_button page-item  next"><a class="page-link" href="javascript:;" data-page="{0}">' + 'Last'+'&nbsp;&rarr;</a></li>', (pages - 1)));
    }
    // Last Button End

    pagination.Append('</ul>');
    // Right Pagination End

    jQuery(_options.target).empty().html(pagination.ToString());
};


jQuery.Format = function (source, params) {
    if (arguments.length === 1) {
        params = "";
        source = "Not found params!";
    }
    if (arguments.length > 2 && params.constructor !== Array) {
        params = $.makeArray(arguments).slice(1);
    }
    if (params.constructor !== Array) {
        params = [params];
    }
    $.each(params, function (i, n) {
        source = source.replace(new RegExp("\\{" + i + "\\}", "g"), function () {
            return n;
        });
    });
    return source;
};

jQuery.LineClear = function (text) {
    return text.replace("\n", "")
        .replace("\'", "")
        .replace("\"", "")
        .replace("\n", "")
        .replace("\r", "")
        .replace("\t", "")
        .replace("\b", "")
        .replace("\f", "");
};



//Fruit Utility
Erp.U = {
    Cache: {
        CanPost: true
    },
    //Ajax
    A: {
        //Request
        Req: {
            //Custom
            C: function(method, action, params, fnSuccessCallback, fnErrorCallback, hideLoader) {
                params = method === 'POST' ? JSON.stringify(params) : params;
                if (!hideLoader)
                    loadingPane.show();
                $.ajax({
                    type: method,
                    url: action,
                    data: params, //JSON.stringify(params),
                    dataType: 'JSON',
                    contentType: 'application/json; charset=utf-8',
                    async: true,
                    cache: false,
                    //timeout: 3000,
                    success: fnSuccessCallback,
                    error: function(jqxhr, settings, thrownError) {
                        if (fnErrorCallback == null) {
                            Erp.U.A.Res.Error(jqxhr, settings, thrownError);
                        } else {
                            fnErrorCallback(jqxhr, settings, thrownError);
                        }
                    },
                    complete: function() {
                        if (!hideLoader)
                            loadingPane.hide();
                    }
                });
            },
            //Form
            F: function(form, fnSuccessCallback, fnErrorCallback, fnBeforeSubmit) {
                //if (Erp.U.Cache.CanPost) {
                //    Erp.U.Cache.CanPost = false;
                //    setTimeout(function () {
                //        Erp.U.Cache.CanPost = true;
                //    }, 3000);
                //}
                //else
                //    return false;

                if (jQuery().inputmask) {
                    $('.maskPhone').inputmask('remove');
                }
                form.ajaxSubmit({
                    dataType: 'json',
                    clearForm: false,
                    resetForm: false,
                    //timeout: 3000,
                    beforeSubmit: function() {
                        loadingPane.show();

                        if (fnBeforeSubmit != null) {
                            fnBeforeSubmit();
                        }
                    },
                    success: fnSuccessCallback,
                    error: function(jqxhr, settings, thrownError) {
                        if (fnErrorCallback == null) {
                            Erp.U.A.Res.Error(jqxhr, settings, thrownError);
                        } else {
                            fnErrorCallback(jqxhr, settings, thrownError);
                        }
                    },
                    complete: function() {
                        loadingPane.hide();
                        Erp.U.P.Mask();
                    }
                });
            },
            // HtmlData
            H: function(method, action, params, fnSuccessCallback, fnErrorCallback) {
                loadingPane.show();
                $.ajax({
                    type: method,
                    url: action,
                    dataType: 'HTML',
                    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                    async: true,
                    cache: false,
                    data: params, //JSON.stringify($params),
                    success: fnSuccessCallback,
                    error: function(jqxhr, settings, thrownError) {
                        if (fnErrorCallback == null) {
                            Erp.U.A.Res.Error(jqxhr, settings, thrownError);
                        } else {
                            fnErrorCallback(jqxhr, settings, thrownError);
                        }
                    },
                    complete: function() {
                        loadingPane.hide();
                    }
                });
            },
            // HtmlData Form
            HForm: function(form, fnSuccessCallback, fnErrorCallback, fnBeforeSubmit) {
                form.ajaxSubmit({
                    dataType: 'HTML',
                    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                    clearForm: true,
                    resetForm: true,
                    //timeout: 3000,
                    beforeSubmit: function() {
                        loadingPane.show();

                        if (fnBeforeSubmit != null) {
                            fnBeforeSubmit();
                        }
                    },
                    success: fnSuccessCallback,
                    error: function(jqxhr, settings, thrownError) {
                        if (fnErrorCallback == null) {
                            Erp.U.A.Res.Error(jqxhr, settings, thrownError);
                        } else {
                            fnErrorCallback(jqxhr, settings, thrownError);
                        }
                    },
                    complete: function() {
                        loadingPane.hide();
                        Erp.U.P.Mask();
                    }
                });
            }
        },

        //Response
        Res: {
            Error: function(jqxhr, settings, thrownError) {
                Erp.U.P.T.Error('<b>An error occurred</b> ',
                    ' Cannot operate due to the error occurred.',
                    function() {
                        var message = '<b>Error Status:</b> ' + jqxhr.status + '<br />';
                        message += '<b>Thrown Error:</b> ' + thrownError + '<br />';
                        message += '<b>Error ResponseText:</b><br />' + jqxhr.responseText;

                        var generator = window.open('', 'ErrorWindow', 'height=600,width=800');
                        generator.document.write(message);
                        generator.document.close();
                    });
            },

            Exception: function(exception) {
                Erp.U.P.T.Error('Error',
                    'Server internal error has occurred.',
                    function() {
                        //var message = "Message: \n" + exception.Message + "\n\n";
                        //alert(message);

                        var generator = window.open('', 'ExceptionWindow', 'height=600,width=800');
                        generator.document.write(exception.Message);
                        generator.document.close();
                    });
            },

            ValidationErrors: function(validationErrors) {
                var errors = '';
                $.each(validationErrors,
                    function(index, item) {
                        errors += item + '<br />';
                    });

                if (!Erp.U.Variable.IsNullOrZero(validationErrors.length)) {
                    Erp.U.P.T.Warning('Doğrulama Hatası ', errors);
                }
            },

            AuthorizationErrors: function() {
                Erp.U.P.T.Warning('Geçersiz Yetki ', 'Buraya giriş yetkiniz bulunmamaktadır.');
            },

            Result: function(response, fnSuccessCallback) {
                // alert(JSON.stringify(response));
                if (typeof response === 'string' && Erp.U.IsJson(response))
                    response = JSON.parse(response);
                else if (typeof response === 'string')
                    response = { Data: response };
                switch (response.ErrorFlag) {
                case -1:
                    Erp.U.A.Res.Exception(response.Exception);
                    return false;
                case 1:
                    Erp.U.A.Res.Exception(response.Exception);
                    return false;

                case -2:
                    Erp.U.A.Res.ValidationErrors(response.ValidationErrors);
                    return false;
                //{"ErrorFlag":10,"Exception":null,"ValidationErrors":["Alt kategori bulunmaktadır.Lütfen önce alt kategorileri siliniz."],"Data":null}
                case 10:
                    Erp.U.A.Res.ValidationErrors(response.ValidationErrors);
                    return false;

                case -3:
                case -4:
                    Erp.U.A.Res.AuthorizationErrors();
                    return false;
                case -5:
                    Erp.U.P.T.Warning('Aktivasyon Hatası', 'Lütfen mailinize gelen aktivasyon kodunu onaylayınız.');
                    return false;
                }
                fnSuccessCallback(response.Data);
                return false;
            }

        }

    },

    Validate: {
        IsValid: function(form) {
            var result = form.valid(); //data('unobtrusiveValidation').validate();
            return result;
        },

        Validator: function(form) {
            $.validator.unobtrusive.parse(form);
        }

    },

    Document: {
        WriteImageBase64: function($this, toImgId) {
            var filesSelected = $this.files;
            if (filesSelected.length > 0) {
                var fileToLoad = filesSelected[0];

                var fileReader = new FileReader();
                fileReader.onload = function(fileLoadedEvent) {
                    var img = document.getElementById(toImgId);
                    img.src = fileLoadedEvent.target.result;
                };
                fileReader.readAsDataURL(fileToLoad);
            }
        },

        QueryString: function(param) {
            param = param.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
            var exp = '[\\?&]' + param + '=([^&#]*)';
            var regexp = new RegExp(exp);
            var results = regexp.exec(window.location.href);
            if (results == null) {
                return '';
            } else {
                return results[1];
            }
        }
    },

    Variable: {
        IsNullOrEmpty: function(value) {
            var result = value == null || value.length == 0;
            return result;
        },

        IsNullOrZero: function(value) {
            var result = value == null || value == 0;
            return result;
        }

    },
    //Plugin

    P: {
        Tab: function() {
            $('a[data-toggle="tab"]').on('click',
                function(e) {
                    return location.hash = $(e.target).attr('href').substr(1);
                });
        },
        Mask: function() {
            if (jQuery().inputmask) {
                $('.maskPhone').inputmask('mask',
                    {
                        'mask': '(999) 999-99-99'
                    });
            }
        },
        Modal: {
            Open: function (action, params, fnSuccessCallback, fnErrorCallback, modal, isLong, disableLetter) {

                $("#ModalAlani").empty().append(' <div id="ajax-modal" style="overflow: auto !important" class="modal fade" role="dialog" tabindex="-1" data-backdrop="" data-keyboard="true"></div>')

                if (modal == null) {
                    modal = $('#ajax-modal');
                }

                Erp.U.A.Req.H('GET',
                    action,
                    params,
                    function(response) {
                        Erp.U.A.Res.Result(response,
                            function (data) {

                                modal.html(data).modal("show");

                                //modal.html(data).modal().on('hidden',
                                //    function() {
                                //        modal.removeClass('bs-modal-lg').empty();
                                //    }).on('shown.bs.modal',
                                //    function(e) {
                                //
                                //    });

                                if (fnSuccessCallback != null) {
                                    fnSuccessCallback(data);
                                }
                                if (isLong) {
                                    modal.addClass('bs-modal-lg').find('>.modal-dialog').addClass('modal-lg');
                                }
                            });
                    },
                    fnErrorCallback);
            },
            Close: function(modal) {
                if (modal == null) {
                    $('#ajax-modal').modal('hide');
                } else {
                    modal.modal('hide');
                }
            }
        },
        Loader: {
            Open: function(object, message) {
                if (message == null) {
                    message = 'İşlem Yapılıyor...';
                }

                Erp.U.P.Loader.Loading(true, message, object);
            },

            Close: function(object, message) {
                if (message == null) {
                    message = 'İşlem Yapılıyor...';
                }

                Erp.U.P.Loader.Loading(false, message, object);
            },

            Loading: function(switchh, str, obj) {
                var loadingEle =
                    $(
                        '<div class="LoadingPanel"><img src="/Content/Images/Loading.gif" alt="" title="Yükleniyor..." /></div>');
                if (switchh) {
                    window.guid = (Math.random() * 99999).toFixed(0);
                    loadingEle
                        .addClass('loading' + guid)
                        .show();
                    //.fadeIn(450);

                    if (obj != null) {
                        obj
                            .addClass('pl oh')
                            .prepend(loadingEle);
                    } else {
                        $('body').prepend(loadingEle);
                    }
                } else {
                    $('.loading' + guid).remove();
                    //$('.loading[id=' + guid + ']').fadeOut(450, function () {
                    //    $(this).remove();
                    //});
                }
            }
        },
        T: {
            Init: function(type, title, message, fnOnClickCallback) {
                toastr.options = {
                    "closeButton": true,
                    "debug": true,
                    "positionClass": 'toast-top-right',
                    "showDuration": '300',
                    "hideDuration": '1000',
                    "timeOut": '3000',
                    "extendedTimeOut": '1000',
                    "showEasing": 'swing',
                    "hideEasing": 'linear',
                    "showMethod": 'fadeIn',
                    "hideMethod": 'fadeOut',
                    "onclick": fnOnClickCallback
                }

                //title = (title != undefined) ? '<b class="db">' + title + '</b>' : "";
                var totalMsg = '<strong>' + title + '</strong> ' + message;
                if (type === 'success') {
                    toastr.success(totalMsg);
                } else if (type === 'info') {
                    toastr.info(totalMsg);
                } else if (type === 'warning') {
                    toastr.warning(totalMsg);
                } else if (type === 'error') {
                    toastr.error(totalMsg);
                }
            },

            Success: function(title, message, fnOnClickCallback) {
                Erp.U.P.T.Init('success', title, message, fnOnClickCallback);
            },

            Warning: function(title, message, fnOnClickCallback) {
                Erp.U.P.T.Init('warning', title, message, fnOnClickCallback);
            },

            Info: function(title, message, fnOnClickCallback) {
                Erp.U.P.T.Init('info', title, message, fnOnClickCallback);
            },

            Error: function(title, message, fnOnClickCallback) {
                Erp.U.P.T.Init('error', title, message, fnOnClickCallback);
            },
            Swal: function(title, message, status) {
                swal(
                    title,
                    message,
                    status
                );
            }
        },
        Date: {
            Use: function(edit) {
                var dt = $('.dateTime');
                if (edit) {
                    dt.each(function() {
                        $(this).val($(this).data('date'));
                    });
                } else
                    dt.val('');
                dt.datepicker({
                    format: 'dd/mm/yyyy',
                    orientation: 'left',
                    autoclose: true,
                    language: 'tr',
                    weekStart: 1,
                });
            },
            UseHour: function() {
	            var dt = $('.dateTime');

                dt.datetimepicker({
	                language: "tr",
	                format: "‎dd/mm/yyyy hh:ii",
	                autoclose: true,
	                //rtl: App.isRTL(),
	                todayBtn: true
                });
            }
        }
    },

    Cookie: {
        Write: function(cName, value, expiredays) {
            var exdate = new Date();
            exdate.setDate(exdate.getDate() + expiredays);
            Erp.U.Cookie.Delete(cName);
            document.cookie =
                cName + '=' + escape(value) + ((expiredays == null) ? '' : ';expires=' + exdate.toUTCString());
        },

        Read: function(cName) {
            if (document.cookie.length > 0) {
                var cStart = document.cookie.indexOf(cName + '=');
                if (cStart !== -1) {
                    cStart = cStart + cName.length + 1;
                    var cEnd = document.cookie.indexOf(';', cStart);
                    if (cEnd === -1) cEnd = document.cookie.length;
                    return unescape(document.cookie.substring(cStart, cEnd));
                }
            }
            return '';
        },

        Delete: function(name) {
            document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        },

        Append: function(cookieName, value, split) {
            if (value != null && value !== '') {
                split = split != null ? split : ',';
                value = value.toString();
                var oldCookie = Erp.U.Cookie.Read(cookieName);
                var nowCookie = [];
                if (Erp.U.IsAny(oldCookie)) {
                    nowCookie = nowCookie.concat(oldCookie.split(split));
                    if (nowCookie.indexOf(value) === -1) {
                        nowCookie.push(value);
                    }
                } else {
                    nowCookie.push(value);
                }
                Erp.U.Cookie.Write(cookieName, nowCookie.join(split), 365);
            }
        },

        Any: function(cookieName, value, split) {
            if (value != null && value !== '') {
                value = value.toString();
                split = split != null ? split : ',';
                var cookie = Erp.U.Cookie.Read(cookieName).split(split);
                return cookie.indexOf(value) > -1;
            }
            return false;
        },

        Replace: function(cookieName, value, split) {
            value = value.toString();
            split = split != null ? split : ',';
            var oldCookie = Erp.U.Cookie.Read(cookieName).split(split);
            var position = oldCookie.indexOf(value);
            if (position !== -1) {
                oldCookie.splice(position, 1);
                Erp.U.Cookie.Write(cookieName, oldCookie.join(split), 365);
            }
        }
    },

    ObjKeyCapitalize: function(obj) {
        for (var i = 0; i < obj.length; i++) {
            var a = obj[i];
            for (var key in a) {
                if (a.hasOwnProperty(key)) {
                    var temp = a[key];
                    delete a[key];
                    a[key.charAt(0).toUpperCase() + key.substring(1)] = temp;
                }
            }
            obj[i] = a;
        }
        return obj;
    },

    UpdateQueryString: function(key, value) {

        // Use window URL if no query string is provided
        var uri = window.location.href;

        // Create a dummy element to parse the URI with
        var a = document.createElement('a'),

            // match the key, optional square bracktes, an equals sign or end of string, the optional value
            regEx = new RegExp(key + '((?:\\[[^\\]]*\\])?)(=|$)(.*)') // Setup some additional variables
            ,
            qsLen,
            keyFound = false;

        // Use the JS API to parse the URI 
        a.href = uri;

        // If the URI doesn't have a query string, add it and return
        if (!a.search) {

            a.search = '?' + key + '=' + value;

            return a.href;
        }

        // Split the query string by ampersands
        var qs = a.search.replace(/^\?/, '').split(/&(?:amp;)?/);
        qsLen = qs.length;

        // Loop through each query string part
        while (qsLen > 0) {

            qsLen--;

            // Check if the current part matches our key
            if (regEx.test(qs[qsLen])) {

                // Replace the current value
                qs[qsLen] = qs[qsLen].replace(regEx, key + '$1') + '=' + value;

                keyFound = true;
            }
        }

        // If we haven't replaced any occurences above, add the new parameter and value
        if (!keyFound) {
            qs.push(key + '=' + value);
        }

        // Set the new query string
        a.search = '?' + qs.join('&');

        return a.href;
    },

    GetRandomColor: function() {
        var letters = '0123456789ABCDEF'.split('');
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    },

    InputEnter: function(e, target) {
        if (e.keyCode === 13) {
            e.preventDefault();
            $(target).trigger('click');
            return false;
        }
        return true;
    },

    FormatCurrency: function(num) {
        num = num.toString().replace(/\$|\,\./g, '');
        if (isNaN(num))
            num = '0';
        num = Math.floor(num * 100 + 0.50000000001);
        num = Math.floor(num / 100).toString();
        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
            num = num.substring(0, num.length - (4 * i + 3)) +
                '.' +
                num.substring(num.length - (4 * i + 3));
        return num;
    },

    IsMobile: function() {
        return (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini|Mobile/i.test(navigator.userAgent
            .toLowerCase()));
    },

    QueryString: function(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    },

    ConvertToEn: function($Html) {
        if (Boolean($Html)) {
            var $TurkishCharacter = 'çÇöÖşŞıİüÜğĞ';
            var $SplitCharacter = 'cCoOsSiIuUgG';
            for (var i = 0; i < $TurkishCharacter.length; i++) {
                var Code = RegExp($TurkishCharacter[i], 'gm');
                $Html = $Html.replace(Code, $SplitCharacter[i]);
            }
        }
        return $Html;
    },

    IsAny: function(data) {
        return data != null && data.length > 0;
    },

    Share: function(socialSite, url, title) {
        url = url == null ? location.href : url;
        var socialUrl = 'https://www.facebook.com/sharer/sharer.php?u=' + url;

        if (socialSite === 'tw' || socialSite === 'twitter') {
            socialUrl = 'http://twitter.com/share?text=' + title + '&url=' + url + '&via=yuzbasiogluoto';
        } else if (socialSite === 'in' || socialSite === 'linkedin') {
            socialUrl = 'https://www.linkedin.com/shareArticle?url=' + url;
        } else if (socialSite === 'gp' || socialSite === 'googleplus') {
            socialUrl = 'https://plus.google.com/share?url=' + url;
        } else if (socialSite === 'whatsapp' || socialSite === 'wht') {
            socialUrl = "https://api.whatsapp.com/send?text=" + encodeURIComponent(url);
        }

        window.open(socialUrl, socialSite + 'win', 'width=500, height=400');
    },

    IsNullOrEmpty: function(value) {
        var result = value == null || value.length === 0;
        return result;
    },

    IsNullOrZero: function(value) {
        var result = value == null || value === 0;
        return result;
    },

    SortBy: function(field, reverse, primer) {
        /// <summary>
        /// Json nesne sıralamasını yapar
        /// Örnek kullanım:Erp.U.SortBy('UserId', true, parseInt);
        /// </summary>
        /// <param name="field">Sıralama yapılacak alan</param>
        /// <param name="reverse">A'dan Z'ye Yada Z'den A'ya</param>
        /// <param name="primer">parseInt/// boşta geçilebilir</param>
        var key = primer ? function(x) { return primer(x[field]) } : function(x) { return x[field] };

        reverse = !reverse ? 1 : -1;

        return function(a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    },

    VehicleLink: function(brandName, modelName, versionName, urunId) {
        return '/' +
            this.SpaceReplace(brandName) +
            '-' +
            this.SpaceReplace(modelName) +
            '-' +
            this.SpaceReplace(versionName) +
            '/ikinci-el-oto-ilan-' +
            urunId;
    },

    SpaceReplace: function(boslukluStr) {
        boslukluStr = boslukluStr.replace(/\ /g, '-');
        boslukluStr = boslukluStr.replace(/\+/g, '');
        return boslukluStr.toLowerCase();
    },

    ContentShow: function(parent) {
        parent = parent == null ? $('.xParent') : parent;
        parent.each(function() {
            var content = $(this).find('.xContent');
            var notFound = $(this).find('.xNotFound');
            var contentItem = content.find('.xItem').length;
            if (contentItem > 0) {
                content.show();
                notFound.hide();
            } else {
                content.hide();
                notFound.show();
            }
        });
    },

    IsJson: function(responseText) {
        try {
            var json = JSON.parse(responseText);
            return true;
        } catch (e) {
            return false;
        }
    },

    SearchBox: function(searchBox, parentName, itemName) {
        $(searchBox).keyup(function() {
            var txtVal = $(this).val().toLowerCase();
            if (Erp.U.IsNullOrEmpty(txtVal)) {
                $(parentName + ' ' + itemName).show();
                return false;
            }
            $(parentName + ' ' + itemName).hide().each(function(i) {
                var dataVal = $(this).data('search').toLowerCase();
                if (dataVal.indexOf(txtVal) !== -1) {
                    $(this).show();
                }
            });
            //$(parentName + ' ' + itemName + '[data-search*="' + txtVal.toLowerCase() + '"]').show();
            return false;
        });
    },

    Tmpl: function(deletedItem, data, cache) {
        var nextItem = deletedItem.next();
        deletedItem.remove();
        if (nextItem.length === 0) {
            $(cache.Tmpl).tmpl(data).appendTo(cache.Body);
        } else
            $(cache.Tmpl).tmpl(data).insertBefore(nextItem);
    },

    ToLower: function(value) {
        return value.toLowerCase();
    },

    InputRegex: function($this, reg, reg2, limit) {
        $this = $($this);
        var val = $this.val();
        if (!reg2)
            reg2 = '';
        var regex = new RegExp(reg, reg2);
        $this.val(val.replace(regex, ''));
        val = $this.val();
        if (limit > 0 && val.length > limit) {
            $this.val(val.substring(0, limit));
        }
    },

    PhoneFormat: function($Html, putZero) {
        if ($Html == null) {
            return '';
        }
        var $NewTel = '';
        if (putZero) {
            $Html = $Html[0] == 0 ? $Html : '0' + $Html;
        }
        $Html = $Html.replace(/[\+\)\(a-zA-Z\W ]+/g, '');
        if (Boolean($Html)) {
            var $SifirControlLength = 10;
            var $Kosul = 'i == 2 || i == 5 || i == 7';
            if ($Html[0] == '0') {
                $SifirControlLength = 11;
                $Kosul = 'i == 0 || i == 3 || i == 6 || i == 8';
            }
            var $Length = $Html.length >= 11 ? $SifirControlLength : $Html.length;
            for (var i = 0; i < $Length; i++) {
                if (eval($Kosul))
                    $NewTel += $Html[i] + ' ';
                else
                    $NewTel += $Html[i];
            }
            return $NewTel;
        }
        return $Html;
    },

    RemoveObjItem(myObjects, prop, valu) {
        return myObjects.filter(function(val) {
            return val[prop] !== valu;
        });

    },

    RemoveArrayItem(array, val) {
        var index = array.indexOf(val);
        if (index > -1) {
            array.splice(index, 1);
        }
        return array;
    },

    GetPager: function(pager, total, id) {
        ToPagerV2({
            count: total,
            currentPage: parseInt(pager.CurrentPage),
            target: id,
            maxView: 10,
            pageCount: pager.TakeCount
        });
    },

    SaveFilterParam: function(currentPage, filter, order, takeCount, defaultOrder) {
        if (takeCount > 0)
            $("#TakeCount").val(takeCount);
        if (filter !== undefined && filter !== null && filter.length > 0) {
            $("#Filter").val(filter);
        }

        $("#CurrentPage").val(currentPage);
        if (order === undefined || order === null) {
            $("#Order").val(defaultOrder);
        } else {
            $("#Order").val(order);
        }
    },

    SaveFilterParamDifferent: function(number, currentPage, filter, order, takeCount, defaultOrder) {
        if (takeCount > 0)
            $("#TakeCount" + number + "").val(takeCount);
        if (filter !== undefined && filter !== null && filter.length > 0) {
            $("#Filter" + number + "").val(filter);
        }

        $("#CurrentPage" + number + "").val(currentPage);
        if (order === undefined || order === null) {
            $("#Order" + number + "").val(defaultOrder);
        } else {
            $("#Order" + number + "").val(order);
        }
    },

    KeyPressActive: function(textbox, button) {
        $('' + textbox + '').bind('keypress',
            function(e) {
                var code = e.keyCode || e.which;
                if (code === 13) {
                   
                    $('' + button + '').click();
                    return false;
                }
            });
    },

    SortOrderColumn: function(response) {
        $(".tablecontent thead tr th a").each(function() {
            $(this).click(function() {
                var t = $(this);
                var order = t.data('order').split('|');
                var prop = order[0];
                var r = '|';
                var ord = order[1];
                var d = "";
                // alert(ord);
                if (ord === 'DESC') {
                    d = 'ASC';
                } else {
                    d = 'DESC';
                }
                var cls = t.find('.angleitems');
                if (cls.hasClass('fa-angle-double-up')) {
                    cls.removeClass('fa-angle-double-up').addClass('fa-angle-double-down');
                } else {
                    cls.removeClass('fa-angle-double-down').addClass('fa-angle-double-up');
                }
                var orderResult = '' + prop + r + d + '';
                $(this).data('order', orderResult);
                //Response döndürür.
                response(orderResult);
                return true;
            });
        });
        return false;
    },

    SortOrderColumnDif: function(tabcls,response) {
        $("." + tabcls+" thead tr th a").each(function() {
            $(this).click(function() {
                var t = $(this);
                var order = t.data('order').split('|');
                var prop = order[0];
                var r = '|';
                var ord = order[1];
                var d = "";
                // alert(ord);
                if (ord === 'DESC') {
                    d = 'ASC';
                } else {
                    d = 'DESC';
                }
                var cls = t.find('.angleitems');
                if (cls.hasClass('fa-angle-double-up')) {
                    cls.removeClass('fa-angle-double-up').addClass('fa-angle-double-down');
                } else {
                    cls.removeClass('fa-angle-double-down').addClass('fa-angle-double-up');
                }
                var orderResult = '' + prop + r + d + '';
                $(this).data('order', orderResult);
                //Response döndürür.
                response(orderResult);
                return true;
            });
        });
        return false;
    },

    ActiveTooltip: function() {
        $(".tooltips").tooltip();
    },
    FastSearch: function(input, placeholder, form, response) {
        input.select2({
            placeholder: placeholder,
            minimumInputLength: 3,
            allowClear: true,
            ajax: {
                url: form.attr('action'),
                dataType: 'json',
                type: "POST",
                error: function(jqXHR, textStatus, errorThrown) {
                    Erp.U.P.T.Warning("Hata! " + textStatus, errorThrown);
                },
                data: function(term, page) {
                    var postData = form.serializeArray();
                    postData.push({ name: "name", value: term });
                    return postData;
                },
                timeout: 10000,
                results: function(data, page) {
                    var parseData = data;
                    
                    return {
                        results: parseData
                    };
                }
            }
        }).on("change",
            function(t) {

            });
    },
    FastSearchColor: function(input, placeholder, form, response) {
        input.select2({
            placeholder: placeholder,
            minimumInputLength: 3,
            allowClear: true,
            ajax: {
                url: form.attr('action'),
                dataType: 'json',
                type: "POST",
                error: function(jqXHR, textStatus, errorThrown) {
                    Erp.U.P.T.Warning("Hata! " + textStatus, errorThrown);
                },
                data: function(term, page) {
                    var postData = form.serializeArray();
                    postData.push({ name: "name", value: term });
                    return postData;
                },
                timeout: 10000,
                results: function(data, page) {
                    var parseData = data;
                    
                    return {
                        results: parseData
                    };
                }
            },
            formatResult: format,
            formatSelection: format
        }).on("change",
            function(t) {

            });
    },

    CheckAll: function(input, classcheck) {
        if ($("" + input + "").is(':checked')) {
            $("" + classcheck + "").attr('checked', true);
        } else {
            $("" + classcheck + "").removeAttr('checked');
        }
    },

    TimeFormat: function (val) {

        //if (val == null || val.length < 1) {
        //    return null;
        //}

        //return val;

        var value =  new Date(parseInt(val.replace("/Date(", "").replace(")/", ""), 10));
        var date = value;

        //var date = val;
        var _currMonth = parseInt(date.getMonth());
        var currMonth = _currMonth + 1;
        if (currMonth < 10) {
            currMonth = "0" + currMonth;
        }

        var day = date.getDate();
        if (day < 10) {
            day = "0" + day;
        }
        
        return day + "." + currMonth + "." + date.getFullYear();
    },

    TimeFormatNew: function (val) {
        if (val == null || val.length < 1) {
            return null;
        }

        var sonuc = val.split(' ')[0];
        if (sonuc.length == 9) {
            sonuc = "0" + sonuc;
        }
        return sonuc;
    },

    GetDistrict: function($this) {

        var val = $this.val();

        $(".Filter_City").val(val);
        $(".Filter_District").val('');

        $(".districtoptgroup").empty().append('<option data-val="-1" value="-1">Tüm İlçeler</option>');

        var cityNo = $this.find('option:selected').attr("data-val");

        if (cityNo < 1) {
            return false;
        }

        Erp.U.A.Req.C("POST",
            "/Rapor/GetDistrict/",
            { id: cityNo },
            function(response) {

                if (response != null && response.length > 0) {
                    $.each(response,
                        function(i, item) {

                            $(".districtoptgroup").append('<option value="' +
                                item.DISTRICT_NAME +
                                '">' +
                                item.DISTRICT_NAME +
                                '</option>')

                        });
                }
            });
    },
    ExportToExcel: function(status) {
        var p = "#Export_";
        $(p + "CurrentPage").val($("#CurrentPage").val());
        $(p + "Filter").val($("#Filter").val());
        $(p + "Order").val($("#Order").val());
        var listTotalCount = $("#FilterResult_Total_Count").val();

        if (listTotalCount > 0) {
            if (status == true)
                $(p + "TakeCount").val(listTotalCount);
            else
                $(p + "TakeCount").val($("#TakeCount").val());
            
            $("#ExportForm").submit();
        } else {
            Erp.U.P.T.Warning("Dikkat", "Listede excel aktarım için kayıt yok .")
        }
    },

    ChangePassword: function() {

        var old = $("#oldpass").val();
        var newpas = $("#newpass").val();
        var newrep = $("#newpassrep").val();

        if (old.length < 1 || newpas.length < 1 || newrep.length < 1) {
            Erp.U.P.T.Warning("Uyarı !", "Lütfen formu eksiksiz doldurunuz.");
            return false;
        }

        bootbox.confirm("İşlemi yapmak istediğinize emin misiniz ? ",
            function(result) {
                if (result === true) {
                    Erp.U.A.Req.C("POST",
                        "/Account/ChangePassword/",
                        { old: old, newpass: newpas, newrep: newrep },
                        function(response) {

                            if (response.Status) {
                                Erp.U.P.T.Success("Bilgi ", response.Message);

                            } else {
                                Erp.U.P.T.Warning("Uyarı ", response.Message);
                            }
                        });
                }
            });
    },
    Pulsate: function(element) {
        $(element).pulsate({
            color: "#399bc3",
            reach: 50,
            repeat: 3,
            speed: 100,
            glow: true
        });
    }
}

// Active Link
var activeLinks = function () {
    var nav = location.pathname;
    SideBarActive(nav);
}();


function SideBarActive(nav) {
    var a = $('.page-sidebar ul li a[href="' + nav + '"]:first');
    var menuContainer = $('.page-sidebar ul');
    menuContainer.children('li.active').removeClass('active');
    menuContainer.children('arrow.open').removeClass('open');

    a.parents('li').each(function () {
        $(this).addClass('active open');
        $(this).children('a > span.arrow').addClass('open');
    });
    a.parents('li').addClass('active');
}
//#Active Link
var tableToExcel = (function () {
    var uri = 'data:application/vnd.ms-excel;base64,',
        template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>',
        base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) },
        format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
    return function (table, name, exclude) {
        table = $(table);
        table = table.clone();
        table.find('thead>tr:gt(0)').remove();
        if (Erp.U.IsAny(exclude)) {
            $.each(exclude, function (i, item) {
                var t = table.find('#' + item);
                var index = t.index();
                table.find('tbody>tr').each(function () {
                    $(this).find('td').eq(index).remove();
                });
                t.remove();
            });
        }
        var ctx = { worksheet: name || 'Worksheet', table: table.html() }
        $('body').append('<a href="javascript:;" id="exportData1asda" style="display:none"></a>');
        document.getElementById('exportData1asda').href = uri + base64(format(template, ctx));
        document.getElementById('exportData1asda').download = name + '.xls';
        document.getElementById('exportData1asda').click();
        $('#exportData1asda').remove();
        //window.location.href = uri + base64(format(template, ctx));
    }
})();


String.prototype.tUpper = function () {
    return this.replace(/i/g, "İ").toLocaleUpperCase();
}

String.prototype.tLower = function () {
    return this.replace(/I/g, "ı").toLocaleLowerCase();
}
String.prototype.isLetter = function () {
    return this.length === 1 && this.match(/[a-zığüşçöİ]/i);
}



function OpenClickPivotCollapse(level) {
    loadingPane.show();

    $('.level' + level).find('.foldunfold').click();

    setTimeout(function () {
        loadingPane.hide();
    }, 1000);

}

function PivotTotalShowHide(levels,$this) {
    if ($this.is(':checked')) {
        var arr = levels.split(',');

        $.each(arr,
            function(i, item) {
                $(".level" + item + " td").each(function() { $(this).css('background', '#f5fbfe') });
                $(".level"+item+" td").each(function() { $(this).text('') });
            });
    } else {
        $("#FiltreButton").click();
    }
}



function CreateCustomerAsModal() {
    $("#customeradd-modal").modal("show");

    SehirList();

}

function CreateCustomerModalSave() {
    var form = $("#formAddCustomer_layout");
    if (!Erp.U.Validate.IsValid(form)) {
        return false;
    }

    bootbox.confirm("Eklemek istediğinize emin misiniz ?",
        function (result) {
            if (result === true) {

                Erp.U.A.Req.F(form,
                    function (data) {
                        if (data.Status == true) {
                            $("#customeradd-modal").modal("hide");
                            Erp.U.P.T.Success(data.Title, "Sayfa yenileniyor.....");
                            setTimeout(function () { location.reload(); }, 2500);

                        } else {
                            Erp.U.P.T.Warning("Uyarı ", data.Message);
                        }
                    });
            }
        });
}

function SehirList() {

    var len = $("#SehirSelect option");
    if (len.html() == undefined || len.length == 0) {

        $.ajax({
            url: '/Adres/SehirGetir/',
            type: "GET",
            dataType: "json",
            success: function (data) {
                $("#SehirSelect").append('<option value="">İl Seçiniz</option>')
                $.each(data,
                    function (i, item) {
                        $("#SehirSelect").append('<option data-kod="' + item.Kodu + '" value="' + item.Adi + '">' + item.Adi + '</option>');
                    });
            }
        });
    }
}

function GetAdresDynamic(source, target, table, emptArea) {
    var selectedVal = $(source + ' option:selected').val();

    $(emptArea).empty();
    $(emptArea).append('<option value="">Seçiniz</option>');

    if (selectedVal == -1) {
        return false;
    }

    var ustKodu = $(source + ' option:selected').attr("data-kod");

    $.ajax({
        url: '/Adres/GetList/?ustKod=' + ustKodu + '&table=' + table,
        type: "GET",
        dataType: "json",
        success: function (data) {

            $.each(data,
                function (i, item) {
                    $(target).append('<option data-kod="' + item.Kodu + '" value="' + item.Adi + '">' + item.Adi + '</option>');
                });

        }
    });
}

function CoorporateShowHide(status) {
    if (status == true) {
        $('.corporateInfo').show(400);
    } else {
        $('.corporateInfo').hide(400);
    }
}

function format(state) {

    if (!state.id) return state.text;
    if (state.color != null) {
        return '<span style="color:'+state.color+'">'+ state.text +'</span>';
    } else {
        return state.text;
    }
    
}


var loadingPane = {
    show: function () {

        //$.isLoading({
        //    text: '<b>Yükleniyor..</b>',
        //    'tpl':
        //        '<span class="isloading-wrapper %wrapper%"><img src="/Assets/loading.gif" width="60" height="60" alt=""/><br/>%text%</span>',
        //});
    },
    hide: function () {
       // $.isLoading("hide");
    }
};