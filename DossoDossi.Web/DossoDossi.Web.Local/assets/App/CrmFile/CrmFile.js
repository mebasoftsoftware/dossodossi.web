﻿var CrmFile = {
  
    Open: function (type,recordId) {

        Erp.U.P.Modal.Open("/CrmFile/Open", { type: type, recordId: recordId }, function () {

        });
    },

    GetFiles: function (type, recordId) {

        $(".fileHtmlDiv").html('<div> Loading...</div>');

        Erp.U.A.Req.H("GET", "/CrmFile/GetFiles", { type: type, recordId : recordId }, function (data) {

            $(".fileHtmlDiv").html(data);
        });
    },
    Create: function () {

        var type = $("#CrmFile_Type").val();
        var recordId = $("#CrmFile_RecordId").val();

        bootbox.confirm("Are you sure you want to save ?",
            function (result) {
                if (result === true) {

                    var form = $("#OrderFileForm");
                    //  $.Block.Open({ target: ".portlet" });
                    Erp.U.A.Req.F(form,
                        function (data) {

                            Erp.U.P.T.Success("Result", "Successfully saved !");
                            CrmFile.GetFiles(type, recordId);
                        });
                }
            });

    },
    Delete: function (id) {

        var type = $("#CrmFile_Type").val();
        var recordId = $("#CrmFile_RecordId").val();

        bootbox.confirm("Are you sure you want to delete file ?",
            function (result) {
                if (result === true) {

                    Erp.U.A.Req.C("POST",
                        "/CrmFile/Delete",
                        { id : id},
                        function (data) {
                            if (data == true) {
                                Erp.U.P.T.Success("Result", "Succesfully deleted !");

                                CrmFile.GetFiles(type, recordId);

                            } else {

                                Erp.U.P.T.Warning("Warning", "An error occured !");
                            }
                        });
                }
            });
    }
}
