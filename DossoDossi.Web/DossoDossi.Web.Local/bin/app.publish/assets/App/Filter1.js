﻿var Filter1 = {
    GetList: function (currentPage, filter, order, takeCount) {
        Erp.U.SaveFilterParamDifferent("1",currentPage, filter, order, takeCount, 'ID|DESC');
        var form = $("#FilterForm1");
        //  $.Block.Open({ target: ".portlet" });
        Erp.U.A.Req.F(form, function (data) {
            //console.log(JSON.stringify(data));
            if (data != null && data.Data != null && data.Data.length > 0) {
                //  alert(JSON.stringify(data.Data));
                var count = data.Pager.CurrentPage === 0 ? 1 : (data.Pager.CurrentPage * data.Pager.TakeCount) + 1;
                $.each(data.Data, function (i, item) {
                    item.Index = count;
                    
                    item.json = JSON.stringify(item);

                    count++;
                });
            }
            
            var innerHtml = Mustache.to_html($('#Template1').html(), { DATA: data.Data });
            $("#List1").empty().append(innerHtml);
            Erp.U.GetPager(data.Pager, data.Total, "#Pager1");
            $("#FilterResult_Total_Count1").val(data.Total);
            $("#countTabTotal1").text(data.Total);

            $('#Pager1 ul li a').click(function () {
                Filter1.GetList($(this).attr("data-page"), data.Pager.Filter, data.Pager.Order);
            });

            Erp.U.ActiveTooltip();
        });
    },
    Event: function () {

        Filter1.GetList(null, null, "ID|DESC", 10);

        $("#TakeNumberList1").on('change', function () {
            var t = $(this).val();
            $("#TakeCount1").val(t);
            Filter1.GetList();
        });

        $("#Refresh1").click(function () {
            $("#Filter1").val('');
            $("#FiltreInput1").val('');
            $(".clearprop1").val('');
            $(".clearpropselect1").val('-1');
            
            Filter1.GetList(null, null, null, 10);
            $("#TakeNumberList").val('10');
            $("#AppendedItemLiSt").empty();
            $(".addtobaskeymultiplebutton").hide();

        });
        $("#FiltreButton1").click(function () {

            $('#Filter1').val($('#FiltreInput1').val());
            var take = $("#TakeNumberList1").val();
            var page = $("#CurrentPage1").val();
            Filter1.GetList(page,null,null,take);
        });

        Erp.U.KeyPressActive('#FiltreInput1', '#FiltreButton1');

        Erp.U.SortOrderColumnDif('tablecontent1',function (orderResult) {
            Filter1.GetList(null, null, orderResult, 0);
        });
    }
    
}
