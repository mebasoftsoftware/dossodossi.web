﻿var totalDto = {
    TotalAmount: 0,
    TotalDiscount : 0,
    TotalNet: 0,
    VatTotal: 0
};





function RandomGet() {
    return Math.floor(Math.random() * 100000000);
}


function SatirEkle() {
    var no = RandomGet();

    var innerHtml = Mustache.to_html($('#TemplateOffer').html(), { DATA: { No: no } });

    $("#ListOffer").append(innerHtml);

    OrderLineCal();
}


function OrderLineCal() {
    var no = 1;
    $(".ordernoline").each(function () {
        $(this).text(no);
        no++;
    });
}


function SatirSil(no) {


    bootbox.confirm("Are you sure want to remove?",
        function (result) {
            if (result === true) {

                $(".l_sel_del_load_" + no).show();
                $(".l_sel_delet_" + no).hide();

                setTimeout(function () {
                    $(".list_line_" + no).remove();
                    OrderLineCal();
                    CalculateAll();
                },
                    300);

            }
        });
}

var selectedLineNo = "";

function OpenSearchProduct(no) {
    selectedLineNo = no;

    var cari = $("#CustomerCode").val();

    if (cari.length < 1) {

        Erp.U.Pulsate("#CustomerCode");
        Erp.U.P.T.Warning("Warning ", "Please choose client.");
        return false;
    }

    Erp.U.P.Modal.Open("/Stock/OpenProduct",
        {},
        function () { });

}

function ChooseProduct(stokId) {

    var json = JSON.parse($("#ProductSearch_Json_" + stokId).val());
    $(".waitingLoad").show();

    $(".sel_unit_code_" + selectedLineNo).val(json.BirimSetiKodu);
    $(".l_sel_product_id_" + selectedLineNo).val(json.StokId);

    var stok = parseFloat(json.EmagazaKodu.replace(',', '.'));

    var isignore = $("#StockControlIgnore").val();

    if (isignore == undefined) {
        if (stok <= 0) {
            Erp.U.P.T.Warning("Warning ", "Stock is not available.");
            return false;
        }
    }

    AmbarSorgula(stokId, json);

    FiyatSorgula(stokId, $("#CustomerCode").val());
}


function AmbarSorgula(stokId, json) {

    var ambarNo = $("#AmbarSelect").val();

    Erp.U.A.Req.C("GET",
        "/Stock/GetStockCalc",
        { stokId: stokId, ambarNo: ambarNo },
        function (data) {

            $(".reserveliToplamAdet_" + selectedLineNo).text(data.Reserve);
            $(".ambarStok_" + selectedLineNo).text(data.Stock);
            $(".netStok_" + selectedLineNo).text(data.NetStock);
            $(".netStok_" + selectedLineNo).attr("data-net",data.NetStock);

            if (data.NetStock <= 0) {

                // return false;
            }

            $("#Item_stok_code_" + selectedLineNo).val(json.MalzemeKodu);
            $("#Item_stok_name_" + selectedLineNo).val(json.MalzemeAdi);

            $(".closebutton").click();

            $("#reservelistmodal_" + selectedLineNo).empty();

            if (data.ReserveList != null && data.ReserveList.length > 0) {

                $.each(data.ReserveList, function (i, item) {
                    $("#reservelistmodal_" + selectedLineNo).append('<tr><td>' + item.TeklifNo + '</td><td> ' + item.CariUnvan + '</td> <td>' + item.Miktar + '</td> <td>' + item.ReserveDate + '</td></tr>')
                });
            }

            //todo netstok 0 altı ise talep açılamaz.
        });
}

function FiyatSorgula(stokId, cariKodu) {

    Erp.U.A.Req.C("GET",
        "/Stock/GetMalzemeFiyat",
        { stokId: stokId, cariKodu: cariKodu},
        function (data) {

            $(".l_sel_price_" + selectedLineNo).val(data.Fiyat);
            $(".l_clientpr_code_" + selectedLineNo).val(data.OzelKodu);

            //todo netstok 0 altı ise talep açılamaz.
        });
}


function IsReserveChange(no) {
    var val = $(".sel_isreserved_" + no).val();

    if (val == "0") {
        $(".reservedendDate_" + no).val('');
        $(".reservedendDate_" + no).attr("disabled", "disabled");
    } else {
        $(".reservedendDate_" + no).removeAttr("disabled");
        var val = $("#SetDateSatirMax").val();
        $(".reservedendDate_" + no).val(val);
        Erp.U.Pulsate(".reservedendDate_" + no);
    }
}


function GetItems() {
    var res = [];

    $(".allitemlistlines").each(function () {
        var no = $(this).attr("data-no");

        var miktar = $(".l_sel_miktar_" + no).val().replace(',', '.');
        var netStock = $(".netStok_" + no).attr("data-net");

        var price = $(".l_sel_price_" + no).val();

        if (price.indexOf(".") >= 0 && price.indexOf(",") >= 0) {
            price = price.replace('.', "");
        }

        var currency = $(".l_sel_doviz_" + no).val();
      
        if (currency != "" && price.length > 0 && miktar.length > 0) {
            var add = {
                LineNo: no,
                Qty: parseFloat(miktar),
                Price: parseFloat(price.replace(',', '.')),
                Currency: currency,
                Discount: $(".sel_discount_" + no).val(),
                Vat: $(".sel_vat_"+no).val()
            };

            if (netStock != undefined && netStock != "" && netStock != null) {

                var netStk = parseFloat(netStock.replace(",", "."));

                if (add.Qty > netStk) {
                    Erp.U.Pulsate(".l_sel_miktar_" + no);
                    $(".l_sel_miktar_" + no).val('');
                    Erp.U.P.T.Warning("Warning ", "Insufficient stock. You can enter max :" + netStk);
                } else {
                    res.push(add);
                }

            } else {
                res.push(add);
            }
        }
    });

    return res;
}


function GetAllItems() {

    var res = [];

    var validLine = true;

    $(".allitemlistlines").each(function () {

        var no = $(this).attr("data-no");

        var miktar = $(".l_sel_miktar_" + no).val();
        var price = $(".l_sel_price_" + no).val();
        var currency = $(".l_sel_doviz_" + no).val();

        var add = {
            Id: $(".lineitemhidden_"+no).val(),
            Qty: parseFloat(miktar),
            Price: parseFloat(price.replace(',', '.')),
            Currency: currency,
            Discount: $(".sel_discount_" + no).val(),
            ProductId: $(".l_sel_product_id_" + no).val(),
            ProductCode: $("#Item_stok_code_" + no).val(),
            ProductName: $("#Item_stok_name_" + no).val(),
            UnitCode: $('.sel_unit_code_' + no).val(),
            IsReserved: $(".sel_isreserved_" + no).val(),
            ReserveDate: $(".reservedendDate_" + no).val(),
            Note: $(".l_sel_note_" + no).val(),
            FrmNo: no,
            CustomerPrCode: $(".l_clientpr_code_" + no).val(),
            Vat: $(".sel_vat_" + no).val()
        };

        if (add.Qty < 0 || add.Qty == "" || isNaN(add.Qty)) {

            Erp.U.Pulsate(".l_sel_miktar_" + no);
            validLine = false;
        }

        if (add.Price < 0 || add.Price== "") {

            Erp.U.Pulsate(".l_sel_price_" + no);
            validLine = false;
        }

        if (add.ProductName.length < 1) {
            Erp.U.Pulsate("#Item_stok_code_" + no);
            Erp.U.Pulsate("#Item_stok_name_" + no);
            validLine = false;
        }

        if (add.IsReserved =="1" && add.ReserveDate.length < 10) {
            Erp.U.Pulsate(".reservedendDate_" + no);
            validLine = false;
        } else {
         
        }

        if (validLine == true) {
            res.push(add);
        }
    });

    if (validLine == false) {
        return [];
    }

    return res;
}

function CalculateAll() {

    var items = GetItems();
    if (items.length == 0) {

        $(".totalAmount_T").text("0");
        $(".totalDiscount_T").text("0");
        $(".totalNet_T").text("0");

        totalDto.TotalAmount = 0;
        totalDto.TotalDiscount = 0;
        totalDto.TotalNet = 0;
        totalDto.VatTotal = 0;

        return false;
    }

    //$(".loadinPro").show();
    $(".loadinPro").text("Loading...");
    var kargoBedeli = $("#KargoBedeli").val();

    Erp.U.A.Req.C("POST", "/Offer/CalculateGeneral",
        { items: items, kargoBedeli: kargoBedeli },
        function (data) {

            $(".loadinPro").text('');

            $(".totalAmount_T").text(data.TotalAmountStr + ' ' + data.Currency);
            $(".totalDiscount_T").text(data.TotalDiscountStr + ' ' + data.Currency);
            $(".totalNet_T").text(data.TotalNetStr + ' ' + data.Currency);
            $(".totalVat_T").text(data.VatTotalStr + ' ' + data.Currency);

            if (data.TotalShipment != null && data.TotalShipment > 0) {

                $(".totalShipment_T").text(data.TotalShipment + ' ' + data.Currency);
                $(".totalShipmentDiv").show();
            } else {
                $(".totalShipmentDiv").hide();
            }


            totalDto = data;

            if (data.Items.length > 0) {

                $.each(data.Items,function (i, item) {

                    $(".sel_aratoplam_" + item.LineNo).text(item.TotalAmountStr + " " + item.Currency);
                    $(".satirNetToplam_" + item.LineNo).text(item.TotalNetStr + ' ' + item.Currency);
                    $(".satirKdvToplam_" + item.LineNo).text(item.VatTotalStr + ' ' + item.Currency);

                });
            }
        });
}

function FormKaydet() {

    var offer = {
        Tarih: $("#TeklifTarih").val(),
        SaticiKodu: $("#SalesmanCode").val(),
        CariKodu: $("#CustomerCode").val(),
        CariUnvan: $("#CustomerName").val(),
        SevkKodu: $("#SelectedShipCode").val(),
        SevkiyatAciklama: $("#ShipAddresNameSelected").text(),
        IsYeri: $("#IsyeriSelect").val(),
        Bolum: $("#BolumSelect").val(),
        Ambar: $("#AmbarSelect").val(),
        VadeKodu: $("#VadeSelect").val(),
        TransferKodu: $("#TasiyiciKodSelect").val(),
        TotalAmount: totalDto.TotalAmount,
        TotalNet: totalDto.TotalNet,
        TotalDiscount: totalDto.TotalDiscount,
        VatTotal: totalDto.VatTotal,
        Currency: totalDto.Currency,
        Aciklama: $("#Aciklama_T").val(),
        OfferType: "1",
        Status: 0,
        MusteriSipNo: $("#CustomerOrderNo").val()
    };

    var isValid = true;

    if (offer.CariKodu.length < 1) {
        Erp.U.P.T.Warning("Warning !", 'Select client');
        Erp.U.Pulsate("#CustomerCode");
        isValid = false;
    }

    if (offer.IsYeri == null || offer.IsYeri.length < 1) {
        Erp.U.P.T.Warning("Warning !", 'Select workplace');
        Erp.U.Pulsate("#CustomerCode");
        isValid = false;
    }

    if (offer.Bolum == null || offer.Bolum.length < 1) {
        Erp.U.P.T.Warning("Warning !", 'Select section');
        Erp.U.Pulsate("#BolumSelect");
        isValid = false;
    }

    if (offer.Ambar == null || offer.Ambar.length < 1) {
        Erp.U.Pulsate("#AmbarSelect");
        Erp.U.P.T.Warning("Warning !", 'Select warehouse');
        isValid = false;
    }

    if (!isValid) {
        return false;
    }

    var items = GetAllItems();

    if (items.length == 0) {
        Erp.U.P.T.Warning("Warning !", 'Please fill in the required fields.');
        return false;
    }




    bootbox.confirm("Are you sure you want to save?",
        function (result) {
            if (result === true) {

                $(".sel_create_button_cl").hide();
                $(".l_sel_create_btuuon_load").show();
                $(".loadinPro").text("Data is processing...")

                Erp.U.A.Req.C("POST",
                    "/Offer/Create",
                    { offer: offer, items: items },
                    function (data) {

                        if (data.Status == true) {

                            $(".CreatedOfferId").val(data.Data);

                            // var genelFormDosya = $("#demandGenelFile").val();

                            $(".loadinPro").html("<span><i class='fa fa-spin fa-spinner'></i> File is saving . PLease Wait.</span>");

                                    var formL = $("#DemandFileItemForm")[0];
                                    var nos = [];
                                    var counts = [];

                                    $(".uploaditemfile").each(function () {
                                        var val = $(this).val();
                                        if (val.length > 1) {

                                            var no = $(this).attr("data-no");
                                            var fileCount = $(this).get(0).files.length;

                                            counts.push(fileCount);
                                            nos.push(no);
                                        }
                                    });

                            if (nos.length > 0) {

                                        $(".demandItemNos").val(nos.join(","));
                                        $(".fileCounts").val(counts.join(","));

                                        var formDataL = new FormData(formL);

                                        FileAdd(formDataL, "/Offer/FileAddForItem", function (respF) {

                                            Erp.U.P.T.Success("Result", data.Message);

                                            $(".sel_create_button_cl").hide();
                                            $(".l_sel_create_btuuon_load").hide();

                                            $(".loadinPro").html(data.Message + '  <a href="/Offer/Index"> Go to Offer List</a>');

                                            setTimeout(function () {
                                                // location.href = "/Demand/MyIndex";
                                            }, 900);
                                        });

                                    } else {

                                        $(".l_sel_create_btuuon_load").hide();

                                        $(".loadinPro").html(data.Message  + '  <a href="/Offer/Index"> Go to Offer List</a>');

                                        $(".sel_create_button_cl").hide();

                                        Erp.U.P.T.Success("Result", data.Message);

                                        setTimeout(function () {
                                            //  location.href = "/Demand/MyIndex";
                                        }, 900);
                                    }

                        } else {

                            $(".sel_create_button_cl").show();
                            $(".l_sel_create_btuuon_load").hide();

                            $(".loadinPro").text(data.Message);

                            Erp.U.P.T.Warning("Warning ", data.Message);
                        }
                    });


            }

        });

}


var deleteItems = [];

function SatirSilEdit(no) {
    bootbox.confirm("Are you sure ?",
        function (result) {
            if (result === true) {

                $(".l_sel_del_load_" + no).show();
                $(".l_sel_delet_" + no).hide();
                setTimeout(function () {
                    $(".list_line_" + no).remove();
                    OrderLineCal();
                    CalculateAll();
                },
                    300);

                deleteItems.push(no);
            }
        });
}


function FormGuncelle() {



    var offer = {
        Id: $(".CreatedOfferId").val(),
        Tarih: $("#TeklifTarih").val(),
        SaticiKodu: $("#SalesmanCode").val(),
        CariKodu: $("#CustomerCode").val(),
        CariUnvan: $("#CustomerName").val(),
        SevkKodu: $("#SelectedShipCode").val(),
        SevkiyatAciklama: $("#ShipAddresNameSelected").text(),
        IsYeri: $("#IsyeriSelect").val(),
        Bolum: $("#BolumSelect").val(),
        Ambar: $("#AmbarSelect").val(),
        VadeKodu: $("#VadeSelect").val(),
        TransferKodu: $("#TasiyiciKodSelect").val(),
        TotalAmount: totalDto.TotalAmount,
        TotalNet: totalDto.TotalNet,
        TotalDiscount: totalDto.TotalDiscount,
        VatTotal: totalDto.VatTotal,
        Currency: totalDto.Currency,
        Aciklama: $("#Aciklama_T").val(),
        OfferType: "1",
        Status: 0,
        MusteriSipNo: $("#CustomerOrderNo").val()
    };

    var isValid = true;

    if (offer.CariKodu.length < 1) {
        Erp.U.P.T.Warning("Warning !", 'Select client');
        Erp.U.Pulsate("#CustomerCode");
        isValid = false;
    }

    if (offer.IsYeri == null || offer.IsYeri.length < 1) {
        Erp.U.P.T.Warning("Warning !", 'Select workplace');
        Erp.U.Pulsate("#CustomerCode");
        isValid = false;
    }

    if (offer.Bolum == null || offer.Bolum.length < 1) {
        Erp.U.P.T.Warning("Warning !", 'Select section');
        Erp.U.Pulsate("#BolumSelect");
        isValid = false;
    }

    if (offer.Ambar == null || offer.Ambar.length < 1) {
        Erp.U.Pulsate("#AmbarSelect");
        Erp.U.P.T.Warning("Warning !", 'Select warehouse');
        isValid = false;
    }

    if (!isValid) {
        return false;
    }

    var items = GetAllItems();

    if (items.length == 0) {
        Erp.U.P.T.Warning("Warning !", 'Please fill in the required fields.');
        return false;
    }

    bootbox.confirm("Are you sure you want to save?",
        function (result) {
            if (result === true) {

                $(".sel_create_button_cl").hide();
                $(".l_sel_create_btuuon_load").show();
                $(".loadinPro").text("Data is processing...")

                Erp.U.A.Req.C("POST",
                    "/Offer/Edit",
                    { offer: offer, items: items, deleteItems: deleteItems },
                    function (data) {

                        if (data.Status == true) {

                            // var genelFormDosya = $("#demandGenelFile").val();

                            $(".loadinPro").html("<span><i class='fa fa-spin fa-spinner'></i> File is saving . Please Wait.</span>");

                            var formL = $("#DemandFileItemForm")[0];
                            var nos = [];
                            var counts = [];

                            $(".uploaditemfile").each(function () {
                                var val = $(this).val();
                                if (val.length > 1) {

                                    var no = $(this).attr("data-no");
                                    var fileCount = $(this).get(0).files.length;

                                    counts.push(fileCount);
                                    nos.push(no);
                                }
                            });

                            if (nos.length > 0) {

                                $(".demandItemNos").val(nos.join(","));
                                $(".fileCounts").val(counts.join(","));

                                var formDataL = new FormData(formL);

                                FileAdd(formDataL, "/Offer/FileAddForItem", function (respF) {

                                    Erp.U.P.T.Success("Result", data.Message);

                                    $(".sel_create_button_cl").hide();
                                    $(".l_sel_create_btuuon_load").hide();

                                    $(".loadinPro").html(data.Message + '  <a href="/Offer/Index"> Go to Offer List</a>');

                                    setTimeout(function () {
                                        // location.href = "/Demand/MyIndex";
                                    }, 900);
                                });

                            } else {

                                $(".l_sel_create_btuuon_load").hide();

                                $(".loadinPro").html(data.Message + '  <a href="/Offer/Index"> Go to Offer List</a>');

                                $(".sel_create_button_cl").hide();

                                Erp.U.P.T.Success("Result", data.Message);

                                setTimeout(function () {
                                    //  location.href = "/Demand/MyIndex";
                                }, 900);
                            }

                        } else {

                            $(".sel_create_button_cl").show();
                            $(".l_sel_create_btuuon_load").hide();

                            $(".loadinPro").text(data.Message);

                            Erp.U.P.T.Warning("Warning ", data.Message);
                        }
                    });


            }

        });


}

function FileAdd(data, url, successResp) {

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: url,
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 6000000,
        beforeSend: function () {

        },
        success: function (response) { //console.log(response);
            successResp(response);
        },
        error: function (jqxhr, settings, thrownError) {
            successResp();
        }
    });

}



function SatirKopyala(ustNo) {


    var no = RandomGet();

    var innerHtml = Mustache.to_html($('#TemplateOffer').html(), { DATA: { No: no } });

    $("#ListOffer").append(innerHtml);

    OrderLineCal();

    $(".l_sel_miktar_" + no).val($(".l_sel_miktar_" + ustNo).val());
    $(".l_sel_price_" + no).val($(".l_sel_price_" + ustNo).val());

    $("#Item_stok_name_" + no).val($("#Item_stok_name_" + ustNo).val());
    $(".l_sel_product_id_" + no).val($(".l_sel_product_id_" + ustNo).val());
    $(".l_sel_note_" + no).val($(".l_sel_note_" + ustNo).val());

    $("#Item_stok_code_" + no).val($("#Item_stok_code_" + ustNo).val());
    $(".sel_isreserved_" + no).val($(".sel_isreserved_" + ustNo).val());
    $(".reservedendDate_" + no).val($(".reservedendDate_" + ustNo).val());
    $(".sel_unit_code_" + no).val($(".sel_unit_code_" + ustNo).val());

    $(".l_sel_doviz_" + no).val($(".l_sel_doviz_" + ustNo).val());
    $(".ambarStok_" + no).text($(".ambarStok_" + ustNo).text());
    $(".netStok_" + no).text($(".netStok_" + ustNo).text());

    $(".reserveliToplamAdet_" + no).text($(".reserveliToplamAdet_" + ustNo).text());

    $(".sel_discount_" + no).val($(".sel_discount_" + ustNo).val());


    CalculateAll();

}