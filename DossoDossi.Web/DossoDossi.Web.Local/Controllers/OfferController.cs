﻿using DossoDossi.Data.DataAccess;
using DossoDossi.Data.Entity;
using DossoDossi.Data.Helper;
using DossoDossi.Data.Models;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace DossoDossi.Web.Local.Controllers
{
    [LoginFilter]
    public class OfferController : Controller
    {
        // GET: Offer
        public ActionResult Index()
        {
            var user = SessionHelper.User;
            if (user == null)
                return RedirectToAction("Login", "Account");

            //if (!user.Roles.CheckRole("OF1"))
            //{
            //    return RedirectToAction("Index", "Home");
            //}

            ViewBag.StatusList = OffersDataAccess.GetAllStatus();

            return View();
        }

        [HttpPost]
        public JsonResult GetList(OfferPagerDto pager)
        {
            var user = SessionHelper.User;
            
            var us = AccountDataAccess.GetById(user.UserId);
            pager.IsYeri = us.Isyeri;

            return Json(OffersDataAccess.GetList(pager));
        }

        public ActionResult Create()
        {
            var user = SessionHelper.User;

            if (user == null)
                return RedirectToAction("Login", "Account");

            ViewBag.User = AccountDataAccess.GetById(user.UserId);
            ViewBag.Isyeri = LogoDataAccess.GetIsyeri();
            ViewBag.Bolum= LogoDataAccess.GetBolumler();
            ViewBag.Ambarlar = LogoDataAccess.GetAmbarlar();
            ViewBag.VadeList = LogoDataAccess.VadeList();
            ViewBag.TasiyiciKodlar = LogoDataAccess.TasiyiciKodlar();
            ViewBag.User = AccountDataAccess.GetById(user.UserId);
            ViewBag.DefCurrency = SettingDataAccess.GetValByCode("Offer.DefaultCurrency");

            return View();
        }

        public ActionResult Edit(Guid id)
        {
            var user = SessionHelper.User;

            if (user == null)
                return RedirectToAction("Login", "Account");

            ViewBag.User = AccountDataAccess.GetById(user.UserId);
            ViewBag.Isyeri = LogoDataAccess.GetIsyeri();
            ViewBag.Bolum = LogoDataAccess.GetBolumler();
            ViewBag.Ambarlar = LogoDataAccess.GetAmbarlar();
            ViewBag.VadeList = LogoDataAccess.VadeList();
            ViewBag.TasiyiciKodlar = LogoDataAccess.TasiyiciKodlar();
            ViewBag.User = AccountDataAccess.GetById(user.UserId);
            ViewBag.DefCurrency = SettingDataAccess.GetValByCode("Offer.DefaultCurrency");

            var offer =  OffersDataAccess.GetByUQ(id);
            ViewBag.Data = offer;

            var items = OffersDataAccess.GetItems(offer.Id);

            var stocks = ProductDataAccess.GetListByIds(items.Select(a => a.ProductId ?? 0).Distinct().ToList());

            foreach (var item in stocks)
            {
                var reserveProc = OffersDataAccess.GetReserveStockCount(item.StokId);
                item.Birim2 = reserveProc.ToString();
                item.NetAgirlik = Math.Round(((item.MerkezStok ?? 0) - reserveProc - (item.BekleyenSiparisMiktar ?? 0)), 2);
            }

            ViewBag.Stocks = stocks;

            return View(items);
        }

        [HttpPost]
        public JsonResult CalculateGeneral(List<OfferCalculateDto> items, double? kargoBedeli=null)
        {
            var resp = OffersDataAccess.CalculateGeneral(items,kargoBedeli);
            return Json(resp, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create(Offer offer, List<OfferItem> items)
        {
            var resp = OffersDataAccess.Create(offer, items);
            return Json(resp, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult Edit(Offer offer, List<OfferItem> items, List<int> deleteItems)
        {
            var resp = OffersDataAccess.Edit(offer, items, deleteItems);
            return Json(resp, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FileAddForItem(int id, string nos, string counts)
        {
            var files = Request.Files;

            if (files == null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            var arr = nos.Split(',').Where(x => !x.ToControl()).ToList();
            var cts = counts.Split(',').Where(x => !x.ToControl()).ToList();

            for (int i = 0; i < arr.Count; i++)
            {
                var no = arr[i];
                var fileCount = int.Parse(cts[i]);

                if (fileCount > 0)
                {
                    for (int j = 0; j < fileCount; j++)
                    {
                        var file = files[i];

                        var extpath = Path.GetExtension(file.FileName);

                        var fileNameOrigin = Path.GetFileNameWithoutExtension(file.FileName);

                        var fileName = fileNameOrigin.Replace(" ", "_").Replace(",", "").SetTurkishCharacterToEnglish() + "_" + DateTime.Today.ToString("yyyyMMdd") + extpath;

                        var localfolder = "/Files/offer/" + id + "/";

                        var directoryMapPath = Server.MapPath("~" + localfolder);
                        var path = Path.Combine(directoryMapPath, fileName);

                        var localpath = localfolder + fileName;

                        if (!Directory.Exists(directoryMapPath))
                        {
                            Directory.CreateDirectory(directoryMapPath);
                        }

                        file.SaveAs(path);

                        OffersDataAccess.UpdateFileDdemandItemByNo(id, no, localpath);
                    }
                }
            }

            return Json(true);
        }

        [HttpPost]
        public JsonResult Delete(int id)
        {
            var user = SessionHelper.User;

            var res = OffersDataAccess.Delete(id, user.UserId);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateStatus(int id, int status)
        {
            var user = SessionHelper.User;
            var res = OffersDataAccess.UpdateStatus(id, status);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult GetDetail(int id)
        {
            ViewBag.Data = OffersDataAccess.GetById(id);
            var items = OffersDataAccess.GetItems(id);

            return PartialView("_OfferDetailPartial", items);
        }

        
    }
}