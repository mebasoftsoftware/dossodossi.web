﻿using DossoDossi.Data.DataAccess;
using DossoDossi.Data.Entity;
using DossoDossi.Data.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DossoDossi.Web.Local.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetList(Pager pager, string barkod, string stoklu,
            string ozelkod,
            string ozelkod2,
            string ozelkod3,
            string ozelkod4,
            string ozelkod5)
        {
            var result = ProductDataAccess.GetList(pager,barkod,stoklu,ozelkod,ozelkod2,ozelkod3,ozelkod4,ozelkod5);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult Insert()
        //{
        //    var suppliers = SupplierDataAccess.AllList();
        //    ViewBag.Supliies = suppliers;
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult Insert(Product model)
        //{
        //    model.Status = true;
        //    model.IsDeleted = false;
        //    model.UQ = Guid.NewGuid();
        //    model.CreataeDate = DateTime.Now;

        //    var data = ProductDataAccess.Create(model);
        //    if (data.Status)
        //    {
        //        return RedirectToAction("Index");
        //    }
        //    var suppliers = SupplierDataAccess.AllList();
        //    ViewBag.Supliies = suppliers;
        //    ViewBag.Hata = data.Message;
        //    return View(model);
        //}

        //public ActionResult Update(int id)
        //{
        //    var data = ProductDataAccess.GetById(id);
        //    var suppliers = SupplierDataAccess.AllList();
        //    ViewBag.Supliies = suppliers;
        //    return View(data);
        //}

        //[HttpPost]
        //public ActionResult Update(Product model)
        //{
        //    var data = ProductDataAccess.Update(model);
        //    if (data.Status)
        //    {
        //        return RedirectToAction("Index");
        //    }
        //    var suppliers = SupplierDataAccess.AllList();
        //    ViewBag.Supliies = suppliers;
        //    ViewBag.Hata = data.Message;

        //    return View(model);
        //}

        //public ActionResult Delete(int id)
        //{

        //    var sonuc = ProductDataAccess.Delete(id);

        //    return Json(sonuc, JsonRequestBehavior.AllowGet);
        //}

        //public ActionResult StatusChange(int id)
        //{
        //    var sonuc = ProductDataAccess.StatusOnchange(id);

        //    return Json(sonuc, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult CollectiveInsert()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CollectiveInsert(HttpPostedFileBase file)
        {
            return View();
        }
    }
}