﻿using DossoDossi.Data.DataAccess;
using DossoDossi.Data.Entity;
using DossoDossi.Data.Helper;
using DossoDossi.Data.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DossoDossi.Web.Local.Controllers
{
    public class ApiController : Controller
    {
        public ActionResult CustomerList(Guid id,string own=null)
        {
            var account = AccountDataAccess.GetByUQ(id.ToString());

            if(account == null)
            {
                return RedirectToAction("Login", "Account");
            }

            ViewBag.Account = account;
            
            return View();
        }

        public ActionResult CustomerListFromSession()
        {
            var user = SessionHelper.User;
            var account = AccountDataAccess.GetById(user.UserId);

            return RedirectToAction("CustomerList", new { id = account.QU });
        }

            [HttpPost]
        public JsonResult GetListCustomer(string code)
        {
            var data = ProjeDataAccess.GetProjeMusteriler(code);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveCustomerSicilNo(Guid account,int id,string val)
        {
            var data = ProjeDataAccess.UpdateMusteriSicil(id,val);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        // GET: Api
        public ActionResult GetActiveFuars()
        {
            var resp = new ResponseMessage();

            var list = ProjeDataAccess.GetActriveProjelers();

            resp.Data = list.Select(a => new
            {
                ProjeId = a.LOGICALREF,
                ProjeKodu = a.CODE,
                ProjeAdi = a.NAME,
                BaslangicTarih = a.BEGDATE.HasValue ? a.BEGDATE.Value.ToString("dd.MM.yyyy") : "",
                BitisTarih = a.ENDDATE.HasValue ? a.ENDDATE.Value.ToString("dd.MM.yyyy") : "",
                OzelKodu = a.SPECODE,
                OzelKodu2 = a.SPECODE2,
                OzelKodu3 = a.SPECODE3,
                YetkiKodu = a.CYPHCODE
            });

            resp.Status = true;
            resp.Message = "OK";
            resp.Code = "200";

            return Json(resp, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetFuarProducts(string code)
        {
            var resp = new ResponseMessage();

            var list = ProjeDataAccess.GetFuarProducts(code);
            resp.Data = list;

            return Json(resp, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetUserBySicilNo(string code, string userNo)
        {
            var resp = new ResponseMessage();

            var user = ProjeDataAccess.GetMusteriByProje(code, userNo,true);

            resp.Data = user;
            resp.Status = user==null?false: true;
            resp.Message = user == null?"Kullanıcı bulunamadı": "OK";
            
            resp.Code = "200";

            if (user != null)
            {
                var list = OffersDataAccess.GetOfferItemListByMusteriIdToday(user.MusteriId);
                resp.Orders = list;
            }
            else
            {
                resp.Orders = new List<OfferItem>();
            }

            return Json(resp, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSettingByCode(string code)
        {
            var result = SettingDataAccess.GetValByCode(code);
            return Json(new ResponseMessage { Data = result, Status = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetUrunByBarkod(string code,string barkod)
        {
            var result = ProductDataAccess.GetProjeByBarkod(code, barkod);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CreateOffer(CreateOfferApiModel model)
        {
            var resp = new ResponseMessage();

            if (model.MusteriId == Guid.Empty)
            {
                resp.Code = "403";
                resp.Message = "Please select a customer.";
                return Json(resp, JsonRequestBehavior.AllowGet);
            }

            if (model.ProjeKodu.ToControl())
            {
                resp.Code = "403";
                resp.Message = "Please select a project code/fuar.";
                return Json(resp, JsonRequestBehavior.AllowGet);
            }

            if (model.Urunlers == null)
            {
                resp.Code = "403";
                resp.Message = "Product must be the least an item";
                return Json(resp, JsonRequestBehavior.AllowGet);
            }

            var musteri = CustomerDataAccess.GetMusteriById(model.MusteriId);

            var offer =
                new Offer
                {
                    UQ = Guid.NewGuid(),
                    Status = 0,
                    Tarih = DateTime.Now,
                    ProjeKodu = model.ProjeKodu,
                    SaticiKodu = "CRM",
                    OfferType = "API",
                    Isyeri = "0",
                    Ambar = "0",
                    CreateUserId = 0,
                    Currency = model.Urunlers.FirstOrDefault().Doviz,
                    MusteriId = model.MusteriId,
                    CariUnvan = musteri.CompanyName.ToControl() ? musteri.Name : musteri.Name + " (" + musteri.CompanyName + ")",
                    EklenmeTarihi = DateTime.Now,
                    Bolum = "0",
                    TotalAmount = model.Urunlers.Sum(a => a.SatisFiyat * (a.SeriAdedi ?? 0)),
                    TotalNet = model.Urunlers.Sum(a => a.SatisFiyat * (a.SeriAdedi ?? 0)),
                };

            var items = model.Urunlers.Select(a => new OfferItem
            {
                Currency = a.Doviz,
                ProductCode = a.StokKodu,
                ProductId = a.StokId,
                ProductName = a.StokAdi,
                Qty = (int)(a.SeriAdedi??0),
                Price = a.SatisFiyat,
                UnitCode = "ADET",
                UQ = Guid.NewGuid(),
                OfferId = 0,
                IsReserved = "0",
            }).ToList();

            var offerResp = OffersDataAccess.Create(offer, items);
            
            if (offerResp.Status)
            {
                offerResp.Message = "Sipariş ön kaydınız alınmıştır.Lütfen kasada işlemi tamamlayınız.Kayıt no : " + offerResp.Data;
            }

            return Json(offerResp, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateOfferOne(string projeKodu,Guid musteriId,Guid urunId)
        {
            var resp = new ResponseMessage();

            if (musteriId == Guid.Empty)
            {
                resp.Code = "403";
                resp.Message = "Please select a customer.";
                return Json(resp, JsonRequestBehavior.AllowGet);
            }

            if (projeKodu.ToControl())
            {
                resp.Code = "403";
                resp.Message = "Please select a project code/fuar.";
                return Json(resp, JsonRequestBehavior.AllowGet);
            }

            if (urunId == Guid.Empty)
            {
                resp.Code = "403";
                resp.Message = "Product must be the least an item";
                return Json(resp, JsonRequestBehavior.AllowGet);
            }

            ProjeTedarikciUrunler urun = ProductDataAccess.GetByProjetTedarikciUrunByUQ(urunId);

            var musteri = CustomerDataAccess.GetMusteriById(musteriId);

            var anyOffer = OffersDataAccess.AnyOfferToday(projeKodu, musteriId);

            if(anyOffer == null)
            {
                var offer =
               new Offer
               {
                   UQ = Guid.NewGuid(),
                   Status = 0,
                   Tarih = DateTime.Now,
                   ProjeKodu = projeKodu,
                   SaticiKodu = "CRM",
                   OfferType = "API",
                   Isyeri = "0",
                   Ambar = "0",
                   CreateUserId = 0,
                   Currency = urun.Doviz,
                   MusteriId = musteriId,
                   CariUnvan = musteri.CompanyName.ToControl() ? musteri.Name : musteri.Name + " (" + musteri.CompanyName + ")",
                   EklenmeTarihi = DateTime.Now,
                   Bolum = "0",
                   TotalAmount = urun.SatisFiyat * (urun.SeriAdedi ?? 0),
                   TotalNet = urun.SatisFiyat * (urun.SeriAdedi ?? 0),
               };

                var items = new List<OfferItem>();

                items.Add(new OfferItem
                {
                    Currency = urun.Doviz,
                    ProductCode = urun.StokKodu,
                    ProductId = urun.StokId,
                    ProductName = urun.StokAdi,
                    Qty = (int)(urun.SeriAdedi ?? 0),
                    Price = urun.SatisFiyat,
                    UnitCode = "ADET",
                    UQ = Guid.NewGuid(),
                    OfferId = 0,
                    IsReserved = "0"
                });

                var offerResp = OffersDataAccess.Create(offer, items);
                
                if (offerResp.Status)
                {
                    offerResp.Message = "Sipariş ön kaydınız alınmıştır.Lütfen kasada işlemi tamamlayınız.Kayıt no : " + offerResp.Data;
                }

                return Json(offerResp, JsonRequestBehavior.AllowGet);

            }else
            {
                var addItem = new OfferItem
                {
                    
                    Currency = urun.Doviz,
                    ProductCode = urun.StokKodu,
                    ProductId = urun.StokId,
                    ProductName = urun.StokAdi,
                    Qty = (int)(urun.SeriAdedi ?? 0),
                    Price = urun.SatisFiyat,
                    UnitCode = "ADET",
                    UQ = Guid.NewGuid(),
                    OfferId = anyOffer.Id,
                    IsReserved = "0",
                };

                var upsertResp = OffersDataAccess.CreateItems(anyOffer, addItem);

                if (upsertResp)
                {
                    resp.Status = true;
                    resp.Message= "Ok";
                }

                return Json(resp, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult PrintOfferPdf(int id)
        {
            var offer = OffersDataAccess.GetById(id);
            ViewBag.Data = offer;
            var items = OffersDataAccess.GetItems(id);

            ViewBag.Cari = CustomerDataAccess.GetByCode(offer.CariKodu);

            ViewBag.Products = ProductDataAccess.GetListByIds(items.Where(a => a.ProductId.HasValue).Select(a => a.ProductId.Value).ToList());

            return View("~/Views/Offer/PrintHtml.cshtml", items);

            //var file = GetPdfPath(id);
            //return File(file, "application/pdf");
        }


        public ActionResult CreateCustomerBarcode(Guid id)
        {
            var musteri = CustomerDataAccess.GetMusteriById(id);
            return View(musteri);
        }

    }
}