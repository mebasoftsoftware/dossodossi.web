﻿using DossoDossi.Data.DataAccess;
using DossoDossi.Data.Entity;
using DossoDossi.Data.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DossoDossi.Web.Local.Controllers
{
    public class AccountController : BaseController
    {
        // GET: Account
       
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoginPost(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                ViewBag.Hata = "Username and password is valid";
                return View("Login");
            }

            var sonuc = AccountDataAccess.Login(username, password);

            if (sonuc != null)
            {
                var roles = AccountDataAccess.GetRoleCodes(sonuc.Id);
                SessionHelper.Login(sonuc, roles);

                return RedirectToAction("Index", "Home");
            }

            ViewBag.Hata = "Wrong username or password...";

            return View("Login");
        }
        public ActionResult LogOut()
        {
            SessionHelper.LogOut();
            return RedirectToAction("Login", "Account");
        }

        public ActionResult Index()
        {
            return View();
        }
        //[LoginFilter]
        //public JsonResult GetList(Pager pager)
        //{

        //    var result = AccountDataAccess.GetList(pager);

        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}
        //[LoginFilter]
        //public ActionResult Insert()
        //{
        //    return View();
        //}
        //[LoginFilter]
        //[HttpPost]
        //public ActionResult Insert(Account model)
        //{
        //    model.Status = true;
        //    model.IsDeleted = false;
        //    model.UQ = Guid.NewGuid();
        //    model.CreataeDate = DateTime.Now;

        //    var data = AccountDataAccess.Create(model);
        //    if (data.Status)
        //    {
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.Hata = "Lütfen verileri düzgün giriniz.";
        //    return View(model);
        //}
        //[LoginFilter]
        //public ActionResult Update(int id)
        //{
        //    var data = AccountDataAccess.GetById(id);
        //    return View(data);
        //}
        //[LoginFilter]
        //[HttpPost]
        //public ActionResult Update(Account model)
        //{
        //    var data = AccountDataAccess.Update(model);
        //    if (data.Status)
        //    {
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.Hata = data.Message;

        //    return View(model);
        //}
        //[LoginFilter]
        //public ActionResult Delete(int id)
        //{
          
        //    var sonuc = AccountDataAccess.Delete(id);

        //    return Json(sonuc, JsonRequestBehavior.AllowGet);
        //}
        //[LoginFilter]
        //public ActionResult StatusChange(int id)
        //{
        //    var sonuc = AccountDataAccess.StatusOnchange(id);

        //    return Json(sonuc, JsonRequestBehavior.AllowGet);
        //}

        //public ActionResult Roles(int? id = null)
        //{
        //    var user = SessionHelper.User;
        //    if (user == null)
        //        return RedirectToAction("Login", "Account");


        //    ViewBag.all = AccountDataAccess.All();

        //    if (id.HasValue)
        //    {
        //        ViewBag.User = AccountDataAccess.GetById(id.Value);
        //        ViewBag.Ownroles = AccountDataAccess.GetRoleCodes(id.Value);
        //        ViewBag.Roles = AccountDataAccess.GetAllRoles();
        //    }

        //    return View();
        //}
        //[HttpPost]
        //public JsonResult UpdateRoles(int id, List<string> arr)
        //{
        //    AccountDataAccess.DeleteRoles(id);

        //    AccountDataAccess.InsertRoles(id, arr);

        //    return Json(true,JsonRequestBehavior.AllowGet);
        //}


    }
}