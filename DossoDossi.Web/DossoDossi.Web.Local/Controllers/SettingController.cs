﻿using DossoDossi.Data.DataAccess;
using DossoDossi.Data.Helper;
using System.Web.Mvc;

namespace DossoDossi.Web.Local.Controllers
{
    public class SettingController : Controller
    {
        [LoginFilter]
        public ActionResult Index()
        {
            var list = SettingDataAccess.GetAllList();
            return View(list);
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult UpdateVal(int id, string val)
        {
            var res = SettingDataAccess.UpdateValue(id, val);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

    }
}