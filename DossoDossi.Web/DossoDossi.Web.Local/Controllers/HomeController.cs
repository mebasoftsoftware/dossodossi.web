﻿using DossoDossi.Data.DataAccess;
using DossoDossi.Data.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DossoDossi.Web.Local.Controllers
{
    [LoginFilter]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var user = SessionHelper.User;

            var us = AccountDataAccess.GetById(user.UserId);

            if (us.Isyeri.HasValue && !us.SalesManCode.ToControl())
            {
                ViewBag.Till = DashBoardDataAccess.HomeTillReportIsyeri(us.Isyeri.Value, us.SalesManCode);
            }
            else
            {
                ViewBag.Till = DashBoardDataAccess.HomeTillReport();
            }

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}