﻿using DossoDossi.Data.DataAccess;
using DossoDossi.Data.Entity;
using DossoDossi.Data.Helper;
using DossoDossi.Data.Models;
using DossoDossi.Data.Tiger;
using System;
using System.Web;
using System.Web.Mvc;

namespace DossoDossi.Web.Local.Controllers
{
    
    public class CustomerController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetList(Pager pager)
        {
            var res = CustomerDataAccess.GetMusteriList(pager);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Detail(Guid id)
        {
            var musteri = CustomerDataAccess.GetMusteriById(id);
            return View(musteri);
        }

        public PartialViewResult NewMusteriPartial()
        {
            return PartialView("_NewMusteriPartial");
        }

        public JsonResult CreateMusteri(Musteriler model)
        {
            var code = SettingDataAccess.GetValByCode("ActiveFuarNo");

            model.Code = model.SicilNo;
            model.Status = true;

            var resp = CustomerDataAccess.UpsertMusteriPost(code, model);

            return Json(resp, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult EditMusteriPartial(Guid id)
        {
            var data = CustomerDataAccess.GetMusteriById(id);

            return PartialView("_EditMusteriPartial",data);
        }


        public JsonResult EditMusteri(Musteriler model)
        {
            var code = SettingDataAccess.GetValByCode("ActiveFuarNo");
            
            model.Code = model.SicilNo;

            var resp = CustomerDataAccess.EditMusteriPost(code, model);

            return Json(resp, JsonRequestBehavior.AllowGet);
        }

    }
}