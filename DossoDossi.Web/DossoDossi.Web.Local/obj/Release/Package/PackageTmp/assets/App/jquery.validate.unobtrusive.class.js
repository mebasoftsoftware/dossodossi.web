﻿// Hata ve doğrulama mesajları için

(function ($) {
    var defaultOptions = {
        errorClass: 'has-error has-feedback',
        validClass: 'has-success has-feedback',
        highlight: function (element, errorClass, validClass) {
            var formGroup = $(element).closest('.form-group');
            formGroup
                .addClass('has-error')
                .removeClass('has-success');

            if (!formGroup.hasClass('has-feedback')) {
                formGroup.addClass('has-feedback');
            }

            //var _feedbackIcon = $(element).closest(".form-group").find(".glyphicon");
            //if (_feedbackIcon.length) {
            //    $(_feedbackIcon)
            //        .removeClass("glyphicon-ok")
            //        .removeClass("glyphicon-remove")
            //        .addClass("glyphicon-remove");
            //}
            //else {
            //    $("<span class='glyphicon glyphicon-remove form-control-feedback' aria-hidden='true'></span>")
            //        .insertAfter(element);
            //}
        },
        unhighlight: function (element, errorClass, validClass) {
            var formGroup = $(element).closest('.form-group');
            formGroup
                .removeClass('has-error')
                .addClass('has-success');

            if (!formGroup.hasClass('has-feedback')) {
                formGroup.addClass('has-feedback');
            }

            //var _feedbackIcon = $(element).closest(".form-group").find(".glyphicon");
            //if (_feedbackIcon.length) {
            //    $(_feedbackIcon)
            //        .removeClass("glyphicon-ok")
            //        .removeClass("glyphicon-remove")
            //        .addClass("glyphicon-ok");
            //}
            //else {
            //    $("<span class='glyphicon glyphicon-ok form-control-feedback' aria-hidden='true'></span>")
            //        .insertAfter(element);
            //}
        }
    };

    $.validator.setDefaults(defaultOptions);

    $.validator.unobtrusive.options = {
        errorClass: defaultOptions.errorClass,
        validClass: defaultOptions.validClass,
    };

})(jQuery);