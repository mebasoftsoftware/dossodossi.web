﻿using Dapper;
using DossoDossi.Data.Entity;
using DossoDossi.Data.Helper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.DataAccess
{
    //public class SupplierDataAccess
    //{
    //    public static PagerResult GetList(Pager pager)
    //    {
    //        try
    //        {
    //            using (var db = new DBDataContext(UtilityHelper.Connection()))
    //            {
    //                var data = db.Suppliers.PagerDynamic(Filter(pager.Filter), pager);
    //                return data;
    //            }
    //        }
    //        catch (Exception e)
    //        {
    //            return new PagerResult
    //            {
    //                Total = 0,
    //                Pager = pager,
    //            };
    //        }
    //    }
    //    private static Expression<Func<Supplier, bool>> Filter(string filter)
    //    {


    //        var pred = PredicateBuilder.True<Supplier>();


    //        if (!filter.ToControl())
    //        {
    //            pred = pred.And(x => x.Name.Contains(filter) || x.Code.Contains(filter));
    //        }
    //        pred = pred.And(a => a.IsDeleted == false);

    //        return pred;
    //    }
    //    public static ResponseMessage Create(Supplier model)
    //    {
    //        try
    //        {
    //            using (var db = new DBDataContext(UtilityHelper.Connection()))
    //            {
    //                db.Suppliers.InsertOnSubmit(model);
    //                db.SubmitChanges();
    //                return new ResponseMessage
    //                {
    //                    Status = true,
    //                    Message = "Ekleme başarılı"
    //                };
    //            }
    //        }
    //        catch (Exception e)
    //        {
    //            return new ResponseMessage
    //            {
    //                Message = "Hata : " + e.Message,
    //                Code = e.StackTrace
    //            };
    //        }
    //    }
    //    public static Supplier GetById(int id)
    //    {
    //        try
    //        {
    //            using (var db = new SqlConnection(UtilityHelper.Connection()))
    //            {
    //                var sql = "select top 1 * from dbo.Suppliers where id=@id and  IsDeleted=0 ";
    //                return db.Query<Supplier>(sql, new { id = id }).FirstOrDefault();
    //            }
    //        }
    //        catch (Exception e)
    //        {

    //            return null;
    //        }
    //    }
    //    public static ResponseMessage Update(Supplier model)
    //    {
    //        try
    //        {
    //            using (var db = new DBDataContext(UtilityHelper.Connection()))
    //            {
    //                var data = db.Suppliers.FirstOrDefault(a => a.Id == model.Id);
    //                data.Address = model.Address;
    //                data.City = model.City;
    //                data.Code = model.Code;
    //                data.County = model.County;
    //                data.Email = model.Email;
    //                data.Name = model.Name;
    //                data.TaxNo = model.TaxNo;
    //                data.TaxOffice = model.TaxOffice;
    //                data.Country = model.Country;
    //                data.UpdateDate = DateTime.Now;
    //                db.SubmitChanges();
    //                return new ResponseMessage
    //                {
    //                    Status = true,
    //                    Message = "Başarıyla kayıt  başarılı"
    //                };
    //            }

    //        }
    //        catch (Exception e)
    //        {

    //            return new ResponseMessage
    //            {
    //                Status = false,
    //                Message = "Bir hata oluştu.",
    //                Code = e.StackTrace
    //            };
    //        }
    //    }

    //    public static bool Delete(int id)
    //    {
    //        try
    //        {
    //            using (var db = new DBDataContext(UtilityHelper.Connection()))
    //            {
    //                var data = db.Suppliers.FirstOrDefault(a => a.Id == id);
    //                data.IsDeleted = true;
    //                data.DeleteDate = DateTime.Now;
    //                db.SubmitChanges();
    //                return true;
    //            }
    //        }
    //        catch (Exception)
    //        {

    //            throw;
    //        }
    //    }
    //    public static ResponseMessage StatusOnchange(int id)
    //    {
    //        try
    //        {
    //            using (var db = new DBDataContext(UtilityHelper.Connection()))
    //            {
    //                var data = db.Suppliers.FirstOrDefault(a => a.Id == id);
    //                if (data.Status == true)
    //                {
    //                    data.Status = false;
    //                    db.SubmitChanges();
    //                    return new ResponseMessage
    //                    {
    //                        Status = true,
    //                        Message = "Başarıyla Durum Değiştirildi."
    //                    };
    //                }
    //                if (data.Status == false)
    //                {
    //                    data.Status = true;
    //                    db.SubmitChanges();
    //                    return new ResponseMessage
    //                    {
    //                        Status = true,
    //                        Message = "Başarıyla Durum Değiştirildi."
    //                    };
    //                }

    //                return new ResponseMessage
    //                {
    //                    Status = false,
    //                    Message = "Lütfen internetinizi kontrol ediniz."
    //                };


    //            }
    //        }
    //        catch (Exception e)
    //        {
    //            return new ResponseMessage
    //            {
    //                Status = false,
    //                Message = "Bir hata oluştu.",
    //                Code = e.StackTrace
    //            };
    //        }
    //    }

    //    public static List<Supplier> AllList()
    //    {
    //        try
    //        {
    //            using (var db = new SqlConnection(UtilityHelper.Connection()))
    //            {
    //                var sql = "select * from dbo.Suppliers where  IsDeleted=0 and Status=1";
    //                return db.Query<Supplier>(sql).ToList();
    //            }
    //        }
    //        catch (Exception e)
    //        {

    //            return new List<Supplier>();
    //        }
    //    }

    //    #region SupplierUser Metotları

    //    public static PagerResult SupplierUserGetList(Pager pager, int id)
    //    {
    //        try
    //        {
    //            using (var db = new DBDataContext(UtilityHelper.Connection()))
    //            {
    //                var data = db.SupplierUsers.PagerDynamic(SupplierUserFilter(pager.Filter, id), pager);
    //                return data;
    //            }
    //        }
    //        catch (Exception e)
    //        {
    //            return new PagerResult
    //            {
    //                Total = 0,
    //                Pager = pager,
    //            };
    //        }
    //    }
    //    private static Expression<Func<SupplierUser, bool>> SupplierUserFilter(string filter, int id)
    //    {


    //        var pred = PredicateBuilder.True<SupplierUser>();


    //        if (!filter.ToControl())
    //        {
    //            pred = pred.And(x => x.Name.Contains(filter) || x.Title.Contains(filter));
    //        }
    //        if (!id.ToControl())
    //        {
    //            pred = pred.And(a => a.SupplierId == id);
    //        }
    //        pred = pred.And(a => a.IsDeleted == false);

    //        return pred;
    //    }
    //    public static ResponseMessage SupplierUserCreate(SupplierUser model)
    //    {
    //        try
    //        {
    //            using (var db = new DBDataContext(UtilityHelper.Connection()))
    //            {
    //                db.SupplierUsers.InsertOnSubmit(model);
    //                db.SubmitChanges();
    //                return new ResponseMessage
    //                {
    //                    Status = true,
    //                    Message = "Ekleme başarılı"
    //                };
    //            }
    //        }
    //        catch (Exception e)
    //        {
    //            return new ResponseMessage
    //            {
    //                Message = "Hata : " + e.Message,
    //                Code = e.StackTrace
    //            };
    //        }
    //    }
    //    public static SupplierUser SupplierUserGetById(int id)
    //    {
    //        try
    //        {
    //            using (var db = new SqlConnection(UtilityHelper.Connection()))
    //            {
    //                var sql = "select top 1 * from dbo.SupplierUsers where id=@id and  IsDeleted=0 ";
    //                return db.Query<SupplierUser>(sql, new { id = id }).FirstOrDefault();
    //            }
    //        }
    //        catch (Exception e)
    //        {

    //            return null;
    //        }
    //    }
    //    public static ResponseMessage SupplierUserUpdate(SupplierUser model)
    //    {
    //        try
    //        {
    //            using (var db = new DBDataContext(UtilityHelper.Connection()))
    //            {
    //                var data = db.SupplierUsers.FirstOrDefault(a => a.Id == model.Id);
    //                data.Email = model.Email;
    //                data.Name = model.Name;
    //                data.Password = model.Password;
    //                data.Phone = model.Phone;
    //                model.Title = data.Title;

    //                data.UpdateDate = DateTime.Now;
    //                db.SubmitChanges();
    //                return new ResponseMessage
    //                {
    //                    Status = true,
    //                    Message = "Başarıyla kayıt  başarılı"
    //                };
    //            }

    //        }
    //        catch (Exception e)
    //        {

    //            return new ResponseMessage
    //            {
    //                Status = false,
    //                Message = "Bir hata oluştu.",
    //                Code = e.StackTrace
    //            };
    //        }
    //    }

    //    public static bool SupplierUserDelete(int id)
    //    {
    //        try
    //        {
    //            using (var db = new DBDataContext(UtilityHelper.Connection()))
    //            {
    //                var data = db.SupplierUsers.FirstOrDefault(a => a.Id == id);
    //                data.IsDeleted = true;
    //                data.DeleteDate = DateTime.Now;
    //                db.SubmitChanges();
    //                return true;
    //            }
    //        }
    //        catch (Exception)
    //        {

    //            throw;
    //        }
    //    }
    //    public static ResponseMessage SupplierUserStatusOnchange(int id)
    //    {
    //        try
    //        {
    //            using (var db = new DBDataContext(UtilityHelper.Connection()))
    //            {
    //                var data = db.SupplierUsers.FirstOrDefault(a => a.Id == id);
    //                if (data.Status == true)
    //                {
    //                    data.Status = false;
    //                    db.SubmitChanges();
    //                    return new ResponseMessage
    //                    {
    //                        Status = true,
    //                        Message = "Başarıyla Durum Değiştirildi."
    //                    };
    //                }
    //                if (data.Status == false)
    //                {
    //                    data.Status = true;
    //                    db.SubmitChanges();
    //                    return new ResponseMessage
    //                    {
    //                        Status = true,
    //                        Message = "Başarıyla Durum Değiştirildi."
    //                    };
    //                }

    //                return new ResponseMessage
    //                {
    //                    Status = false,
    //                    Message = "Lütfen internetinizi kontrol ediniz."
    //                };


    //            }
    //        }
    //        catch (Exception e)
    //        {
    //            return new ResponseMessage
    //            {
    //                Status = false,
    //                Message = "Bir hata oluştu.",
    //                Code = e.StackTrace
    //            };
    //        }
    //    }

    //    #endregion

    //    #region User Areas Metotları

    //    public static SupplierUser Login(string userName, string password,int id)
    //    {
    //        try
    //        {
    //            using (var db = new SqlConnection(UtilityHelper.Connection()))
    //            {
    //                var sql = "select top 1* from SupplierUsers where Email=@us and Password=@pass and IsDeleted=0 and SupplierId=@id";
    //                return db.Query<SupplierUser>(sql, new { us = userName, pass = password,id=id }).FirstOrDefault();
    //            }
    //        }
    //        catch (Exception e)
    //        {

    //            return null;
    //        }
    //    }
    //    public static Supplier GetByCode(string code)
    //    {
    //        try
    //        {
    //            using (var db = new SqlConnection(UtilityHelper.Connection()))
    //            {
    //                var sql = "select * from Suppliers where Code=@c and IsDeleted=0";
    //                return db.Query<Supplier>(sql, new { c = code }).FirstOrDefault();
    //            }
    //        }
    //        catch (Exception e)
    //        {

    //            return null;
    //        }
    //    }


    //    #endregion
    //}
}
