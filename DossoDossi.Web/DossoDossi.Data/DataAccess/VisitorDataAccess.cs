﻿using Dapper;
using DossoDossi.Data.Entity;
using DossoDossi.Data.Helper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.DataAccess
{
   //public class VisitorDataAccess
   // {
   //     public static PagerResult GetList(Pager pager)
   //     {
   //         try
   //         {
   //             using (var db = new DBDataContext(UtilityHelper.Connection()))
   //             {
   //                 var data = db.Visitor.PagerDynamic(Filter(pager.Filter), pager);
   //                 return data;
   //             }
   //         }
   //         catch (Exception e)
   //         {
   //             return new PagerResult
   //             {
   //                 Total = 0,
   //                 Pager = pager,
   //             };
   //         }
   //     }
   //     private static Expression<Func<Visitor, bool>> Filter(string filter)
   //     {


   //         var pred = PredicateBuilder.True<Visitor>();


   //         if (!filter.ToControl())
   //         {
   //             pred = pred.And(x => x.Name.Contains(filter) || x.Title.Contains(filter));
   //         }
   //         pred = pred.And(a => a.IsDeleted == false);
   //         return pred;
   //     }
   //     public static ResponseMessage Create(Visitor model)
   //     {
   //         try
   //         {
   //             using (var db = new DBDataContext(UtilityHelper.Connection()))
   //             {
   //                 db.Visitors.InsertOnSubmit(model);
   //                 db.SubmitChanges();
   //                 return new ResponseMessage
   //                 {
   //                     Status = true,
   //                     Message = "Ekleme başarılı"
   //                 };
   //             }
   //         }
   //         catch (Exception e)
   //         {
   //             return new ResponseMessage
   //             {
   //                 Message = "Hata : " + e.Message,
   //                 Code = e.StackTrace
   //             };
   //         }
   //     }
   //     public static Visitor GetById(int id)
   //     {
   //         try
   //         {
   //             using (var db = new SqlConnection(UtilityHelper.Connection()))
   //             {
   //                 var sql = "select top 1 * from dbo.Visitors where id=@id and  IsDeleted=0 ";
   //                 return db.Query<Visitor>(sql, new { id = id }).FirstOrDefault();
   //             }
   //         }
   //         catch (Exception e)
   //         {

   //             return null;
   //         }
   //     }

   //     public static ResponseMessage Update(Visitor model)
   //     {
   //         try
   //         {
   //             using (var db = new DBDataContext(UtilityHelper.Connection()))
   //             {
   //                 var data = db.Visitors.FirstOrDefault(a => a.Id == model.Id);
   //                 data.Description = model.Description;
   //                 data.Email = model.Email;
   //                 data.Name = model.Name;
   //                 data.Phone = model.Phone;
   //                 data.TaxNo = model.TaxNo;
   //                 data.TaxOffice = model.TaxOffice;
   //                 data.Title = model.Title;
                    
   //                 data.UpdateDate = DateTime.Now;
   //                 db.SubmitChanges();
   //                 return new ResponseMessage
   //                 {
   //                     Status = true,
   //                     Message = "Başarıyla kayıt  başarılı"
   //                 };
   //             }

   //         }
   //         catch (Exception e)
   //         {

   //             return new ResponseMessage
   //             {
   //                 Status = false,
   //                 Message = "Bir hata oluştu.",
   //                 Code = e.StackTrace
   //             };
   //         }
   //     }

   //     public static bool Delete(int id)
   //     {
   //         try
   //         {
   //             using (var db = new DBDataContext(UtilityHelper.Connection()))
   //             {
   //                 var data = db.Visitors.FirstOrDefault(a => a.Id == id);
   //                 data.IsDeleted = true;
   //                 data.DeleteDate = DateTime.Now;
   //                 db.SubmitChanges();
   //                 return true;
   //             }
   //         }
   //         catch (Exception)
   //         {

   //             return false;
   //         }
   //     }
   //     public static ResponseMessage StatusOnchange(int id)
   //     {
   //         try
   //         {
   //             using (var db = new DBDataContext(UtilityHelper.Connection()))
   //             {
   //                 var data = db.Visitors.FirstOrDefault(a => a.Id == id);
   //                 if (data.Status == true)
   //                 {
   //                     data.Status = false;
   //                     db.SubmitChanges();
   //                     return new ResponseMessage
   //                     {
   //                         Status = true,
   //                         Message = "Başarıyla Durum Değiştirildi."
   //                     };
   //                 }
   //                 if (data.Status == false)
   //                 {
   //                     data.Status = true;
   //                     db.SubmitChanges();
   //                     return new ResponseMessage
   //                     {
   //                         Status = true,
   //                         Message = "Başarıyla Durum Değiştirildi."
   //                     };
   //                 }

   //                 return new ResponseMessage
   //                 {
   //                     Status = false,
   //                     Message = "Lütfen internetinizi kontrol ediniz."
   //                 };


   //             }
   //         }
   //         catch (Exception e)
   //         {
   //             return new ResponseMessage
   //             {
   //                 Status = false,
   //                 Message = "Bir hata oluştu.",
   //                 Code = e.StackTrace
   //             };
   //         }




   //     }
   // }
}
