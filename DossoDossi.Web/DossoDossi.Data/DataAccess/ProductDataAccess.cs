﻿using DossoDossi.Data.Entity;
using DossoDossi.Data.Helper;
using DossoDossi.Data.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;

namespace DossoDossi.Data.DataAccess
{
    public class ProductDataAccess
    {
        public static PagerResult<View_Malzemeler> GetList(Pager pager, string barkod, string stoklu,
            string ozelkod,
            string ozelkod2,
            string ozelkod3,
            string ozelkod4,
            string ozelkod5)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_Malzemelers.PagerDynamic(Filter(pager.Filter, barkod, stoklu, ozelkod, ozelkod2, ozelkod3, ozelkod4, ozelkod5), pager);
                }
            }
            catch (Exception ex)
            {
                return new PagerResult<View_Malzemeler>
                {
                    Data = new System.Collections.Generic.List<View_Malzemeler>(),
                    ErrorMessage = ex.Message,
                    StackTrace = ex.StackTrace
                };
            }
        }

        public static View_Malzemeler GetByBarkod(string bKod)
        {
            try
            {
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static List<string> GetGroupNamesSelect(string v)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    string query = "SELECT " + v + " FROM View_Malzemeler WHERE LEN(" + v + ") > 0 GROUP BY " + v;
                    return db.Query<string>(query).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<string>();
            }
        }

        public static PagerResult<View_SatinAlmaSihirbaz> GetWizardList(Pager pager)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_SatinAlmaSihirbazs.PagerDynamic(FilterWizard(pager.Filter), pager);
                }
            }
            catch (Exception ex)
            {
                return new PagerResult<View_SatinAlmaSihirbaz>
                {
                    Data = new List<View_SatinAlmaSihirbaz>(),
                    ErrorMessage = ex.Message,
                    StackTrace = ex.StackTrace
                };
            }
        }

        public static PagerResult<OnTheRoadProduct> GetOnTheRoadList(Pager pager, string orderNo, string supplier, string po)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.OnTheRoadProducts.PagerDynamic(FilterOnTheRoad(pager.Filter, orderNo, supplier, po), pager);
                }
            }
            catch (Exception ex)
            {
                return new PagerResult<OnTheRoadProduct>
                {
                    Data = new List<OnTheRoadProduct>(),
                    ErrorMessage = ex.Message,
                    StackTrace = ex.StackTrace
                };
            }
        }

        public static ResponseMessage<ProjeTedarikciUrunler> GetProjeByBarkod(string code, string barkod)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var data = db.Query<ProjeTedarikciUrunler>("SELECT TOP 1 * FROM [ProjeTedarikciUrunler] WITH (NOLOCK) WHERE [Barkodu] = @barkod And [ProjeKodu] = @code", new { code, barkod }).FirstOrDefault();

                    if (data == null)
                    {
                        return new ResponseMessage<ProjeTedarikciUrunler>
                        {
                            Message = "Kayıt bulunamadı / Not found"
                        };
                    }

                    return new ResponseMessage<ProjeTedarikciUrunler>
                    {
                        Message = "ok",
                        Data = data,
                        Status = true
                    };
                }
            }
            catch (Exception ex)
            {
                return new ResponseMessage<ProjeTedarikciUrunler>
                {
                    Message = "Kayıt hatası"
                };
            }
        }

        private static Expression<Func<View_SatinAlmaSihirbaz, bool>> FilterWizard(string filter)
        {
            var pred = PredicateBuilder.True<View_SatinAlmaSihirbaz>();

            if (!filter.ToControl())
            {
                pred = pred.And(a => a.MalzemeKodu.Contains(filter) || a.MalzemeAdi.Contains(filter));
            }

            return pred;
        }

        private static Expression<Func<OnTheRoadProduct, bool>> FilterOnTheRoad(string filter, string orderNo, string supplier, string po)
        {
            var pred = PredicateBuilder.True<OnTheRoadProduct>();

            if (!filter.ToControl())
            {
                pred = pred.And(a => a.ProductCode.Contains(filter) || a.ProductName.Contains(filter));
            }

            if (!orderNo.ToControl())
            {
                pred = pred.And(a => a.OrderNo.Contains(orderNo));
            }

            if (!supplier.ToControl())
            {
                pred = pred.And(a => a.SupplierCode.Contains(supplier) || a.SupplierName.Contains(supplier));
            }

            if (!po.ToControl())
            {
                pred = pred.And(a => a.DeliveryCode.Contains(po));
            }

            return pred;
        }


        public static PagerResult<View_MalzemeHareketleri> GetListTransaction(Pager pager, int id, int? isyeri)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_MalzemeHareketleris.PagerDynamic(FilterTransaction(pager.Filter, id, isyeri), pager);
                }
            }
            catch (Exception ex)
            {
                return new PagerResult<View_MalzemeHareketleri>
                {
                    Data = new System.Collections.Generic.List<View_MalzemeHareketleri>(),
                    ErrorMessage = ex.Message,
                    StackTrace = ex.StackTrace
                };
            }
        }

        public static List<UrunAmbarToplamDto> StokAmbarToplamlariGetir(int id)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    string query = "EXEC Proc_AmbarToplamListGetir @StokId = @id";
                    return db.Query<UrunAmbarToplamDto>(query, new { id }).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<UrunAmbarToplamDto>();
            }
        }

        public static ProjeTedarikciUrunler GetByProjetTedarikciUrunByUQ(Guid id)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    string query = "SELECT TOP 1 * FROM [ProjeTedarikciUrunler] WITH (NOLOCK) WHERE Id = @id ";
                    return db.Query<ProjeTedarikciUrunler>(query, new { id }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static View_MalzemeCariFiyatlar GetMalzemeFiyat(int stokId, string cariKodu)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    var list = db.View_MalzemeCariFiyatlars.Where(a => a.StokId == stokId && a.CariKodu == cariKodu).ToList();

                    if (list.Any())
                    {
                        return list.OrderByDescending(a => a.SonTarih).FirstOrDefault();
                    }

                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static List<int> GetContainsByNamesIds(string malzemeAdi)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    var l = db.View_SiparisDetaylis.Where(a => a.MalzemeAciklamasi.Contains(malzemeAdi) || a.MalzemeKodu.Contains(malzemeAdi)).OrderByDescending(a => a.SatirId).Take(1000).Select(x => x.FisId).Distinct().ToList();
                    return l;
                }
            }
            catch (Exception ex)
            {
                return new List<int>();
            }
        }

        public static List<OnTheRoadProduct> GetOnTheRoadListByStockId(int id)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.OnTheRoadProducts.Where(a => (a.Qty - a.IncomingQty) > 0 && a.ProductId == id).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<OnTheRoadProduct>();
            }
        }

        public static OnTheRoadProduct GetOnTheRoadDetailById(int id)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.OnTheRoadProducts.FirstOrDefault(x => x.Id == id);
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static ResponseMessage UpsertOnTheRoadData(ExcelDynamicDto fat)
        {
            try
            {
                var resp = new ResponseMessage();

                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    var productCode = fat.HeadVal("ProductCode");
                    var orderNo = fat.HeadVal("OrderNo");

                    var date = fat.HeadValDate("Date");

                    var dt = fat.HeadVal("Date");

                    if (date == null)
                    {
                        if (dt.Contains("-"))
                        {
                            date = DateTime.Parse(dt);
                        }
                        else
                        {
                            date = dt.ParseDate();
                        }
                    }

                    if (date == null)
                    {
                        resp.Message = "Line : " + fat.No + ". Xls Date field is required.";
                        return resp;
                    }

                    var add = new OnTheRoadProduct
                    {
                        Note1 = fat.HeadVal("Note"),
                        SupplierCode = fat.HeadVal("SupplierCode"),
                        OrderNo = fat.HeadVal("OrderNo"),
                        OrderDate = date.Value,
                        CreatedDate = DateTime.Now,
                        CreateUser = SessionHelper.User.Name,
                        Qty = double.Parse(fat.HeadVal("Qty")),
                        IncomingQty = double.Parse(fat.HeadVal("IncomingQty")),
                        DeliveryDate = fat.HeadValDate("DeliveryDate"),
                        DeliveryCode = fat.HeadVal("DeliveryCode"),
                        SendType = fat.HeadVal("DeliveryMethod"),
                        ProductCode = productCode,
                        UQ = Guid.NewGuid()
                    };

                    var cari = CustomerDataAccess.GetByCode(add.SupplierCode);

                    if (cari == null)
                    {
                        resp.Message = "Line : " + fat.No + ". Product " + add.ProductCode + " - OrderNo : " + add.OrderNo + ". Error =  Supplier not found in LOGO !";
                        return resp;
                    }

                    add.SupplierName = cari.ChUnvani;

                    var anyData = db.OnTheRoadProducts.FirstOrDefault(x => x.ProductCode == productCode && x.OrderNo == orderNo);

                    if (anyData == null)
                    {
                        var urun = GetByCode(productCode.Trim());

                        if (urun == null)
                        {
                            resp.Message = "Line : " + fat.No + ". Product " + add.ProductCode + " | Error = (Product not found in LOGO !)";
                            return resp;
                        }

                        add.ProductId = urun.StokId;
                        add.ProductName = urun.MalzemeAdi;


                        db.OnTheRoadProducts.InsertOnSubmit(add);
                        db.SubmitChanges();

                        resp.Message = "Line : " + fat.No + ". Product " + add.ProductCode + " - OrderNo : " + add.OrderNo + " ->Success. New inserted";
                        resp.Status = true;
                        return resp;
                    }

                    anyData.Note1 = add.Note1;
                    anyData.SupplierCode = add.SupplierCode;
                    anyData.SupplierName = add.SupplierName;
                    anyData.Qty = add.Qty;
                    anyData.IncomingQty = add.IncomingQty;
                    anyData.DeliveryCode = add.DeliveryCode;
                    anyData.SendType = add.SendType;
                    anyData.DeliveryDate = add.DeliveryDate;

                    db.SubmitChanges();

                    resp.Status = true;
                    resp.Message = "Line : " + fat.No + ". Product " + add.ProductCode + " - OrderNo : " + add.OrderNo + " ->Success .  Updated data";
                    return resp;
                }
            }
            catch (Exception ex)
            {
                return new ResponseMessage
                {
                    ErrorMessage = ex.Message,
                    Message = "Error. Msg : " + ex.Message + "Dt : " + fat.HeadVal("Date"),
                    Code = ex.StackTrace
                };
            }
        }

        public static List<View_Malzemeler> GetListByIds(List<int> ids)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql = "SELECT * FROM View_Malzemeler WHERE StokId IN @ids";
                    return db.Query<View_Malzemeler>(sql, new { ids }).ToList();
                }
            }
            catch (Exception)
            {
                return new List<View_Malzemeler>();
            }
        }

        public static string GetCariStokKoduEnSon(int stokId, string cariKodu)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var q = "SELECT Top 1 it.CustomerPrCode FROM OfferItems as it inner join Offers as  o on it.OfferId = o.Id where o.CariKodu = @code And it.ProductId = @id AND LEN(it.CustomerPrCode) > 0 ORDER BY it.Id DESC";
                    var d = db.Query<string>(q, new { id = stokId, code = cariKodu }).FirstOrDefault();

                    return d;
                }

            }
            catch (Exception)
            {
                return string.Empty;
            }
        }



        public static View_Malzemeler GetById(int id)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_Malzemelers.Where(a => a.StokId == id).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static View_Malzemeler GetByCode(string code)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<View_Malzemeler>("SELECT * FROM View_Malzemeler  WHERE  MalzemeKodu = @code", new { code }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private static Expression<Func<View_Malzemeler, bool>> Filter(string filter, string barkod, string stoklu, string ozelkod,
            string ozelkod2,
            string ozelkod3,
            string ozelkod4,
            string ozelkod5)
        {
            var pred = PredicateBuilder.True<View_Malzemeler>();

            if (!filter.ToControl())
            {
                pred = pred.And(a => a.MalzemeAdi.Contains(filter) || a.MalzemeKodu.Contains(filter) || a.NAME3.Contains(filter));
            }

            if (!barkod.ToControl())
            {
                pred = pred.And(a => a.UreticiKodu.Contains(barkod));
            }

            if (!stoklu.ToControl())
            {
                if (stoklu == "1")
                {
                    pred = pred.And(a => a.MerkezStok > 0);
                }
                else if (stoklu == "2")
                {
                    pred = pred.And(a => a.BekleyenSiparisMiktar > 0);
                }
                else if (stoklu == "3")
                {
                    pred = pred.And(a => a.YoldakiMiktar > 0);
                }
                else if (stoklu == "4")
                {
                    pred = pred.And(a => a.UretimdekiMiktar > 0);
                }
            }

            if (!ozelkod.ToControl())
            {
                pred = pred.And(a => a.OzelKodu == ozelkod);
            }

            if (!ozelkod2.ToControl())
            {
                pred = pred.And(a => a.OzelKodu2 == ozelkod2);
            }

            if (!ozelkod3.ToControl())
            {
                pred = pred.And(a => a.OzelKodu3 == ozelkod3);
            }

            if (!ozelkod4.ToControl())
            {
                pred = pred.And(a => a.OzelKodu4 == ozelkod4);
            }

            if (!ozelkod5.ToControl())
            {
                pred = pred.And(a => a.OzelKodu5 == ozelkod5);
            }
            return pred;
        }

        private static Expression<Func<View_MalzemeHareketleri, bool>> FilterTransaction(string filter, int id, int? isyeri)
        {
            var pred = PredicateBuilder.True<View_MalzemeHareketleri>().And(a => a.StokId == id);

            if (!filter.ToControl())
            {
                pred = pred.And(a => a.MusteriKodu.Contains(filter) || a.MusteriAdi.Contains(filter));
            }

            if (isyeri.HasValue && isyeri.Value > 0)
            {
                pred = pred.And(a => a.IsyeriNo == isyeri.Value);
            }

            return pred;
        }

        public static List<View_Malzemeler> GetAllList()
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<View_Malzemeler>("SELECT * FROM [dbo].[View_Malzemeler] where Kullanimda =  'Kullanimda'").ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<View_Malzemeler>();
            }
        }


        public static ResponseMessage CreateOnTheRoadData(CreateOnTheRoadDto model)
        {
            try
            {
                var uq = Guid.NewGuid();

                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    foreach (var item in model.Items)
                    {
                        var add = new OnTheRoadProduct
                        {
                            CreatedDate = DateTime.Now,
                            CreateUser = model.CreateUser,
                            UQ = uq,
                            DeliveryCode = model.DeliveryCode,
                            DeliveryDate = model.DeliveryDate,
                            Qty = item.Qty,
                            IncomingQty = 0,
                            ProductId = item.ProductId,
                            ProductCode = item.ProductCode,
                            ProductName = item.ProductName,
                            Note1 = model.Note1,
                            OrderNo = model.OrderNo,
                            OrderDate = model.OrderDate,
                            SendType = model.SendType,
                            SupplierCode = model.SupplierCode,
                            SupplierName = model.SupplierName,
                            Description = item.Note,
                            LeadTime = model.LeadTime
                        };

                        db.OnTheRoadProducts.InsertOnSubmit(add);
                    }

                    db.SubmitChanges();
                }

                return new ResponseMessage
                {
                    Status = true,
                    Message = "Succesfully Saved"
                };
            }
            catch (Exception ex)
            {
                return new ResponseMessage
                {
                    Message = "ERROR : " + ex.Message,
                    Code = ex.StackTrace,
                    Status = false,
                    Type = "Create"
                };
            }
        }

        public static ResponseMessage EditOnTheRoadData(OnTheRoadProduct model)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    var d = db.OnTheRoadProducts.FirstOrDefault(a => a.Id == model.Id);

                    d.Qty = model.Qty;
                    d.IncomingQty = model.IncomingQty;
                    d.Note1 = model.Note1;
                    d.Description = model.Description;
                    d.DeliveryCode = model.DeliveryCode;
                    d.DeliveryDate = model.DeliveryDate;
                    d.SendType = model.SendType;
                    d.UpdatedDate = DateTime.Now;
                    d.LeadTime = model.LeadTime;

                    db.SubmitChanges();
                }

                return new ResponseMessage
                {
                    Status = true,
                    Message = "Succesfully Saved"
                };

            }
            catch (Exception ex)
            {
                return new ResponseMessage
                {
                    Message = "ERROR : " + ex.Message,
                    Code = ex.StackTrace,
                    Status = false,
                    Type = "Update"
                };
            }
        }

        public static bool DeleteOnTheRoad(int id)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    var d = db.OnTheRoadProducts.FirstOrDefault(a => a.Id == id);

                    db.OnTheRoadProducts.DeleteOnSubmit(d);
                    db.SubmitChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
