﻿using DossoDossi.Data.Entity;
using DossoDossi.Data.Helper;

using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using DossoDossi.Data.Models;

namespace DossoDossi.Data.DataAccess
{
    public class CustomerDataAccess
    {
        public static PagerResult<View_CariHesaplar> GetList(Pager pager, string isall,string start)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_CariHesaplars.PagerDynamic(Filter(pager.Filter,isall, start), pager);
                }
            }
            catch (Exception ex)
            {
                return new PagerResult<View_CariHesaplar>
                {
                    Data = new List<View_CariHesaplar>(),
                    ErrorMessage = ex.Message,
                    StackTrace = ex.StackTrace
                };
            }
        }

        public static PagerResult<Musteriler> GetMusteriList(Pager pager)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.Musterilers.PagerDynamic(FilterMusteri(pager.Filter), pager);
                }
            }
            catch (Exception ex)
            {
                return new PagerResult<Musteriler>
                {
                    Data = new List<Musteriler>(),
                    Pager = pager,
                    StackTrace = ex.StackTrace
                };
            }
        }

        private static Expression<Func<Musteriler, bool>> FilterMusteri(string filter)
        {
            var pred = PredicateBuilder.True<Musteriler>();

            if(!filter.ToControl())
            {
                pred = pred.And(a => a.Name.Contains(filter) || a.Code.Contains(filter));
            }

            return pred;
        }

        private static Expression<Func<View_CariHesaplar, bool>> Filter(string filter, string isall,string start)
        {
            var pred = PredicateBuilder.True<View_CariHesaplar>();

            if (!filter.ToControl())
            {
                pred = pred.And(a => a.MusteriKodu.Contains(filter) || a.MusteriUnvani.Contains(filter));
            }

            if(!isall.ToControl()&& isall == "1")
            {

            }
            else
            {
                var user = SessionHelper.User;

                var us = AccountDataAccess.GetById(user.UserId);

                if (us.Isyeri.HasValue && us.Isyeri.Value > 0)
                {
                    pred = pred.And(a => a.YetkiKodu == us.Isyeri.Value.ToString());
                }
            }

            if (!start.ToControl())
            {
                pred = pred.And(a => a.ChKodu.StartsWith(start));
            }

            return pred;
        }

        public static PagerResult<View_CariHesapHareketleri> GetListTransaction(Pager pager, int id)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_CariHesapHareketleris.PagerDynamic(FilterTransaction(pager.Filter, id), pager);
                }
            }
            catch (Exception ex)
            {
                return new PagerResult<View_CariHesapHareketleri>
                {
                    Data = new System.Collections.Generic.List<View_CariHesapHareketleri>(),
                    ErrorMessage = ex.Message,
                    StackTrace = ex.StackTrace
                };
            }
        }

   

        private static Expression<Func<View_CariHesapHareketleri, bool>> FilterTransaction(string filter, int id)
        {
            var pred = PredicateBuilder.True<View_CariHesapHareketleri>().And(a => a.CariId == id);

            if (!filter.ToControl())
            {
                pred = pred.And(a => a.FisNo.Contains(filter) || a.SatirAciklamasi.Contains(filter));
            }

            return pred;
        }

        public static Musteriler GetMusteriById(Guid musteriId)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.Musterilers.Where(a => a.MusteriId== musteriId).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

     

        public static View_CariHesaplar GetById(int id)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_CariHesaplars.Where(a => a.CariId== id).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static View_CariHesaplar GetByIdParam(string param, string val)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<View_CariHesaplar>("SELECT TOP 1 * FROM View_CariHesaplar WHERE "+param+" = @val",new {val }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static void StokAktarimUpdate(Guid markaId, bool? aktarim)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    db.Query("UPDATE ProjeTedarikciUrunler SET IsAktarim = @aktarim WHERE PTrId = markaId", new { markaId, aktarim });
                }
            }
            catch (Exception ex)
            {
                //
            }
        }

        public static void DeleteStokAktarim(Guid markaId, bool? aktarim)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    db.Query("DELETE FROM ProjeTedarikciUrunler WHERE IsAktarim = @aktarim WHERE PTrId = markaId", new { markaId, aktarim });
                }
            }
            catch (Exception ex)
            {
                //
            }
        }


        public static View_CariHesaplar GetByCode(string code)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_CariHesaplars.Where(a => a.ChKodu == code).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static bool DeleteAllMusteri(string code)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    db.Query("DELETE FROM ProjeMusteriler WHERE ProjeKodu = @code", new { code });
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool DeleteAllTedarik(string code)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    db.Query("DELETE FROM ProjeTedarikciler WHERE ProjeKodu = @code", new { code });
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool DeleteLineMusteri(Guid id)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    db.Query("DELETE FROM ProjeMusteriler WHERE Id  = @id", new { id});
                }

                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool DeleteLineStok(Guid id)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    db.Query("DELETE FROM [ProjeTedarikciUrunler] WHERE Id  = @id", new { id });
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool DeleteLineTedarik(Guid id)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    db.Query("DELETE FROM ProjeTedarikciler WHERE Id  = @id", new { id});
                }

                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static ResponseMessage UpsertMusteriPost(string code, Musteriler model)
        {
            try
            {

                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                        model.Status = true;
                        db.Musterilers.InsertOnSubmit(model);
                        db.SubmitChanges();

                        var musteriId = model.MusteriId;

                    var any = db.ProjeMusterilers.Where(a => a.ProjeKodu == code && a.MusteriId == musteriId).Any();

                    if (!any)
                    {
                        db.ProjeMusterilers.InsertOnSubmit(new ProjeMusteriler
                        {
                            Id = Guid.NewGuid(),
                            MusteriId = musteriId,
                            ProjeKodu = code
                        });

                        db.SubmitChanges();
                    }

                    return new ResponseMessage
                    {
                        Message = "Aktarım OK",
                        Status = true
                    };
                }
                }
            catch (Exception ex)
            {
                return new ResponseMessage
                {
                    Message = "Hata oluştu.",
                    Code = ex.Message,
                    ErrorMessage = ex.StackTrace
                };
            }
        }

        public static ResponseMessage UpsertMusteriMultiple(List<Musteriler> list)
        {
            try
            {
                var projeKodu = SettingDataAccess.GetValByCode("ActiveFuarNo");

                using (var db = new DBDataContext(UtilityHelper.Connection))
                {

                    foreach(var item in list)
                    {
                        var any = db.Musterilers.Any(a => a.MusteriId == item.MusteriId);
                        if (any)
                        {
                            return new ResponseMessage
                            {
                                Status = true,
                                Message = "Zaten var"
                            };
                        }

                        db.Musterilers.InsertOnSubmit(item);

                        var add = new ProjeMusteriler
                        {
                            Id = Guid.NewGuid(),
                            MusteriId = item.MusteriId,
                            ProjeKodu = projeKodu
                        };

                        db.ProjeMusterilers.InsertOnSubmit(add);
                        db.SubmitChanges();
                    }

                    //var listItems = list.Select(a =>);

                    //db.ProjeMusterilers.InsertAllOnSubmit(listItems);
                    //db.SubmitChanges();

                    return new ResponseMessage
                    {
                        Message = "Aktarım OK",
                        Status = true
                    };
                }
            }
            catch (Exception ex)
            {
                return new ResponseMessage
                {
                    Message = "Hata oluştu.",
                    Code = ex.Message,
                    ErrorMessage = ex.StackTrace
                };
            }
        }

        public static ResponseMessage EditMusteriPost(string code, Musteriler model)
        {
            try
            {

                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    var must = db.Musterilers.FirstOrDefault(a => a.MusteriId == model.MusteriId);

                        var musteriId = must.MusteriId;

                        must.Status = model.Status;
                        must.Name = model.Name;
                        must.Phone = model.Phone;
                    
                        must.Country = model.Country;
                        must.City = model.City;
                        must.Address = model.Address;
                        must.Note = model.Note;
                        must.Phone = model.Phone;
                        must.Email= model.Email;
                        must.SicilNo= model.SicilNo;
                        must.Code= model.SicilNo;

                        must.TaxOffice= model.TaxNo;
                        must.TaxOffice= model.TaxOffice;
                        must.CompanyName= model.CompanyName;

                        must.CompanyName = model.CompanyName;

                        db.SubmitChanges();

                    var any = db.ProjeMusterilers.Where(a => a.ProjeKodu == code && a.MusteriId == musteriId).Any();

                    if (!any)
                    {
                        db.ProjeMusterilers.InsertOnSubmit(new ProjeMusteriler
                        {
                            Id = Guid.NewGuid(),
                            MusteriId = musteriId,
                            ProjeKodu = code
                        });

                        db.SubmitChanges();
                    }

                    return new ResponseMessage
                    {
                        Message = "Aktarım OK",
                        Status = true
                    };
                }
                }
            catch (Exception ex)
            {
                return new ResponseMessage
                {
                    Message = "Hata oluştu.",
                    Code = ex.Message,
                    ErrorMessage = ex.StackTrace
                };
            }
        }

        public static ResponseMessage UpsertTedarikciPost(ProjeTedarikciler model)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    //int musteriId = 0;

                    //var must = db.View_CariHesaplars.FirstOrDefault(a => a.ChKodu == model.CariKodu);

                    //if (must == null)
                    //{
                    //    return new ResponseMessage
                    //    {
                    //        Message = "Kayıt cari değil." + model.CariKodu
                    //    };
                    //}
                    //else
                    //{

                    //    musteriId = must.CariId;
                    //}

                    var any = db.ProjeTedarikcilers.Where(a => a.ProjeKodu == model.ProjeKodu&& a.MarkaId== model.MarkaId).FirstOrDefault();

                    if (any==null)
                    {
                        db.ProjeTedarikcilers.InsertOnSubmit(new ProjeTedarikciler
                        {
                            Id = model.Id,
                            CariId = model.CariId,
                            CariKodu = model.CariKodu,
                            CariUnvan = model.CariUnvan,
                            KullaniciAdi = model.KullaniciAdi,
                            Sifre = model.Sifre,
                            ProjeKodu = model.ProjeKodu,
                            MarkaId = model.MarkaId
                        });

                        db.SubmitChanges();
                    }
                    else{

                        any.KullaniciAdi = model.KullaniciAdi;
                        any.CariUnvan = model.CariUnvan;
                        any.Sifre = model.Sifre;

                        db.SubmitChanges();
                    }

                    return new ResponseMessage
                    {
                        Message = "Aktarım OK",
                        Status = true
                    };
                }


            }
            catch (Exception ex)
            {
                return new ResponseMessage
                {
                    Message = "Hata oluştu.",
                    Code = ex.Message,
                    ErrorMessage = ex.StackTrace
                };
            }
        }

        public static ResponseMessage UpsertStockPost(ProjeTedarikciUrunler model)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    //var malz = db.View_Malzemelers.FirstOrDefault(a => a.MalzemeKodu == model.StokKodu);

                    //if (malz == null)
                    //{
                    //    return new ResponseMessage
                    //    {
                    //        Message = model.StokKodu + " kodlu ürün yok"
                    //    };
                    //}

                    var any = db.ProjeTedarikciUrunlers.FirstOrDefault(a => a.ProjeKodu == model.ProjeKodu 
                    && a.Barkodu == model.Barkodu);
                    if (any == null)
                    {
                        model.CreatedOn = DateTime.Now;
                        model.IsAktarim = true;
                        //model.StokId = malz.StokId;
                        //model.StokAdi = malz.MalzemeAdi;

                        db.ProjeTedarikciUrunlers.InsertOnSubmit(model);

                        db.SubmitChanges();
                    }
                    else
                    {
                        any.Marka = model.Marka;
                        any.SatisFiyat = model.SatisFiyat;
                        any.SeriAdedi = model.SeriAdedi;
                        any.GrupKodu = model.GrupKodu;
                        any.Aciklama2 = model.Aciklama2;
                        model.IsAktarim = true;

                        db.SubmitChanges();
                    }

                    return new ResponseMessage
                    {
                        Status = true,
                        Message = "Aktarım Ok"
                    };

                }

                }
            catch (Exception ex)
            {
                return new ResponseMessage
                {
                    Message = "Hata oluştu.",
                    Code = ex.Message,
                    ErrorMessage = ex.StackTrace
                };
            }
        }

        public static List<View_CariHesaplar> GetByIds(List<int> cIds)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_CariHesaplars.Where(a =>cIds.Contains(a.CariId)).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<View_CariHesaplar>();
            }
        }

        public static List<CariHesapDto> GetByIdsDapper(List<int> ids)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<CariHesapDto>("SELECT * FROM View_CariHesaplar WHERE CariId IN @ids", new { ids }).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<CariHesapDto>();
            }
        }

        public static List<View_CariHesaplar> GetByIds(List<string> cIds)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_CariHesaplars.Where(a =>cIds.Contains(a.ChKodu)).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<View_CariHesaplar>();
            }
        }
    }
}
