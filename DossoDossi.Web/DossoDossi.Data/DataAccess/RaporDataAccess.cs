﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using System.Threading.Tasks;
using System.Data.SqlClient;
using DossoDossi.Data.Helper;
using DossoDossi.Data.Models.Report;
using DossoDossi.Data.Models;

namespace DossoDossi.Data.DataAccess
{
    public class RaporDataAccess
    {
        public static List<DailyReportDetailDto> GetDailyReport(DateTime start, DateTime end, int firmNo)
        {
            try
            {
                var g = firmNo.ToString("D3");

                using (var db = new SqlConnection(UtilityHelper.LogoConnection))
                {
                    var sql = "SELECT O.DATE_,O.FICHENO , CL.CODE as CLCODE, CL.DEFINITION_ as CLNAME,  L.STOCKREF,I.CODE, I.NAME, L.AMOUNT,ROUND( (L.PRICE /(CASE WHEN L.REPORTRATE = 0 THEN 99999999999999 ELSE L.REPORTRATE END)),5) as PRICE," +
                        " ISNULL((SELECT TOP 1 ROUND( ((VATMATRAH / AMOUNT) / (CASE WHEN L.REPORTRATE = 0 THEN 99999999999999 ELSE L.REPORTRATE END)),5) AS PRICE FROM LG_" + g + "_01_STLINE as AL " +
                        "WHERE AL.TRCODE IN (1,14) AND AL.STFICHEREF >0 AND AL.STOCKREF = L.STOCKREF ORDER BY AL.LOGICALREF DESC ),0) as ALPRICE" +
                        " FROM LG_" + g + "_01_STFICHE as O " +
                        "LEFT JOIN LG_" + g + "_CLCARD as CL  ON CL.LOGICALREF= O.CLIENTREF " +
                        "INNER JOIN LG_" + g + "_01_STLINE as L  ON L.STFICHEREF = O.LOGICALREF " +
                        "INNER JOIN LG_" + g + "_ITEMS as I ON I.LOGICALREF = L.STOCKREF " +
                        "WHERE O.TRCODE = 8 AND CONVERT(date, O.DATE_) >= @start AND CONVERT(date, O.DATE_) <= @end  AND I.CODE <> 'ÿ'";
                    return db.Query<DailyReportDetailDto>(sql, new { start, end, firmNo }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<DailyReportDetailDto> GetDailyReportByCountry(DateTime start, DateTime end, int firmNo)
        {
            try
            {
                var g = firmNo.ToString("D3");

                using (var db = new SqlConnection(UtilityHelper.LogoConnection))
                {
                    var sql = "SELECT O.DATE_,O.FICHENO , CL.CODE as CLCODE, CL.DEFINITION_ as CLNAME , CL.COUNTRY, I.SPECODE, " +
                        " L.STOCKREF,I.CODE, I.NAME, L.AMOUNT,ROUND( (L.PRICE /(CASE WHEN L.REPORTRATE = 0 THEN 99999999999999 ELSE L.REPORTRATE END)),5) as PRICE," +
                        " ISNULL((SELECT TOP 1 ROUND( ((VATMATRAH / AMOUNT) / (CASE WHEN L.REPORTRATE = 0 THEN 99999999999999 ELSE L.REPORTRATE END)),5) AS PRICE FROM LG_" + g + "_01_STLINE as AL " +
                        "WHERE AL.TRCODE IN (1,14) AND AL.STFICHEREF >0 AND AL.STOCKREF = L.STOCKREF ORDER BY AL.LOGICALREF DESC ),0) as ALPRICE" +
                        " FROM LG_" + g + "_01_STFICHE as O " +
                        "LEFT JOIN LG_" + g + "_CLCARD as CL  ON CL.LOGICALREF= O.CLIENTREF " +
                        "INNER JOIN LG_" + g + "_01_STLINE as L  ON L.STFICHEREF = O.LOGICALREF " +
                        "INNER JOIN LG_" + g + "_ITEMS as I ON I.LOGICALREF = L.STOCKREF " +
                        "WHERE O.TRCODE = 8 AND CONVERT(date, O.DATE_) >= @start AND CONVERT(date, O.DATE_) <= @end  AND I.CODE <> 'ÿ'";
                    return db.Query<DailyReportDetailDto>(sql, new { start, end, firmNo }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<DossoDossi.Data.Helper.SelectItemDto> GetAllFirm()
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.LogoConnection))
                {
                    var sql = "select NR as id, NAME as text from L_CAPIFIRM";
                    return db.Query<Helper.SelectItemDto>(sql).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<Helper.SelectItemDto>();
            }
        }

        public static double InventoryTotal(int firmNo)
        {
            try
            {
                var g = firmNo.ToString("D3");
                using (var db = new SqlConnection(UtilityHelper.LogoConnection))
                {
                    var sql = "SELECT ROUND(SUM(T.Stock * T.AlPrice),2) FROM " +
                    "(" +
                    "SELECT " +
                    "(SELECT SUM(GNT.ONHAND + GNT.TEMPOUT) " +
                    "FROM dbo.L_CAPIWHOUSE AMB " +
                    "LEFT OUTER JOIN  dbo.LV_"+g+"_01_GNTOTST GNT  ON AMB.NR = GNT.INVENNO AND GNT.STOCKREF = IT.LOGICALREF " +
                    "WHERE AMB.FIRMNR = 1) as Stock, " +
                    "ISNULL((SELECT TOP 1 ROUND(((VATMATRAH / AMOUNT) / (CASE WHEN AL.REPORTRATE = 0 THEN 99999999999999 ELSE AL.REPORTRATE END)), 5) AS PRICE " +
                    "FROM LG_"+g+"_01_STLINE as AL " +
                    "WHERE AL.TRCODE IN (1,14) AND AL.STFICHEREF >0 AND AL.STOCKREF = IT.LOGICALREF ORDER BY AL.LOGICALREF DESC),0)  as AlPrice " +
                    "FROM LG_"+g+"_ITEMS IT " +
                    "WHERE IT.ACTIVE = 0 AND (SELECT SUM(GNT.ONHAND + GNT.TEMPOUT) " +
                    "FROM dbo.L_CAPIWHOUSE AMB " +
                    "LEFT OUTER JOIN  dbo.LV_"+g+"_01_GNTOTST GNT  ON AMB.NR = GNT.INVENNO AND GNT.STOCKREF = IT.LOGICALREF " +
                    "WHERE AMB.FIRMNR = 1) > 0  )  as T";

                    return db.Query<double>(sql).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static List<InventoryDetailDto> InventoryTotalDetailList(int firmNo)
        {
            try
            {
                var g = firmNo.ToString("D3");
                using (var db = new SqlConnection(UtilityHelper.LogoConnection))
                {
                    var sql = "SELECT T.* FROM " +
                    "(" +
                    "SELECT " +
                    "IT.CODE as StokKodu, " +
                    "IT.NAME as StokAdi , " +
                    "IT.SPECODE as OzelKodu , " +
                    "IT.LOGICALREF as StokId, " +
                    "(SELECT SUM(GNT.ONHAND + GNT.TEMPOUT) " +
                    "FROM dbo.L_CAPIWHOUSE AMB " +
                    "LEFT OUTER JOIN  dbo.LV_" + g + "_01_GNTOTST GNT  ON AMB.NR = GNT.INVENNO AND GNT.STOCKREF = IT.LOGICALREF " +
                    "WHERE AMB.FIRMNR = 1) as Stock, " +
                    "ISNULL((SELECT TOP 1 ROUND(((VATMATRAH / AMOUNT) / (CASE WHEN AL.REPORTRATE = 0 THEN 99999999999999 ELSE AL.REPORTRATE END)), 5) AS PRICE " +
                    "FROM LG_" + g + "_01_STLINE as AL " +
                    "WHERE AL.TRCODE IN (1,14) AND AL.STFICHEREF > 0 AND AL.STOCKREF = IT.LOGICALREF ORDER BY AL.LOGICALREF DESC),0)  as AlPrice " +
                    "FROM LG_" + g + "_ITEMS IT " +
                    "WHERE IT.ACTIVE = 0 AND (SELECT SUM(GNT.ONHAND + GNT.TEMPOUT) " +
                    "FROM dbo.L_CAPIWHOUSE AMB " +
                    "LEFT OUTER JOIN  dbo.LV_" + g + "_01_GNTOTST GNT  ON AMB.NR = GNT.INVENNO AND GNT.STOCKREF = IT.LOGICALREF " +
                    "WHERE AMB.FIRMNR = "+ firmNo + ") > 0  )  as T";

                    return db.Query<InventoryDetailDto>(sql).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<InventoryDetailDto>();
            }
        }

        public static double AlinanHizmetMasrafToplam(DateTime start, DateTime end, int firmNo)
        {
            try
            {
                var g = firmNo.ToString("D3");

                using (var db = new SqlConnection(UtilityHelper.LogoConnection))
                {
                    var sql = "SELECT ISNULL(ROUND(SUM(STLINE.LINENET / STLINE.REPORTRATE),2),0) " +
                    "FROM LG_" + g + "_01_STLINE STLINE " +
                    "INNER JOIN LG_" + g + "_01_INVOICE INVOICE  ON INVOICE.LOGICALREF=STLINE.INVOICEREF " +
                    "WHERE STLINE.LINETYPE = 4 AND  INVOICE.TRCODE = 4 AND INVOICE.DATE_ >= @start AND INVOICE.DATE_ <= @end AND STLINE.REPORTRATE <>0";

                    return db.Query<double>(sql, new { start, end }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static List<ExpenseDetailDto> AlinanHizmetMasrafList(DateTime start, DateTime end, int firmNo)
        {
            try
            {
                var g = firmNo.ToString("D3");

                using (var db = new SqlConnection(UtilityHelper.LogoConnection))
                {
                    var sql = "SELECT  " +

"STLINE.LOGICALREF as SatirId, "+
"INVOICE.FICHENO as FaturaNo, " +
"SRVCARD.CODE as HizmetKodu,  " +
"SRVCARD.DEFINITION_ as HizmetAdi, " +
"CLCARD.CODE as ChKodu, " +
"CLCARD.DEFINITION_ as ChUnvan, " +
"STLINE.AMOUNT as BirimMiktar, " +
"STLINE.PRICE as BirimFiyat, " +
"ROUND(STLINE.LINENET / STLINE.REPORTRATE, 2) as DovizliToplam, " +
"STLINE.REPORTRATE as DovizParite, " +
" QS_INVOICETRCODES.INVOICETRDEFINITION AS FaturaTuru, " +
"  INVOICE.DATE_ AS Tarih, " +
" INVOICE.DOCODE AS BelgeNo, " +
" INVOICE.GENEXP1 AS Aciklama1, " +
" INVOICE.GENEXP2 AS Aciklama2, " +
" INVOICE.GENEXP3 AS Aciklama3, " +
" INVOICE.GENEXP4 AS Aciklama4, " +
" STLINE.LINEEXP as SatirAciklama " +
                    "FROM LG_" + g + "_01_STLINE STLINE " +
                    "INNER JOIN LG_" + g + "_01_INVOICE INVOICE  ON INVOICE.LOGICALREF=STLINE.INVOICEREF " +
    "LEFT JOIN LG_" + g + "_SRVCARD SRVCARD  ON SRVCARD.LOGICALREF = STLINE.STOCKREF AND STLINE.LINETYPE IN(4,11) " +
    "LEFT JOIN QS_INVOICETRCODES QS_INVOICETRCODES  ON QS_INVOICETRCODES.INVOICETRCODE = INVOICE.TRCODE " +
    "LEFT JOIN LG_" + g + "_CLCARD CLCARD  ON CLCARD.LOGICALREF = INVOICE.CLIENTREF " +
                    "WHERE STLINE.LINETYPE IN (4,11) AND  INVOICE.TRCODE = 4 AND INVOICE.DATE_ >= @start AND INVOICE.DATE_ <= @end AND STLINE.REPORTRATE <>0";

                    return db.Query<ExpenseDetailDto>(sql, new { start, end }).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<ExpenseDetailDto>();
            }
        }

        public static List<BorcAlacakCariDto> BorcAlacakCariGrupList(int firmNo)
        {
            var g = firmNo.ToString("D3");
            try
            {
                using (var db = new SqlConnection(UtilityHelper.LogoConnection))
                {
                    var sql = "SELECT " +
"CLCARD.CODE AS 'ChKodu',  " +
"CLCARD.DEFINITION_ AS 'ChUnvani',  " +
"CLCARD.SPECODE2 AS 'OzelKod2',  " +
"CLCARD.COUNTRY AS 'Ulke',  " +
" " +
"SUM(CLFLINE.AMOUNT) AS 'Tutar',  " +
"SUM((CASE CLFLINE.SIGN WHEN 0 THEN CLFLINE.AMOUNT ELSE 0 END)) AS 'Borc',  " +
"SUM((CASE CLFLINE.SIGN WHEN 1 THEN CLFLINE.AMOUNT ELSE 0 END)) AS 'Alacak',  " +
"SUM((CASE CLFLINE.SIGN WHEN 1 THEN CLFLINE.AMOUNT * -1 ELSE CLFLINE.AMOUNT END)) AS 'Bakiye',  " +
" " +
"SUM((CASE CLFLINE.SIGN WHEN 0 THEN CLFLINE.TRNET ELSE 0 END)) AS 'IslemDoviziBorc',  " +
"SUM((CASE CLFLINE.SIGN WHEN 1 THEN CLFLINE.TRNET ELSE 0 END)) AS 'IslemDoviziAlacak',  " +
"SUM((CASE CLFLINE.SIGN WHEN 1 THEN CLFLINE.TRNET * -1 ELSE CLFLINE.TRNET END)) AS 'IslemDoviziBakiye',  " +
" " +
" " +
"SUM((CASE CLFLINE.SIGN WHEN 0 THEN CLFLINE.REPORTNET ELSE 0 END)) AS 'RaporlamaDovizBorc',  " +
"SUM((CASE CLFLINE.SIGN WHEN 1 THEN CLFLINE.REPORTNET ELSE 0 END)) AS 'RaporlamaDoviziAlacak',  " +
"SUM((CASE CLFLINE.SIGN WHEN 1 THEN CLFLINE.REPORTNET * -1 ELSE CLFLINE.REPORTNET END)) AS 'RaporlamaDoviziBakiyesi' " +
" " +
"FROM LG_" + g + "_01_CLFLINE CLFLINE " +
" " +
" LEFT JOIN LG_" + g + "_CLCARD CLCARD ON CLFLINE.CLIENTREF = CLCARD.LOGICALREF " +
" LEFT JOIN L_CAPIDIV L_CAPIDIV ON CLFLINE.BRANCH = L_CAPIDIV.NR AND L_CAPIDIV.FIRMNR = '" + g + "' " +
" LEFT JOIN L_CAPIDEPT L_CAPIDEPT ON CLFLINE.DEPARTMENT = L_CAPIDEPT.NR AND L_CAPIDEPT.FIRMNR = '" + g + "' " +
" LEFT JOIN L_TRADGRP L_TRADGRP ON CLFLINE.TRADINGGRP = L_TRADGRP.GCODE collate database_default " +
" LEFT JOIN LG_" + g + "_EMCENTER EMCENTER1 ON CLFLINE.CLCENTERREF = EMCENTER1.LOGICALREF " +
" LEFT JOIN LG_" + g + "_EMUHACC EMUHACC1 ON CLFLINE.CLACCREF = EMUHACC1.LOGICALREF " +
" LEFT JOIN LG_" + g + "_EMUHACC EMUHACC2 ON CLFLINE.CASHACCOUNTREF = EMUHACC2.LOGICALREF " +
" lEFT JOIN LG_" + g + "_EMCENTER EMCENTER2 ON CLFLINE.CASHCENTERREF = EMCENTER2.LOGICALREF " +
" LEFT JOIN L_CURRENCYLIST L_CURRENCYLIST ON L_CURRENCYLIST.CURTYPE = CLFLINE.TRCURR AND L_CURRENCYLIST.FIRMNR = '" + g + "' " +
" LEFT JOIN L_CAPIFIRM CAPIFIRM ON CAPIFIRM.LOCALCTYP = L_CURRENCYLIST.CURTYPE AND CAPIFIRM.NR = '" + g + "' " +
" LEFT JOIN LG_" + g + "_PAYPLANS PAYPLANS ON CLFLINE.PAYDEFREF = PAYPLANS.LOGICALREF " +
" LEFT JOIN LG_" + g + "_01_EMFICHE LG_" + g + "_01_EMFICHE ON CLFLINE.ACCFICHEREF = LG_" + g + "_01_EMFICHE.LOGICALREF " +
" LEFT JOIN LG_" + g + "_01_INVOICE INVOICE ON INVOICE.LOGICALREF = CLFLINE.SOURCEFREF AND CLFLINE.MODULENR = 4 AND FROMKASA = 0 " +
" LEFT JOIN LG_" + g + "_01_CSTRANS TRANS ON CLFLINE.SOURCEFREF = TRANS.LOGICALREF AND CLFLINE.MODULENR = 6 AND TRANS.LINENO_ = 1 " +
" LEFT JOIN LG_" + g + "_01_CSCARD CSCARD ON CSCARD.LOGICALREF = TRANS.CSREF " +
" LEFT JOIN LG_" + g + "_01_KSLINES KSLINES ON CLFLINE.SOURCEFREF = KSLINES.LOGICALREF AND CLFLINE.MODULENR = 10 " +
" LEFT JOIN LG_" + g + "_01_INVOICE P_INVOICE ON P_INVOICE.LOGICALREF = CLFLINE.SOURCEFREF AND CLFLINE.MODULENR IN(4) " +
" LEFT JOIN LG_" + g + "_01_CLFICHE P_CLFICHE ON P_CLFICHE.LOGICALREF = CLFLINE.SOURCEFREF AND CLFLINE.MODULENR IN(5) " +
" LEFT JOIN LG_" + g + "_01_CSROLL P_CSROLL ON P_CSROLL.LOGICALREF = CLFLINE.SOURCEFREF AND CLFLINE.MODULENR IN(6) " +
" LEFT JOIN LG_" + g + "_01_BNFLINE P_BNFLINE ON P_BNFLINE.LOGICALREF = CLFLINE.SOURCEFREF AND CLFLINE.MODULENR IN(7) " +
" LEFT JOIN LG_" + g + "_01_BNFICHE P_BNFICHE ON P_BNFICHE.LOGICALREF = P_BNFLINE.SOURCEFREF " +
" LEFT JOIN LG_" + g + "_PROJECT PROJECT ON " +
" PROJECT.LOGICALREF = (CASE " +
" WHEN CLFLINE.MODULENR IN(4) THEN P_INVOICE.PROJECTREF " +
"WHEN CLFLINE.MODULENR IN(5) THEN P_CLFICHE.PROJECTREF " +
"WHEN CLFLINE.MODULENR IN(6) THEN P_CSROLL.PROJECTREF " +
"WHEN CLFLINE.MODULENR IN(7) THEN P_BNFICHE.PROJECTREF " +
"WHEN CLFLINE.MODULENR IN(10) THEN CLFLINE.CLPRJREF " +
"ELSE 0 " +
" END) " +
" " +
" LEFT JOIN " +
" ( " +
" SELECT CARDREF AS CLIENTREF, " +
" CONVERT(VARCHAR, SIGN)+'_' + CONVERT(VARCHAR, modulenr) + '_' + CONVERT(VARCHAR, FICHEREF) FISREF,  " +
" CONVERT(DATETIME, SUM(CONVERT(INT, DATE_) * TOTAL) / SUM(TOTAL)) ORT_VADE " +
"    FROM LG_" + g + "_01_PAYTRANS " +
" GROUP BY CARDREF, CONVERT(VARCHAR, SIGN) + '_' + CONVERT(VARCHAR, modulenr) + '_' + CONVERT(VARCHAR, FICHEREF) " +
" ) AS ORT_VADELER ON ORT_VADELER.CLIENTREF = CLFLINE.CLIENTREF " +
" AND ORT_VADELER.FISREF = " +
" CONVERT(VARCHAR, CLFLINE.SIGN) + '_' + CONVERT(VARCHAR, CLFLINE.modulenr) + '_' + CONVERT(VARCHAR, CASE WHEN CLFLINE.MODULENR = 5 THEN CLFLINE.LOGICALREF ELSE CLFLINE.sourcefref END) " +
"WHERE(CLFLINE.MODULENR <> 4 OR(CLFLINE.MODULENR = 4 AND CLFLINE.SOURCEFREF NOT IN(SELECT LOGICALREF FROM LG_" + g + "_01_INVOICE WHERE FROMKASA = 1))) AND CLFLINE.DEVIR = 0  " +
"group by CLCARD.CODE, CLCARD.DEFINITION_,CLCARD.SPECODE2 ,CLCARD.COUNTRY ";

                    return db.Query<BorcAlacakCariDto>(sql).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<BorcAlacakCariDto>();
            }
        }

        public static double GetBankBalanceDynmaic(string where, int firmNo)
        {
            try
            {
                var g = firmNo.ToString("D3");

                using (var db = new SqlConnection(UtilityHelper.LogoConnection))
                {
                    var sql =
                        "SELECT SUM((CASE BNFLINE.SIGN WHEN 1 THEN ROUND(BNFLINE.AMOUNT /(CASE WHEN BNFLINE.REPORTRATE = 0 THEN 99999999999999 ELSE BNFLINE.REPORTRATE END),5) *-1 ELSE ROUND(BNFLINE.AMOUNT / (CASE WHEN BNFLINE.REPORTRATE = 0 THEN 99999999999999 ELSE BNFLINE.REPORTRATE END),5) END)) " +
                        "FROM LG_" + g + "_01_BNFLINE BNFLINE " +
                        "LEFT JOIN LG_" + g + "_BANKACC BANKACC ON BNFLINE.BNACCREF = BANKACC.LOGICALREF" +
                        " WHERE TransType = 1 " + where;
                    return db.Query<double>(sql).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public static List<BankaBakiyeDto> GetBankBalanceDynmaicDetailList(string where, int firmNo)
        {
            try
            {
                var g = firmNo.ToString("D3");

                using (var db = new SqlConnection(UtilityHelper.LogoConnection))
                {
                    var sql =
                        "SELECT BANKACC.CODE as BankaKodu,BANKACC.DEFINITION_ as BankaAdi , SUM((CASE BNFLINE.SIGN WHEN 1 THEN ROUND(BNFLINE.AMOUNT / (CASE WHEN BNFLINE.REPORTRATE = 0 THEN 99999999999999 ELSE BNFLINE.REPORTRATE END),5) *-1 " +
                        "ELSE ROUND(BNFLINE.AMOUNT / (CASE WHEN BNFLINE.REPORTRATE = 0 THEN 99999999999999 ELSE BNFLINE.REPORTRATE END),5) END)) as Bakiye " +
                        "FROM LG_" + g + "_01_BNFLINE BNFLINE " +
                        "LEFT JOIN LG_" + g + "_BANKACC BANKACC ON BNFLINE.BNACCREF = BANKACC.LOGICALREF " +
                        "WHERE TransType = 1 " + where + " GROUP BY BANKACC.CODE,BANKACC.DEFINITION_";
                    return db.Query<BankaBakiyeDto>(sql).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<BankaBakiyeDto>();
            }
        }

        public static double GetToplamBankaBakiyeTL(string where)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "SELECT SUM(Bakiye) FROM dbo.R_BankaHareketleri WHERE " + where;
                    return db.Query<double>(sql).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public static double GetToplamBankaBakiyeDiger(string where)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "SELECT SUM(IslemDoviziBakiye) FROM dbo.R_BankaHareketleri WHERE " + where;
                    return db.Query<double>(sql).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public static List<BankaBakiyeDto> GetBankaveBakiyelerGroupTL(string where)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select BankaKodu,BankaAdi,BankaHesapKodu, Sum(Bakiye) as Bakiye from R_BankaHareketleri where " +
                        where + " group by BankaKodu,BankaAdi,BankaHesapKodu";
                    return db.Query<BankaBakiyeDto>(sql).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<BankaBakiyeDto>();
            }
        }
        public static List<BankaBakiyeDto> GetBankaveBakiyelerGroupDiger(string where)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select BankaKodu,BankaAdi,BankaHesapKodu, Sum(IslemDoviziBakiye) as Bakiye from R_BankaHareketleri where " +
                        where + " group by BankaKodu,BankaAdi,BankaHesapKodu";
                    return db.Query<BankaBakiyeDto>(sql).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<BankaBakiyeDto>();
            }
        }

        public static List<BankaBakiyeDto> GetBankaveBakiyelerTL(string kodu, string where)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select BankaKodu,BankaAdi,BankaHesapKodu, Sum(Bakiye) as Bakiye from R_BankaHareketleri where BankaAdi = @kodu AND " + where + "  group by BankaKodu,BankaAdi,BankaHesapKodu";
                    return db.Query<BankaBakiyeDto>(sql, new { kodu }).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<BankaBakiyeDto>();
            }
        }
        public static List<BankaBakiyeDto> GetBankaveBakiyelerDiger(string kodu, string where)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select BankaKodu,BankaAdi,BankaHesapKodu, Sum(IslemDoviziBakiye) as Bakiye from R_BankaHareketleri where BankaAdi = @kodu AND " + where + "  group by BankaKodu,BankaAdi,BankaHesapKodu";
                    return db.Query<BankaBakiyeDto>(sql, new { kodu }).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<BankaBakiyeDto>();
            }
        }

        public static double GetToplamKasaBakiye()
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select Sum(Bakiye) as Bakiye from R_KasaHareketleri";
                    return db.Query<double>(sql).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public static List<KasaBakiyeDto> GetKasaBakiyeler()
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select KasaKodu,KasaAdi, Sum(Bakiye) as Bakiye from R_KasaHareketleri group by KasaKodu,KasaAdi";
                    return db.Query<KasaBakiyeDto>(sql).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<KasaBakiyeDto>();
            }
        }

        public static List<R_BorcAlacakTakip> GetBorcluCariler()
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select CariHesapAdi,CariHesapKod,Sum(Kalan) as Kalan from R_BorcAlacakTakip where BorcAlacak = 'Borc' And AcikKapali = 'Kapanmadi' group by CariHesapAdi,CariHesapKod";
                    return db.Query<R_BorcAlacakTakip>(sql).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<R_BorcAlacakTakip>();
            }
        }

        public static List<R_BorcAlacakTakip> GetAlacakCariler()
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select CariHesapAdi,CariHesapKod,Sum(Kalan) as Kalan from R_BorcAlacakTakip where BorcAlacak = 'Alacak' And AcikKapali = 'Kapanmadi' group by CariHesapAdi,CariHesapKod";
                    return db.Query<R_BorcAlacakTakip>(sql).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<R_BorcAlacakTakip>();
            }

        }

        public static double GetToplamCekBakiye(string doviz)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select SUM(TOTAL) as Toplam from R_CekSenetHareketleri where Durumu = 'Portfoyde' AND CsTuru  =  'Musteri Ceki' AND Doviz = @doviz";
                    return db.Query<double>(sql, new { doviz }).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public static double GetToplamSenetBakiye(string doviz)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select SUM(Tutar) as Toplam from R_CekSenetHareketleri where CsTuru = 'Musteri Senedi' AND Doviz = @doviz group by CsTuru";

                    return db.Query<double>(sql, new { doviz }).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public static List<R_BankaHareketleri> GetBankaveBakiyelerList(string hesapkodu, string where)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select * from R_BankaHareketleri where BankaHesapKodu = @hesapkodu AND " +
                        where;

                    return db.Query<R_BankaHareketleri>(sql, new { hesapkodu }).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<R_BankaHareketleri>();
            }
        }

        public static List<R_BankaHareketleri> GetBankaveBakiyelerListDateRange(DateTime start, DateTime end, int firmNo, string bankCode)
        {
            try
            {
                var g = firmNo.ToString("D3");

                using (var db = new SqlConnection(UtilityHelper.LogoConnection))
                {
                    var query = "SELECT " +
"BNFLINE.TRANSTYPE AS TransType, " +
"BNFLINE.TRCODE AS TrCode, " +
"BNFLINE.MODULENR AS ModuleNr,  " +
"PROJECT.CODE AS ProjeKoduGenel,  " +
"PROJECT.NAME AS ProjeAdiGenel,  " +
"PROJECT.SPECODE AS ProjeOzelKoduGenel,  " +
"PROJECT.CYPHCODE AS ProjeYetkiKoduGenel,  " +
"PROJECT.BEGDATE AS ProjeBaslangicTarihiGenel,  " +
"PROJECT.ENDDATE AS ProjeBitisTarihiGenel,  " +
"PROJECT.PRJRESPON AS ProjeSorumlusuGenel , " +
"PROJECT1.CODE AS 'ProjeKoduSatir',  " +
"PROJECT1.NAME AS 'ProjeAdiSatir',  " +
"PROJECT1.SPECODE AS 'ProjeOzelKoduSatir',  " +
"PROJECT1.CYPHCODE AS 'ProjeYetkiKoduSatir',  " +
"PROJECT1.BEGDATE AS 'ProjeBaslangicTarihiSatir',  " +
"PROJECT1.ENDDATE AS 'ProjeBitisTarihiSatir',  " +
"PROJECT1.PRJRESPON AS 'ProjeSorumlusuSatir' , " +
"BNFLINE.TRANSTYPE as HareketTuruKodu, " +
"BNFLINE.TRCODE AS FisTuruKodu, " +
"BNFLINE.MODULENR, " +
"BNCARD.CODE AS 'BankaKodu',  " +
"BNCARD.DEFINITION_ AS 'BankaAdi',  " +
"BANKACC.CODE AS 'BankaHesapKoduSatir', " +
"BANKACC.CARDTYPE, " +
"BNFLINE.TRCURR, " +
"BANKACC.DEFINITION_ AS 'BankaHesapAdiSatir',  " +
"'BankaHesabiTipiSatir' = " +
"CASE BANKACC.CARDTYPE WHEN 1 THEN 'Ticari Hesap' " +
" WHEN 2 THEN 'Kredi Hesabi' " +
" WHEN 3 THEN 'Dovizli Ticar' " +
" WHEN 4 THEN 'Dovizli Kredi' ELSE 'Tanimsiz' END, " +
" " +
"(CASE " +
"WHEN BANKACC.CARDTYPE IN(1,3) AND BNFLINE.TRANSTYPE = 1 THEN 'Cari Hesap' " +
"WHEN BANKACC.CARDTYPE IN(1,3) AND BNFLINE.TRANSTYPE = 2 THEN 'Tahsil Senetleri' " +
"WHEN BANKACC.CARDTYPE IN(1,3) AND BNFLINE.TRANSTYPE = 3 THEN 'Takas Cekleri' " +
"WHEN BANKACC.CARDTYPE IN(1,3) AND BNFLINE.TRANSTYPE = 4 THEN 'Kesilen Cekler' " +
"WHEN BANKACC.CARDTYPE IN(2,4) AND BNFLINE.TRANSTYPE = 1 THEN 'Teminat Senetleri' " +
"WHEN BANKACC.CARDTYPE IN(2,4) AND BNFLINE.TRANSTYPE = 2 THEN 'Teminat Cekleri' " +
"WHEN BANKACC.CARDTYPE IN(2,4) AND BNFLINE.TRANSTYPE = 3 THEN 'Senet Karsiligi Kredi' " +
"WHEN BANKACC.CARDTYPE IN(2,4) AND BNFLINE.TRANSTYPE = 4 THEN 'Cek Karsiligi Kredi' " +
"ELSE 'Diger' END) AS 'HareketTuruSatir', " +
"BANKACC.CODE AS 'BankaHesapKoduGenel', " +
"BANKACC.DEFINITION_ AS 'BankaHesapAdiGenel',  " +
"BANKACC.CODE AS 'BankaHesapKodu' , " +
"BANKACC.DEFINITION_ AS 'BankHesapAdi' ,  " +
"'BankaHesabiTipiGenel' = " +
"CASE BANKACC.CARDTYPE WHEN 1 THEN 'Ticari Hesap' " +
" WHEN 2 THEN 'Kredi Hesabi' " +
" WHEN 3 THEN 'Dovizli Ticari' " +
" WHEN 4 THEN 'Dovizli Kredi' ELSE 'Tanimsiz' END, " +
" " +
"'BankaHesabiTipi' = " +
"CASE BANKACC.CARDTYPE WHEN 1 THEN 'Ticari Hesap' " +
" WHEN 2 THEN 'Kredi Hesabi' " +
" WHEN 3 THEN 'Dovizli Ticari' " +
" WHEN 4 THEN 'Dovizli Kredi' ELSE 'Tanimsiz' END, " +
"(CASE " +
"WHEN BANKACC.CARDTYPE IN(1,3) AND BNFLINE.TRANSTYPE = 1 THEN 'Cari Hesap' " +
"WHEN BANKACC.CARDTYPE IN(1,3) AND BNFLINE.TRANSTYPE = 2 THEN 'Tahsil Senetleri' " +
"WHEN BANKACC.CARDTYPE IN(1,3) AND BNFLINE.TRANSTYPE = 3 THEN 'Takas Cekleri' " +
"WHEN BANKACC.CARDTYPE IN(1,3) AND BNFLINE.TRANSTYPE = 4 THEN 'Kesilen Cekler' " +
"WHEN BANKACC.CARDTYPE IN(2,4) AND BNFLINE.TRANSTYPE = 1 THEN 'Teminat Senetleri' " +
"WHEN BANKACC.CARDTYPE IN(2,4) AND BNFLINE.TRANSTYPE = 2 THEN 'Teminat Cekleri' " +
"WHEN BANKACC.CARDTYPE IN(2,4) AND BNFLINE.TRANSTYPE = 3 THEN 'Senet Karsiligi Kredi' " +
"WHEN BANKACC.CARDTYPE IN(2,4) AND BNFLINE.TRANSTYPE = 4 THEN 'Cek Karsiligi Kredi' " +
"ELSE 'Diger' END) AS 'HareketTuruGenel', " +
"(CASE " +
"WHEN BANKACC.CARDTYPE IN(1,3) AND BNFLINE.TRANSTYPE = 1 THEN 'Cari Hesap' " +
"WHEN BANKACC.CARDTYPE IN(1,3) AND BNFLINE.TRANSTYPE = 2 THEN 'Tahsil Senetleri' " +
"WHEN BANKACC.CARDTYPE IN(1,3) AND BNFLINE.TRANSTYPE = 3 THEN 'Takas Cekleri' " +
"WHEN BANKACC.CARDTYPE IN(1,3) AND BNFLINE.TRANSTYPE = 4 THEN 'Kesilen Cekler' " +
"WHEN BANKACC.CARDTYPE IN(2,4) AND BNFLINE.TRANSTYPE = 1 THEN 'Teminat Senetleri' " +
"WHEN BANKACC.CARDTYPE IN(2,4) AND BNFLINE.TRANSTYPE = 2 THEN 'Teminat Cekleri' " +
"WHEN BANKACC.CARDTYPE IN(2,4) AND BNFLINE.TRANSTYPE = 3 THEN 'Senet Karsiligi Kredi' " +
"WHEN BANKACC.CARDTYPE IN(2,4) AND BNFLINE.TRANSTYPE = 4 THEN 'Cek Karsiligi Kredi' " +
"ELSE 'Diger' END) AS 'HareketTuru' , " +
"'IslemTuru' = CASE BNFLINE.TRCODE " +
"   WHEN 1 THEN 'Banka Islem Fisi' " +
" WHEN 2 THEN 'Banka Virman Fisi' " +
" WHEN 3 THEN 'Gelen Havale/EFT' " +
" WHEN 4 THEN 'Gonderilen Havale/EFT' " +
" WHEN 5 THEN 'Banka Acilis Fisi' " +
" WHEN 10 THEN 'Cek Cikis (Banka Tahsil)' " +
" ELSE 'Tanimsiz' END,  " +
"BNFLINE.TRANNO AS 'IslemNo',  " +
"BNFLINE.DOCODE AS 'BelgeNo',  " +
" BNFLINE.DATE_ AS 'Tarih',  " +
"BNFLINE.LINEEXP AS 'SatirAciklamasi', " +
"CLCARD.CODE AS 'ChKodu',  " +
"CLCARD.DEFINITION_ AS 'ChUnvani',  " +
"EMUHACC.CODE AS 'MuhasebeHesapKodu',  " +
"EMCENTER.CODE AS 'MasrafMerkeziKodu',  " +
"L_CAPIDEPT.NAME AS 'Bolum', " +
"L_CAPIDEPT.nr AS 'BolumNo' ,  " +
"L_CAPIDIV.NAME AS 'Isyeri',  " +
"L_CAPIDIV.NR AS 'IsyeriNo' ,  " +
"BNFLINE.SPECODE AS 'OzelKod',  " +
"BNFLINE.CYPHCODE AS 'YetkiKodu',  " +
"BNFLINE.TRADINGGRP AS 'TicariIslemGrubu', " +
"(CASE BNFLINE.ACCOUNTED " +
" WHEN 0 THEN 'Muhasebelesmemis' " +
" WHEN 1 THEN 'Muhasebelesmis' " +
" ELSE 'Diger' END) AS 'Muhasebelesmismi',  " +
"BNFLINE.AMOUNT AS 'Tutar',  " +
"(CASE BNFLINE.SIGN WHEN 0 THEN ROUND(BNFLINE.AMOUNT / (CASE WHEN BNFLINE.REPORTRATE = 0 THEN 99999999999999 ELSE BNFLINE.REPORTRATE END),5) ELSE 0 END) AS 'Borc',  " +
"(CASE BNFLINE.SIGN WHEN 1 THEN ROUND(BNFLINE.AMOUNT / (CASE WHEN BNFLINE.REPORTRATE = 0 THEN 99999999999999 ELSE BNFLINE.REPORTRATE END),5) ELSE 0 END) AS 'Alacak',  " +
"(CASE BNFLINE.SIGN WHEN 1 THEN ROUND(BNFLINE.AMOUNT / (CASE WHEN BNFLINE.REPORTRATE = 0 THEN 99999999999999 ELSE BNFLINE.REPORTRATE END),5)*-1 ELSE ROUND(BNFLINE.AMOUNT / BNFLINE.REPORTRATE,5) END) AS 'Bakiye',  " +
" " +
"BNFLINE.REPORTRATE AS 'RdKuru', " +
"BNFLINE.REPORTNET AS 'RdTutari', " +
"'RdBorc' = CASE BNFLINE.SIGN WHEN 0 THEN BNFLINE.REPORTNET ELSE 0 END, " +
"'RdAlacak' = CASE BNFLINE.SIGN WHEN 1 THEN BNFLINE.REPORTNET ELSE 0 END, " +
"'RdBakiye' = CASE BNFLINE.SIGN WHEN 1 THEN BNFLINE.REPORTNET * -1 ELSE BNFLINE.REPORTNET END, " +
"    (CASE BNFLINE.TRCURR WHEN 0 THEN 'TL' ELSE L_CURRENCYLIST.CURCODE END) AS 'IslemDoviziTuru',  " +
"BNFLINE.TRRATE AS 'IslemDovizKuru',  " +
"BNFLINE.TRNET AS 'IslemDovizTutari',  " +

"(CASE BNFLINE.CANCELLED WHEN 0 THEN 'Iptal Edilmemis' ELSE 'Iptal Edilmis' END) AS 'Iptalmi',  " +
"'BankaHesapReferansi' = BNFLINE.BANKREFNR, " +
"'ProvizyonReferansi' = PROVISIONREF, " +
"'GcbNumarasi' = BNFLINE.CUSTOMDOCNR, " +
"'BankaIslemGrupNumarasi' = BNFLINE.TRANGRPNO, " +
"'BankaIslemGrupTarihi' = BNFLINE.TRANGRPDATE, " +
"BNFLINE.TRANGRPLINENO as BankaIslemGrupSatirNumarasi, " +
"BNFLINE.CLBNACCOUNTNO AS ChBankaHesapNo " +
"FROM LG_" + g + "_01_BNFLINE BNFLINE " +
" LEFT JOIN LG_" + g + "_BNCARD BNCARD ON BNFLINE.BANKREF = BNCARD.LOGICALREF " +
" LEFT JOIN LG_" + g + "_BANKACC BANKACC  ON BNFLINE.BNACCREF = BANKACC.LOGICALREF " +
" LEFT JOIN LG_" + g + "_01_BNFICHE BNFICHE  ON BNFLINE.SOURCEFREF = BNFICHE.LOGICALREF " +
" LEFT JOIN LG_" + g + "_BANKACC BANKACC1  ON BNFICHE.BNACCOUNTREF = BANKACC1.LOGICALREF " +
" LEFT JOIN LG_" + g + "_PROJECT PROJECT  ON BNFICHE.PROJECTREF = PROJECT.LOGICALREF " +
" LEFT JOIN LG_" + g + "_PROJECT PROJECT1  ON BNFLINE.PROJECTREF = PROJECT1.LOGICALREF " +
" LEFT JOIN LG_" + g + "_CLCARD CLCARD  ON BNFLINE.CLIENTREF = CLCARD.LOGICALREF " +
" LEFT JOIN LG_" + g + "_EMUHACC EMUHACC  ON BNFLINE.ACCOUNTREF = EMUHACC.LOGICALREF " +
" LEFT JOIN LG_" + g + "_EMCENTER EMCENTER  ON BNFLINE.CENTERREF = EMCENTER.LOGICALREF " +
" LEFT JOIN L_CAPIDEPT L_CAPIDEPT  ON BNFLINE.DEPARTMENT = L_CAPIDEPT.NR AND L_CAPIDEPT.FIRMNR = '" + g + "' " +
" LEFT JOIN L_CAPIDIV L_CAPIDIV  ON BNFLINE.BRANCH = L_CAPIDIV.NR AND L_CAPIDIV.FIRMNR = '" + g + "' " +
" LEFT JOIN L_CURRENCYLIST L_CURRENCYLIST  ON L_CURRENCYLIST.CURTYPE = BNFLINE.TRCURR AND L_CURRENCYLIST.FIRMNR = '" + g + "' " +
"WHERE TransType = 1 AND BNFLINE.DATE_ >= @start AND  BNFLINE.DATE_  <= @end AND BANKACC.CODE = @bankCode";

                    return db.Query<R_BankaHareketleri>(query, new { start, end, bankCode }).ToList();
                }
            }
            catch (Exception e)
            {
                throw e; 
            }
        }

        public static List<R_KasaHareketleri> GetKasaBakiyelerList(string kodu)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<R_KasaHareketleri>("SELECT * FROM R_KasaHareketleri WHERE KasaKodu = @kodu", new { kodu }).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<R_KasaHareketleri>();
            }

        }

        public static List<R_BorcAlacakTakip> GetBorcluCarilerList(string kodu)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql = "select * from R_BorcAlacakTakip where BorcAlacak = 'Borc' And AcikKapali = 'Kapanmadi' AND CariHesapKod = @kodu";
                    return db.Query<R_BorcAlacakTakip>(sql, new { kodu }).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<R_BorcAlacakTakip>();
            }
        }

        public static List<R_BorcAlacakTakip> GetAlacakCarilerList(string kodu)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql = "select * from R_BorcAlacakTakip where BorcAlacak = 'Alacak' And AcikKapali = 'Kapanmadi' AND CariHesapKod = @kodu";
                    return db.Query<R_BorcAlacakTakip>(sql, new { kodu }).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<R_BorcAlacakTakip>();
            }
        }

        public static List<R_CekSenetHareketleri> GetCeklerList(string turu, string durumu, string doviz)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select * from R_CekSenetHareketleri where CsTuru = @turu AND Durumu = @durumu AND Doviz = @doviz";
                    return db.Query<R_CekSenetHareketleri>(sql, new { turu, durumu, doviz }).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<R_CekSenetHareketleri>();
            }
        }
        public static List<R_CekSenetHareketleri> GetCeklerList(string turu, string durumu, string doviz, string yil, string ay)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select * from R_CekSenetHareketleri where CsTuru = @turu AND Durumu = @durumu AND Doviz = @doviz AND YEAR(VadeTarihi)= @yil AND MONTH(VadeTarihi) = @ay";
                    return db.Query<R_CekSenetHareketleri>(sql, new { turu, durumu, doviz, yil, ay }).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<R_CekSenetHareketleri>();
            }
        }

        public static List<CekSenetAyBazliTotalDto> GetSenetler(string doviz, string yil, string ay, string csTuru, string durumu)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select Kod, Aciklama, Sum(TOTAL) as Tutar from R_CekSenetHareketleri where CsTuru = @csTuru AND Doviz = @doviz AND Durumu = @durumu  AND YEAR(VadeTarihi) = @yil AND Month(VadeTarihi) = @ay group by Kod,Aciklama";
                    return db.Query<CekSenetAyBazliTotalDto>(sql, new { doviz, ay, yil, csTuru, durumu }).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<CekSenetAyBazliTotalDto>();
            }
        }

        public static List<CekSenetAyBazliTotalDto> GetSenetlerGenel(string doviz)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select CsTuru,Doviz,Durumu, Sum(TOTAL) as Tutar from R_CekSenetHareketleri where Doviz = @doviz group by CsTuru,Durumu,Doviz";
                    return db.Query<CekSenetAyBazliTotalDto>(sql, new { doviz }).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<CekSenetAyBazliTotalDto>();
            }
        }

        public static List<R_CekSenetHareketleri> GetSenetlerList(string doviz, string csTuru, string durumu, string yil, string ay)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select * from R_CekSenetHareketleri where CsTuru = @csTuru AND Durumu = @durumu AND Doviz = @doviz AND YEAR(VadeTarihi) = @yil AND Month(VadeTarihi) = @ay ";
                    return db.Query<R_CekSenetHareketleri>(sql, new { csTuru, durumu, doviz, yil, ay }).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<R_CekSenetHareketleri>();
            }
        }

        public static List<R_SatisFaturalari> GetUrunGrup()
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select StokGrupKodu, Sum(SatirNetTutari) as SatirNetTutari from R_SatisFaturalari group by StokGrupKodu order by StokGrupKodu";
                    return db.Query<R_SatisFaturalari>(sql).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<R_SatisFaturalari>();
            }
        }
        public static double GetUrunGrupToplam()
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select Sum(SatirNetTutari) as Toplam from R_SatisFaturalari";
                    return db.Query<double>(sql).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public static List<R_SatisFaturalari> GetUrunGrupList(string kodu)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select * from R_SatisFaturalari where StokGrupKodu = @kodu";
                    return db.Query<R_SatisFaturalari>(sql, new { kodu }).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<R_SatisFaturalari>();
            }
        }

        public static List<AyToplamDto> GetAyBazli()
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql = "select Month(Tarih) as Ay,Sum(SatirNetTutari) as Toplam from R_SatisFaturalari group by Month(Tarih)";
                    var list = db.Query<AyToplamDto>(sql).ToList();

                    foreach (var item in list)
                    {
                        item.AyAciklama = UtilityHelper.GetMonths().FirstOrDefault(a => a.id == item.Ay.ToString())
                            .text;
                    }

                    return list;
                }
            }
            catch (Exception e)
            {
                return new List<AyToplamDto>();
            }
        }

        public static List<R_SatisFaturalari> GetAyBazliList(int ay)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select * from R_SatisFaturalari where Month(Tarih) = @ay";
                    return db.Query<R_SatisFaturalari>(sql, new { ay }).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<R_SatisFaturalari>();
            }
        }

        public static List<SehirToplamDto> GetBolge()
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select Upper(ChSehir) as Sehir,Sum(SatirNetTutari) as Toplam from R_SatisFaturalari group by ChSehir";
                    return db.Query<SehirToplamDto>(sql).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<SehirToplamDto>();
            }
        }

        public static List<R_SatisFaturalari> GetBolgeList(string kodu)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select * from R_SatisFaturalari where ChSehir = @kodu";
                    return db.Query<R_SatisFaturalari>(sql, new { kodu }).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<R_SatisFaturalari>();
            }
        }

        public static List<SatisElemanToplamDto> GetSatisEleman()
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select SatisElemaniAdi as Adi,SatisElemaniKodu as Kodu,Sum(SatirNetTutari) as Toplam from R_SatisFaturalari group by SatisElemaniAdi,SatisElemaniKodu";
                    return db.Query<SatisElemanToplamDto>(sql).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<SatisElemanToplamDto>();
            }
        }

        public static List<R_SatisFaturalari> GetSatisElemanList(string satici, string sehir)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select * from R_SatisFaturalari where SatisElemaniKodu = @satici AND ChSehir = @sehir";
                    return db.Query<R_SatisFaturalari>(sql, new { satici, sehir }).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<R_SatisFaturalari>();
            }
        }

        public static List<SatisElemanToplamDto> GetSatisElemanBolgelerToplam(string kodu)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select ChSehir as Kodu,Sum(SatirNetTutari) as Toplam from R_SatisFaturalari where SatisElemaniKodu = @kodu group by ChSehir";
                    return db.Query<SatisElemanToplamDto>(sql, new { kodu }).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<SatisElemanToplamDto>();
            }
        }

        public static List<CariToplamDto> GetCariBazli()
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select CariHesapUnvani as Adi,CariHesapKodu as Kodu ,Sum(SatirNetTutari) as Toplam from R_SatisFaturalari group by CariHesapKodu,CariHesapUnvani";
                    return db.Query<CariToplamDto>(sql).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<CariToplamDto>();
            }
        }

        public static List<R_SatisFaturalari> GetCariBazliList(string kodu)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select * from R_SatisFaturalari where CariHesapKodu = @kodu";
                    return db.Query<R_SatisFaturalari>(sql, new { kodu }).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<R_SatisFaturalari>();
            }
        }

        public static List<R_Bankalar> GetBankalar()
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<R_Bankalar>("SELECT * FROM R_Bankalar").ToList();
                }
            }
            catch (Exception e)
            {
                return new List<R_Bankalar>();
            }
        }

        public static double GetToplamMusteri()
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select Sum(Kalan) as Kalan from R_BorcAlacakTakip where BorcAlacak = 'Borc' And AcikKapali = 'Kapanmadi' And [CariHesapKod] like '120.%'";
                    return db.Query<double>(sql).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public static double GetToplamTedarikci()
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select Sum(Kalan) as Kalan from R_BorcAlacakTakip where BorcAlacak = 'Borc' And AcikKapali = 'Kapanmadi' And [CariHesapKod] like '320.%'";
                    return db.Query<double>(sql).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public static List<R_BorcAlacakTakip> GetMusterilerList(string id)
        {

            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select CariHesapAdi,CariHesapKod,Sum(Kalan) as Kalan from R_BorcAlacakTakip where BorcAlacak = 'Borc' And AcikKapali = 'Kapanmadi' AND CariHesapKod like '" + id + ".%' group by CariHesapAdi,CariHesapKod";
                    return db.Query<R_BorcAlacakTakip>(sql).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<R_BorcAlacakTakip>();
            }

        }

        public static List<R_CariHesaplar> GetMusterilerAllList(string id)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select * from R_CariHesaplar where MusteriKodu like '" + id + ".%'";
                    return db.Query<R_CariHesaplar>(sql).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<R_CariHesaplar>();
            }
        }

        public static List<R_Malzemekartlari> GetMalzemeStoklar()
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select * from R_Malzemekartlari";
                    return db.Query<R_Malzemekartlari>(sql).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<R_Malzemekartlari>();
            }
        }

        public static List<R_MalzemeHareketleri> GetMalzemeHareket(string kodu)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select * from R_MalzemeHareketleri where [MalzemeKodu] = @kodu";
                    return db.Query<R_MalzemeHareketleri>(sql, new { kodu }).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<R_MalzemeHareketleri>();
            }
        }

        public static List<CekTutarGroupSumDto> CekToplamByCsTuruGroupBy(string doviz)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "SELECT CsTuru,Durumu,Doviz,COALESCE(SUM(TOTAL),0) AS Toplam FROM dbo.R_CekSenetHareketleri where Doviz = @doviz  GROUP BY CsTuru,Durumu,Doviz";
                    return db.Query<CekTutarGroupSumDto>(sql, new { doviz }).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<CekTutarGroupSumDto>();
            }
        }

        public static List<CekSenetAyBazliTotalDto> GetSenetlerAyBazli(string doviz, string csTuru, string durumu)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "select CsTuru,Doviz,Durumu,Month(VadeTarihi) as Ay, Year(VadeTarihi) as Yil, COALESCE( Sum(TOTAL),0) as Tutar" +
                        " from R_CekSenetHareketleri where Doviz = @doviz AND CsTuru = @csTuru AND Durumu = @durumu group by Month(VadeTarihi), Year(VadeTarihi), CsTuru,Durumu,Doviz";
                    return db.Query<CekSenetAyBazliTotalDto>(sql, new { doviz, csTuru, durumu }).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<CekSenetAyBazliTotalDto>();
            }
        }
    }
}
