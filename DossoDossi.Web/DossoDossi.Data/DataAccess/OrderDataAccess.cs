﻿using DossoDossi.Data.Entity;
using DossoDossi.Data.Helper;
using DossoDossi.Data.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.DataAccess
{
    public class OrderDataAccess
    {
        public static PagerResult<View_SiparisFisler> GetList(OrderFilter p)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    var pager = new Pager
                    {
                        CurrentPage = p.CurrentPage,
                        TakeCount = p.TakeCount,
                        Order = p.Order
                    };

                    return db.View_SiparisFislers.PagerDynamic(Filter(p), pager);
                }
            }
            catch (Exception ex)
            {
                return new PagerResult<View_SiparisFisler>
                {
                    Data = new List<View_SiparisFisler>(),
                    ErrorMessage = ex.Message,
                    StackTrace = ex.StackTrace
                };
            }
        }

        public static PagerResult<View_SatinAlmaFisler> GetPurchList(Pager pager)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_SatinAlmaFislers.PagerDynamic(FilterPurch(pager.Filter), pager);
                }
            }
            catch (Exception ex)
            {
                return new PagerResult<View_SatinAlmaFisler>
                {
                    Data = new List<View_SatinAlmaFisler>(),
                    ErrorMessage = ex.Message,
                    StackTrace = ex.StackTrace
                };
            }
        }



        public static PagerResult<View_SatisIrsaliyeHareketler> GetSalesDispList(Pager pager)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_SatisIrsaliyeHareketlers.PagerDynamic(FilterSatIrs(pager.Filter), pager);
                }
            }
            catch (Exception ex)
            {
                return new PagerResult<View_SatisIrsaliyeHareketler>
                {
                    Data = new List<View_SatisIrsaliyeHareketler>(),
                    ErrorMessage = ex.Message,
                    StackTrace = ex.StackTrace
                };
            }
        }

        private static Expression<Func<View_SatisIrsaliyeHareketler,bool>> FilterSatIrs(string filter)
        {
            var pred = PredicateBuilder.True<View_SatisIrsaliyeHareketler>();

            if (!filter.ToControl())
            {
                pred = pred.And(a => a.MusteriHesapKodu.Contains(filter) || a.MusteriHesapUnvani.Contains(filter) || a.MalzemeAdi.Contains(filter) || a.IrsaliyeNo.Contains(filter)|| a.MalzemeKodu.Contains(filter));
            }

            return pred;
        }


        public static List<View_SatinAlmaSiparisDetayli> GetPurchDetayListById(int id)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<View_SatinAlmaSiparisDetayli>("SELECT * FROM View_SatinAlmaSiparisDetayli WHERE FisId = @id", new { id }).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<View_SatinAlmaSiparisDetayli>();
            }
        }

        public static List<View_SiparisDetayli> GetDetayListById(int id)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<View_SiparisDetayli>("SELECT * FROM View_SiparisDetayli WHERE FisId = @id", new { id }).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<View_SiparisDetayli>();
            }
        }

        private static Expression<Func<View_SiparisFisler, bool>> Filter(OrderFilter f)
        {
            var pred = PredicateBuilder.True<View_SiparisFisler>();

            if (!f.Filter.ToControl())
            {
                pred = pred.And(a => a.CariKod.Contains(f.Filter) || a.CariUnvan.Contains(f.Filter) || a.BelgeNo.Contains(f.Filter) || a.FisNo.Contains(f.Filter));
            }

            if (f.Isyeri.HasValue && f.Isyeri.Value > 0)
            {
               pred = pred.And(a => a.IsyeriNo == f.Isyeri.Value);
            }

            if(!f.SaticiKodu.ToControl())
            {
                pred = pred.And(a => a.SaticiAdi.Contains(f.SaticiKodu) || a.Satici.Contains(f.SaticiKodu));
            }

            if(!f.CariAdi.ToControl())
            {
                pred = pred.And(a => a.CariKod.Contains(f.CariAdi) || a.CariUnvan.Contains(f.CariAdi));
            }

            if (!f.MalzemeAdi.ToControl())
            {
                var ids = ProductDataAccess.GetContainsByNamesIds(f.MalzemeAdi);

                if (!ids.Any())
                {
                    pred = pred.And(a => a.FisId == -1);
                }
                else
                {
                    pred = pred.And(a => ids.Contains(a.FisId));
                }
            }

            if (f.Start.HasValue)
            {
                pred = pred.And(a => a.FisTarih >= f.Start.Value);
            }

            if (f.End.HasValue)
            {
                pred = pred.And(a => a.FisTarih <= f.End.Value);
            }

            return pred;
        }

        public static List<View_SiparisDetayli> GetBekleyenSiparislerById(int id,int? isyeri)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    var q = db.View_SiparisDetaylis.Where(a => a.StokId == id && (a.Miktar - a.SevkedilenMiktar) > 0);

                    if (isyeri.HasValue && isyeri.Value> 0)
                    {
                        q = q.Where(a => a.IsyeriNo== isyeri.Value);
                    }

                    return q.ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<View_SiparisDetayli>();
            }
        }

        private static Expression<Func<View_SatinAlmaFisler, bool>> FilterPurch(string filter)
        {
            var pred = PredicateBuilder.True<View_SatinAlmaFisler>();

            if (!filter.ToControl())
            {
                pred = pred.And(a => a.CariKod.Contains(filter) || a.CariUnvan.Contains(filter) || a.BelgeNo.Contains(filter) || a.FisNo.Contains(filter));
            }

            return pred;
        }

        public static List<View_SiparisDetayli> GetBekleyenSiparisler(DateTime? date =null)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql = "SELECT * FROM View_SiparisDetayli WHERE KalanMiktar > 0";

                    if (date.HasValue)
                    {
                        sql = sql + " YEAR(FisTarih)  = YEAR(@date) AND MONTH(FisTarih)  = MONTH(@date) AND DAY(FisTarih) = DAY(@date)";
                    }

                    return db.Query<View_SiparisDetayli>(sql,new { date }).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<View_SiparisDetayli>();
            }
        }

        public static List<View_SiparisFisler> GetFislerByFisNos(List<string> ids)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql = "SELECT * FROM View_SiparisFisler WHERE FisNo In @ids";
                    return db.Query<View_SiparisFisler>(sql,new { ids }).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<View_SiparisFisler>();
            }
        }

        public static List<View_SatinAlmaFisler> GetSatinFislerByFisNos(List<string> ids)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql = "SELECT * FROM View_SatinAlmaFisler WHERE FisNo In @ids";
                    return db.Query<View_SatinAlmaFisler>(sql,new { ids }).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<View_SatinAlmaFisler>();
            }
        }

        public static List<View_SatinAlmaSiparisDetayli> GetBekleyenSatinAlmaSiparisler(DateTime? date)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql = "SELECT * FROM [View_SatinAlmaSiparisDetayli] WHERE [BekleyenMiktar] > 0";

                    if (date.HasValue)
                    {
                        sql = sql + " YEAR(Tarih)  = YEAR(@date) AND MONTH(Tarih)  = MONTH(@date) AND DAY(Tarih) = DAY(@date)";
                    }

                    return db.Query<View_SatinAlmaSiparisDetayli>(sql, new { date }).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<View_SatinAlmaSiparisDetayli>();
            }
        }

        public static List<View_SiparisDetayli> GetSiparislerByFisNos(List<string> fisNos)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql = "SELECT * FROM View_SiparisDetayli WHERE FisNo IN @fisNos";
                    return db.Query<View_SiparisDetayli>(sql, new { fisNos }).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<View_SiparisDetayli>();
            }
        }

        public static List<View_SatisIrsaliyeHareketler> GetIrsaliyeHareketByFisNo(string dispatchNo)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql = "SELECT * FROM View_SatisIrsaliyeHareketler WHERE [IrsaliyeNo]  = @no";
                    return db.Query<View_SatisIrsaliyeHareketler>(sql, new { no = dispatchNo }).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<View_SatisIrsaliyeHareketler>();
            }
        }

        public static List<View_SatinAlmaIrsaliyeDetayli> GetSatinalmaIrsaliyeHareketByFisNo(string dispatchNo)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql = "SELECT * FROM View_SatinAlmaIrsaliyeDetayli WHERE FisNo  = @no";
                    return db.Query<View_SatinAlmaIrsaliyeDetayli>(sql, new { no = dispatchNo }).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<View_SatinAlmaIrsaliyeDetayli>();
            }
        }

        public static View_SiparisFisler GetSiparisByFisId(int id)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql = "SELECT * FROM View_SiparisFisler WHERE FisId  = @id";
                    return db.Query<View_SiparisFisler>(sql, new { id}).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static View_SatinAlmaFisler GetSatinAlmaSiparisByFisId(int id)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql = "SELECT * FROM View_SatinAlmaFisler WHERE FisId  = @id";
                    return db.Query<View_SatinAlmaFisler>(sql, new { id }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static bool UpdateOrderAciklaam4(int fisId, string status)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql = "EXEC PROC_SiparisFisStatusUpdate  @Id = @fisId,@Status = @status";
                    db.Query(sql, new { fisId, status });
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static void XmlLog(string json, string title)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql = "INSERT INTO XmlLog(Id,Tarih,Json,Title)values(NEWID(),GetDATE(),@json,@title)";
                    db.Query(sql, new { json,title });
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
