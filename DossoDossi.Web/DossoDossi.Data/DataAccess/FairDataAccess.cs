﻿using Dapper;
using DossoDossi.Data.Entity;
using DossoDossi.Data.Helper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.DataAccess
{
  //public  class FairDataAccess
  //  {
  //      public static PagerResult GetList(Pager pager)
  //      {
  //          try
  //          {
  //              using (var db = new DBDataContext(UtilityHelper.Connection()))
  //              {
  //                  var data = db.Fairs.PagerDynamic(Filter(pager.Filter), pager);
  //                  return data;
  //              }
  //          }
  //          catch (Exception e)
  //          {
  //              return new PagerResult
  //              {
  //                  Total = 0,
  //                  Pager = pager,
  //              };
  //          }
  //      }
  //      private static Expression<Func<Fair, bool>> Filter(string filter)
  //      {


  //          var pred = PredicateBuilder.True<Fair>();


  //          if (!filter.ToControl())
  //          {
  //              pred = pred.And(x => x.Name.Contains(filter) );
  //          }
  //          pred = pred.And(a => a.IsDeleted == false);

  //          return pred;
  //      }
  //      public static ResponseMessage Create(Fair model)
  //      {
  //          try
  //          {
  //              using (var db = new DBDataContext(UtilityHelper.Connection()))
  //              {
  //                  db.Fairs.InsertOnSubmit(model);
  //                  db.SubmitChanges();
  //                  return new ResponseMessage
  //                  {
  //                      Status = true,
  //                      Message = "Ekleme başarılı"
  //                  };
  //              }
  //          }
  //          catch (Exception e)
  //          {
  //              return new ResponseMessage
  //              {
  //                  Message = "Hata : " + e.Message,
  //                  Code = e.StackTrace
  //              };
  //          }
  //      }
  //      public static Fair GetById(int id)
  //      {
  //          try
  //          {
  //              using (var db = new SqlConnection(UtilityHelper.Connection()))
  //              {
  //                  var sql = "select top 1 * from dbo.Fairs where id=@id and  IsDeleted=0 ";
  //                  return db.Query<Fair>(sql, new { id = id }).FirstOrDefault();
  //              }
  //          }
  //          catch (Exception e)
  //          {

  //              return null;
  //          }
  //      }
  //      public static ResponseMessage Update(Fair model)
  //      {
  //          try
  //          {
  //              using (var db = new DBDataContext(UtilityHelper.Connection()))
  //              {
  //                  var data = db.Fairs.FirstOrDefault(a => a.Id == model.Id);
  //                  data.Description = model.Description;
  //                  data.EndDate = model.EndDate;
  //                  data.Name = model.Name;
  //                  data.StartDate = model.StartDate;
  //                  data.UpdateDate = DateTime.Now;
  //                  db.SubmitChanges();
  //                  return new ResponseMessage
  //                  {
  //                      Status = true,
  //                      Message = "Başarıyla kayıt  başarılı"
  //                  };
  //              }

  //          }
  //          catch (Exception e)
  //          {

  //              return new ResponseMessage
  //              {
  //                  Status = false,
  //                  Message = "Bir hata oluştu.",
  //                  Code = e.StackTrace
  //              };
  //          }
  //      }

  //      public static bool Delete(int id)
  //      {
  //          try
  //          {
  //              using (var db = new DBDataContext(UtilityHelper.Connection()))
  //              {
  //                  var data = db.Fairs.FirstOrDefault(a => a.Id == id);
  //                  data.IsDeleted = true;
  //                  data.DeleteDate = DateTime.Now;
  //                  db.SubmitChanges();
  //                  return true;
  //              }
  //          }
  //          catch (Exception)
  //          {

  //              throw;
  //          }
  //      }
  //      public static ResponseMessage StatusOnchange(int id)
  //      {
  //          try
  //          {
  //              using (var db = new DBDataContext(UtilityHelper.Connection()))
  //              {
  //                  var data = db.Fairs.FirstOrDefault(a => a.Id == id);
  //                  if (data.Status == true)
  //                  {
  //                      data.Status = false;
  //                      db.SubmitChanges();
  //                      return new ResponseMessage
  //                      {
  //                          Status = true,
  //                          Message = "Başarıyla Durum Değiştirildi."
  //                      };
  //                  }
  //                  if (data.Status == false)
  //                  {
  //                      data.Status = true;
  //                      db.SubmitChanges();
  //                      return new ResponseMessage
  //                      {
  //                          Status = true,
  //                          Message = "Başarıyla Durum Değiştirildi."
  //                      };
  //                  }

  //                  return new ResponseMessage
  //                  {
  //                      Status = false,
  //                      Message = "Lütfen internetinizi kontrol ediniz."
  //                  };


  //              }
  //          }
  //          catch (Exception e)
  //          {
  //              return new ResponseMessage
  //              {
  //                  Status = false,
  //                  Message = "Bir hata oluştu.",
  //                  Code = e.StackTrace
  //              };
  //          }
  //      }

  //      #region stant metotları


  //      public static PagerResult FairstandGetList(Pager pager, int id)
  //      {
  //          try
  //          {
  //              using (var db = new DBDataContext(UtilityHelper.Connection()))
  //              {
  //                  var data = db.View_StandInAccountInFairInSuppliers.PagerDynamic(FairstandFilter(pager.Filter,id), pager);
  //                  return data;
  //              }
  //          }
  //          catch (Exception e)
  //          {
  //              return new PagerResult
  //              {
  //                  Total = 0,
  //                  Pager = pager,
  //              };
  //          }
  //      }
  //      private static Expression<Func<View_StandInAccountInFairInSupplier, bool>> FairstandFilter(string filter, int id)
  //      {


  //          var pred = PredicateBuilder.True<View_StandInAccountInFairInSupplier>();


  //          if (!filter.ToControl())
  //          {
  //              pred = pred.And(x => x.Name.Contains(filter)|| x.AccountName.Contains(filter)|| x.FairName.Contains(filter)|| x.SupplierName.Contains(filter));
  //          }
  //          if (!id.ToControl())
  //          {
  //              pred = pred.And(a => a.FairId == id);
  //          }
  //          pred = pred.And(a => a.IsDeleted == false);

  //          return pred;
  //      }
  //      public static ResponseMessage FairstandCreate(Stand model)
  //      {
  //          try
  //          {
  //              using (var db = new DBDataContext(UtilityHelper.Connection()))
  //              {
  //                  db.Stands.InsertOnSubmit(model);
  //                  db.SubmitChanges();
  //                  return new ResponseMessage
  //                  {
  //                      Status = true,
  //                      Message = "Ekleme başarılı"
  //                  };
  //              }
  //          }
  //          catch (Exception e)
  //          {
  //              return new ResponseMessage
  //              {
  //                  Message = "Hata : " + e.Message,
  //                  Code = e.StackTrace
  //              };
  //          }
  //      }
  //      public static View_StandInAccountInFairInSupplier FairstandGetById(int id)
  //      {
  //          try
  //          {
  //              using (var db = new SqlConnection(UtilityHelper.Connection()))
  //              {
  //                  var sql = "select top 1 * from View_StandInAccountInFairInSupplier where Id=@id and  IsDeleted=0 ";
  //                  return db.Query<View_StandInAccountInFairInSupplier>(sql, new { id = id }).FirstOrDefault();
  //              }
  //          }
  //          catch (Exception e)
  //          {

  //              return null;
  //          }
  //      }
  //      public static ResponseMessage FairstandUpdate(Stand model)
  //      {
  //          try
  //          {
  //              using (var db = new DBDataContext(UtilityHelper.Connection()))
  //              {
  //                  var data = db.Stands.FirstOrDefault(a => a.Id == model.Id);
  //                  data.AccountId = model.AccountId;
  //                  data.Description = model.Description;
  //                  data.FairId = model.FairId;
  //                  data.Name = model.Name;
  //                  data.SupplierId = model.SupplierId;
  //                  data.UpdateDate = DateTime.Now;
  //                  db.SubmitChanges();
  //                  return new ResponseMessage
  //                  {
  //                      Status = true,
  //                      Message = "Başarıyla kayıt  başarılı"
  //                  };
  //              }

  //          }
  //          catch (Exception e)
  //          {

  //              return new ResponseMessage
  //              {
  //                  Status = false,
  //                  Message = "Bir hata oluştu.",
  //                  Code = e.StackTrace
  //              };
  //          }
  //      }

  //      public static bool FairstandDelete(int id)
  //      {
  //          try
  //          {
  //              using (var db = new DBDataContext(UtilityHelper.Connection()))
  //              {
  //                  var data = db.Stands.FirstOrDefault(a => a.Id == id);
  //                  data.IsDeleted = true;
  //                  data.DeleteDate = DateTime.Now;
  //                  db.SubmitChanges();
  //                  return true;
  //              }
  //          }
  //          catch (Exception)
  //          {

  //              throw;
  //          }
  //      }
  //      public static ResponseMessage FairstandStatusOnchange(int id)
  //      {
  //          try
  //          {
  //              using (var db = new DBDataContext(UtilityHelper.Connection()))
  //              {
  //                  var data = db.Stands.FirstOrDefault(a => a.Id == id);
  //                  if (data.Status == true)
  //                  {
  //                      data.Status = false;
  //                      db.SubmitChanges();
  //                      return new ResponseMessage
  //                      {
  //                          Status = true,
  //                          Message = "Başarıyla Durum Değiştirildi."
  //                      };
  //                  }
  //                  if (data.Status == false)
  //                  {
  //                      data.Status = true;
  //                      db.SubmitChanges();
  //                      return new ResponseMessage
  //                      {
  //                          Status = true,
  //                          Message = "Başarıyla Durum Değiştirildi."
  //                      };
  //                  }

  //                  return new ResponseMessage
  //                  {
  //                      Status = false,
  //                      Message = "Lütfen internetinizi kontrol ediniz."
  //                  };


  //              }
  //          }
  //          catch (Exception e)
  //          {
  //              return new ResponseMessage
  //              {
  //                  Status = false,
  //                  Message = "Bir hata oluştu.",
  //                  Code = e.StackTrace
  //              };
  //          }
  //      }




  //      #endregion

  //      #region Ürün Stant İlişkisi



  //      public static List<View_ProductInStand> ProductsInStandsList(int id)
  //      {
  //          try
  //          {
  //              using (var db = new SqlConnection(UtilityHelper.Connection()))
  //              {
  //                  var sql = "select * from dbo.View_ProductInStand where StandId=@id and  IsDeleted=0";
  //                  return db.Query<View_ProductInStand>(sql, new { id = id }).ToList();
  //              }
  //          }
  //          catch (Exception e)
  //          {

  //              return null;
  //          }
  //      }

  //      public static ResponseMessage StandInProductsInsert(List<StandInProduct> items)
  //      {
  //          try
  //          {
  //              using (var db = new DBDataContext(UtilityHelper.Connection()))
  //              {
  //                  foreach (var item in items)
  //                  {
                       
  //                      var data = new StandInProduct
  //                      {
  //                          CreataeDate = DateTime.Now,
  //                          Currency=item.Currency,
  //                           FairId=item.FairId,
  //                           Price=item.Price,
  //                           ProductId=item.ProductId,
  //                           Qty=item.Qty,
  //                           StandId=item.StandId,
  //                           IsDeleted=false,
  //                           Status=true,
  //                           UQ=Guid.NewGuid(),
  //                           SuplierId=item.SuplierId
  //                      };
  //                      db.StandInProducts.InsertOnSubmit(data);
  //                      db.SubmitChanges();
  //                  }
               
  //                  return new ResponseMessage
  //                  {
  //                      Status = true,
  //                      Message = "Ekleme başarılı"
  //                  };
  //              }
  //          }
  //          catch (Exception e)
  //          {
  //              return new ResponseMessage
  //              {
  //                  Message = "Hata : " + e.Message,
  //                  Code = e.StackTrace
  //              };
  //          }
  //      }

  //      public static void StandInProductDelete(int id)
  //      {
  //          try
  //          {
  //              using (var db = new SqlConnection(UtilityHelper.Connection()))
  //              {
  //                  var sql = "delete from StandInProducts where StandId=@id";
  //                  db.Query(sql, new { id = id });
  //              }
  //          }
  //          catch (Exception e)
  //          {

  //          }
  //      }
    
  //      #endregion

  //  }
}
