﻿using DossoDossi.Data.Entity;
using DossoDossi.Data.Helper;
using DossoDossi.Data.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.DataAccess
{
    public class OffersDataAccess
    {

        public static PagerResult<View_Offer> GetList(OfferPagerDto pager)
        {
            try
            {
                var p = new Pager
                {
                    CurrentPage = pager.CurrentPage,
                    Order = pager.Order,
                    TakeCount = pager.TakeCount
                };

                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_Offers.PagerDynamic(Filter(pager), p);
                }
            }
            catch (Exception ex)
            {
                return new PagerResult<View_Offer>
                {
                    ErrorMessage = ex.Message,
                    StackTrace = ex.StackTrace,
                    Data = new List<View_Offer>()
                };
            }
        }

        public static List<OfferStatusDto> GetAllStatus()
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<OfferStatusDto>("select * from OfferStatus").ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<OfferStatusDto>();
            }
        }

        private static Expression<Func<View_Offer, bool>> Filter(OfferPagerDto f)
        {
            var pred = PredicateBuilder.True<View_Offer>();


            if (!f.Filter.ToControl())
            {
                pred = pred.And(a => a.CariKodu.Contains(f.Filter) || a.CariUnvan.Contains(f.Filter) || a.TeklifNo.Contains(f.Filter));
            }

            if (f.CreateUserId.HasValue)
            {
                pred = pred.And(a => a.CreateUserId == f.CreateUserId.Value);
            }

            //if (f.UserParentId.HasValue)
            //{
            //    pred = pred.And(a => a.UserParentId == f.UserParentId.Value);
            //}

            if (f.Status.HasValue)
            {
                pred = pred.And(a => a.Status == f.Status.Value);
            }

            if (!f.StatusMultiple.ToControl())
            {
                var ids = f.StatusMultiple.Split(',').Select(x => int.Parse(x)).ToList();
                pred = pred.And(x => ids.Contains(x.Status));
            }

            if (!f.TeklifNo.ToControl())
            {
                pred = pred.And(a => a.TeklifNo.Contains(f.TeklifNo));
            }

            if (!f.CustomerName.ToControl())
            {
                pred = pred.And(a => a.CariKodu.Contains(f.CustomerName) || a.CariUnvan.Contains(f.CustomerName));
            }

            if (f.IsYeri.HasValue && f.IsYeri.Value > 0)
            {
                var i = f.IsYeri.Value.ToString();
                pred = pred.And(a => a.Isyeri == i);
            }

            if (f.Start.HasValue)
            {
                pred = pred.And(a => a.Tarih >= f.Start.Value);
            }

            if (f.End.HasValue)
            {
                pred = pred.And(a => a.Tarih <= f.End.Value);
            }
            return pred;
        }

        public static List<OfferItem> GetOfferItemListByMusteriIdToday(Guid musteriId)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql = "EXEC PROC_GetMusteriOfferItemList @MusteriId = @id";
                    return db.Query<OfferItem>(sql, new { id = musteriId }).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<OfferItem>();
            }
        }

        public static List<OfferReserveListDto> GetReserveStockList(int id)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql = "EXEC PROC_OfferReserveList @StokId = @id";
                    return db.Query<OfferReserveListDto>(sql, new { id }).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<OfferReserveListDto>();
            }
        }

        public static Offer AnyOfferMusteri(Guid musteriId, string projeKodu)
        {
            try
            {
                var t = DateTime.Today;
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.Offers.FirstOrDefault(a => a.MusteriId == musteriId && a.ProjeKodu == projeKodu && a.Tarih.Year ==t.Year 
                    && a.Tarih.Month == t.Month 
                    && a.Tarih.Day == t.Day);
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static double GetReserveStockCount(int id)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql = "EXEC PROC_OfferReserveCount @StokId = @id";
                    return db.Query<double>(sql, new { id }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }



        public static OfferCalculateGeneralDto CalculateGeneral(List<OfferCalculateDto> items, double? kargoBedeli = null)
        {
            try
            {
                if (items == null || !items.Any())
                {
                    return new OfferCalculateGeneralDto
                    {
                        Items = new List<OfferCalculateDto>(),
                        TotalAmountStr = "0",
                        TotalDiscountStr = "0",
                        TotalNetStr = "0",
                        TotalShipment = null
                    };
                }

                foreach (var item in items)
                {
                    var amnt = item.Qty * item.Price;

                    item.TotalAmount = amnt;
                    item.TotalAmountStr = amnt.ToString("N2");

                    var discT = Math.Round(amnt * (item.Discount / 100), 2);

                    item.DiscountTotal = discT;

                    var araToplam = item.TotalAmount - discT;

                    item.VatTotal = araToplam * (item.Vat / 100);
                    item.VatTotalStr = item.VatTotal.ToString("N2");

                    item.TotalNet = araToplam + item.VatTotal;

                    item.TotalNetStr = item.TotalNet.ToString("N2");

                }

                var res = new OfferCalculateGeneralDto
                {
                    TotalNet = items.Sum(a => a.TotalNet),
                    TotalAmount = items.Sum(a => a.TotalAmount),
                    Items = items,
                    VatTotal = items.Sum(a => a.VatTotal)
                };

                res.TotalDiscount = items.Sum(a => a.DiscountTotal);

                res.TotalAmountStr = res.TotalAmount.ToString("N2");
                res.TotalDiscountStr = res.TotalDiscount.ToString("N2");

                res.Currency = SettingDataAccess.GetValByCode("Offer.DefaultCurrency");
                res.VatTotalStr = res.VatTotal.ToString("N2");

                res.TotalShipment = kargoBedeli;

                if (kargoBedeli.HasValue && kargoBedeli.Value > 0)
                {
                    res.TotalNet = res.TotalNet + kargoBedeli.Value;
                }

                res.TotalNetStr = res.TotalNet.ToString("N2");
                return res;
            }
            catch (Exception ex)
            {
                return new OfferCalculateGeneralDto
                {
                    Items = new List<OfferCalculateDto>(),
                    TotalAmountStr = "0",
                    TotalDiscountStr = "0",
                    TotalNetStr = "0"
                };
            }
        }

        public static Offer AnyOfferToday(string projeKodu, Guid musteriId)
        {
            try
            {
                var t = DateTime.Today;

                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.Offers.FirstOrDefault(a => a.MusteriId == musteriId && a.ProjeKodu == projeKodu && a.Tarih.Year == t.Year && a.Tarih.Month == t.Month && a.Tarih.Day == t.Day);
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static bool UpsertItems(Offer anyOffer, List<OfferItem> items)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    items.ForEach(a => a.OfferId = anyOffer.Id);
                    items.ForEach(a => a.UQ = Guid.NewGuid());

                    db.OfferItems.InsertAllOnSubmit(items);

                    db.SubmitChanges();

                    UpdateTotalAmount(anyOffer.Id);
                }
                return true;
                }
            catch (Exception ex)
            {
                return false;
            }
        }

        private static void UpdateTotalAmount(int id)
        {
            try
            {
                using(var db = new SqlConnection(UtilityHelper.Connection))
                {
                    db.Query("EXEC PROC_UpdateOfferTotalAmount @Id = @id", new { id });
                }
                }
            catch (Exception ex)
            {

            }
        }

        public static View_Offer GetOfferByFisId(int id)
        {
            try
            {

                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_Offers.FirstOrDefault(a => a.SiparisFisId == id);
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static View_Offer GetById(int id)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<View_Offer>("SELECT TOP 1 * FROM View_Offers WHERE Id = @id", new { id }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }



        public static List<OfferItem> GetItems(int id)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<OfferItem>("SELECT  * FROM OfferItems WHERE OfferId = @id", new { id }).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<OfferItem>();
            }
        }


        public static View_Offer GetByUQ(Guid id)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<View_Offer>("SELECT TOP 1 * FROM View_Offers WHERE UQ = @id", new { id }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static object UpdateStatus(int id, int status)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    db.Query("update Offers set Status = @status where Id = @id", new { id, status });
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool CreateItems(Offer anyOffer, OfferItem addItem)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    addItem.UQ = Guid.NewGuid();

                    db.OfferItems.InsertOnSubmit(addItem);

                    db.SubmitChanges();
                }

                UpdateTotalAmount(anyOffer.Id);

                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static void UpdateFisId(int id, string code)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    db.Query("update Offers set Status = @status, [SiparisFisId] = @code where Id = @id", new { id, status = 4, code });

                    //rezerve kapatılır.
                    db.Query("update OfferItems SET IsReserved = 0 where OfferId = @id AND IsReserved = 1", new { id });
                }
            }
            catch (Exception ex)
            {
                //
            }
        }

        public static bool Delete(int id, int userId)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    db.Query("update Offers set DeleteDate = GetDATE(),[DeleteUserId] = @userId where Id = @id", new { id, userId });
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private static void UpdateOfferNo(int id)
        {
            try
            {
                var prefix = SettingDataAccess.GetValByCode("Offer.NoPrefix");

                var code = id.ToString("D5");

                var no = (prefix ?? "") + code;

                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    db.Query("update Offers set TeklifNo = @code where Id = @id", new { id, code = no });
                }
            }
            catch (Exception)
            {
                //
            }
        }
        public static ResponseMessage Create(Offer offer, List<OfferItem> items)
        {

            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    offer.CreateUserId = offer.CreateUserId;
                    offer.UQ = Guid.NewGuid();
                    offer.TotalAmount = Math.Round(offer.TotalAmount ?? 0, 2);
                    offer.TotalNet = Math.Round(offer.TotalNet ?? 0, 2);

                    offer.EklenmeTarihi = DateTime.Now;

                    //if (userData.ParentId.HasValue)
                    //{
                    //    var parent = AccountDataAccess.GetById(userData.ParentId.Value);
                    //    if (parent != null)
                    //    {
                    //        offer.AcceptUserId = parent.Id;
                    //    }
                    //}

                    db.Offers.InsertOnSubmit(offer);
                    db.SubmitChanges();

                    items.ForEach(a => a.OfferId = offer.Id);
                    items.ForEach(a => a.UQ = Guid.NewGuid());

                    db.OfferItems.InsertAllOnSubmit(items);
                    db.SubmitChanges();

                    UpdateOfferNo(offer.Id);

                    return new ResponseMessage
                    {
                        Status = true,
                        Message = "Başarıyla kaydedildi",
                        Data = offer.Id
                    };
                }
            }
            catch (Exception ex)
            {
                return new ResponseMessage
                {
                    Message = "ERROR : " + ex.Message,
                    Code = ex.StackTrace,
                    Status = false,
                    Type = "Offer_Create"
                };
            }
        }

        public static ResponseMessage Edit(Offer offer, List<OfferItem> items, List<int> deleteItems)
        {
            var user = SessionHelper.User;
            var userData = AccountDataAccess.GetById(user.UserId);
            try
            {

                using (var db = new DBDataContext(UtilityHelper.Connection))
                {

                    var d = db.Offers.FirstOrDefault(a => a.Id == offer.Id);

                    d.Isyeri = offer.Isyeri;
                    d.MusteriSipNo = offer.MusteriSipNo;
                    d.SaticiKodu = offer.SaticiKodu;
                    d.SevkiyatAciklama = offer.SevkiyatAciklama;
                    d.SevkKodu = offer.SevkKodu;
                    d.Status = offer.Status;
                    d.SiparisFisId = offer.SiparisFisId;
                    d.Tarih = offer.Tarih;
                    d.TotalAmount = offer.TotalAmount;
                    d.TotalDiscount = offer.TotalDiscount;
                    d.TotalNet = offer.TotalNet;
                    d.VadeKodu = offer.VadeKodu;
                    d.CariKodu = offer.CariKodu;
                    d.CariUnvan = offer.CariUnvan;
                    d.Aciklama = offer.Aciklama;
                    d.VatTotal = offer.VatTotal;


                    db.SubmitChanges();

                    if (deleteItems != null && deleteItems.Any())
                    {
                        DeleteItems(deleteItems);
                    }


                    items.ForEach(a => a.OfferId = offer.Id);

                    if (items.Any(c => c.Id == 0))
                    {
                        items.ForEach(a => a.UQ = Guid.NewGuid());
                        var adItems = items.Where(x => x.Id == 0).ToList();
                        db.OfferItems.InsertAllOnSubmit(adItems);
                        db.SubmitChanges();
                    }

                    if (items.Any(x => x.Id > 0))
                    {
                        var adItems = items.Where(x => x.Id > 0).ToList();
                        var edtIds = adItems.Select(a => a.Id).ToList();

                        var editItems = db.OfferItems.Where(a => edtIds.Contains(a.Id)).ToList();

                        foreach (var item in editItems)
                        {
                            var i = items.FirstOrDefault(a => a.Id == item.Id);

                            item.FrmNo = i.Id.ToString();
                            item.IsReserved = i.IsReserved;
                            item.ReserveDate = i.ReserveDate;
                            item.ProductId = i.ProductId;
                            item.ProductCode = i.ProductCode;
                            item.ProductName = i.ProductName;
                            item.Qty = i.Qty;
                            item.Price = i.Price;
                            item.UnitCode = i.UnitCode;
                            item.Note = i.Note;
                            item.SatirId = null;
                            item.Discount = i.Discount;
                            item.CustomerPrCode = i.CustomerPrCode;
                            item.Vat = i.Vat ?? 0;
                        }

                        db.SubmitChanges();

                    }

                    return new ResponseMessage
                    {
                        Status = true,
                        Message = "Succesfully Saved",
                        Data = offer.Id
                    };
                }
            }
            catch (Exception ex)
            {
                return new ResponseMessage
                {
                    Message = "ERROR : " + ex.Message,
                    Code = ex.StackTrace,
                    Status = false,
                    Type = "Offer_Update"
                };
            }
        }


        public static void DeleteItems(List<int> ids)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    db.Query("DELETE FROM OfferItems WHERE Id IN @ids", new { ids });
                }
            }
            catch (Exception ex)
            {

            }
        }

        public static void UpdateFileDdemandItemByNo(int offerId, string frmNo, string path)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "UPDATE OfferItems set [FilePath] = Coalesce(FilePath,'') + ','+ @path WHERE OfferId = @offerId AND FrmNo = @frmNo";
                    db.Query(sql, new { offerId, frmNo, path });
                }
            }
            catch (Exception e)
            {
            }
        }
    }
}
