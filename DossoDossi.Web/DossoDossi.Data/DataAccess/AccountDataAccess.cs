﻿using DossoDossi.Data.Entity;
using DossoDossi.Data.Helper;
using Dapper;


using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;


namespace DossoDossi.Data.DataAccess
{
    public class AccountDataAccess
    {
        public static Account Login(string username, string password)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    var data = db.Accounts.Where(a => a.UserName == username && a.Password == password && a.Status == true).FirstOrDefault();
                    return data;
                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public static Account GetById(int id)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    var data = db.Accounts.Where(a => a.Id == id).FirstOrDefault();
                    return data;
                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public static List<string> GetRoleCodes(int id)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<string>("SELECT RoleCode FROM AccountInRoles where AccountId = @id", new { id }).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<string>();
            }
        }

        public static Account GetByUQ(string id)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<Account>("SELECT TOP 1 * FROM Account where QU = @id", new { id }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static List<Account> All()
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    var data = db.Accounts.Where(a=>a.Status ==true).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {

                return new List<Account>();
            }
        }

        public static bool Insert(Account model)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {

                    db.Accounts.InsertOnSubmit(model);
                    db.SubmitChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool Update(Account model)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    var data = db.Accounts.FirstOrDefault(a=>a.Id ==model.Id);

                    data.DepartmanNameCode = model.DepartmanNameCode;
                    data.Email = model.Email;
                    data.Name = model.Name;
                    data.ParentId = model.ParentId;
                    data.Password = model.Password;
                    data.Phone = model.Phone;
                    data.Type = model.Type;
                    data.UserName = model.UserName;
                    data.Bolum = model.Bolum;
                    data.Isyeri = model.Isyeri;
                    data.SalesManCode = model.SalesManCode;
                    data.ProjeKodu = model.ProjeKodu;
                   
                    db.SubmitChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }
        public static bool Active(int id)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    var data = db.Accounts.Where(a => a.Id == id).FirstOrDefault();
                    if (data.Status == true)
                    {
                        data.Status = false;
                    }
                    else
                    {
                        data.Status = true;
                    }

                    db.SubmitChanges();
                    return true;

                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public static void InsertRoles(int id, List<string> arr)
        {
            try
            {
                if(arr==null || !arr.Any())
                {
                    return;
                    //
                }
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    db.Query("DELETE FROM AccountInRoles where AccountId = @id", new { id });
                }

                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    foreach (var item in arr)
                    {
                        db.Query("INSERT INTO AccountInRoles(Id,RoleCode,AccountId) values(NEWID(),@item,@id)", new { id, item });
                    }
                }
            }
            catch (Exception ex)
            {
                //
            }
        }

        public static void DeleteRoles(int id)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    db.Query("DELETE FROM AccountInRoles where AccountId = @id", new { id });
                }
            }
            catch (Exception ex)
            {
            }
        }

        public static bool Delete(int id)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    var data = db.Accounts.FirstOrDefault(a=>a.Id==id);
                    db.Accounts.DeleteOnSubmit(data);
                    db.SubmitChanges();
                    return true;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static List<Role> GetAllRoles()
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<Role>("SELECT * FROM Roles").ToList();
                }
            }
            catch (Exception)
            {
                return new List<Role>();
            }
        }






    }

}
