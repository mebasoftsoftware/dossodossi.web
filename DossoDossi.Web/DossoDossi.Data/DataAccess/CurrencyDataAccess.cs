﻿using DossoDossi.Data.Entity;
using DossoDossi.Data.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.DataAccess
{
   public class CurrencyDataAccess
    {
        private static List<View_Kurlar> GetCurrencyToday()
        {
            try
            {
                var today = DateTime.Today;

                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_Kurlars.Where(a => a.TARIH == today).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<View_Kurlar>();
            }
        }
        public static List<View_Kurlar> GetCurrency()
        {
            var list = GetCurrencyToday();

            if (list.Any())
            {
                return list;
            }

            TcmbDataAccess.Run();

            return GetCurrencyToday();
        }
    }
}
