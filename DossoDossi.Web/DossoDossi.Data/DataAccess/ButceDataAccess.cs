﻿using Dapper;
using DossoDossi.Data.Entity;
using DossoDossi.Data.Helper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.DataAccess
{
    public class ButceDataAccess
    {

        public static PagerResult<Butceler> GetList(Pager pager, int? yil)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.Butcelers.PagerDynamic(Filter(pager.Filter, yil), pager);
                }
            }
            catch (Exception e)
            {
                return new PagerResult<Butceler>();
            }
        }

        private static Expression<Func<Butceler, bool>> Filter(string filter, int? yil)
        {
            var pred = PredicateBuilder.True<Butceler>();

            if (!filter.ToControl())
            {
                pred = pred.And(a =>
                    a.HesapKodu.Contains(filter) || a.HesapAdi.Contains(filter) || a.HesapAdiEn.Contains(filter) || a.MasrafMerkeziKodu.Contains(filter));
            }

            if (yil.HasValue)
            {
                pred = pred.And(a => a.Yil == yil.Value);
            }

            return pred;
        }

        public static bool Upsert(Butceler model)
        {

            var id = ButceDataAccess.AnyBeforeAdded(model);
            if (id.HasValue && id.Value != Guid.Empty)
            {
                model.Id = id.Value;
                Update(model);
            }
            else
            {
                Create(model);
            }

            return true;
        }

        public static Guid? AnyBeforeAdded(Butceler model)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    var any = db.Butcelers.Where(a => a.Yil == model.Yil && a.HesapKodu == model.HesapKodu && a.MasrafMerkeziKodu == model.MasrafMerkeziKodu && a.Proje == model.Proje && a.Urun == model.Urun).Select(a => a.Id).FirstOrDefault();

                    return any;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public static ResponseMessage Create(Butceler model)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    model.Id = Guid.NewGuid();
                    model.Proje = model.Proje.ToControl() ? "0" : model.Proje;
                    model.Urun = model.Urun.ToControl() ? "0" : model.Urun;

                    db.Butcelers.InsertOnSubmit(model);
                    db.SubmitChanges();
                }

                return new ResponseMessage { Status = true, Message = "OK" };
            }
            catch (Exception e)
            {
                return new ResponseMessage { Status = false, Message = e.Message };
            }
        }

        public static ResponseMessage Update(Butceler model)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    var d = db.Butcelers.FirstOrDefault(a => a.Id == model.Id);

                    d.Yil = model.Yil;
                    d.HesapKodu = model.HesapKodu;
                    d.MasrafMerkeziKodu = model.MasrafMerkeziKodu;

                    d.Proje = model.Proje.ToControl() ? "0" : model.Proje;
                    d.Urun = model.Urun.ToControl() ? "0" : model.Urun;
                    d.HesapAdi = model.HesapAdi;
                    d.HesapAdiEn = model.HesapAdiEn;
                    d.Aciklama = model.Aciklama;
                    d.Ocak = model.Ocak;
                    d.Subat = model.Subat;
                    d.Mart = model.Mart;
                    d.Nisan = model.Nisan;
                    d.Mayis = model.Mayis;
                    d.Haziran = model.Haziran;
                    d.Temmuz = model.Temmuz;
                    d.Agustos = model.Agustos;
                    d.Eylul = model.Eylul;
                    d.Ekim = model.Ekim;
                    d.Kasim = model.Kasim;
                    d.Aralik = model.Aralik;


                    d.Tip = model.Tip;

                    db.SubmitChanges();
                }

                return new ResponseMessage { Status = true, Message = "OK" };
            }
            catch (Exception e)
            {
                return new ResponseMessage { Status = false, Message = e.Message };
            }
        }

        public static bool Delete(Guid id)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    var d = db.Butcelers.FirstOrDefault(a => a.Id == id);
                    db.Butcelers.DeleteOnSubmit(d);
                    db.SubmitChanges();
                }

                return true;

            }
            catch (Exception e)
            {
                return false;
            }

        }


        public static Butceler GetByMasrafYil(string masraf, int yil, string proje, string urun, string hesapKod, Guid? butceId)
        {
            try
            {

                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    Butceler d;

                    if (butceId.HasValue)
                    {
                        return db.Query<Butceler>("select top 1 * from Butceler WHERE Id = @butceId", new { butceId })
                            .FirstOrDefault();
                    }


                    proje = proje.ToControl() ? "0" : proje;
                    urun = urun.ToControl() ? "0" : urun;

                    var sql =
                        "select top 1 * from Butceler WHERE MasrafMerkeziKodu = @masraf AND COALESCE(Proje,'0') = @proje AND Yil = @yil AND COALESCE(Urun,'0') = @urun AND [HesapKodu] = @hesapKod ";

                    d = db.Query<Butceler>(sql, new { yil, masraf, hesapKod, proje, urun }).FirstOrDefault();


                    return d;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }



        private static decimal GetButceTutariById(Butceler d, int ay)
        {
            if (d == null) return 0M;

            return d.Ocak + d.Subat + d.Mart + d.Nisan + d.Mayis + d.Haziran + d.Temmuz + d.Agustos + d.Eylul + d.Ekim +
                   d.Kasim + d.Aralik;

            switch (ay)
            {
                case 1:
                    return d.Ocak;

                case 2:
                    return d.Subat;

                case 3:
                    return d.Mart;

                case 4:
                    return d.Nisan;

                case 5:
                    return d.Mayis;

                case 6:
                    return d.Haziran;

                case 7:
                    return d.Temmuz;

                case 8:
                    return d.Agustos;

                case 9:
                    return d.Eylul;

                case 10:
                    return d.Ekim;

                case 11:
                    return d.Kasim;
                case 12:
                    return d.Aralik;
                default:
                    return 0M;
            }

        }

        public static Butceler GetById(Guid id)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    var d = db.Butcelers.FirstOrDefault(a => a.Id == id);
                    return d;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        //  private  static  decimal BekliyorToplam

        //public static decimal GerceklesenGetir(string hesapKodu)
        //{
        //    try
        //    {
        //        using (var db = new SqlConnection(UtilityHelper.Connection))
        //        {
        //            var sql = "EXEC PROC_GetTotalGerceklesenByHesapKodu @Kod = @hesapKodu";
        //            return db.Query<decimal>(sql, new { hesapKodu }).FirstOrDefault();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return 0M;
        //    }
        //}

        public static decimal GerceklesenGetir(int yil, string hesapKodu, string masrafMerkezi, string proje, string urun)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql = "EXEC PROC_GetTotalGerceklesenByHesapla @Yil = @yil, @Kod = @hesapKodu, @Masraf = @masrafMerkezi , @Proje = @proje, @Urun = @urun";
                    return db.Query<decimal>(sql, new { yil, hesapKodu, masrafMerkezi, proje, urun }).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                return 0M;
            }
        }

        public static decimal OnayBekleyenToplam(string costCode, int yil, int ay, string proje, string urun, string hesapKod)
        {
            try
            {
                proje = proje.ToControl() ? "0" : proje;
                urun = urun.ToControl() ? "0" : urun;

                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql = "EXEC PROC_GetTalepOnayBekleyenToplam @CostCode = @costCode, @Yil = @yil, @Proje = @proje, @Urun = @urun, @HesapKod = @hesapKod";
                    var list = db.Query<decimal>(sql, new { costCode, yil, ay, proje, urun, hesapKod }).ToList();
                    return list.Sum(x => x);
                }
            }
            catch (Exception e)
            {
                return 0M;
            }
        }

        public static List<string> GetAllByButcesiOlanList()
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql = "select [MasrafMerkeziKodu] from [View_MasrafMerkezleri]";
                    return db.Query<string>(sql).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<string>();
            }
        }

        public static PagerResult<View_MasrafMerkezleri> LogoMasrafMerkezGetList(Pager pager)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_MasrafMerkezleris.PagerDynamic(FilterMasraf(pager.Filter), pager);
                }
            }
            catch (Exception e)
            {
                return new PagerResult<View_MasrafMerkezleri>();
            }
        }

        private static Expression<Func<View_MasrafMerkezleri, bool>> FilterMasraf(string filter)
        {
            var pred = PredicateBuilder.True<View_MasrafMerkezleri>();

            if (!filter.ToControl())
            {
                pred = pred.And(x =>
                    x.MasrafMerkeziKodu.Contains(filter) || x.MasrafMerkeziAciklamasi.Contains(filter));
            }

            return pred;
        }

        public static List<View_MuhasebeKodlari> GetMuhasebeKodlari()
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_MuhasebeKodlaris.ToList();
                }
            }
            catch (Exception e)
            {
                return new List<View_MuhasebeKodlari>();
            }
        }

        public static List<View_MasrafMerkezleri> MasrafMerkeziList()
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_MasrafMerkezleris.ToList();
                }
            }
            catch (Exception e)
            {
                return new List<View_MasrafMerkezleri>();
            }
        }

        public static List<View_Projeler> ProjeList()
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_Projelers.ToList();
                }
            }
            catch (Exception e)
            {
                return new List<View_Projeler>();
            }
        }

        //public static List<View_Urunler> UrunList()
        //{
        //    try
        //    {
        //        using (var db = new DBDataContext(UtilityHelper.Connection))
        //        {
        //            return db.View_Urunlers.ToList();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return new List<View_Urunler>();
        //    }
        //}

        public static List<Butceler> ButceList(int yil, string cost, string proje, string urun, string hesapKodu)
        {
            try
            {
                proje = proje.ToControl() ? "-1" : proje;
                urun = urun.ToControl() ? "-1" : urun;

                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql = "select * from Butceler where Yil = @yil " +
                              (cost == "-1" ? "" : " AND  MasrafMerkeziKodu = @cost ") +
                              (proje == "-1" ? "" : " AND Proje = @proje ") +
                              (urun == "-1" ? "" : " AND Urun = @urun ") +
                              (!hesapKodu.ToControl() ? " AND HesapKodu = @hesapKodu " : "");

                    return db.Query<Butceler>(sql, new { yil, cost, proje, urun, hesapKodu }).ToList();
                }

            }
            catch (Exception e)
            {
                return new List<Butceler>();
            }
        }

        public static List<View_Gerceklesenler> GerceklesenlerGetir(int yil, int? ay, string cost, string proje, string urun, string hesapKodu)
        {
            try
            {
                urun = urun.ToControl() ? "-1" : urun;
                proje = proje.ToControl() ? "-1" : urun;


                var result = new List<View_Gerceklesenler>();

                //mevcut yıl nisan ve aralıka kadar
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql = "select * from View_Gerceklesenler where Ay > 3 AND Yil = @yil" +
                              (cost == "-1" ? "" : " AND  MasrafMerkeziKodu = @cost ") +
                              (proje == "-1" ? "" : " AND ProjeKodu = @proje ") +
                              (urun == "-1" ? "" : " AND Urun = @urun ") +
                              (!hesapKodu.ToControl() ? " AND MuhHesabiKodu = @hesapKodu " : "")
                        ;

                    var list = db.Query<View_Gerceklesenler>(sql, new { yil, cost, proje, urun, hesapKodu }).ToList();

                    if (list.Any())
                    {
                        foreach (var item in list)
                        {
                            if (item.ProjeKodu.ToControl())
                            {
                                item.ProjeKodu = "0";
                            }

                            if (item.Urun.ToControl())
                            {
                                item.Urun = "0";
                            }
                        }

                        result.AddRange(list);
                    }

                }

                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql = "select * from View_Gerceklesenler where Ay < 4 AND Yil = @yil" +
                              (cost == "-1" ? "" : " AND  MasrafMerkeziKodu = @cost ") +
                              (proje == "-1" ? "" : " AND ProjeKodu = @proje ") +
                              (urun == "-1" ? "" : " AND Urun = @urun ") +
                              (!hesapKodu.ToControl() ? " AND MuhHesabiKodu = @hesapKodu " : "")
                        ;

                    var list = db.Query<View_Gerceklesenler>(sql, new { yil = yil + 1, cost, proje, urun, hesapKodu }).ToList();

                    if (list.Any())
                    {
                        foreach (var item in list)
                        {
                            if (item.ProjeKodu.ToControl())
                            {
                                item.ProjeKodu = "0";
                            }

                            if (item.Urun.ToControl())
                            {
                                item.Urun = "0";
                            }
                        }

                        result.AddRange(list);
                    }
                }


                return result;


            }
            catch (Exception e)
            {
                return new List<View_Gerceklesenler>();
            }
        }

        public static List<View_Malzemeler> GetHizmetKodlari()
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<View_Malzemeler>("EXEC PROC_ButceMuhHesapKodlariGetir").ToList();
                }
            }
            catch (Exception e)
            {
                return new List<View_Malzemeler>();
            }
        }

        public static List<string> GetSpecialCostCodes()
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<string>("SELECT Code FROM CostSpecialList").ToList();
                }
            }
            catch (Exception e)
            {
                return new List<string>();
            }
        }
    }
}
