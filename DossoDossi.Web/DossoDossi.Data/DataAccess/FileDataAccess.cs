﻿using DossoDossi.Data.Helper;
using DossoDossi.Data.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.DataAccess
{
  public  class FileDataAccess
    {
        public static List<DossoDossiFileDto> GetFiles(int tableId, string recordId)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<DossoDossiFileDto>
                        ("SELECT * FROM CrmFiles WHERE TableId = @tableId AND RecordId = @recordId", 
                        new { tableId,recordId})
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<DossoDossiFileDto>();
            }
        }

        public static bool Delete(Guid id)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    db.Query("DELETE FROM CrmFiles WHERE Id = @id",new { id });
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool Create(DossoDossiFileDto model)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    db.Query("INSERT INTO CrmFiles(Id,TableId, RecordId, FileName, FilePath, CreatedOn)" +
                        " VALUES(NEWID(),@TableId,@RecordId,@FileName,@FilePath,GETDATE())",
                        new { 
                            model.TableId, 
                            model.RecordId, 
                            model.FileName,
                            model.FilePath 
                        });
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
