﻿using DossoDossi.Data.Helper;
using DossoDossi.Data.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace DossoDossi.Data.DataAccess
{
    public  class DashBoardDataAccess
    {
        public static List<HomeCountReportDto> HomeTillReport()
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var id = SessionHelper.User.UserId;

                    return db.Query<HomeCountReportDto>("EXEC dbo.PROC_GetHomeIndexTill @Id = @id", new { id }).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<HomeCountReportDto>();
            }
        }

        public static List<HomeCountReportDto> HomeTillReportIsyeri(int isyeri, string code)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var id = SessionHelper.User.UserId;

                    return db.Query<HomeCountReportDto>("EXEC dbo.PROC_GetHomeIndexTillUser @Isyeri = @isyeri, @SalesmanCode = @code", new { isyeri,code }).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<HomeCountReportDto>();
            }
        }
    }
}
