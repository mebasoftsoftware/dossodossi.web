﻿using DossoDossi.Data.Entity;
using DossoDossi.Data.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.DataAccess
{
  public  class LogoDataAccess
    {
        public static List<View_Ambarlar> GetAmbarlar()
        {
            try
            {
                using(var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_Ambarlars.ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<View_Ambarlar>();
            }
        }

        public static List<View_Bolumler> GetBolumler()
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_Bolumlers.ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<View_Bolumler>();
            }
        }

        public static List<View_TasiyiciKodlar> TasiyiciKodlar()
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_TasiyiciKodlars.ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<View_TasiyiciKodlar>();
            }
        }

        public static List<View_CariSevkAdresler> GetCariSevkAdres(string code)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_CariSevkAdreslers.Where(a=>a.CARI_KODU == code).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<View_CariSevkAdresler>();
            }
        }

        public static List<View_Ulkeler> GetCountry()
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_Ulkelers.ToList();;
                }
            }
            catch (Exception ex)
            {
                return new List<View_Ulkeler>();
            }
        }

        public static List<View_SaticiKodlar> GetSaticiKodlar()
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_SaticiKodlars.ToList(); ;
                }
            }
            catch (Exception ex)
            {
                return new List<View_SaticiKodlar>();
            }
        }

        public static List<View_CariSevkAdresler> GetCariSevkAdres(int cariId)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_CariSevkAdreslers.Where(a => a.CARI_LREF == cariId).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<View_CariSevkAdresler>();
            }
        }

        public static List<View_Isyeri> GetIsyeri()
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_Isyeris.ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<View_Isyeri>();
            }
        }

        public static List<View_OdemePlanlari> VadeList()
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_OdemePlanlaris.ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<View_OdemePlanlari>();
            }
        }
    }
}
