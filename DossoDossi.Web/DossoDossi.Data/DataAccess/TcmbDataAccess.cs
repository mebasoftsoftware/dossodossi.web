﻿using DossoDossi.Data.Helper;

using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Xml;
using DossoDossi.Data.Models;

namespace DossoDossi.Data.DataAccess
{
   public class TcmbDataAccess
    {
        public static void Run()
        {
            try
            {
                var listCurCode = GetCurs();

                var contains = SettingDataAccess.GetValByCode("Tcmb.Currency.List").Split(',').Select(x => short.Parse(x)).ToList();

                listCurCode = listCurCode.Where(x => contains.Contains(x.Type)).ToList();

                XmlTextReader xmlTextReader = new XmlTextReader("http://www.tcmb.gov.tr/kurlar/today.xml");
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load((XmlReader)xmlTextReader);

                string str = "";

                DateTime simdi = DateTime.Today;
                long num = (long)(simdi.Day + 256 * simdi.Month + 65536 * simdi.Year);

                foreach (var item in listCurCode)
                {
                    try
                    {
                        str = item.Code;
                        xmlDocument.SelectSingleNode("/Tarih_Date/@Tarih");
                        XmlNode xmlNode1 = xmlDocument.SelectSingleNode(string.Format("Tarih_Date/Currency[@Kod='{0}']", (object)item.Code));
                        short? currType = new short?((short)0);

                        if (xmlNode1 != null)
                        {
                            currType = item.Type;

                            xmlDocument.SelectSingleNode(string.Format("Tarih_Date/Currency[@Kod='{0}']/Isim", (object)item.Code));
                            XmlNode xmlNode2 = xmlDocument.SelectSingleNode(string.Format("Tarih_Date/Currency[@Kod='{0}']/@Kod", (object)item.Code));
                            XmlNode xmlNode3 = xmlDocument.SelectSingleNode(string.Format("Tarih_Date/Currency[@Kod='{0}']/ForexBuying", (object)item.Code));
                            XmlNode xmlNode4 = xmlDocument.SelectSingleNode(string.Format("Tarih_Date/Currency[@Kod='{0}']/ForexSelling", (object)item.Code));
                            XmlNode xmlNode5 = xmlDocument.SelectSingleNode(string.Format("Tarih_Date/Currency[@Kod='{0}']/BanknoteBuying", (object)item.Code));
                            XmlNode xmlNode6 = xmlDocument.SelectSingleNode(string.Format("Tarih_Date/Currency[@Kod='{0}']/BanknoteSelling", (object)item.Code));

                            //efektif alış
                            double rates4 = !(xmlNode2.Value != "JPY") ? GetConvertedValue<double>((object)xmlNode5.InnerText.Replace('.', ',')) / 100.0 : GetConvertedValue<double>((object)xmlNode5.InnerText.Replace('.', ','));

                            //efektif satış
                            double rates1 = !(xmlNode2.Value != "JPY") ? GetConvertedValue<double>((object)xmlNode6.InnerText.Replace('.', ',')) / 100.0 : GetConvertedValue<double>((object)xmlNode6.InnerText.Replace('.', ','));

                            //döviz alış
                            double rates3 = !(xmlNode2.Value != "JPY") ? GetConvertedValue<double>((object)xmlNode3.InnerText.Replace('.', ',')) / 100.0 : GetConvertedValue<double>((object)xmlNode3.InnerText.Replace('.', ','));

                            //döviz satış
                            double rates2 = !(xmlNode2.Value != "JPY") ? GetConvertedValue<double>((object)xmlNode4.InnerText.Replace('.', ',')) / 100.0 : GetConvertedValue<double>((object)xmlNode4.InnerText.Replace('.', ','));

                            int date_int = (int)num;

                            AddDoviz(simdi, currType, rates1, rates2, rates3, rates4, item.Code);
                        }
                        else
                        {

                        }

                    }
                    catch (Exception e)
                    {
                    }

                }


            }
            catch (Exception e)
            {

            }

        }

        private static bool AddDoviz(DateTime date, short? type, double rates1, double rates2, double rates3, double rates4, string kurKodu)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var getIdSql = "select top 1 Id from GunlukKurlar WHERE TARIH = @date AND KurNo = @type";
                    var dt = db.Query<int>(getIdSql, new { date, type }).FirstOrDefault();

                    if (dt > 0)
                    {
                        //update

                        var upssql =
                            "UPDATE GunlukKurlar Kur1= @rates1 ,Kur2= @rates2 ,Kur3 = @rates3 ,Kur4 = @rates4  WHERE Id = @id";

                        db.Query(upssql, new { id = dt, date, type, rates1, rates2, rates3, rates4, edate = DateTime.Today });
                    }
                    else
                    {
                        //insert 
                        var iisql =
                            "insert into GunlukKurlar (TARIH,KurNo,Kur1,Kur2,Kur3,Kur4,KurKodu) values(@date,@type,@rates1,@rates2,@rates3,@rates4,@kurKodu)";

                        db.Query(iisql, new { date, type, rates1, rates2, rates3, rates4, kurKodu });
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                return true;
            }
        }


        public static T GetConvertedValue<T>(object value)
        {
            object convertedValue = ConvertValue<T>(value, null);
            if (convertedValue != null)
                return (T)convertedValue;

            return Activator.CreateInstance<T>();

        }

        private static object ConvertValue<T>(object value, object defaultValue)
        {
            if (value == null)
                return defaultValue;
            try
            {
                if (value is IConvertible)
                    return Convert.ChangeType(value, typeof(T), new CultureInfo("tr-TR"));
                else
                    return Convert.ChangeType(value, typeof(T));
            }

            catch (Exception e)
            {
                return defaultValue;
            }
        }

        private static List<CurListType> GetCurs()
        {
            return new List<CurListType>
            {
                new CurListType
                {
                    Code = "USD",
                    Type = 1
                },
                new CurListType
                {
                    Code = "EUR",
                    Type = 20
                },
                new CurListType
                {
                    Code = "GBP",
                    Type = 17
                },
                new CurListType
                {
                    Code = "DEM",
                    Type = 2
                },
                new CurListType
                {
                    Code = "AUD",
                    Type = 3
                },
                new CurListType
                {
                    Code = "DKK",
                    Type = 6
                },
                new CurListType
                {
                    Code = "SEK",
                    Type = 10
                },
                new CurListType
                {
                    Code = "CHF",
                    Type = 11
                },
                new CurListType
                {
                    Code = "JPY",
                    Type = 13
                },

                new CurListType
                {
                    Code = "CAD",
                    Type = 14
                },
                new CurListType
                {
                    Code = "KWD",
                    Type = 15
                },
                new CurListType
                {
                    Code = "CNY",
                    Type = 25
                },
            };
        }
    }
}
