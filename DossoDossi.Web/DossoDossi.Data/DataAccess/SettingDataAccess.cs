﻿using DossoDossi.Data.Entity;
using DossoDossi.Data.Helper;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace DossoDossi.Data.DataAccess
{
    public class SettingDataAccess
    {
        public static string GetValByCode(string code)
        {
            try
            {
                using(var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<string>("SELECT TOP 1 Value FROM Settings WHERE Code = @code", new { code }).FirstOrDefault();
                }
            }
            catch (System.Exception ex)
            {
                return null;
            }
        }

        public static List<Setting> GetAllList()
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<Setting>("SELECT * FROM Settings").ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<Setting>();
            }
        }

        public static bool UpdateValue(int id, string val)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                     db.Query<Setting>("UPDATE Settings SET Value = @val WHERE Id = @id",new { id, val });
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
