﻿using Dapper;
using DossoDossi.Data.Entity;
using DossoDossi.Data.Helper;
using DossoDossi.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace DossoDossi.Data.DataAccess
{
   public class ProjeDataAccess
    {
        public static PagerResult<View_Projeler> GetList(Pager pager)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_Projelers.PagerDynamic(Filter(pager.Filter), pager);
                }
            }
            catch (Exception ex)
            {
                return new PagerResult<View_Projeler>
                {
                    Data = new List<View_Projeler>(),
                    Pager = pager
                };
            }
        }

      

        private static Expression<Func<View_Projeler,bool>> Filter(string filter)
        {
            var pred = PredicateBuilder.True<View_Projeler>();

            if (!filter.ToControl())
            {
                pred = pred.And(a => a.CODE.Contains(filter) || a.NAME.Contains(filter));
            }

            return pred;
        }

        public static View_Projeler GetById(int id)
        {
            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_Projelers.Where(a => a.LOGICALREF == id).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static List<View_ProjeMusteriler> GetProjeMusteriler(string code)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<View_ProjeMusteriler>("SELECT * FROM View_ProjeMusteriler WHERE ProjeKodu = @code", new { code }).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<View_ProjeMusteriler>();
            }
        }

        public static bool UpdateMusteriSicil(int id,string val)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    db.Query("UPDATE Musteriler SET SicilNo = @val,Code= @val WHERE Id = @id", new { id, val });
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static List<ProjectTedarikListDto> GetProjeTedarikciler(string code)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<ProjectTedarikListDto>("SELECT" +
                        " p.*, " +
                        "(SELECT COUNT(*) FROM ProjeTedarikciUrunler as u where u.PTrId = p.Id AND u.ProjeKodu = p.ProjeKodu) as Adet " +
                        "FROM ProjeTedarikciler as p WHERE ProjeKodu = @code", new { code }).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<ProjectTedarikListDto>();
            }
        }

     

        public static List<ProjeTedarikciUrunler> GetTedarikciUrunler(string code, Guid id)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<ProjeTedarikciUrunler>("SELECT * FROM ProjeTedarikciUrunler as p " +
                        " WHERE p.[PTrId] = @id And p.ProjeKodu = @code ", new { code, id }).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<ProjeTedarikciUrunler>();
            }
        }

        public static List<View_Projeler> GetActriveProjelers()
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<View_Projeler>("SELECT * FROM [dbo].[View_Projeler]  where ACTIVE = 0 And BEGDATE <= GetDate() And ENDDATE >=GETDATE()").ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<View_Projeler>();
            }
        }
        public static List<View_Projeler> GetTumProjeler()
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<View_Projeler>("SELECT * FROM [dbo].[View_Projeler] ").ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<View_Projeler>();
            }
        }

        public static List<ProjeTedarikciUrunler> GetFuarProducts(string code)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<ProjeTedarikciUrunler>("SELECT * FROM [dbo].[ProjeTedarikciUrunler] where ProjeKodu = @code",new { code}).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<ProjeTedarikciUrunler>();
            }
        }
        public static List<ProjeTedarikciUrunler> GetFuarProductsByCari(Guid? ptrId, string code)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<ProjeTedarikciUrunler>("SELECT * FROM [dbo].[ProjeTedarikciUrunler] where ProjeKodu = @code AND PTrId = @ptrId", new { code, ptrId }).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<ProjeTedarikciUrunler>();
            }
        }

        public static Musteriler GetMusteriByProje(string projeKodu, string userNo,bool onlineCheck)
        {
            
            try
            {
                Musteriler data = null;
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                     data =  db.Query<Musteriler>("SELECT TOP 1 " +
                        " m.* FROM [dbo].[Musteriler] as m  " +
                        "inner join ProjeMusteriler as p " +
                        "ON m.MusteriId = p.MusteriId where m.SicilNo = @userNo And p.ProjeKodu = @projeKodu"
                        , new { projeKodu, userNo }).FirstOrDefault();
                }

                if (data != null)
                {
                    return data;
                }

                //online kontrol etsin mi
                if (onlineCheck)
                {
                    data = OnlineUpsert(projeKodu, userNo);
                }

                return data;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private static Musteriler OnlineUpsert(string projeKodu, string userNo)
        {

            try
            {
                using (var db = new SqlConnection(UtilityHelper.ConnectionStr("CloudContext")))
                {
                    var a = db.Query<DDCloudMusteriDto>("select * from Musteriler where MusteriKodu = @no And IsDeleted = 0", new { no = userNo }).FirstOrDefault();

                    if (a != null)
                    {
                        var list = new List<Musteriler>();

                        var data = new Musteriler
                        {
                            MusteriId = a.MusteriId,
                            Code = a.MusteriKodu,
                            SicilNo = a.MusteriKodu,
                            Name = a.Ad + (!a.Soyad.ToControl() ? " " + a.Soyad : ""),
                            UQ = a.MusteriId,
                            City = a.Sehir,
                            Country = a.Ulke,
                            Phone = a.Telefon,
                        };
                        list.Add(data);
                        CustomerDataAccess.UpsertMusteriMultiple(list);
                        return data;
                    }

                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
