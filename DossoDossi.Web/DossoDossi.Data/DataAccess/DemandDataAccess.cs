﻿using DossoDossi.Data.Entity;
using DossoDossi.Data.Helper;

using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DossoDossi.Data.Models;

namespace DossoDossi.Data.DataAccess
{
   public class DemandDataAccess
    {
        public static PagerResult<View_Demand> GetList(OfferPagerDto pager)
        {
            try
            {

                var p = new Pager
                {
                    CurrentPage = pager.CurrentPage,
                    Order = pager.Order,
                    TakeCount = pager.TakeCount
                };

                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    return db.View_Demands.PagerDynamic(Filter(pager), p);
                }
            }
            catch (Exception ex)
            {
                return new PagerResult<View_Demand>
                {
                    ErrorMessage = ex.Message,
                    StackTrace = ex.StackTrace,
                    Data = new List<View_Demand>()
                };
            }
        }

        private static Expression<Func<View_Demand, bool>> Filter(OfferPagerDto f)
        {
            var pred = PredicateBuilder.True<View_Demand>().And(a => a.DeleteDate == null);

            if (!f.Filter.ToControl())
            {
                pred = pred.And(a => a.CariKodu.Contains(f.Filter) || a.CariUnvan.Contains(f.Filter) || a.DemandNo.Contains(f.Filter));
            }

            if (f.CreateUserId.HasValue)
            {
                pred = pred.And(a => a.CreateUserId == f.CreateUserId.Value);
            }

            if (f.Status.HasValue)
            {
                pred = pred.And(a => a.Status == f.Status.Value);
            }

            if (!f.StatusMultiple.ToControl())
            {
                var ids = f.StatusMultiple.Split(',').Select(x => int.Parse(x)).ToList();
                pred = pred.And(x => ids.Contains(x.Status));
            }

            if (!f.TeklifNo.ToControl())
            {
                pred = pred.And(a => a.DemandNo.Contains(f.TeklifNo));
            }

            if (!f.CustomerName.ToControl())
            {
                pred = pred.And(a => a.CariKodu.Contains(f.CustomerName) || a.CariUnvan.Contains(f.CustomerName));
            }

            if (f.IsYeri.HasValue && f.IsYeri.Value > 0)
            {
                var i = f.IsYeri.Value.ToString();
                pred = pred.And(a => a.Isyeri == i);
            }

            if (f.Start.HasValue)
            {
                pred = pred.And(a => a.Tarih >= f.Start.Value);
            }

            if (f.End.HasValue)
            {
                pred = pred.And(a => a.Tarih <= f.End.Value);
            }

            return pred;
        }

        public static Demand GetById(int id)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<Demand>("SELECT TOP 1 * FROM Demands WHERE Id = @id", new { id }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static Demand GetByUQ(Guid id)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<Demand>("SELECT TOP 1 * FROM Demands WHERE UQ = @id", new { id }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static object UpdateStatus(int id, int status)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    db.Query("update Demands set Status = @status where Id = @id", new { id, status });
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool Delete(int id, int userId)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    db.Query("update Demands set DeleteDate = GetDATE(),[DeleteUserId] = @userId WHERE Id = @id", new { id, userId });
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private static void UpdateDemandNo(int id)
        {
            try
            {
                var prefix = SettingDataAccess.GetValByCode("Offer.NoPrefix");

                var code = id.ToString("D5");

                var no = (prefix ?? "") + code;

                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    db.Query("update Demands set DemandNo = @code WHERE Id = @id", new { id, code = no });
                }
            }
            catch (Exception)
            {
                //
            }
        }


        public static List<DemandItem> GetItems(int id)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    return db.Query<DemandItem>("SELECT  * FROM DemandItems WHERE DemandId = @id", new { id }).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<DemandItem>();
            }
        }

        public static ResponseMessage Create(Demand offer, List<DemandItem> items)
        {
            var user = SessionHelper.User;
            var userData = AccountDataAccess.GetById(user.UserId);

            try
            {
                using (var db = new DBDataContext(UtilityHelper.Connection))
                {
                    offer.CreateUserId = user.UserId;
                    offer.UQ = Guid.NewGuid();
                    offer.TotalAmount = Math.Round(offer.TotalAmount ?? 0, 2);
                    offer.TotalNet = Math.Round(offer.TotalNet ?? 0, 2);

                    offer.EklenmeTarihi = DateTime.Now;

                    if (userData.ParentId.HasValue)
                    {
                        var parent = AccountDataAccess.GetById(userData.ParentId.Value);
                        if (parent != null)
                        {
                            offer.AcceptUserId = parent.Id;
                        }
                    }

                    db.Demands.InsertOnSubmit(offer);
                    db.SubmitChanges();

                    items.ForEach(a => a.DemandId = offer.Id);
                    items.ForEach(a => a.UQ = Guid.NewGuid());

                    db.DemandItems.InsertAllOnSubmit(items);
                    db.SubmitChanges();

                    UpdateDemandNo(offer.Id);

                    return new ResponseMessage
                    {
                        Status = true,
                        Message = "Succesfully Saved",
                        Data = offer.Id
                    };
                }
            }
            catch (Exception ex)
            {
                return new ResponseMessage
                {
                    Message = "ERROR : " + ex.Message,
                    Code = ex.StackTrace,
                    Status = false,
                    Type = "Offer_Create"
                };
            }
        }

        public static ResponseMessage Edit(Demand offer, List<DemandItem> items, List<int> deleteItems)
        {
            var user = SessionHelper.User;
            var userData = AccountDataAccess.GetById(user.UserId);
            try
            {

                using (var db = new DBDataContext(UtilityHelper.Connection))
                {

                    var d = db.Demands.FirstOrDefault(a => a.Id == offer.Id);

                    d.Isyeri = offer.Isyeri;
                    d.MusteriSipNo = offer.MusteriSipNo;
                    d.SaticiKodu = offer.SaticiKodu;
                    d.SevkiyatAciklama = offer.SevkiyatAciklama;
                    d.SevkKodu = offer.SevkKodu;
                    d.Status = offer.Status;
                    d.SiparisFisId = offer.SiparisFisId;
                    d.Tarih = offer.Tarih;
                    d.TotalAmount = offer.TotalAmount;
                    d.TotalDiscount = offer.TotalDiscount;
                    d.TotalNet = offer.TotalNet;
                    d.VadeKodu = offer.VadeKodu;
                    d.CariKodu = offer.CariKodu;
                    d.CariUnvan = offer.CariUnvan;
                    d.Aciklama = offer.Aciklama;
                    d.VatTotal = offer.VatTotal;
                    d.TeslimTarihi = offer.TeslimTarihi;

                    db.SubmitChanges();

                    if (deleteItems != null && deleteItems.Any())
                    {
                        DeleteItems(deleteItems);
                    }

                    items.ForEach(a => a.DemandId = offer.Id);

                    if (items.Any(c => c.Id == 0))
                    {
                        items.ForEach(a => a.UQ = Guid.NewGuid());
                        var adItems = items.Where(x => x.Id == 0).ToList();
                        db.DemandItems.InsertAllOnSubmit(adItems);
                        db.SubmitChanges();
                    }

                    if (items.Any(x => x.Id > 0))
                    {
                        var adItems = items.Where(x => x.Id > 0).ToList();
                        var edtIds = adItems.Select(a => a.Id).ToList();

                        var editItems = db.DemandItems.Where(a => edtIds.Contains(a.Id)).ToList();

                        foreach (var item in editItems)
                        {
                            var i = items.FirstOrDefault(a => a.Id == item.Id);

                            item.FrmNo = i.Id.ToString();
                            item.ProductId = i.ProductId;
                            item.ProductCode = i.ProductCode;
                            item.ProductName = i.ProductName;
                            item.Qty = i.Qty;
                            item.Price = i.Price;
                            item.UnitCode = i.UnitCode;
                            item.Note = i.Note;
                            item.SatirId = null;
                            item.Discount = i.Discount;
                            item.CustomerPrCode = i.CustomerPrCode;
                            item.Vat = i.Vat ?? 0;
                        }

                        db.SubmitChanges();

                    }

                    return new ResponseMessage
                    {
                        Status = true,
                        Message = "Succesfully Saved",
                        Data = offer.Id
                    };
                }
            }
            catch (Exception ex)
            {
                return new ResponseMessage
                {
                    Message = "ERROR : " + ex.Message,
                    Code = ex.StackTrace,
                    Status = false,
                    Type = "Offer_Update"
                };
            }
        }


        public static void DeleteItems(List<int> ids)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    db.Query("DELETE FROM DemandItems WHERE Id IN @ids", new { ids });
                }
            }
            catch (Exception ex)
            {

            }
        }

        public static void UpdateFileDdemandItemByNo(int offerId, string frmNo, string path)
        {
            try
            {
                using (var db = new SqlConnection(UtilityHelper.Connection))
                {
                    var sql =
                        "UPDATE DemandItems SET [FilePath] = Coalesce(FilePath,'') + ','+ @path WHERE DemandId = @offerId AND FrmNo = @frmNo";
                    db.Query(sql, new { offerId, frmNo, path });
                }
            }
            catch (Exception e)
            {
            }
        }

    }
}
