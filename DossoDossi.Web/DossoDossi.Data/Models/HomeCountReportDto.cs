﻿namespace DossoDossi.Data.Models
{
    public class HomeCountReportDto
    {
        public int Id{ get; set; }
        public string Type{ get; set; }
        public double Total{ get; set; }
        public double Adet{ get; set; }
        public string Code{ get; set; }
        public string Name{ get; set; }
        public string Title{ get; set; }
        public string Description{ get; set; }
        public string Link{ get; set; }
        public string Color{ get; set; }
        public string Icon { get; set; }
        public string Class { get; set; }
    }
}
