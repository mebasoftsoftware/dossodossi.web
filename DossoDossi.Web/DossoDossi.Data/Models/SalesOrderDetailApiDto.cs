﻿using System;
using System.Collections.Generic;

namespace DossoDossi.Data.Models
{
    [Serializable]
    public class SalesOrderDetailApiDto
    {
        public string FieldDescription { get; set; }
        public string orderNumber { get; set; }
        public string orderType { get; set; }
        public string customerPoNo { get; set; }
        public string paymentTerms { get; set; }
        public string paymentTermsCode { get; set; }
        public string orderDate { get; set; }
        public string requestDate { get; set; }
        public string shippingInstructions { get; set; }
        public string carrier1 { get; set; }
        public string carrier1Account { get; set; }
        public string incoTermsCode { get; set; }
        public string incoTermsText { get; set; }
        public string orderComments { get; set; }
        public string type { get; set; }
        public string customerCode { get; set; }
        public string customerName { get; set; }
        public string dispatchNumber { get; set; }
        public string orderStatus { get; set; }
        public string orderStatusDesc { get; set; }
        public string broekmanStatus { get; set; }

        public SiparisShipDetailDto shipToDetails { get; set; }
        public SiparisShipDetailDto billToDetails { get; set; }
        public List<SiparisItemApiDto> orderItems { get; set; }

        public SalesOrderDetailApiDto()
        {
            this.shipToDetails = new SiparisShipDetailDto();
            this.billToDetails = new SiparisShipDetailDto();
            orderItems = new List<SiparisItemApiDto>();
        }
    }
}
