﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Models
{
    public class SelectItemDto
    {
        public string id { get; set; }
        public string text { get; set; }
        public int ayint { get; set; }
    }
}
