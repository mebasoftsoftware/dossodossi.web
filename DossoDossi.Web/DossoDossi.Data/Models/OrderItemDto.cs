﻿namespace DossoDossi.Data.Models
{
    public  class OrderItemDto
    {
        public int OrderId { get; set; }
        public int ItemId { get; set; }
        public int SatirId { get; set; }
        public string ProductCode { get; set; }
        public string ProductDescription { get; set; }
        public int Type { get; set; }
        public double Qty { get; set; }
        public double Price { get; set; }
        public double? Discount { get; set; }
        public decimal TaxRate { get; set; }
        public decimal Vat { get; set; }
        public string UnitCode { get; set; }
        public string FrmNo { get; set; }
        public string TeslimatKodu { get; set; }
        public string Note { get; set; }
        public string SiraNo { get; set; }
        public string CustomerPrCode { get; set; }
        public string Description { get; set; }
        public string FisNo { get; set; }
       
    }
}
