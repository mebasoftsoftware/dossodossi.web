﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Models
{
    public class AmbarAktarimSatirDto
    {
        public string UrunKodu { get; set; }
        public string StokId { get; set; }
        public string Birim { get; set; }
        public decimal Adet { get; set; }
        public string TeslimatKodu { get; set; }

    }
}
