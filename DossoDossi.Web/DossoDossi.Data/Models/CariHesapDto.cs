﻿

namespace DossoDossi.Data.Models
{
   public class CariHesapDto
    {
		public string Firma {get;set;}

		public string Donem {get;set;}

		public string Kullanimdami {get;set;}

		public string ChKartTipi {get;set;}

		public string MusteriKodu {get;set;}

		public string MusteriUnvani {get;set;}

		public string OzelKod {get;set;}

		public string YetkiKodu {get;set;}

		public string Adres1 {get;set;}

		public string Adres2 {get;set;}

		public string Sehir {get;set;}

		public string Ulke {get;set;}
		public string UlkeKodu {get;set;}

		public string PostaKodu {get;set;}

		public string TelefonNumarasi1 {get;set;}

		public string TelefonNumarasi2 {get;set;}

		public string FaksNumarasi1 {get;set;}

		public string VergiNumarasi {get;set;}

		public string VergiDairesi {get;set;}

		public string Ilgili {get;set;}

		public string Ilgili2 {get;set;}

		public string Ilgili3 {get;set;}

		public System.Nullable<double> IndirimYuzdesi {get;set;}

		public string OdemePlanKodu {get;set;}

		public string OdemePlaniAciklamasi {get;set;}

		public string EmailAdresi {get;set;}

		public string WebAdresi {get;set;}

		public string AboneEkBilgi {get;set;}

		public string OtomatikOdemeYapilanBanka {get;set;}

		public string MagazaKartiKodu {get;set;}

		public string VergiNo {get;set;}

		public string BlokeOlmus {get;set;}

		public string BankaSubeNo {get;set;}

		public string BankaSubeNo2 {get;set;}

		public string BankaSubeNo3 {get;set;}

		public string BankaSubeNo4 {get;set;}

		public string BankaSubeNo5 {get;set;}

		public string BankaSubeNo6 {get;set;}

		public string BankaSubeNo7 {get;set;}

		public string BankaHesapNo1 {get;set;}

		public string BankaHesapNo2 {get;set;}

		public string BankaHesapNo3 {get;set;}

		public string BankaHesapNo4 {get;set;}

		public string BankaHesapNo5 {get;set;}

		public string BankaHesapNo6 {get;set;}

		public string BankaHesapNo7 {get;set;}

		public System.Nullable<short> YazismaDili {get;set;}

		public string TeslimYontemi {get;set;}

		public string TasiyiciFirma {get;set;}

		public System.Nullable<short> DovizTuru {get;set;}

		public System.Nullable<short> BolgeNo {get;set;}

		public string VeriAktarimNo {get;set;}

		public string TicariIslemGrubu {get;set;}

		public System.Nullable<int> IsAkisReferansi {get;set;}

		public System.Nullable<decimal> Plaka {get;set;}

		public string GrupSirketKodu {get;set;}

		public string OzelKod2 {get;set;}

		public string OzelKod3 {get;set;}

		public string OzelKod4 {get;set;}

		public string OzelKod5 {get;set;}

		public string ChKodu {get;set;}

		public string ChUnvani {get;set;}

		public string MuhHesapKodu {get;set;}

		public string MuhHesapAdi {get;set;}

		public int CariId {get;set;}
	}
}
