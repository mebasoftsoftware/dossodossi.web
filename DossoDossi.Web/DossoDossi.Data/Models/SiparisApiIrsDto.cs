﻿using System;
using System.Collections.Generic;

namespace DossoDossi.Data.Models
{
    public class SiparisApiIrsDto
    {
        public string FieldDescription { get; set; }
        public string incoTermsCode { get; set; }
        public string orderNo { get; set; }
        public string serviceLevel { get; set; }
        public string carrier { get; set; }
        public string freightCost { get; set; }
        public string shipQty { get; set; }
        public string containerScaleWeight { get; set; }
        public string containerHeight { get; set; }
        public string containerNo { get; set; }
        public string coo { get; set; }
        public string quantityShipped { get; set; }
        public string containerTrackingNo { get; set; }
        public string containerLength { get; set; }
        public string containerWidth { get; set; }
        public string itemCode { get; set; }
        public string lineSeqNo { get; set; }
    }

    public class PurchaseOrderDeliveryDto
    {
        public string orderNo { get; set; }
        public DateTime deliveryDate{ get; set; }
        public string description{ get; set; }
        public List<PurchaseOrderItemDto> items{ get; set; }

        public PurchaseOrderDeliveryDto()
        {
            this.items = new List<PurchaseOrderItemDto>();
        }
    }

    public class PurchaseOrderItemDto
    {
        public string deliveredQuantity { get; set; }
        public string itemNo { get; set; }
        public string itemCode { get; set; }
        public string lineSeqNo { get; set; }
    }
}
