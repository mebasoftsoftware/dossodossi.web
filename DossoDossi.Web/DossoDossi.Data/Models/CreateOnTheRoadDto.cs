﻿using System;
using System.Collections.Generic;

namespace DossoDossi.Data.Models
{
    public  class CreateOnTheRoadDto
    {
        public DateTime OrderDate { get; set; }

        public string CreateUser { get; set; }

        public string SupplierCode { get; set; }

        public string SupplierName { get; set; }

        public string LeadTime { get; set; }

        public string OrderNo { get; set; }

        public string SendType { get; set; }

        public string DeliveryCode { get; set; }

        public DateTime? DeliveryDate { get; set; }

        public string Note1 { get; set; }
        public string Note2 { get; set; }

        public List<CreateOnTheRoadItemDto> Items { get; set; }
    }
}
