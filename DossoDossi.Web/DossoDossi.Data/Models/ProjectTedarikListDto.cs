﻿

using System;

namespace DossoDossi.Data.Models
{
   public class ProjectTedarikListDto
    {
		public System.Guid Id{get;set;}

		public string ProjeKodu{get;set;}

		public int CariId{get;set;}

		public string CariKodu{get;set;}

		public string CariUnvan{get;set;}

		public string KullaniciAdi{get;set;}

		public string Sifre{get;set;}
		public string OzelKod2{get;set;}
		public string OzelKod3{get;set;}
		public string OzelKod4{get;set;}
		public int Adet{get;set;}
		public Guid?  MarkaId{get;set;}
	}
}
