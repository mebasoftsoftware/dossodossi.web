﻿using System;
using System.Collections.Generic;

namespace DossoDossi.Data.Models
{
    public class ExcelDynamicDto
    {
        public string No { get; set; }
        public string BayiKodum { get; set; }
        public int CreateUserId { get; set; }
        public string SessionUserName { get; set; }
        public List<ExcelItemDto> List { get; set; }
    }

    public class ExcelItemDto
    {
        public string Baslik { get; set; }
        public string Deger { get; set; }
        public DateTime? Date { get; set; }
    }
}
