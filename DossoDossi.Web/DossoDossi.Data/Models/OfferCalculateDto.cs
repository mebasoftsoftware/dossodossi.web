﻿namespace DossoDossi.Data.Models
{
    public class OfferCalculateDto
    {
        public string LineNo { get; set; }

        public double Qty{ get; set; }

        public string Currency{ get; set; }

        public double Price { get; set; }

        public double Discount { get; set; }
        public double DiscountTotal { get; set; }

        public double TotalAmount { get; set; }

        public string TotalAmountStr { get; set; }

        public double TotalNet { get; set; }
        public double Vat { get; set; }
        public double VatTotal { get; set; }
        public string VatTotalStr { get; set; }

        public string TotalNetStr { get; set; }

    

    }
}
