﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Models
{
    [Serializable]
    public class SalesOrderListApiDto
    {
        public string FieldDescription { get; set; }
        public string orderNumber { get; set; }
        public string orderStatus { get; set; }
        public string orderType { get; set; }
        public string customerPoNo { get; set; }
        public string paymentTerms { get; set; }
        public string paymentTermsCode { get; set; }
        public string orderDate { get; set; }
        public string orderHour { get; set; }
        public string deliveryDate { get; set; }
        public string shippingInstructions { get; set; }
        public string carrier1 { get; set; }
        public string carrier1Account { get; set; }
        public string incoTermsCode { get; set; }
        public string incoTermsText { get; set; }
        public string orderComments { get; set; }
        public string type{ get; set; }
        public string customerCode{ get; set; }
        public string customerName{ get; set; }
        public string dispatchNumber{ get; set; }
        public string broekmanStatus { get; set; }

        public SiparisShipDetailDto shipToDetails { get; set; }
        public SiparisShipDetailDto billToDetails { get; set; }
        public List<SiparisItemApiDto> orderItems { get; set; }

        public SalesOrderListApiDto()
        {
            this.shipToDetails = new SiparisShipDetailDto();
            this.billToDetails = new SiparisShipDetailDto();
            orderItems = new List<SiparisItemApiDto>();
        }
    }

    [Serializable]
    public class SiparisItemApiDto
    {
        public string itemNo { get; set; }
        public string itemName { get; set; }
        public string itemCode { get; set; }
        public string lineSequenceNo { get; set; }
        public string quantityOrdered { get; set; }
        public string shippingQuantity { get; set; }
        public string remainingQuantity { get; set; }
        public string uom { get; set; }
        public string customerProductClientCode { get; set; }
    }

    [Serializable]
    public class SiparisShipDetailDto
    {
        public string code { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string address3 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string countryCode { get; set; }
        public string countryName { get; set; }

        public string postalCode { get; set; }

    }
}
