﻿using System;

namespace DossoDossi.Data.Models
{
    public class OfferReserveListDto
    {
        public float Miktar{ get; set; }
        public string ReserveDate{ get; set; }
        public string TeklifNo{ get; set; }
        public string CariKodu{ get; set; }
        public string CariUnvan{ get; set; }
    }
}
