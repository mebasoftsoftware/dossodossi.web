﻿

namespace DossoDossi.Data.Models.Report
{
   public class R_CekSenetHareketleri
    {
		public string Firma{get;set;}

		public string Donem{get;set;}

		public string ProjeKoduGenel{get;set;}

		public string ProjeAdiGenel{get;set;}

		public string ProjeOzelKoduGenel{get;set;}

		public string ProjeYetkiKoduGenel{get;set;}

		public System.Nullable<System.DateTime> PprojeBaslangicTarihiGenel{get;set;}

		public System.Nullable<System.DateTime> ProjeBitisTarihiGenel{get;set;}

		public string ProjeSorumlusuGenel{get;set;}

		public int CsDosyaRef{get;set;}

		public string Doviz{get;set;}

		public System.Nullable<double> TOTAL{get;set;}

		public System.Nullable<double> REPORTNET{get;set;}

		public System.Nullable<double> TLTUTAR{get;set;}

		public System.Nullable<double> Kur{get;set;}

		public System.Nullable<double> ToplamTutYeni{get;set;}

		public string Kod{get;set;}

		public string Aciklama{get;set;}

		public string CsTuru{get;set;}

		public System.Nullable<short> HareketNo{get;set;}

		public string IslemTuru{get;set;}

		public string Durumu{get;set;}

		public string KarmodulNo{get;set;}

		public string BankaHesapKodu{get;set;}

		public string BankaHesapAciklamasi{get;set;}

		public string PortfoyNo{get;set;}

		public string SeriNo{get;set;}

		public string CekinAitOlduguBanka{get;set;}

		public string CsOzelKod{get;set;}

		public string CsYetkiKodu{get;set;}

		public string OdemeYeri{get;set;}

		public string Borclu{get;set;}

		public string Kefil{get;set;}

		public string Muhabir{get;set;}

		public System.Nullable<short> Sube{get;set;}

		public System.Nullable<System.DateTime> VadeTarihi{get;set;}

		public System.Nullable<System.DateTime> TanzimTarihi{get;set;}

		public System.Nullable<System.DateTime> HareketTarihi{get;set;}

		public System.Nullable<double> Pul{get;set;}

		public System.Nullable<double> Tutar{get;set;}

		public string BordroNo{get;set;}

		public System.Nullable<short> CekinKacinciHareketi{get;set;}

		public System.Nullable<short> BordroSatirNo{get;set;}

		public System.Nullable<short> IslemDovizTuru{get;set;}

		public System.Nullable<double> IslemDovizKuru{get;set;}

		public System.Nullable<double> IslemDovizTutari{get;set;}

		public System.Nullable<double> RapDovizKuru{get;set;}

		public System.Nullable<double> RapDovizTutari{get;set;}

		public System.Nullable<short> RisktenTus{get;set;}

		public string MuhasebeKodu{get;set;}

		public string MasrafMerkeziKodu{get;set;}

		public System.Nullable<short> Kasadan{get;set;}

		public string Devirmi{get;set;}

		public string Kullanimdami{get;set;}

		public string CekIptalmi{get;set;}

		public string HareketIptal{get;set;}

		public string Muhasebelesmismi{get;set;}

		public string DevirCekimi{get;set;}

		public System.Nullable<double> TahEdilenRdKuru{get;set;}

		public System.Nullable<double> TahEdilenIslemDvzKuru{get;set;}

		public int CsHareketRef{get;set;}

		public System.Nullable<int> CkayitRef{get;set;}

		public System.Nullable<int> BordroRef{get;set;}

		public System.Nullable<int> BordroKartReferansi{get;set;}

		public System.Nullable<int> KarsiMuhHesabiRef{get;set;}

		public System.Nullable<int> KarsiMasMerkRef{get;set;}

		public string CariUnvan{get;set;}
	}
}
