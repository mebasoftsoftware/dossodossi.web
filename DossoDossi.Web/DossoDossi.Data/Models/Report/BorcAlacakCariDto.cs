﻿namespace DossoDossi.Data.Models.Report
{
    public class BorcAlacakCariDto
    {
        public string ChKodu { get; set; }
        public string OzelKod2 { get; set; }
        public string ChUnvani { get; set; }
        public string Ulke { get; set; }
        public double Tutar { get; set; }
        public double Borc { get; set; }
        public double Alacak { get; set; }
        public double Bakiye { get; set; }
        public double IslemDoviziBorc { get; set; }
        public double IslemDoviziAlacak { get; set; }
        public double IslemDoviziBakiye { get; set; }
        public double RaporlamaDovizBorc { get; set; }
        public double RaporlamaDoviziAlacak { get; set; }
        public double RaporlamaDoviziBakiyesi { get; set; }
    }
}
