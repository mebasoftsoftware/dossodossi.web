﻿

namespace DossoDossi.Data.Models.Report
{
  public  class R_Malzemekartlari
    {
		public string Firma{get;set;}

		public string Donem{get;set;}

		public string Kullanimda{get;set;}

		public string MalzemeSinifimi{get;set;}

		public string KartTipi{get;set;}

		public string MalzemeKodu{get;set;}

		public string MalzemeAdi{get;set;}

		public string OzelKodu{get;set;}

		public string OzelKodu2{get;set;}

		public string OzelKodu3{get;set;}

		public string OzelKodu4{get;set;}

		public string OzelKodu5{get;set;}

		public string YetkiKodu{get;set;}

		public string GrupKodu{get;set;}

		public string GrupAdi{get;set;}

		public string UreticiKodu{get;set;}

		public string BirimSetiKodu{get;set;}

		public System.Nullable<double> En{get;set;}

		public System.Nullable<double> Boy{get;set;}

		public System.Nullable<double> Yukseklik{get;set;}

		public System.Nullable<double> NetAgirlik{get;set;}

		public System.Nullable<double> BrutAgirlik{get;set;}

		public System.Nullable<double> Alan{get;set;}

		public System.Nullable<double> NetHacim{get;set;}

		public System.Nullable<double> BrutHacim{get;set;}

		public double BirinciBirim{get;set;}

		public double BirinciBirimCarpani{get;set;}

		public string Birim1{get;set;}

		public double IkinciBirim{get;set;}

		public double IkinciBirimCarpani{get;set;}

		public string Birim2{get;set;}

		public double UcuncuBirim{get;set;}

		public double UcuncuBirimCarpani{get;set;}

		public string Birim3{get;set;}

		public double DorduncuBirim{get;set;}

		public double DorduncuBirimCarpani{get;set;}

		public string Birim4{get;set;}

		public string OdemePlaniKodu{get;set;}

		public System.Nullable<double> RafOmruGun{get;set;}

		public string RafOmruSuresi{get;set;}

		public string OtvKodu{get;set;}

		public System.Nullable<double> AlisKdv{get;set;}

		public System.Nullable<double> SatisKdv{get;set;}

		public System.Nullable<double> IadeKdv{get;set;}

		public System.Nullable<double> PerakendeIadeKdvOrani{get;set;}

		public System.Nullable<double> PerakendeSatisKdvOrani{get;set;}

		public string Erisim{get;set;}

		public System.Nullable<short> SatinalmaKullYeri{get;set;}

		public System.Nullable<short> SatisKullYeri{get;set;}

		public System.Nullable<short> MalzYonKullYeri{get;set;}

		public string EmagazaKodu{get;set;}

		public string MarkaKodu{get;set;}

		public string MarkaAciklamasi{get;set;}

		public string GtipCode{get;set;}

		public string IhracatKategoriNo{get;set;}

		public string IsoNo{get;set;}

		public string UretimYeri{get;set;}

		public System.Nullable<double> DagitimPuani{get;set;}

		public System.Nullable<double> OrtStoktaKalmaSuresi{get;set;}

		public System.Nullable<double> RafOmru{get;set;}

		public string ResimVarmi{get;set;}

		public string DetayAciklama{get;set;}

		public System.Nullable<short> AmortismanTuru{get;set;}

		public System.Nullable<double> AmortismanOrani{get;set;}

		public System.Nullable<short> AmortismanSuresi{get;set;}

		public System.Nullable<double> HurdaDegeri{get;set;}

		public System.Nullable<short> Degerlenebilir{get;set;}

		public System.Nullable<short> DegerlemeOranlari{get;set;}

		public System.Nullable<short> KistAmortDurumu{get;set;}

		public System.Nullable<short> UlusalAmortismanTuru{get;set;}

		public System.Nullable<double> UlusalAmortismanOrani{get;set;}

		public System.Nullable<short> UlusalAmortismanSuresi{get;set;}

		public System.Nullable<short> UlusalDegerleme{get;set;}

		public System.Nullable<short> UlusalDegerlemeAmortismani{get;set;}

		public System.Nullable<short> UlusalKistAmortisman{get;set;}

		public System.Nullable<short> Onaylanmis{get;set;}

		public System.Nullable<int> KalKontrRef{get;set;}

		public System.Nullable<int> BirimSetiReferansi{get;set;}

		public int MalzemeDosyaAdresi{get;set;}

		public System.Nullable<int> IsAkisReferansi{get;set;}

		public string BirimliBarkod1{get;set;}

		public string BirimliBarkod2{get;set;}

		public string BirimliBarkod3{get;set;}

		public string BirimliBarkod4{get;set;}

		public string BirinciBirimBarkod1{get;set;}

		public string BirinciBirimBarkod2{get;set;}

		public string BirinciBirimBarkod3{get;set;}

		public string BirinciBirimBarkod4{get;set;}

		public string IkinciBirimBarkod1{get;set;}

		public string IkinciBirimBarkod2{get;set;}

		public string IkinciBirimBarkod3{get;set;}

		public string IkinciBirimBarkod4{get;set;}

		public string UcuncuBirimBarkod1{get;set;}

		public string UcuncuBirimBarkod2{get;set;}

		public string UcuncuBirimBarkod3{get;set;}

		public string UcuncuBirimBarkod4{get;set;}

		public string DorduncuBirimBarkod1{get;set;}

		public string DorduncuBirimBarkod2{get;set;}

		public string DorduncuBirimBarkod3{get;set;}

		public string DorduncuBirimBarkod4{get;set;}
	}
}
