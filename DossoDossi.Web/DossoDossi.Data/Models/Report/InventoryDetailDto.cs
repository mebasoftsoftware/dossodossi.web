﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Models.Report
{
   public class InventoryDetailDto
    {
        public int StokId{ get; set; }
        public string StokKodu{ get; set; }
        public string StokAdi { get; set; }
        public string OzelKodu { get; set; }
        public double Stock { get; set; }
        public double AlPrice { get; set; }
    }
}
