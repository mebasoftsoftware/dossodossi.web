﻿
namespace DossoDossi.Data.Models.Report
{
   public class R_Bankalar
    {
		public string Firma{get;set;}

		public string Donem{get;set;}

		public string BankaKullanimdami{get;set;}

		public string BankaKodu{get;set;}

		public string BankaAdi{get;set;}

		public string Subesi{get;set;}

		public string SubeNo{get;set;}

		public string Adres1{get;set;}

		public string Adres2{get;set;}

		public string Sehir{get;set;}

		public string Ulke{get;set;}

		public string PostaKodu{get;set;}

		public string Telefon1{get;set;}

		public string Telefon2{get;set;}

		public string Faks{get;set;}

		public string Yetkili{get;set;}

		public string Email{get;set;}

		public string InternetAdresi{get;set;}

		public string BankaHesapKodu{get;set;}

		public string BankaHesapAciklamasi{get;set;}

		public string BankaHesapOzelKodu{get;set;}

		public string BankaHesapYetkiKodu{get;set;}

		public System.Nullable<double> CekKrediMarji{get;set;}

		public System.Nullable<double> SenetKrediMarji{get;set;}

		public System.Nullable<double> CekKrediLimiti{get;set;}

		public System.Nullable<double> SenetKrediLimiti{get;set;}

		public System.Nullable<double> YillikCariHesapFaizi{get;set;}

		public System.Nullable<double> AylikSenetKarsiligiKrediFaizi{get;set;}

		public System.Nullable<double> AylikCekKarsiligiKrediFaizi{get;set;}

		public System.Nullable<double> StopajOrani{get;set;}

		public System.Nullable<double> FonOrani{get;set;}

		public System.Nullable<short> HesapDovizi{get;set;}

		public string BankaHesabiKullanimdami{get;set;}
	}
}
