﻿

namespace DossoDossi.Data.Models.Report
{
  public  class R_MalzemeHareketleri
    {
		public string Firma{get;set;}

		public string Donem{get;set;}

		public System.Nullable<int> KayitAdresi{get;set;}

		public System.Nullable<short> FisTuruNo{get;set;}

		public string FisTuru{get;set;}

		public string Iptalmi{get;set;}

		public string Faturalasmismi{get;set;}

		public string PlanlananGerceklesen{get;set;}

		public string GirisCikisKodu{get;set;}

		public string FisNo{get;set;}

		public string BelgeNo{get;set;}

		public string FaturaNo{get;set;}

		public System.Nullable<System.DateTime> Tarih{get;set;}

		public string Zaman{get;set;}

		public string FizOzelKodu{get;set;}

		public string YetkiKodu{get;set;}

		public string UretimEmriNo{get;set;}

		public string KaynakTuru{get;set;}

		public string HedefTuru{get;set;}

		public System.Nullable<short> KaynakIsyeriNo{get;set;}

		public System.Nullable<short> KaynakBolumNo{get;set;}

		public string KaynakIsyeri{get;set;}

		public System.Nullable<short> IsyeriNo{get;set;}

		public string KaynakBolum{get;set;}

		public System.Nullable<short> BolumNo{get;set;}

		public string KaynakFabrika{get;set;}

		public string Ambar{get;set;}

		public System.Nullable<short> KaynakAmbarMlytGrup{get;set;}

		public string HedefBolum{get;set;}

		public System.Nullable<short> HBolumNo{get;set;}

		public string HedefIsyeri{get;set;}

		public System.Nullable<short> HIsyeriNo{get;set;}

		public string HedefFabrika{get;set;}

		public string HedefAmbar{get;set;}

		public string MusteriKodu{get;set;}

		public string MusteriAdi{get;set;}

		public string MusteriOzelKodu{get;set;}

		public string MusteriYetkiKodu{get;set;}

		public string MusteriSehir{get;set;}

		public string MuhasebeKodu{get;set;}

		public string MuhasebeAdi{get;set;}

		public string MasrafMrkKodu{get;set;}

		public string MasrafMrkAdi{get;set;}

		public string PromosyonKodu{get;set;}

		public string SatitTuru{get;set;}

		public string MalzemeKodu{get;set;}

		public string MalzemeAdi{get;set;}

		public string MalzemeOzelKodu{get;set;}

		public string MalzemeOzelKodu2{get;set;}

		public string MalzemeOzelKodu3{get;set;}

		public string MalzemeOzelKodu4{get;set;}

		public string MalzemeOzelKodu5{get;set;}

		public System.Nullable<double> Miktar{get;set;}

		public string BirimKodu{get;set;}

		public string BirimSetiKodu{get;set;}

		public System.Nullable<double> CevrimKatsayisi1{get;set;}

		public System.Nullable<double> CevrimKatsayisi2{get;set;}

		public System.Nullable<double> BoyutKatsayisi1{get;set;}

		public System.Nullable<double> BoyutKatsayisi2{get;set;}

		public System.Nullable<double> BoyutKatsayisi3{get;set;}

		public System.Nullable<double> BoyutKatsayisi4{get;set;}

		public System.Nullable<double> BoyutKatsayisi5{get;set;}

		public System.Nullable<double> BoyutKatsayisi6{get;set;}

		public System.Nullable<double> PlanlananMiktar{get;set;}

		public System.Nullable<double> AnaBirimMiktar{get;set;}

		public System.Nullable<double> Promosyon{get;set;}

		public System.Nullable<double> BirimFiyat{get;set;}

		public System.Nullable<double> Kdv{get;set;}

		public System.Nullable<double> Tutar{get;set;}

		public System.Nullable<double> KdvMatrahi{get;set;}

		public System.Nullable<decimal> SatirNetTutari{get;set;}

		public System.Nullable<decimal> Indirim{get;set;}

		public System.Nullable<decimal> Masraf{get;set;}

		public System.Nullable<double> RaporlamaDovizKuru{get;set;}

		public System.Nullable<double> RdBirimFiyati{get;set;}

		public System.Nullable<double> RdTutari{get;set;}

		public System.Nullable<double> RdKDV{get;set;}

		public System.Nullable<double> RdNetSatirTutari{get;set;}

		public string IslemDvzTuru{get;set;}

		public System.Nullable<double> IslemDvzBirimFiyati{get;set;}

		public System.Nullable<double> IslemDvzKdv{get;set;}

		public System.Nullable<double> IslemDvzTutar{get;set;}

		public System.Nullable<double> FiyatFarki{get;set;}

		public System.Nullable<double> FiyatFarkiMaliyeti{get;set;}

		public string FiyatFarkiAciklama{get;set;}

		public string SatirOzelKodu{get;set;}

		public string TeslimatKodu{get;set;}

		public string CikisIzlemeNumarasi{get;set;}

		public string SiparisFisNo{get;set;}

		public System.Nullable<System.DateTime> SiparisFisTarihi{get;set;}

		public string SatirOdemePlaniKodu{get;set;}

		public string SatirOdemePlaniAciklamasi{get;set;}

		public System.Nullable<double> OtvOrani{get;set;}

		public System.Nullable<double> OtvTutari{get;set;}

		public System.Nullable<double> HesaplananOtv{get;set;}

		public System.Nullable<double> OtvMaliyeti{get;set;}

		public System.Nullable<double> OtvIadeMaliyeti{get;set;}

		public System.Nullable<double> RdOtvMaliyeti{get;set;}

		public System.Nullable<double> RdOtvIadeMaliyeti{get;set;}

		public string IadeIslemiMaliyetTuru{get;set;}

		public System.Nullable<double> IadeFisleriIcinIadeMaliyeti{get;set;}

		public System.Nullable<double> IadeFisleriIcinDovizliIadeMaliyeti{get;set;}

		public System.Nullable<double> CikisFisleriCikisMaliyeti{get;set;}

		public System.Nullable<double> CikisFisleriDovizliCikisMaliyeti{get;set;}

		public System.Nullable<double> IadeMiktari{get;set;}

		public string SevkiyatHesabiKodu{get;set;}

		public string SevkiyatHesabiUnvani{get;set;}

		public string SevkiyatAdresiKodu{get;set;}

		public string SevkiyatAdresiAciklamasi{get;set;}

		public string IrsaliyeNo{get;set;}

		public string Aciklama1{get;set;}

		public string Aciklama2{get;set;}

		public string Aciklama3{get;set;}

		public string Aciklama4{get;set;}

		public string DokumanIzlemeNumarasi{get;set;}

		public string SevkiyatTuru{get;set;}

		public string SevkiyatTuruAciklamasi{get;set;}

		public string TasiyiciKodu{get;set;}

		public string TasiyiciAciklamasi{get;set;}

		public string PaketKoliNo{get;set;}

		public System.Nullable<int> DagitimEmriReferansi{get;set;}

		public string VaryantKodu{get;set;}

		public string VaryantAdi{get;set;}

		public string TicariIslemGrubu{get;set;}

		public System.Nullable<double> SatirIndirimi{get;set;}

		public System.Nullable<double> FaturaAltIndirimi{get;set;}
	}
}
