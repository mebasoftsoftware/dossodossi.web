﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Models.Report
{
   public class BankaBakiyeDto
    {
        public string BankaKodu { get; set; }
        public string BankaHesapKodu { get; set; }
        public string BankaAdi { get; set; }
        public string Logo { get; set; }
        public double Bakiye { get; set; }

    }
}
