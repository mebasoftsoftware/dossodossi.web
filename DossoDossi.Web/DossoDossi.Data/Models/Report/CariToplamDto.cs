﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Models.Report
{
  public  class CariToplamDto
    {
        public string Adi { get; set; }
        public string Kodu { get; set; }
        public double Toplam { get; set; }
    }
}
