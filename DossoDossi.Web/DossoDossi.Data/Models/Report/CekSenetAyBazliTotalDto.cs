﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Models.Report
{
    public class CekSenetAyBazliTotalDto
    {
        public string Doviz { get; set; }
        public string Kod { get; set; }
        public string Aciklama { get; set; }
        public string Durumu { get; set; }
        public double Tutar { get; set; }
        public string Yil { get; set; }
        public string CsTuru { get; set; }
        public string Ay { get; set; }
    }
}
