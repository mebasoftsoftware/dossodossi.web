﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Models.Report
{
   public class ExpenseDetailDto
    {
        public int SatirId { get; set; }
        public string FaturaNo { get; set; }
        public string HizmetKodu { get; set; }
        public string HizmetAdi { get; set; }
        public string ChKodu { get; set; }
        public string ChUnvan { get; set; }
        public double BirimMiktar { get; set; }
        public double BirimFiyat { get; set; }
        public double DovizliToplam { get; set; }
        public double DovizParite { get; set; }
        public string FaturaTuru { get; set; }
        public DateTime Tarih { get; set; }
        public string BelgeNo { get; set; }
        public string Aciklama1 { get; set; }
        public string Aciklama2 { get; set; }
        public string Aciklama3 { get; set; }
        public string Aciklama4 { get; set; }
        public string SatirAciklama { get; set; }
    }
}
