﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Models.Report
{
   public class SehirToplamDto
    {
        public string Sehir { get; set; }
        public double Toplam { get; set; }
    }
}
