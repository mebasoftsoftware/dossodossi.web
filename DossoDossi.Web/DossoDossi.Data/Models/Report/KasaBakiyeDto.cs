﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Models.Report
{
   public class KasaBakiyeDto
    {
        public string KasaKodu { get; set; }
        public string KasaAdi { get; set; }
        public double Bakiye { get; set; }
    }
}
