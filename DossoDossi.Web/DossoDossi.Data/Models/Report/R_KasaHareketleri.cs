﻿

namespace DossoDossi.Data.Models.Report
{
   public class R_KasaHareketleri
    {
		public string Firma{get;set;}

		public string Donem{get;set;}

		public string ProjeKoduGenel{get;set;}

		public string ProjeAdiGenel{get;set;}

		public string ProjeOzelKoduGenel{get;set;}

		public string ProjeYetkiKoduGenel{get;set;}

		public System.Nullable<System.DateTime> ProjeBaslangicTarihiGenel{get;set;}

		public System.Nullable<System.DateTime> ProjeBitisTarihiGenel{get;set;}

		public string ProjeSorumlusuGenel{get;set;}

		public string KasaKodu{get;set;}

		public string KasaAdi{get;set;}

		public string KasaAciklamasi{get;set;}

		public string VirmanKasaKodu{get;set;}

		public string VirmanKasaAciklamasi{get;set;}

		public string MuhasebeHesabiKodu{get;set;}

		public string MasrafMerkeziKodu{get;set;}

		public string MasrafMerkeziAciklamasi{get;set;}

		public string KarsiKasaMuhasebeHesabi{get;set;}

		public string KarsiKasaMasrafMerkeziKodu{get;set;}

		public string IslemNo{get;set;}

		public System.Nullable<System.DateTime> Tarih{get;set;}

		public System.Nullable<short> IslemTuruKodu{get;set;}

		public string Bolum{get;set;}

		public System.Nullable<short> BolumNo{get;set;}

		public string Isyeri{get;set;}

		public System.Nullable<short> IsyeriNo{get;set;}

		public string OzelKod{get;set;}

		public string YetkiKodu{get;set;}

		public string KasaIslemAciklamasi{get;set;}

		public string SatirAciklamasi{get;set;}

		public System.Nullable<double> Tutar{get;set;}

		public System.Nullable<double> Borc{get;set;}

		public System.Nullable<double> Alacak{get;set;}

		public System.Nullable<double> Bakiye{get;set;}

		public System.Nullable<double> RdKuru{get;set;}

		public System.Nullable<double> RdTutar{get;set;}

		public System.Nullable<double> RdBorc{get;set;}

		public System.Nullable<double> RdAlacak{get;set;}

		public System.Nullable<double> RdBakiye{get;set;}

		public string IslemDoviziKuru{get;set;}

		public System.Nullable<double> IslemDoviziTutari{get;set;}

		public System.Nullable<double> IslemDovizTutari{get;set;}

		public string BorcAlacak{get;set;}

		public string Muhasebelesmismi{get;set;}

		public string Iptalmi{get;set;}

		public string MuhasebeFisiNumarasi{get;set;}

		public System.Nullable<System.DateTime> MuhasebeFisiTarihi{get;set;}

		public string MuhasebelestirmeIptalmi{get;set;}

		public string GenelDovizTuru{get;set;}

		public string SatirDovizTuru{get;set;}

		public string TicariIslemGrubu{get;set;}

		public string DetayAciklamaVarmi{get;set;}

		public System.Nullable<short> BolgeNo{get;set;}

		public string HizmetTipi{get;set;}

		public System.Nullable<double> StopajOrani{get;set;}

		public System.Nullable<double> PusulasiFonPayiOrani{get;set;}

		public string PusulasiPlaka{get;set;}

		public string StopajMuhasebeKodu{get;set;}

		public string FonPayiMuhasebeKodu{get;set;}

		public string Adresi{get;set;}

		public int KayitAdresi{get;set;}

		public System.Nullable<int> Transref{get;set;}
	}
}
