﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Models.Report
{
   public class CekTutarGroupSumDto
    {
        public string CsTuru { get; set; }
        public string Durumu { get; set; }
        public string Doviz { get; set; }
        public double Toplam { get; set; }
    }
}
