﻿
namespace DossoDossi.Data.Models.Report
{
   public class R_SatisFaturalari
	{
		public string Firma{get;set;}

		public string Donem{get;set;}

		public int KayitAdresi{get;set;}

		public string ProjeKoduGenel{get;set;}

		public string ProjeAdiGenel{get;set;}

		public string ProjeOzelKoduGenel{get;set;}

		public string ProjeYetkiKoduGenel{get;set;}

		public System.Nullable<System.DateTime> ProjeBaslangicTarihiGenel{get;set;}

		public System.Nullable<System.DateTime> ProjeBitisTarihiGenel{get;set;}

		public string ProjeSorumlusuGenel{get;set;}

		public string Faturalasmismi{get;set;}

		public System.Nullable<short> FaturaTuruNo{get;set;}

		public string FaturaTuru{get;set;}

		public string IrsaliyeNo{get;set;}

		public string FaturaNo{get;set;}

		public System.Nullable<System.DateTime> Tarih{get;set;}

		public string BelgeNo{get;set;}

		public string CariHesapKodu{get;set;}

		public string CariHesapUnvani{get;set;}

		public string CariHesapVergiDairesi{get;set;}

		public string CariHesapVergiNumarasi{get;set;}

		public string ChOzelKodu{get;set;}

		public string ChOzelKodu2{get;set;}

		public string ChOzelKodu3{get;set;}

		public string ChOzelKodu4{get;set;}

		public string ChOzelKodu5{get;set;}

		public string ChYetkiKodu{get;set;}

		public string ChSehir{get;set;}

		public string OdemePlaniKodu{get;set;}

		public string OdemePlaniAciklamasi{get;set;}

		public string TicariIslemGrupKodu{get;set;}

		public string TicariIslemGrupAciklamasi{get;set;}

		public string Isyeri{get;set;}

		public System.Nullable<short> IsyeriNo{get;set;}

		public string Bolum{get;set;}

		public System.Nullable<short> BolumNo{get;set;}

		public string Fabrika{get;set;}

		public string Ambar{get;set;}

		public string FaturaOzelKodu{get;set;}

		public string FaturaYetkiKodu{get;set;}

		public string IrsaliyeOzelKodu{get;set;}

		public string IrsaliyeYetkiKodu{get;set;}

		public string SatisElemaniKodu{get;set;}

		public string SatisElemaniAdi{get;set;}

		public string MalzemeOzelKodu{get;set;}

		public string MalzemeOzelKodu2{get;set;}

		public string MalzemeOzelKodu3{get;set;}

		public string MalzemeOzelKodu4{get;set;}

		public string MalzemeOzelKodu5{get;set;}

		public string SatirTuru{get;set;}

		public string StokGrupKodu{get;set;}

		public string StokGrupAdi{get;set;}

		public string StokKodu{get;set;}

		public string StokAdi{get;set;}

		public System.Nullable<decimal> Miktar{get;set;}

		public System.Nullable<decimal> NetMiktar{get;set;}

		public System.Nullable<double> AnaBirimMiktar{get;set;}

		public System.Nullable<decimal> BirimFiyat{get;set;}

		public System.Nullable<decimal> Tutar{get;set;}

		public System.Nullable<double> Indirim{get;set;}

		public System.Nullable<double> Promosyon{get;set;}

		public System.Nullable<double> Masraf{get;set;}

		public System.Nullable<double> KdvOrani{get;set;}

		public System.Nullable<double> Kdv{get;set;}

		public System.Nullable<double> NetSatisTutari{get;set;}

		public System.Nullable<double> KdvMatrahi{get;set;}

		public System.Nullable<double> RaporlamaDovizKuru{get;set;}

		public System.Nullable<double> RdBirimFiyati{get;set;}

		public System.Nullable<double> RdTutari{get;set;}

		public System.Nullable<double> RdKdv{get;set;}

		public System.Nullable<double> RdNetSatirTutari{get;set;}

		public System.Nullable<double> FiyatFarki{get;set;}

		public System.Nullable<double> FiyatFarkiMaliyeti{get;set;}

		public string FiyatFarkiAciklama{get;set;}

		public string BirimKodu{get;set;}

		public string BirimSetiKodu{get;set;}

		public string SatirOzelKodu{get;set;}

		public string SatirOzelKoduAciklamasi{get;set;}

		public string TeslimatKodu{get;set;}

		public string TeslimatAciklamasi{get;set;}

		public string SiparisFisNo{get;set;}

		public System.Nullable<System.DateTime> SiparisFisTarihi{get;set;}

		public string SatirOdemePlaniKodu{get;set;}

		public string SatirOdemePlaniAciklamasi{get;set;}

		public System.Nullable<double> CikisMaliyeti{get;set;}

		public System.Nullable<double> CikisMaliyetiRd{get;set;}

		public string SatirAciklamasi{get;set;}

		public string SevkiyatHesabiKodu{get;set;}

		public string SevkiyatHesabiUnvani{get;set;}

		public string SevkiyatAdresiKodu{get;set;}

		public string SevkiyatAdresiAciklamasi{get;set;}

		public string DokumanIzlemeNumarasi{get;set;}

		public string SevkiyatTuru{get;set;}

		public string SevkiyatTuruAciklamasi{get;set;}

		public string TasiyiciKodu{get;set;}

		public string TasiyiciAciklamasi{get;set;}

		public string PaketKoliNo{get;set;}

		public System.Nullable<int> DagitimEmriReferansi{get;set;}

		public string HizmetAciklamasi{get;set;}

		public System.Nullable<int> FaturaRef{get;set;}

		public System.Nullable<int> StficheRef{get;set;}

		public System.Nullable<int> IsAkisReferansi{get;set;}

		public int Logref{get;set;}

		public string IhrGenelOperasyonFisTipi{get;set;}

		public string IhrGenelOperasyonDosyaKodu{get;set;}

		public string IhrGenelOperasyonDosyaAdi{get;set;}

		public System.Nullable<short> IhrGenelUlkeTipi{get;set;}

		public string IhrGenelUlkeKodu{get;set;}

		public string IhrGenelUlkeAdi{get;set;}

		public string IhrGenelOdemeTipiKodu{get;set;}

		public string IhrGenelOdemeTipiAdi{get;set;}

		public System.Nullable<int> IhrGenelGumrukKodu{get;set;}

		public System.Nullable<int> IhrGenelGumrukAdi{get;set;}

		public string IhrGenelGumrukBeyannameNumarasi{get;set;}

		public System.Nullable<System.DateTime> IhrGenelGumrukBeyannameTarihi{get;set;}

		public string IhrGenelAracPlakasi{get;set;}

		public System.Nullable<int> IhrGenelSonucTarihi{get;set;}

		public string IhrGenelTeslimKodu{get;set;}

		public string IhrGenelTeslimAdi{get;set;}

		public string IhrGenelYuklemeAcentasiKodu{get;set;}

		public string IhrGenelYuklemeAcentasiAdi{get;set;}

		public System.Nullable<double> IhrGenelNakliyeTutari{get;set;}

		public System.Nullable<double> IhrGenelSigortaTutari{get;set;}

		public System.Nullable<int> IhrSatirGumrukKodu{get;set;}

		public System.Nullable<int> IhrSatirGumrukAdi{get;set;}

		public string IhrSatirEximKrediKodu{get;set;}

		public string IhrSatirEximKrediAdi{get;set;}

		public string IhrSatirGumrukBeyannameNumarasi{get;set;}

		public System.Nullable<System.DateTime> IhrSatirGumrukBeyannameTarihi{get;set;}

		public System.Nullable<double> IhrSatirEximbankParitesi{get;set;}

		public System.Nullable<double> IhrSatirIplParitesi{get;set;}

		public System.Nullable<short> IhrSatirHedefUlkeTipi{get;set;}

		public string IhrSatirHedefUlkeKodu{get;set;}

		public string IhrSatirHedefUlkeAdi{get;set;}

		public string IhrSatirOnDurumDokumanNumarasi{get;set;}

		public string IhrSatirSerbestBolgeCikisDokumanKodu{get;set;}

		public System.Nullable<double> IhrSatirCifBedeli{get;set;}

		public System.Nullable<double> IhrSatirGeciciKabulParitesi{get;set;}

		public System.Nullable<decimal> Plaka{get;set;}

		public string VaryantKodu{get;set;}

		public string VaryantAdi{get;set;}

		public string SatirCikisIzlKodu{get;set;}

		public string Kodu{get;set;}

		public string Aciklama{get;set;}

		public System.Nullable<short> FaturaSatirNo{get;set;}

		public string ProjeKodu{get;set;}

		public string ProjeAdi{get;set;}

		public System.Nullable<decimal> SatirNetTutari{get;set;}

		public string SatisElemaniKoduSatir{get;set;}

		public string SatisElemaniAdiSatir{get;set;}

		public string IslemDvzTuru{get;set;}

		public System.Nullable<double> IslemDvzBirimFiyati{get;set;}

		public System.Nullable<double> IslemDvzKdv{get;set;}

		public System.Nullable<double> IslemDvzTutari{get;set;}

		public System.Nullable<double> IslemDvzNetAlimTutari{get;set;}

		public System.Nullable<int> FisRefNo{get;set;}

		public System.Nullable<int> FatRefNo{get;set;}
	}
}
