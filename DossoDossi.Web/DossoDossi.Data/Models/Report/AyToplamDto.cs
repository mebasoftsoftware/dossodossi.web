﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Models.Report
{
   public class AyToplamDto
    {
        public int Ay { get; set; }
        public string AyAciklama { get; set; }
        public double Toplam { get; set; }
    }
}
