﻿

namespace DossoDossi.Data.Models.Report
{
   public class R_BankaHareketleri
    {
		public string Firma{get;set;}

		public string Donem{get;set;}

		public System.Nullable<short> TransType{get;set;}

		public System.Nullable<short> TrCode{get;set;}

		public System.Nullable<short> ModuleNr{get;set;}

		public string ProjeKoduGenel{get;set;}

		public string ProjeAdiGenel{get;set;}

		public string ProjeOzelKoduGenel{get;set;}

		public string ProjeYetkiKoduGenel{get;set;}

		public System.Nullable<System.DateTime> ProjeBaslangicTarihiGenel{get;set;}

		public System.Nullable<System.DateTime> ProjeBitisTarihiGenel{get;set;}

		public string ProjeSorumlusuGenel{get;set;}

		public string ProjeKoduSatir{get;set;}

		public string ProjeAdiSatir{get;set;}

		public string ProjeOzelKoduSatir{get;set;}

		public string ProjeYetkiKoduSatir{get;set;}

		public System.Nullable<System.DateTime> ProjeBaslangicTarihiSatir{get;set;}

		public System.Nullable<System.DateTime> ProjeBitisTarihiSatir{get;set;}

		public string ProjeSorumlusuSatir{get;set;}

		public System.Nullable<short> HareketTuruKodu{get;set;}

		public System.Nullable<short> FisTuruKodu{get;set;}

		public System.Nullable<short> ModulNumarasi{get;set;}

		public string BankaKodu{get;set;}

		public string BankaAdi{get;set;}

		public string BankaHesapKoduSatir{get;set;}

		public string BankaHesapAdiSatir{get;set;}

		public string BankaHesabiTipiSatir{get;set;}

		public string HareketTuruSatir{get;set;}

		public string BankaHesapKoduGenel{get;set;}

		public string BankaHesapAdiGenel{get;set;}

		public string BankaHesapKodu{get;set;}

		public string BankHesapAdi{get;set;}

		public string BankaHesabiTipiGenel{get;set;}

		public string BankaHesabiTipi{get;set;}

		public string HareketTuruGenel{get;set;}

		public string HareketTuru{get;set;}

		public string IslemTuru{get;set;}

		public string IslemNo{get;set;}

		public string BelgeNo{get;set;}

		public System.Nullable<System.DateTime> Tarih{get;set;}

		public string SatirAciklamasi{get;set;}

		public string ChKodu{get;set;}

		public string ChUnvani{get;set;}

		public string MuhasebeHesapKodu{get;set;}

		public string MasrafMerkeziKodu{get;set;}

		public string Bolum{get;set;}

		public System.Nullable<short> BolumNo{get;set;}

		public string Isyeri{get;set;}

		public System.Nullable<short> IsyeriNo{get;set;}

		public string OzelKod{get;set;}

		public string YetkiKodu{get;set;}

		public string TicariIslemGrubu{get;set;}

		public string Muhasebelesmismi{get;set;}

		public double? Tutar {get;set;}

		public double? Borc {get;set;}

		public double? Alacak {get;set;}

		public System.Nullable<double> Bakiye{get;set;}

		public System.Nullable<double> RdKuru{get;set;}

		public System.Nullable<double> RdTutari{get;set;}

		public System.Nullable<double> RdBorc{get;set;}

		public System.Nullable<double> RdAlacak{get;set;}

		public System.Nullable<double> RdBakiye{get;set;}

		public string IslemDoviziTuru{get;set;}

		public System.Nullable<double> IslemDovizKuru{get;set;}

		public System.Nullable<double> IslemDovizTutari{get;set;}

		public System.Nullable<double> IslemDoviziBorc{get;set;}

		public System.Nullable<double> IslemDoviziAlacak{get;set;}

		public System.Nullable<double> IslemDoviziBakiye{get;set;}

		public System.Nullable<double> RaporlamaDovizTuru{get;set;}

		public System.Nullable<double> RaporlamaDovizTutari{get;set;}

		public System.Nullable<double> RaporlamaDoviziBorc{get;set;}

		public System.Nullable<double> RaporlamaDoviziAlacak{get;set;}

		public string Iptalmi{get;set;}

		public string BankaHesapReferansi{get;set;}

		public System.Nullable<int> ProvizyonReferansi{get;set;}

		public string GcbNumarasi{get;set;}

		public string BankaIslemGrupNumarasi{get;set;}

		public System.Nullable<System.DateTime> BankaIslemGrupTarihi{get;set;}

		public System.Nullable<short> BankaIslemGrupSatirNumarasi{get;set;}

		public string ChBankaHesapNo{get;set;}
	}
}
