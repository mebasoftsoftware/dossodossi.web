﻿
namespace DossoDossi.Data.Models.Report
{
   public class R_BorcAlacakTakip
    {
		public string Firma{get;set;}

		public string Donem{get;set;}

		public int KayitAdresi{get;set;}

		public System.Nullable<System.DateTime> HareketTarihi{get;set;}

		public string CariHesapKod{get;set;}

		public string CariHesapAdi{get;set;}

		public string ModulAdi{get;set;}

		public string HareketTuru{get;set;}

		public System.Nullable<System.DateTime> KarsiHareketTarihi{get;set;}

		public string KarsiHareketTuru{get;set;}

		public string BorcAlacak{get;set;}

		public string AcikKapali{get;set;}

		public string FisNo{get;set;}

		public string TicariIslemGrubu{get;set;}

		public string DovizKodu{get;set;}

		public string DovizAdi{get;set;}

		public System.Nullable<System.DateTime> VadeTarihi{get;set;}

		public System.Nullable<System.DateTime> OdemeTarihi{get;set;}

		public System.Nullable<int> VadeUyg{get;set;}

		public System.Nullable<int> VadeGrc{get;set;}

		public System.Nullable<double> Toplam{get;set;}

		public System.Nullable<double> Odenen{get;set;}

		public System.Nullable<double> Kalan{get;set;}

		public System.Nullable<int> ChHesapReferansi{get;set;}

		public int EvrakReferansi{get;set;}

		public System.Nullable<int> KarsiIslemReferansi{get;set;}

		public System.Nullable<short> Kur{get;set;}

		public string Isyeri{get;set;}

		public System.Nullable<decimal> GeriOdemeKomisyonOrani{get;set;}

		public string EvrakNo{get;set;}

		public string SatirAciklamasi{get;set;}
	}
}
