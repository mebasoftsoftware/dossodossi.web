﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Models
{
    [Serializable]
    public class PurchaseOrderListApiDto
    {
        public string FieldDescription { get; set; }
        public string orderNumber { get; set; }
        public string orderType { get; set; }
        public string customerPoNo { get; set; }
        public string paymentTerms { get; set; }
        public string paymentTermsCode { get; set; }
        public string orderDate { get; set; }
        public string requestDate { get; set; }
        public string shippingInstructions { get; set; }
        public string carrier1 { get; set; }
        public string carrier1Account { get; set; }
        public string incoTermsCode { get; set; }
        public string incoTermsText { get; set; }
        public string orderComments { get; set; }
        public string type { get; set; }

        public string supplierCode { get; set; }
        public string supplierName { get; set; }

        public List<SatinAlmaSiparisItemApiDto> orderItems { get; set; }
        public SatinAmaSiparisShipDetailDto supplierDetails{ get; set; }
        public SiparisShipDetailDto shipToDetails { get; set; }
        public PurchaseOrderListApiDto()
        {
            orderItems = new List<SatinAlmaSiparisItemApiDto>();
            supplierDetails = new SatinAmaSiparisShipDetailDto();
            shipToDetails = new SiparisShipDetailDto();
        }
    }

    [Serializable]
    public class SatinAlmaSiparisItemApiDto
    {
        public string itemNo { get; set; }
        public string itemName { get; set; }
        public string itemCode { get; set; }
        public string lineSequenceNo { get; set; }
        public string quantityOrdered { get; set; }
        public string takedQuantity { get; set; }
        public string remainingQuantity { get; set; }
        public string uom { get; set; }
    }

    [Serializable]
    public class SatinAmaSiparisShipDetailDto
    {
        public string code { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string address3 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string countryCode { get; set; }
        public string countryName { get; set; }
        public string postalCode { get; set; }
    }
}
