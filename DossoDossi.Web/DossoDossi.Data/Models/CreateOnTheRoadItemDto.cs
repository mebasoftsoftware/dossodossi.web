﻿namespace DossoDossi.Data.Models
{
    public class CreateOnTheRoadItemDto
    {
        public string LineNo { get; set; }

        public double Qty { get; set; }

        public int ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }

        public string Note { get; set; }

        public string UnitCode { get; set; }
     
    }
}
