﻿namespace DossoDossi.Data.Models
{
    public class UrunAmbarToplamDto
    {
        public int AmbarNo { get; set; }
        public string Ambar { get; set; }
        public float? StokMiktari { get; set; }
        public int STOCKREF { get; set; }
    }

}
