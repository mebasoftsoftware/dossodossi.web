﻿using DossoDossi.Data.Entity;
using System;
using System.Collections.Generic;

namespace DossoDossi.Data.Models
{
    public class CreateOfferApiModel
    {
        public Guid MusteriId{ get; set; }
        public string ProjeKodu{ get; set; }
        public List<ProjeTedarikciUrunler> Urunlers { get; set; }

    }
}
