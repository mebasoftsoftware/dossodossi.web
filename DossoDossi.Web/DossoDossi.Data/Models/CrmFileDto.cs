﻿using System;

namespace DossoDossi.Data.Models
{
    public class DossoDossiFileDto
    {
        public Guid Id{ get; set; }
        public int TableId{ get; set; }
        public string RecordId{ get; set; }
        public string FileName{ get; set; }
        public string FilePath{ get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
