﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Models
{
    public class CurListType
    {
        public string Code { get; set; }
        public short Type { get; set; }
    }
}
