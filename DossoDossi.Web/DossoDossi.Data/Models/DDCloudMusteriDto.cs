﻿
using System;

namespace DossoDossi.Data.Models
{
   public class DDCloudMusteriDto
    {
        public Guid MusteriId { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Ulke { get; set; }
        public string Sehir { get; set; }
        public string Telefon { get; set; }
        public string MusteriKodu { get; set; }
        public int nFirmaId { get; set; }
    }
}
