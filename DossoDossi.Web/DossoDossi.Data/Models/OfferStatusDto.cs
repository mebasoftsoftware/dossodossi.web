﻿namespace DossoDossi.Data.Models
{
    public class OfferStatusDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string BackColor{ get; set; }
        public string TextColor{ get; set; }
    }
}
