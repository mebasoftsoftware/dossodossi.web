﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Models
{
    public class ButcelerDto
    {
        public string Yil { get; set; }

        public string HesapKodu { get; set; }

        public string HesapAdi { get; set; }

        public string HesapAdiEn { get; set; }

        public string MasrafMerkeziKodu { get; set; }

        public string Proje { get; set; }

        public string Urun { get; set; }

        public string Ocak { get; set; }

        public string Subat { get; set; }

        public string Mart { get; set; }

        public string Nisan { get; set; }

        public string Mayis { get; set; }

        public string Haziran { get; set; }

        public string Temmuz { get; set; }

        public string Agustos { get; set; }

        public string Eylul { get; set; }

        public string Ekim { get; set; }

        public string Kasim { get; set; }

        public string Aralik { get; set; }

        public string Aciklama { get; set; }
    }
}
