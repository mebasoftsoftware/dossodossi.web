﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Models
{
     public class LogoCustomerDeliveryAddressRequestDto
    {
        public int CariId { get; set; }

        public string AdresKodu { get; set; }

        public string AdresAciklama { get; set; }
        public string AdresAlan1 { get; set; }
        public string AdresAlan2 { get; set; }
        public string UlkeKodu { get; set; }
        public string UlkeAdi { get; set; }
        public string SehirKodu { get; set; }
        public string SehirAdi { get; set; }
        public string IlceKodu { get; set; }
        public string IlceAdi { get; set; }
        public string SemtKodu { get; set; }
        public string SemtAdi { get; set; }
        public string PostaKodu { get; set; }
        public string FaksNo { get; set; }
        public string TelefonNo1 { get; set; }
        public string TelefonNo2 { get; set; }

        public string Ilgili { get; set; }

        public string EPostaAdresi { get; set; }


    }
}
