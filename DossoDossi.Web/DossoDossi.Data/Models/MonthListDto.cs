﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Models
{
    public class MonthListDto
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public string Title { get; set; }
    }
}
