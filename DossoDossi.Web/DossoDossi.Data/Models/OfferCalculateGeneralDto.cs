﻿using System;
using System.Collections.Generic;

namespace DossoDossi.Data.Models
{
   public class OfferCalculateGeneralDto
    {
        public double TotalDiscount { get; set; }

        public string TotalDiscountStr { get; set; }

        public double TotalAmount { get; set; }

        public double? TotalShipment { get; set; }

        public string TotalAmountStr { get; set; }

        public double TotalNet { get; set; }
        public double VatTotal { get; set; }
        public string VatTotalStr { get; set; }

        public string TotalNetStr { get; set; }

        public string Currency { get; set; }

        public List<OfferCalculateDto> Items { get; set; }

    }
}
