﻿using System;
using System.Collections.Generic;

namespace DossoDossi.Data.Models
{
    public class OrderDto
    {
        public OrderDto()
        {
            //varsayılan olarak 7 hane
            LineRound = 7;
            Items = new List<OrderItemDto>();
        }

        public int FisId { get; set; }
        public int FisNo { get; set; }
        public int OrderId { get; set; }
        public string SaticiKodu { get; set; }
        public string CariKodu { get; set; }
        public int FirmNo { get; set; }
        public int OfferId { get; set; }
        public string VadeKodu { get; set; }
        public string MusteriSiparisNo { get; set; }

        public string Currency{ get; set; }
        public decimal ExchangeRate { get; set; }
        public string SevAdresKodu { get; set; }
        public string BelgeNo { get; set; }
        public string TeslimSekli { get; set; }
        public string OzelKodu { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? TeslimTarihi { get; set; }
        public List<OrderItemDto> Items { get; set; }
        /// <summary>
        /// Satır yuvalrma adedi virgulden sonra
        /// </summary>
        public int LineRound { get; set; }

        public string IsYeri { get; set; }
        public string Bolum { get; set; }
        public string AmbarNo { get; set; }
        public string TasiyiciKodu { get; set; }
        public string IzlemeNumarasi { get; set; }
        public string Aciklama3 { get; set; }

        public bool? KampanyaUygula { get; set; }
        public string Note1 { get; set; }
        public string Note2 { get; set; }
        public string Note3 { get; set; }
        public int? SevkId { get; set; }
     

        public double? KargoBedeli { get; set; }

    }
}
