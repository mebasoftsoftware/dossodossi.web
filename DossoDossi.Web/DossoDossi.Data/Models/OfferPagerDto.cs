﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Models
{
   public class OfferPagerDto
    {
        public int CurrentPage { get; set; }
        public string Filter { get; set; }
        public string Order { get; set; }
        public int TakeCount { get; set; }


        public int? Status{ get; set; }
        public int? CreateUserId { get; set; }
        public int? UserParentId { get; set; }
        public string StatusMultiple { get; set; }
        public string TeklifNo { get; set; }

        public string CustomerName { get; set; }

        public int? IsYeri { get; set; }

        public DateTime? Start{ get; set; }
        public DateTime? End{ get; set; }

    }
}
