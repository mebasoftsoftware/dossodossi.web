﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Models
{
    public class AmbarTransferAktarimDto
    {
        public int CekilecekAmbarNo { get; set; }
        public string CekilecekAmbarAdi { get; set; }
        public int HedefAmbarNo { get; set; }
        public string HedefAmbarAdi { get; set; }
        public string IslemiYapan { get; set; }
        public string BelgeNo { get; set; }
        public string Aciklama1 { get; set; }
        public string Aciklama2 { get; set; }

        public int? WebSiparisId { get; set; }
        public int FirmNo { get; set; }

        public List<AmbarAktarimSatirDto> Satirlar { get; set; }
    }
}
