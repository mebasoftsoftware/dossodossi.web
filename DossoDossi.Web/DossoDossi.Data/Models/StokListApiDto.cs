﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Models
{
    [Serializable]
   public class StokListApiDto
    {
        public string itemCode { get; set; }
        public string itemName{ get; set; }
        public string itemStatus{ get; set; }
        public string groupCode{ get; set; }
        public string speCode{ get; set; }
        public string uom{ get; set; }
        public string erpStockAmount{ get; set; }
    }
}
