﻿using System;

namespace DossoDossi.Data.Models
{
    public class DailyReportDetailDto
    {
        public int STOCKREF{ get; set; }
        public string CODE{ get; set; }
        public string FICHENO { get; set; }
        public string COUNTRY { get; set; }
        public string SPECODE { get; set; }
        public DateTime? DATE_{ get; set; }
        public string NAME{ get; set; }
        public string CLNAME{ get; set; }
        public string CLCODE{ get; set; }
        public double AMOUNT{ get; set; }
        public double PRICE{ get; set; }
        public double ALPRICE{ get; set; }

    }
}
