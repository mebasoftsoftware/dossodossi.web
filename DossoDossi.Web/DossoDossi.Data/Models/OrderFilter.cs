﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Models
{
   public class OrderFilter
    {
        public int CurrentPage { get; set; }
        public string Filter { get; set; }
        public string Order { get; set; }
        public int TakeCount { get; set; }

        public int? Isyeri{ get; set; }
        public string SaticiKodu{ get; set; }

        public string CariAdi{ get; set; }

        public string MalzemeAdi { get; set; }

        public DateTime? Start{ get; set; }

        public DateTime? End { get; set; }
    }
}
