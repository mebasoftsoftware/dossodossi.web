﻿using DossoDossi.Data.DataAccess;
using DossoDossi.Data.Helper;
using DossoDossi.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityObjects;

namespace DossoDossi.Data.Tiger
{
  public  class LogoAdresService :App
    {
        public ResponseMessage CreateCustomerTigerDeliveryAddress(LogoCustomerDeliveryAddressRequestDto model)
        {
            var resp = new ResponseMessage();
            try
            {
                UnityObjects.Data address = tiger.NewDataObject(DataObjectType.doArpShipLic);
                address.New();
                // 15.01.2016 "ARP_CODE" B2B_MuhasebeHesapKodlari view'inden gelen cardRef e göre CODE'dir.
                var customer = CustomerDataAccess.GetById(model.CariId);
                if (customer == null)
                {
                    resp.Status = false;
                    resp.Message = "" + model.CariId;
                    return resp;
                }

                address.DataFields.FieldByName("ARP_CODE").Value = customer.ChKodu;   // "Cari hesap kodu";
                address.DataFields.FieldByName("CODE").Value = model.AdresKodu;      // "Kod ";
                address.DataFields.FieldByName("DESCRIPTION").Value = model.AdresAciklama.ToUpperCase().StringCut(50);  // Açıklama";
                address.DataFields.FieldByName("ADDRESS1").Value = model.AdresAlan1.ToUpperCase();     // "Adres Alanı 1";
                address.DataFields.FieldByName("ADDRESS2").Value = model.AdresAlan2.ToUpperCase();     // "Adres Alanı 1";
                address.DataFields.FieldByName("DISTRICT_CODE").Value = model.SemtKodu; //semt kodu
                address.DataFields.FieldByName("DISTRICT").Value = model.SemtAdi; //semt adı
                address.DataFields.FieldByName("TOWN_CODE").Value = model.IlceKodu;  //ilçe kodu
                address.DataFields.FieldByName("TOWN").Value = model.IlceAdi; //ilçe adı
                address.DataFields.FieldByName("CITY_CODE").Value = model.SehirKodu;  //şehir kodu
                address.DataFields.FieldByName("CITY").Value = model.SehirAdi; //şehir adı
                address.DataFields.FieldByName("COUNTRY_CODE").Value = model.UlkeKodu;
                address.DataFields.FieldByName("COUNTRY").Value = model.UlkeAdi;
                address.DataFields.FieldByName("POSTAL_CODE").Value = model.PostaKodu;
                address.DataFields.FieldByName("TELEPHONE1").Value = ReplacePhone(model.TelefonNo1);
                address.DataFields.FieldByName("TELEPHONE2").Value = ReplacePhone(model.TelefonNo2);
                address.DataFields.FieldByName("FAX").Value = ReplacePhone(model.FaksNo); //faks no 
                address.DataFields.FieldByName("INCHANGE").Value = model.Ilgili.ToUpperCase(); //ilgili kişi
                address.DataFields.FieldByName("EMAIL_ADDR").Value = model.EPostaAdresi; //email adres

                ValidateErrors err = address.ValidateErrors;

                if (address.Post())
                {
                    resp.Data = (string)address.DataFields.FieldByName("CODE").Value;
                    resp.Status= true;
                    resp.Message = "OK";
                }
                else
                {
                    resp.Status = false;
                    resp.Message = address.ErrorDesc;
                    for (int i = 0; i < err.Count; i++)
                    {
                        var stra = $"{err[i].Error} - {err[i].ID};";
                        resp.Message = resp.Message + "<br/>" + stra;
                    }
                }

                return resp;
            }
            catch (Exception ex)
            {
                resp.Status = false;
                resp.Message = "Error/Hata " + ex.Message;
                return resp;
            }
        }

        private static string ReplacePhone(string title)
        {
            if (string.IsNullOrEmpty(title))
                return title;

            return title.Replace(@"(", string.Empty)
              .Replace(@")", string.Empty)
              .Replace(@"+", string.Empty)
              .Replace(@"-", string.Empty)
              .Trim();
        }
    }
}
