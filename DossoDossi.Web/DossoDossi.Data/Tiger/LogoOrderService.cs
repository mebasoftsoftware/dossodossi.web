﻿using DossoDossi.Data.DataAccess;
using DossoDossi.Data.Helper;
using DossoDossi.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityObjects;

namespace DossoDossi.Data.Tiger
{
    public class LogoOrderService : App
    {
        public ResponseMessage CreateOrder(OrderDto Order)
        {
            try
            {
                var resp = new ResponseMessage();
                var nfi = new NumberFormatInfo
                {
                    NumberDecimalSeparator = ".",
                    NumberGroupSeparator = ""
                };

                UnityObjects.Data siparis = tiger.NewDataObject(DataObjectType.doSalesOrderSlip);

                siparis.New();
                var now = DateTime.Now;

                object myDate = null;
                tiger.PackDate(Order.CreateDate.Day, Order.CreateDate.Month, Order.CreateDate.Year, ref myDate);

                object myTime = null;
                tiger.PackTime(Order.CreateDate.Hour, Order.CreateDate.Minute, Order.CreateDate.Second, ref myTime);

                siparis.DataFields.FieldByName("NUMBER").Value = "~"; // "Sipariş No";
                siparis.DataFields.FieldByName("AUXIL_CODE").Value = Order.OzelKodu;
                siparis.DataFields.FieldByName("DATE").Value = myDate; // "Tarih";
                siparis.DataFields.FieldByName("TIME").Value = myTime; //"Zaman";

                siparis.DataFields.FieldByName("ARP_CODE").Value = Order.CariKodu;
                siparis.DataFields.FieldByName("DOC_TRACK_NR").Value = ""; // Doküman İzleme Numarası;
                siparis.DataFields.FieldByName("DOC_NUMBER").Value = Order.BelgeNo;
                siparis.DataFields.FieldByName("SALESMAN_CODE").Value = Order.SaticiKodu; // "Satıcı Kodu";
                siparis.DataFields.FieldByName("PAYMENT_CODE").Value = Order.VadeKodu; // "V045";  
                siparis.DataFields.FieldByName("SHIPLOC_CODE").Value = Order.SevAdresKodu;
                siparis.DataFields.FieldByName("ORDER_STATUS").Value = 4;

                siparis.DataFields.FieldByName("NOTES1").Value = Order.Note1;
                siparis.DataFields.FieldByName("NOTES2").Value = Order.Note2;

                //TODO bu 2 alanın kod alanı lazım.
                siparis.DataFields.FieldByName("DIVISION").Value = Order.IsYeri;
                siparis.DataFields.FieldByName("DEPARTMENT").Value = Order.Bolum;

                siparis.DataFields.FieldByName("SOURCE_WH").Value = Order.AmbarNo;
                siparis.DataFields.FieldByName("CURRSEL_TOTAL").Value = 1;
                siparis.DataFields.FieldByName("CURRSEL_DETAILS").Value = 2;
                siparis.DataFields.FieldByName("CUST_ORD_NO").Value = Order.MusteriSiparisNo;
                siparis.DataFields.FieldByName("ITEXT").Value = Order.MusteriSiparisNo;

                siparis.DataFields.FieldByName("SHIPPING_AGENT").Value = Order.TasiyiciKodu;

                Lines detay = siparis.DataFields.FieldByName("TRANSACTIONS").Lines;

                int index = 0;
                foreach (var item in Order.Items)
                {
                    if (detay.AppendLine())
                    {
                        //eğer iskonto var ise birim fiyat revize olur.
                        if (item.Discount.HasValue && item.Discount.Value > 0)
                        {
                            item.Price = item.Price - (item.Price * (item.Discount.Value) / 100);
                        }

                        detay[index].FieldByName("TYPE").Value = 0; // Hizmet
                        detay[index].FieldByName("MASTER_CODE").Value = item.ProductCode; //"Ürün Kodu";
                        detay[index].FieldByName("UNIT_CODE").Value = item.UnitCode; // "Ürün Birimi (Adet,KG,Metre gibi)";
                        detay[index].FieldByName("QUANTITY").Value = item.Qty; // 1;  //"Sayısı";
                        detay[index].FieldByName("PRICE").Value = Math.Round(((decimal)item.Price), Order.LineRound).ToString(nfi);
                        detay[index].FieldByName("PC_PRICE").Value = Math.Round(item.Price, Order.LineRound).ToString(nfi);
                        detay[index].FieldByName("EXCLINE_PRICE").Value = Math.Round(item.Price, Order.LineRound).ToString(nfi);
                        detay[index].FieldByName("EDT_PRICE").Value = Math.Round(item.Price, Order.LineRound).ToString(nfi);
                        detay[index].FieldByName("VAT_RATE").Value = item.Vat.ToString(nfi); // "KDV";
                        detay[index].FieldByName("RC_XRATE").Value = 1; // "KDV";
                        detay[index].FieldByName("VAT_INCLUDED").Value = 0; //KDV Hariç
                        detay[index].FieldByName("SOURCE_WH").Value = Order.AmbarNo;
                        detay[index].FieldByName("PAYMENT_CODE").Value = Order.VadeKodu;
                        detay[index].FieldByName("AUXIL_CODE").Value = item.CustomerPrCode;
                        detay[index].FieldByName("DELVRY_CODE").Value = item.TeslimatKodu;
                        detay[index].FieldByName("DUE_DATE").Value = Order.TeslimTarihi;

                        detay[index].FieldByName("TRANS_DESCRIPTION").Value = item.Note;

                        //teslimat kodu alanına indirim oranı basıldı
                        detay[index].FieldByName("DELVRY_CODE").Value = (item.Discount.HasValue ? item.Discount.Value : 0).ToString();

                        // detay[index].FieldByName("CURR_PRICE").Value = 1; // Dövizli fiyat
                        //  detay[index].FieldByName("TRANS_DESCRIPTION").Value = item.ProductCode;
                        //  detay[index].FieldByName("RC_XRATE").Value = Order.ExchangeRate.ToString(nfi);
                    }

                    index += 1;
                }

                if (Order.KargoBedeli.HasValue && Order.KargoBedeli.Value > 0)
                {
                    var settingCodeShippingCost = SettingDataAccess.GetValByCode("Logo.ShippingCostCode");

                    if (detay.AppendLine())
                    {
                        detay[index].FieldByName("TYPE").Value = 3; // Hizmet
                        detay[index].FieldByName("MASTER_CODE").Value = settingCodeShippingCost;
                        detay[index].FieldByName("DETAIL_LEVEL").Value = 1;
                        detay[index].FieldByName("CALC_TYPE").Value = 1;
                        detay[index].FieldByName("QUANTITY").Value = 0;
                        detay[index].FieldByName("TOTAL").Value = Order.KargoBedeli.Value.ToString("N5", nfi);
                        detay[index].FieldByName("VAT_BASE").Value = Order.KargoBedeli.Value.ToString("N5", nfi);
                        detay[index].FieldByName("VAT_RATE").Value = 0; // "KDV";
                        detay[index].FieldByName("RC_XRATE").Value = 1;
                    }

                    index += 1;
                }

                ValidateErrors err = siparis.ValidateErrors;
                // GL_CODE alanında gönderdiğimiz muhasebe kodunu kendi alır. 
                siparis.FillAccCodes();

                if (Order.KampanyaUygula.HasValue && Order.KampanyaUygula.Value)
                {
                    siparis.ApplyCampaign();
                }

                if (siparis.Post())
                {
                    resp.Status = true;
                    resp.Message = "Succesfully Saved";
                    resp.Data = siparis.DataFields.FieldByName("TOTAL_NET").Value.ToString();
                    resp.Code = siparis.DataFields.DBFieldByName("LOGICALREF").Value.ToString();

                    OffersDataAccess.UpdateFisId(Order.OfferId, resp.Code);
                }
                else
                {
                    resp.Status = false;
                    resp.Code = siparis.ErrorCode.ToString();
                    resp.Message = "Error on occured . err1";
                    resp.Type = siparis.DBErrorDesc;
                    resp.Data = siparis.ErrorDescDetail;

                    for (int i = 0; i < err.Count; i++)
                    {
                        string stra = string.Format("{0} - {1};", err[i].Error, err[i].ID);
                        resp.Message = resp.Message + stra;
                    }
                }

                return resp;
            }
            catch (Exception e)
            {
                return new ResponseMessage
                {
                    Message = "Logo tiger instance problem.Please contact it department Error :  " + e.Message,
                    Code = e.Message + " | " + e.StackTrace
                };
            }
        }

        public ResponseMessage UpdateOrder(OrderDto Order)
        {
            try
            {
                var resp = new ResponseMessage();
                var nfi = new NumberFormatInfo
                {
                    NumberDecimalSeparator = ".",
                    NumberGroupSeparator = ""
                };

                UnityObjects.Data siparis = tiger.NewDataObject(DataObjectType.doSalesOrderSlip);

                siparis.Read(Order.FisId);

                var now = DateTime.Now;

                object myDate = null;
                tiger.PackDate(Order.CreateDate.Day, Order.CreateDate.Month, Order.CreateDate.Year, ref myDate);

                object myTime = null;
                tiger.PackTime(Order.CreateDate.Hour, Order.CreateDate.Minute, Order.CreateDate.Second, ref myTime);

                siparis.DataFields.FieldByName("NUMBER").Value = "~"; // "Sipariş No";
                siparis.DataFields.FieldByName("AUXIL_CODE").Value = Order.OzelKodu;
                siparis.DataFields.FieldByName("DATE").Value = myDate; // "Tarih";
                siparis.DataFields.FieldByName("TIME").Value = myTime; //"Zaman";

                siparis.DataFields.FieldByName("ARP_CODE").Value = Order.CariKodu;
                siparis.DataFields.FieldByName("DOC_TRACK_NR").Value = ""; // Doküman İzleme Numarası;
                //siparis.DataFields.FieldByName("DOC_NUMBER").Value = Order.BelgeNo;
                siparis.DataFields.FieldByName("SALESMAN_CODE").Value = Order.SaticiKodu; // "Satıcı Kodu";
                siparis.DataFields.FieldByName("PAYMENT_CODE").Value = Order.VadeKodu; // "V045";  
                siparis.DataFields.FieldByName("SHIPLOC_CODE").Value = Order.SevAdresKodu;
                siparis.DataFields.FieldByName("ORDER_STATUS").Value = 4;

                siparis.DataFields.FieldByName("NOTES1").Value = Order.Note1;
                siparis.DataFields.FieldByName("NOTES2").Value = Order.Note2;

                //TODO bu 2 alanın kod alanı lazım.
                siparis.DataFields.FieldByName("DIVISION").Value = Order.IsYeri;
                //siparis.DataFields.FieldByName("DEPARTMENT").Value = Order.Bolum;

                siparis.DataFields.FieldByName("SOURCE_WH").Value = Order.AmbarNo;
                siparis.DataFields.FieldByName("CURRSEL_TOTAL").Value = 1;
                siparis.DataFields.FieldByName("CURRSEL_DETAILS").Value = 2;
                siparis.DataFields.FieldByName("CUST_ORD_NO").Value = Order.MusteriSiparisNo;

                siparis.DataFields.FieldByName("SHIPPING_AGENT").Value = Order.TasiyiciKodu;

                Lines detay = siparis.DataFields.FieldByName("TRANSACTIONS").Lines;

                int index = 0;
                foreach (var item in Order.Items)
                {
                    if (detay.AppendLine())
                    {
                        //eğer iskonto var ise birim fiyat revize olur.
                        if (item.Discount.HasValue && item.Discount.Value > 0)
                        {
                            item.Price = item.Price - (item.Price * (item.Discount.Value) / 100);
                        }

                        detay[index].FieldByName("TYPE").Value = 0; // Hizmet
                        detay[index].FieldByName("MASTER_CODE").Value = item.ProductCode; //"Ürün Kodu";
                        detay[index].FieldByName("UNIT_CODE").Value = item.UnitCode; // "Ürün Birimi (Adet,KG,Metre gibi)";
                        detay[index].FieldByName("QUANTITY").Value = item.Qty; // 1;  //"Sayısı";
                        detay[index].FieldByName("PRICE").Value = Math.Round(((decimal)item.Price), Order.LineRound).ToString(nfi);
                        detay[index].FieldByName("PC_PRICE").Value = Math.Round(item.Price, Order.LineRound).ToString(nfi);
                        detay[index].FieldByName("EXCLINE_PRICE").Value = Math.Round(item.Price, Order.LineRound).ToString(nfi);
                        detay[index].FieldByName("EDT_PRICE").Value = Math.Round(item.Price, Order.LineRound).ToString(nfi);
                        detay[index].FieldByName("VAT_RATE").Value = item.Vat.ToString(nfi); // "KDV";
                        detay[index].FieldByName("RC_XRATE").Value = 1; // "KDV";
                        detay[index].FieldByName("VAT_INCLUDED").Value = 0; //KDV Hariç
                        detay[index].FieldByName("SOURCE_WH").Value = Order.AmbarNo;
                        detay[index].FieldByName("PAYMENT_CODE").Value = Order.VadeKodu;
                        detay[index].FieldByName("AUXIL_CODE").Value = item.CustomerPrCode;
                        detay[index].FieldByName("DELVRY_CODE").Value = item.TeslimatKodu;
                        detay[index].FieldByName("DUE_DATE").Value = Order.TeslimTarihi;

                        detay[index].FieldByName("TRANS_DESCRIPTION").Value = item.Note;

                        //teslimat kodu alanına indirim oranı basıldı
                        detay[index].FieldByName("DELVRY_CODE").Value = (item.Discount.HasValue ? item.Discount.Value : 0).ToString();

                        // detay[index].FieldByName("CURR_PRICE").Value = 1; // Dövizli fiyat
                        //  detay[index].FieldByName("TRANS_DESCRIPTION").Value = item.ProductCode;
                        //  detay[index].FieldByName("RC_XRATE").Value = Order.ExchangeRate.ToString(nfi);
                    }

                    index += 1;
                }

                if (Order.KargoBedeli.HasValue && Order.KargoBedeli.Value > 0)
                {
                    var settingCodeShippingCost = SettingDataAccess.GetValByCode("Logo.ShippingCostCode");

                    if (detay.AppendLine())
                    {
                        detay[index].FieldByName("TYPE").Value = 3; // Hizmet
                        detay[index].FieldByName("MASTER_CODE").Value = settingCodeShippingCost;
                        detay[index].FieldByName("DETAIL_LEVEL").Value = 1;
                        detay[index].FieldByName("CALC_TYPE").Value = 1;
                        detay[index].FieldByName("QUANTITY").Value = 0;
                        detay[index].FieldByName("TOTAL").Value = Order.KargoBedeli.Value.ToString("N5", nfi);
                        detay[index].FieldByName("VAT_BASE").Value = Order.KargoBedeli.Value.ToString("N5", nfi);
                        detay[index].FieldByName("VAT_RATE").Value = 0; // "KDV";
                        detay[index].FieldByName("RC_XRATE").Value = 1;
                    }

                    index += 1;
                }

                ValidateErrors err = siparis.ValidateErrors;
                // GL_CODE alanında gönderdiğimiz muhasebe kodunu kendi alır. 
                siparis.FillAccCodes();

                if (Order.KampanyaUygula.HasValue && Order.KampanyaUygula.Value)
                {
                    siparis.ApplyCampaign();
                }

                if (siparis.Post())
                {
                    resp.Status = true;
                    resp.Message = "Succesfully Saved";
                    resp.Data = siparis.DataFields.FieldByName("TOTAL_NET").Value.ToString();
                    resp.Code = siparis.DataFields.DBFieldByName("LOGICALREF").Value.ToString();
                }
                else
                {
                    resp.Status = false;
                    resp.Code = siparis.ErrorCode.ToString();
                    resp.Message = "Error on occured . err1";
                    resp.Type = siparis.DBErrorDesc;
                    resp.Data = siparis.ErrorDescDetail;

                    for (int i = 0; i < err.Count; i++)
                    {
                        string stra = string.Format("{0} - {1};", err[i].Error, err[i].ID);
                        resp.Message = resp.Message + stra;
                    }
                }

                return resp;
            }
            catch (Exception e)
            {
                return new ResponseMessage
                {
                    Message = "Logo tiger instance problem.Please contact it department Error :  " + e.Message,
                    Code = e.Message + " | " + e.StackTrace
                };
            }
        }

        public ResponseMessage DeleteSalesOrder(int id)
        {
            var resp = new ResponseMessage();

            try
            {
                var siparis = tiger.NewDataObject(DataObjectType.doSalesOrderSlip);

                ValidateErrors errSiparis = siparis.ValidateErrors;
                siparis.Delete(id);

                if (siparis.ErrorCode != 0)
                {
                    resp.Status = false;
                    resp.Message = "Delete error : Msg = " + siparis.ErrorDesc;
                }
                else
                {
                    resp.Status = true;
                    resp.Message = "Sales order was deleted. Successfully !";
                }

            }
            catch (Exception ex)
            {
                resp.Status = false;
                resp.Message = "Tiger instance error : Msg = " + ex.Message;
                resp.ErrorMessage = ex.StackTrace;
            }

            return resp;
        }

        public ResponseMessage CreatePurchOrder(OrderDto Order)
        {

            try
            {
                var resp = new ResponseMessage();
                var nfi = new NumberFormatInfo
                {
                    NumberDecimalSeparator = ".",
                    NumberGroupSeparator = ""
                };

                UnityObjects.Data siparis = tiger.NewDataObject(DataObjectType.doPurchOrderSlip);

                siparis.New();
                var now = DateTime.Now;

                object myDate = null;
                tiger.PackDate(now.Day, now.Month, now.Year, ref myDate);

                object myTime = null;
                tiger.PackTime(now.Hour, now.Minute, now.Second, ref myTime);

                siparis.DataFields.FieldByName("NUMBER").Value = "~"; // "Sipariş No";
                siparis.DataFields.FieldByName("AUXIL_CODE").Value = Order.OzelKodu;
                siparis.DataFields.FieldByName("DATE").Value = myDate; // "Tarih";
                siparis.DataFields.FieldByName("TIME").Value = myTime; //"Zaman";

                siparis.DataFields.FieldByName("ARP_CODE").Value = Order.CariKodu;
                siparis.DataFields.FieldByName("DOC_TRACK_NR").Value = Order.MusteriSiparisNo; // Doküman İzleme Numarası;
                siparis.DataFields.FieldByName("CUST_ORD_NO").Value = Order.MusteriSiparisNo;

                siparis.DataFields.FieldByName("DOC_NUMBER").Value = Order.BelgeNo;
                siparis.DataFields.FieldByName("SALESMAN_CODE").Value = Order.SaticiKodu; // "Satıcı Kodu";
                siparis.DataFields.FieldByName("PAYMENT_CODE").Value = Order.VadeKodu; // "V045";  

                siparis.DataFields.FieldByName("SHIPLOC_CODE").Value = Order.SevAdresKodu;

                siparis.DataFields.FieldByName("RC_RATE").Value = 1;//raporlama döviz kuru
                siparis.DataFields.FieldByName("ORDER_STATUS").Value = 1;
                siparis.DataFields.FieldByName("NOTES1").Value = Order.Note1;

                siparis.DataFields.FieldByName("DIVISION").Value = Order.IsYeri;
                siparis.DataFields.FieldByName("DEPARTMENT").Value = Order.Bolum;
                siparis.DataFields.FieldByName("SOURCE_WH").Value = Order.AmbarNo;


                siparis.DataFields.FieldByName("CURRSEL_TOTAL").Value = 1;
                siparis.DataFields.FieldByName("CURRSEL_DETAILS").Value = 2;

                //siparis.DataFields.FieldByName("CURR_TRANSACTIN").Value = 53;
                //siparis.DataFields.FieldByName("TC_RATE").Value = 1;

                Lines detay = siparis.DataFields.FieldByName("TRANSACTIONS").Lines;

                int index = 0;
                foreach (var item in Order.Items)
                {
                    if (detay.AppendLine())
                    {
                        detay[index].FieldByName("TYPE").Value = 0; // Hizmet 4 kart 0 olacak

                        detay[index].FieldByName("MASTER_CODE").Value = item.ProductCode; //"Ürün Kodu";

                        detay[index].FieldByName("UNIT_CODE").Value = item.UnitCode; // "Ürün Birimi (Adet,KG,Metre gibi)";

                        detay[index].FieldByName("QUANTITY").Value = item.Qty; // 1;  //"Sayısı";

                        detay[index].FieldByName("PRICE").Value = Math.Round(item.Price, Order.LineRound).ToString(nfi);
                        detay[index].FieldByName("PC_PRICE").Value = Math.Round(item.Price, Order.LineRound).ToString(nfi);

                        detay[index].FieldByName("RC_XRATE").Value = 1;
                        detay[index].FieldByName("PR_RATE").Value = 1;

                        detay[index].FieldByName("VAT_RATE").Value = item.Vat.ToString(nfi); // "KDV";

                        detay[index].FieldByName("VAT_INCLUDED").Value = 0; //KDV Hariç

                        detay[index].FieldByName("SOURCE_WH").Value = Order.AmbarNo;

                        detay[index].FieldByName("TRANS_DESCRIPTION").Value = item.Note;

                        detay[index].FieldByName("DUE_DATE").Value = Order.TeslimTarihi;

                        //  detay[index].FieldByName("RC_XRATE").Value = Order.ExchangeRate.ToString(nfi);
                    }

                    index += 1;
                }

                ValidateErrors err = siparis.ValidateErrors;
                // GL_CODE alanında gönderdiğimiz muhasebe kodunu kendi alır. 
                siparis.FillAccCodes();

                if (siparis.Post())
                {
                    resp.Status = true;
                    resp.Message = "Succesfully Saved !";
                    resp.Data = siparis.DataFields.FieldByName("TOTAL_NET").Value.ToString();
                    resp.Code = siparis.DataFields.DBFieldByName("LOGICALREF").Value.ToString();

                }
                else
                {
                    resp.Status = false;
                    resp.Code = siparis.ErrorCode.ToString();
                    resp.Type = siparis.DBErrorDesc;

                    for (int i = 0; i < err.Count; i++)
                    {
                        string stra = string.Format("{0} - {1};", err[i].Error, err[i].ID);

                        resp.Code = resp.Code + stra;
                    }

                    resp.Message = "An error occured . </br>" + resp.Code;
                }

                return resp;
            }
            catch (Exception e)
            {
                return new ResponseMessage
                {
                    Message = "Logo  instance problem.Please contact it department Error :  " + e.Message,
                    Code = e.Message + " | " + e.StackTrace
                };
            }

        }

        public ResponseMessage<string> CreateDisPatch(int orderLogicalRef, OrderDto Order)
        {

            var resp = new ResponseMessage<string>();

            NumberFormatInfo nfi = new NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            nfi.NumberGroupSeparator = "";
            nfi.PercentDecimalDigits = 7;

            try
            {
                var customerCode = "";
                UnityObjects.Data siparis = tiger.NewDataObject(DataObjectType.doSalesOrderSlip);

                siparis.Read(orderLogicalRef);

                UnityObjects.Data irsaliye = tiger.NewDataObject(DataObjectType.doSalesDispatch); // DataObjectType.doSalesDispatch

                irsaliye.New();

                var now = DateTime.Now;

                object myDate = null;
                tiger.PackDate(now.Day, now.Month, now.Year, ref myDate);

                object myTime = null;
                tiger.PackTime(now.Hour, now.Minute, now.Second, ref myTime);


                customerCode = (string)siparis.DataFields.FieldByName("ARP_CODE").Value;
                irsaliye.DataFields.FieldByName("NUMBER").Value = "~"; // "Sipariş No";
                irsaliye.DataFields.FieldByName("TYPE").Value = 8;
                irsaliye.DataFields.FieldByName("DOC_NUMBER").Value = siparis.DataFields.FieldByName("DOC_NUMBER").Value;
                irsaliye.DataFields.FieldByName("AUXIL_CODE").Value = siparis.DataFields.FieldByName("AUXIL_CODE").Value;

                irsaliye.DataFields.FieldByName("DATE").Value = myDate; // "Tarih";
                irsaliye.DataFields.FieldByName("SHIP_DATE").Value = myDate; // "Tarih";
                irsaliye.DataFields.FieldByName("DOC_DATE").Value = myDate; // "Tarih";
                irsaliye.DataFields.FieldByName("TIME").Value = myTime; //"Zaman";
                irsaliye.DataFields.FieldByName("DOC_TIME").Value = myTime; //"Zaman";
                irsaliye.DataFields.FieldByName("SHIP_TIME").Value = myTime; //"Zaman";

                irsaliye.DataFields.FieldByName("ARP_CODE").Value = siparis.DataFields.FieldByName("ARP_CODE").Value;
                //irsaliye.DataFields.FieldByName("GL_CODE").Value = siparis.DataFields.FieldByName("GL_CODE").Value;
                irsaliye.DataFields.FieldByName("RC_RATE").Value = siparis.DataFields.FieldByName("RC_RATE").Value;

                irsaliye.DataFields.FieldByName("SALESMANCODE").Value = siparis.DataFields.FieldByName("SALESMAN_CODE").Value;
                irsaliye.DataFields.FieldByName("PAYMENT_CODE").Value = siparis.DataFields.FieldByName("PAYMENT_CODE").Value;
                irsaliye.DataFields.FieldByName("SHIPLOC_CODE").Value = siparis.DataFields.FieldByName("SHIPLOC_CODE").Value;


                irsaliye.DataFields.FieldByName("NOTES1").Value = siparis.DataFields.FieldByName("NOTES1").Value;
                irsaliye.DataFields.FieldByName("NOTES2").Value = Order.Note2;
                irsaliye.DataFields.FieldByName("NOTES3").Value = Order.Note3;
                irsaliye.DataFields.FieldByName("NOTES4").Value = siparis.DataFields.FieldByName("NOTES4").Value;


                irsaliye.DataFields.FieldByName("CURRSEL_TOTALS").Value =
              siparis.DataFields.FieldByName("CURRSEL_TOTAL").Value;
                irsaliye.DataFields.FieldByName("CURRSEL_DETAILS").Value =
                    siparis.DataFields.FieldByName("CURRSEL_DETAILS").Value;
                irsaliye.DataFields.FieldByName("ITEXT").Value = siparis.DataFields.FieldByName("ITEXT").Value;
                irsaliye.DataFields.FieldByName("DOC_TRACK_NR").Value = Order.IzlemeNumarasi;

                irsaliye.DataFields.FieldByName("DIVISION").Value = siparis.DataFields.FieldByName("DIVISION").Value;

                irsaliye.DataFields.FieldByName("SHIPPING_AGENT").Value = Order.TasiyiciKodu;


                //Lines siparissatir = siparis.DataFields.FieldByName("TRANSACTIONS").Lines;

                Lines irsaliyesatir = irsaliye.DataFields.FieldByName("TRANSACTIONS").Lines;

                //var siparislerSatirlar = OrderDataAccess.GetDetayListById(orderLogicalRef);
                var siparislerSatirlar = OrderDataAccess.GetSiparislerByFisNos(Order.Items.Select(a => a.FisNo).Distinct().ToList());

                //int satirCount = siparissatir.Count;

                var settingCodeShippingCost = SettingDataAccess.GetValByCode("Logo.ShippingCostCode");


                var kargoList = Order.Items.Where(a => a.ProductCode == settingCodeShippingCost).ToList();

                int index = 0;

                for (int i = 0; i < Order.Items.Where(a => a.ProductCode != settingCodeShippingCost).Count(); i++)
                {
                    var orderItem = Order.Items.Where(a => a.ProductCode != settingCodeShippingCost).ToArray()[i];

                    var malzeme = siparislerSatirlar.Where(a => a.MalzemeKodu != settingCodeShippingCost).FirstOrDefault(a => a.SatirId == orderItem.SatirId);

                    if (irsaliyesatir.AppendLine())
                    {
                        irsaliyesatir[i].FieldByName("TYPE").Value = 0;
                        irsaliyesatir[i].FieldByName("MASTER_CODE").Value = malzeme.MalzemeKodu;
                        irsaliyesatir[i].FieldByName("QUANTITY").Value = orderItem.Qty;
                        irsaliyesatir[i].FieldByName("PRICE").Value = malzeme.FiyatTL.Value;
                        irsaliyesatir[i].FieldByName("PC_PRICE").Value = malzeme.FiyatTL.Value;
                        irsaliyesatir[i].FieldByName("DESCRIPTION").Value = orderItem.Description;
                        irsaliyesatir[i].FieldByName("EDT_PRICE").Value = Math.Round(malzeme.FiyatTL.Value, Order.LineRound);
                        irsaliyesatir[i].FieldByName("TC_XRATE").Value = 1;
                        irsaliyesatir[i].FieldByName("RC_XRATE").Value = 1;
                        irsaliyesatir[i].FieldByName("UNIT_CODE").Value = malzeme.Birim;
                        irsaliyesatir[i].FieldByName("ORDER_REFERENCE").Value = orderItem.SatirId;
                    }

                    index = index+  1;
                }

                var siparislerSatirlarKargo = siparislerSatirlar.Where(a => a.MalzemeKodu == settingCodeShippingCost);

                if (siparislerSatirlarKargo.Any() && Order.Items.Any(a => a.ProductCode == settingCodeShippingCost))
                {
                    if (irsaliyesatir.AppendLine())
                    {
                        irsaliyesatir[index].FieldByName("TYPE").Value = 3; // Hizmet
                        irsaliyesatir[index].FieldByName("MASTER_CODE").Value = settingCodeShippingCost;
                        irsaliyesatir[index].FieldByName("DETAIL_LEVEL").Value = 1;
                        irsaliyesatir[index].FieldByName("RC_XRATE").Value = 1;

                        irsaliyesatir[index].FieldByName("DISCEXP_CALC").Value = 1;

                        irsaliyesatir[index].FieldByName("QUANTITY").Value = 0;
                        irsaliyesatir[index].FieldByName("TOTAL").Value = siparislerSatirlarKargo.Sum(a => a.FiyatTL.Value);
                        irsaliyesatir[index].FieldByName("VAT_BASE").Value =siparislerSatirlarKargo.Sum(a => a.FiyatTL.Value);
                        irsaliyesatir[index].FieldByName("EXCHLINE_TOTAL").Value =siparislerSatirlarKargo.Sum(a => a.FiyatTL.Value);
                        irsaliyesatir[index].FieldByName("EXCHLINE_VAT_MATRAH").Value =siparislerSatirlarKargo.Sum(a => a.FiyatTL.Value);

                        if(siparislerSatirlarKargo.Count() == 1)
                        {
                            irsaliyesatir[index].FieldByName("ORDER_REFERENCE").Value = siparislerSatirlarKargo.FirstOrDefault().SatirId;
                        }
                    }

                    index += 1;
                }

                ValidateErrors err = irsaliye.ValidateErrors;
                irsaliye.FillAccCodes();


                if (irsaliye.Post())
                {
                    var no = (string)irsaliye.DataFields.FieldByName("NUMBER").Value;
                    resp.Code = no;
                    resp.Status = true;
                    resp.Message = "Sales dispatch succesfully created.New Erp dispatch number  = " + no;
                    resp.Data = no;
                }
                else
                {
                    resp.Status = false;
                    resp.Code = irsaliye.ErrorCode.ToString() + irsaliye.DBErrorDesc;
                    resp.Message = irsaliye.ErrorDesc + irsaliye.DBErrorDesc;

                    resp.ErrorMessage = irsaliye.ErrorDescDetail + irsaliye.ErrorDesc + irsaliye.ErrorCode + irsaliye.DBErrorDesc;

                    for (int i = 0; i < err.Count; i++)
                    {
                        string stra = string.Format("{0} - {1};", err[i].Error, err[i].ID);
                        resp.Message = resp.Message + stra;
                    }

                }

                return resp;
            }
            catch (Exception e)
            {
                return new ResponseMessage<string>
                {
                    Message = "Logo  instance problem.Please contact it department Error :  " + e.Message,
                    Code = e.Message + " | " + e.StackTrace,
                    Type = e.StackTrace,
                    ErrorMessage = e.StackTrace
                };
            }
        }

        public ResponseMessage<string> DeleteDisPatch(int irsaliyeId)
        {
            var resp = new ResponseMessage<string>
            {
                Type = "Sales_Dispatch_Delete"
            };

            try
            {

                UnityObjects.Data irsaliye = tiger.NewDataObject(DataObjectType.doSalesDispatch);

                irsaliye.Delete(irsaliyeId);

                if (irsaliye.ErrorCode != 0)
                {
                    resp.Status = false;
                    resp.Message = irsaliye.ErrorDesc;
                    resp.ErrorMessage = irsaliye.DBErrorDesc;
                    resp.Code = irsaliye.ErrorCode.ToString();
                    return resp;
                }

                resp.Message = "Deleted successfully";
                resp.Status = true;
                return resp;
            }
            catch (Exception e)
            {
                return new ResponseMessage<string>
                {
                    Message = "Logo instance problem.Please contact it department Error :  " + e.Message,
                    Code = e.Message + " | " + e.StackTrace,
                    Type = "Sales_Dispatch_Delete"
                };
            }
        }

        public ResponseMessage<string> CreatePurchaseDisPatch(int fisId, PurchaseOrderDeliveryDto request)
        {
            var resp = new ResponseMessage<string>();
            NumberFormatInfo nfi = new NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            nfi.NumberGroupSeparator = "";
            nfi.PercentDecimalDigits = 7;

            try
            {
                var now = DateTime.Now;

                object myDate = null;

                tiger.PackDate(now.Day, now.Month, now.Year, ref myDate);

                object myTime = null;
                tiger.PackTime(now.Hour, now.Minute, now.Second, ref myTime);

                var siparis = tiger.NewDataObject(DataObjectType.doPurchOrderSlip);

                siparis.Read(fisId);

                var irsaliye = tiger.NewDataObject(DataObjectType.doPurchDispatch); // DataObjectType.doSalesDispatch

                irsaliye.New();

                irsaliye.DataFields.FieldByName("NUMBER").Value = "~"; // "Sipariş No";
                irsaliye.DataFields.FieldByName("TYPE").Value = 1;
                irsaliye.DataFields.FieldByName("DOC_NUMBER").Value = request.orderNo;

                irsaliye.DataFields.FieldByName("AUXIL_CODE").Value = siparis.DataFields.FieldByName("AUXIL_CODE").Value;

                irsaliye.DataFields.FieldByName("DATE").Value = myDate; // "Tarih";
                irsaliye.DataFields.FieldByName("TIME").Value = myTime; //"Zaman";

                irsaliye.DataFields.FieldByName("DOC_DATE").Value = myDate;
                irsaliye.DataFields.FieldByName("DOC_TIME").Value = myTime;

                // irsaliye.DataFields.FieldByName("DATE_CREATED").Value = myDate;

                irsaliye.DataFields.FieldByName("ARP_CODE").Value = siparis.DataFields.FieldByName("ARP_CODE").Value;

                irsaliye.DataFields.FieldByName("RC_RATE").Value = siparis.DataFields.FieldByName("RC_RATE").Value;
                irsaliye.DataFields.FieldByName("SALESMANCODE").Value = siparis.DataFields.FieldByName("SALESMAN_CODE").Value;
                irsaliye.DataFields.FieldByName("PAYMENT_CODE").Value = siparis.DataFields.FieldByName("PAYMENT_CODE").Value;
                irsaliye.DataFields.FieldByName("SHIPLOC_CODE").Value = siparis.DataFields.FieldByName("SHIPLOC_CODE").Value;
                irsaliye.DataFields.FieldByName("NOTES1").Value = siparis.DataFields.FieldByName("NOTES1").Value;
                irsaliye.DataFields.FieldByName("NOTES2").Value = siparis.DataFields.FieldByName("NOTES2").Value;
                irsaliye.DataFields.FieldByName("NOTES3").Value = siparis.DataFields.FieldByName("NOTES3").Value;
                irsaliye.DataFields.FieldByName("NOTES4").Value = siparis.DataFields.FieldByName("NOTES4").Value;

                irsaliye.DataFields.FieldByName("CURRSEL_TOTALS").Value = siparis.DataFields.FieldByName("CURRSEL_TOTAL").Value;
                irsaliye.DataFields.FieldByName("CURRSEL_DETAILS").Value = siparis.DataFields.FieldByName("CURRSEL_DETAILS").Value;

                irsaliye.DataFields.FieldByName("DOC_TRACK_NR").Value =
                    siparis.DataFields.FieldByName("DOC_TRACK_NR").Value;

                irsaliye.DataFields.FieldByName("CURR_TRANSACTION").Value =
                    siparis.DataFields.FieldByName("CURR_TRANSACTIN").Value; // İşlem döviz cinsi 
                irsaliye.DataFields.FieldByName("TC_RATE").Value =
                    siparis.DataFields.FieldByName("TC_RATE").Value;


                irsaliye.DataFields.FieldByName("ITEXT").Value = siparis.DataFields.FieldByName("ITEXT").Value;

                Lines irsaliyesatir = irsaliye.DataFields.FieldByName("TRANSACTIONS").Lines;
                int index = 0;


                var siparislerSatirlar = OrderDataAccess.GetPurchDetayListById(fisId);

                foreach (var item in request.items)
                {
                    if (irsaliyesatir.AppendLine())
                    {

                        var malzeme = siparislerSatirlar.FirstOrDefault(a => a.MalzemeKodu == item.itemCode);

                        irsaliyesatir[index].FieldByName("TYPE").Value = 0; // Hizmet 4 kart 0 olacak
                        irsaliyesatir[index].FieldByName("MASTER_CODE").Value = item.itemCode; //"Ürün Kodu";
                        irsaliyesatir[index].FieldByName("UNIT_CODE").Value = malzeme.BirimKodu; // "Ürün Birimi (Adet,KG,Metre gibi)";
                        irsaliyesatir[index].FieldByName("QUANTITY").Value = item.deliveredQuantity; // 1;  //"Sayısı";

                        irsaliyesatir[index].FieldByName("PRICE").Value = malzeme.BirimFiyat.Value.ToString(nfi);
                        irsaliyesatir[index].FieldByName("PC_PRICE").Value = malzeme.BirimFiyat.Value.ToString(nfi);

                        irsaliyesatir[index].FieldByName("RC_XRATE").Value = 1.ToString(nfi);

                        irsaliyesatir[index].FieldByName("EDT_PRICE").Value = malzeme.BirimFiyat.Value.ToString(nfi);

                        irsaliyesatir[index].FieldByName("VAT_RATE").Value = malzeme.Kdv; // "KDV";

                        irsaliyesatir[index].FieldByName("DESCRIPTION").Value = request.description.StringCut(250);

                        irsaliyesatir[index].FieldByName("ORDER_REFERENCE").Value = malzeme.SatirId;
                    }

                    index += 1;
                }

                ValidateErrors err = irsaliye.ValidateErrors;

                irsaliye.FillAccCodes();

                if (irsaliye.Post())
                {
                    var no = (string)irsaliye.DataFields.FieldByName("NUMBER").Value;
                    resp.Code = no;
                    resp.Status = true;
                    resp.Message = "Purchase dispatch succesfully created.Dispatch number  = " + no;
                    resp.Data = no;
                }
                else
                {
                    resp.Status = false;
                    resp.Code = irsaliye.ErrorCode.ToString();
                    resp.Message = "Error. Msg  : " + irsaliye.ErrorDesc;
                }

                return resp;
            }
            catch (Exception ex)
            {
                resp.Status = false;
                resp.Message = "Error logo tiger instance. Msg  : " + ex.Message;
                resp.Code = ex.StackTrace;
                return resp;
            }
        }

        public ResponseMessage<string> DeletePurchDisPatch(int irsaliyeId)
        {
            var resp = new ResponseMessage<string>
            {
                Type = "Sales_Dispatch_Delete"
            };

            try
            {

                UnityObjects.Data irsaliye = tiger.NewDataObject(DataObjectType.doPurchDispatch);

                irsaliye.Delete(irsaliyeId);

                if (irsaliye.ErrorCode != 0)
                {
                    resp.Status = false;
                    resp.Message = irsaliye.ErrorDesc;
                    resp.ErrorMessage = irsaliye.DBErrorDesc;
                    resp.Code = irsaliye.ErrorCode.ToString();
                    return resp;
                }

                resp.Message = "Deleted successfully";
                resp.Status = true;
                return resp;
            }
            catch (Exception e)
            {
                return new ResponseMessage<string>
                {
                    Message = "Logo instance problem.Please contact it department Error :  " + e.Message,
                    Code = e.Message + " | " + e.StackTrace,
                    Type = "Purchase_Dispatch_Delete"
                };
            }
        }


        public ResponseMessage AmbarAktarimYap(AmbarTransferAktarimDto model)
        {

            ResponseMessage resp = new ResponseMessage();
            try
            {
               
                var rezerveData = tiger.NewDataObject(DataObjectType.doMaterialSlip);
                rezerveData.New();
                var now = DateTime.Now;
                object myDate = null;
                tiger.PackDate(now.Day, now.Month, now.Year, ref myDate);

                object myTime = null;
                tiger.PackTime(now.Hour, now.Minute, now.Second, ref myTime);

                rezerveData.DataFields.FieldByName("GROUP").Value = 3;
                rezerveData.DataFields.FieldByName("TYPE").Value = 25;
                rezerveData.DataFields.FieldByName("NUMBER").Value = '~';
                rezerveData.DataFields.FieldByName("DOC_NUMBER").Value = model.BelgeNo; //'Rezerveden Çıkış '
                rezerveData.DataFields.FieldByName("DATE").Value = myDate; //date;
                rezerveData.DataFields.FieldByName("TIME").Value = myTime; //saatdondur(time);
                rezerveData.DataFields.FieldByName("SOURCE_WH").Value = model.CekilecekAmbarNo; // RezerveAmbarNo;
                rezerveData.DataFields.FieldByName("SOURCE_COST_GRP").Value = 0; // merkez;
                rezerveData.DataFields.FieldByName("SOURCE_DIVISION_NR").Value = 0; //rezerve ambar merkez;
                rezerveData.DataFields.FieldByName("DEST_WH").Value = model.HedefAmbarNo; // rezerve ambar;
                rezerveData.DataFields.FieldByName("DEST_COST_GRP").Value = 0; // rezerve ambar;
                rezerveData.DataFields.FieldByName("DEST_DIVISION_NR").Value = 0; // merkez;
                rezerveData.DataFields.FieldByName("DATE_CREATED").Value = now; //date;
                rezerveData.DataFields.FieldByName("HOUR_CREATED").Value = now.Hour; //saati;
                rezerveData.DataFields.FieldByName("MIN_CREATED").Value = now.Minute; //dakika;
                rezerveData.DataFields.FieldByName("SEC_CREATED").Value = now.Second; //saniye;
                rezerveData.DataFields.FieldByName("CURRSEL_TOTALS").Value = 1;
                rezerveData.DataFields.FieldByName("CURRSEL_DETAILS").Value = 1;
                rezerveData.DataFields.FieldByName("APPROVE").Value = 1; // fişi kilitleme işlemi
                rezerveData.DataFields.FieldByName("APPROVE_DATE").Value = now.Date;
                rezerveData.DataFields.FieldByName("FOOTNOTE1").Value = model.Aciklama1;
                rezerveData.DataFields.FieldByName("FOOTNOTE2").Value = model.WebSiparisId.HasValue
                    ? "Sipariş No : " + model.WebSiparisId.Value
                    : string.Empty;
                rezerveData.DataFields.FieldByName("FOOTNOTE3").Value = model.IslemiYapan;
                //DATE; // fişi kilitme tarihi datetime.now // kesin gerekli

                Lines detay = rezerveData.DataFields.FieldByName("TRANSACTIONS").Lines;

                int index = 0;
                foreach (var item in model.Satirlar)
                {
                    if (detay.AppendLine())
                    {
                        detay[index].FieldByName("ITEM_CODE").Value = item.UrunKodu; //item.ProductCode;
                        detay[index].FieldByName("LINE_TYPE").Value = 0;
                        detay[index].FieldByName("SOURCEINDEX").Value =
                            model.CekilecekAmbarNo; //Viewden gelen RezerveAmbarNo;
                        detay[index].FieldByName("SOURCECOSTGRP").Value =
                            model.CekilecekAmbarNo; //Viewden gelen RezerveAmbarNo;
                        detay[index].FieldByName("DESTINDEX").Value = model.HedefAmbarNo; // merkez ambar;
                        detay[index].FieldByName("DESTCOSTGRP").Value = model.HedefAmbarNo; //merkez ambar;
                        detay[index].FieldByName("LINE_NUMBER").Value = 1;
                        detay[index].FieldByName("DELVRY_CODE").Value = item.TeslimatKodu; //viewden gelen TeslimatKodu
                        detay[index].FieldByName("QUANTITY").Value = (int)item.Adet; //rezerveden alınacak adet;
                        detay[index].FieldByName("UNIT_CODE").Value = item.Birim; //Viewden gelen birim;
                        detay[index].FieldByName("UNIT_CONV1").Value = 1;
                        detay[index].FieldByName("UNIT_CONV2").Value = 1;
                        detay[index].FieldByName("EU_VAT_STATUS").Value = 4;
                        detay[index].FieldByName("EDT_CURR").Value = 1;
                    }

                    index += 1;
                }

                ValidateErrors err = rezerveData.ValidateErrors;

                if (rezerveData.Post())
                {
                    resp.Code = (string)rezerveData.DataFields.FieldByName("NUMBER").Value;
                    resp.Data = rezerveData;
                    resp.Status = true;
                    string mess =
                        " Transfer is success.. User :  " +
                        model.IslemiYapan;
                    resp.Message = mess;
                }
                else
                {
                    resp.Status = false;
                    resp.Code = rezerveData.ErrorCode.ToString();
                    resp.Message = rezerveData.ErrorDesc;
                    for (int i = 0; i < err.Count; i++)
                    {
                        string stra = string.Format("{0} - {1};", err[i].Error, err[i].ID);
                        string erro =
                            " Error --> " +
                            stra +
                            resp.Code;
                        resp.Code = erro;
                    }
                }

                return resp;
            }
            catch (Exception ex)
            {
                resp.Status = false;
                string excptmes =
                    "Transfer Error -->Code : 011 .  " +
                    ex.Message;

                resp.Message = excptmes;
                return resp;
            }
        }

    }
}
