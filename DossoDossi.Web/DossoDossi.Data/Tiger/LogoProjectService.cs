﻿using DossoDossi.Data.Helper;
using DossoDossi.Data.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityObjects;

namespace DossoDossi.Data.Tiger
{
    public class LogoProjectService : App
    {
        public ResponseMessage Create(ProjectUpsertDto model)
        {
            try
            {
                var resp = new ResponseMessage();

                var nfi = new NumberFormatInfo
                {
                    NumberDecimalSeparator = ".",
                    NumberGroupSeparator = ""
                };

                UnityObjects.Data data = tiger.NewDataObject(DataObjectType.doProject);

                data.New();

                var now = DateTime.Now;

                object myDate = null;
                tiger.PackDate(now.Day, now.Month, now.Year, ref myDate);

                object myTime = null;
                tiger.PackTime(now.Hour, now.Minute, now.Second, ref myTime);

                data.DataFields.FieldByName("CODE").Value = model.Code;
                data.DataFields.FieldByName("NAME").Value = model.Name;
                data.DataFields.FieldByName("BEGIN_DATE").Value = model.Start.ToString("dd.MM.yyyy");
                data.DataFields.FieldByName("END_DATE").Value = model.End.ToString("dd.MM.yyyy");
                data.DataFields.FieldByName("DATE_CREATED").Value = DateTime.Now.ToString("dd.MM.yyyy");

                ValidateErrors err = data.ValidateErrors;
                // GL_CODE alanında gönderdiğimiz muhasebe kodunu kendi alır. 
                data.FillAccCodes();

                if (data.Post())
                {
                    resp.Status = true;
                    resp.Message = "Succesfully Saved";
                    resp.Code = "200";
                }
                else
                {
                    resp.Status = false;
                    resp.Code = data.ErrorCode.ToString();
                    resp.Message = "Error on occured . err1";
                    resp.Type = data.DBErrorDesc;
                    resp.Data = data.ErrorDescDetail;

                    for (int i = 0; i < err.Count; i++)
                    {
                        string stra = string.Format("{0} - {1};", err[i].Error, err[i].ID);
                        resp.Message = resp.Message + stra;
                    }
                }

                return resp;
            }
            catch (Exception e)
            {
                return new ResponseMessage
                {
                    Message = "Hata oluştu Msg :" + e.Message,
                    Code = e.Message + " | " + e.StackTrace
                };
            }
        }

        public ResponseMessage Edit(ProjectUpsertDto model)
        {
            try
            {
                var resp = new ResponseMessage();

                var nfi = new NumberFormatInfo
                {
                    NumberDecimalSeparator = ".",
                    NumberGroupSeparator = ""
                };

                UnityObjects.Data data = tiger.NewDataObject(DataObjectType.doProject);

                data.Read(model.Id);

                var now = DateTime.Now;

                object myDate = null;
                tiger.PackDate(now.Day, now.Month, now.Year, ref myDate);

                object myTime = null;
                tiger.PackTime(now.Hour, now.Minute, now.Second, ref myTime);

                data.DataFields.FieldByName("CODE").Value = model.Code;
                data.DataFields.FieldByName("NAME").Value = model.Name;
                data.DataFields.FieldByName("BEGIN_DATE").Value = model.Start;
                data.DataFields.FieldByName("END_DATE").Value = model.End;

                ValidateErrors err = data.ValidateErrors;

                // GL_CODE alanında gönderdiğimiz muhasebe kodunu kendi alır. 
                data.FillAccCodes();

                if (data.Post())
                {
                    resp.Status = true;
                    resp.Message = "Başarıyla güncellendi.";
                    resp.Code = "200";
                }
                else
                {
                    resp.Status = false;
                    resp.Code = data.ErrorCode.ToString();
                    resp.Message = "Error on occured . err1";
                    resp.Type = data.DBErrorDesc;
                    resp.Data = data.ErrorDescDetail;

                    for (int i = 0; i < err.Count; i++)
                    {
                        string stra = string.Format("{0} - {1};", err[i].Error, err[i].ID);
                        resp.Message = resp.Message + stra;
                    }
                }

                return resp;
            }
            catch (Exception e)
            {
                return new ResponseMessage
                {
                    Message = "Hata oluştu msg :  " + e.Message,
                    Code = e.Message + " | " + e.StackTrace
                };
            }
        }

        public ResponseMessage Delete(int id)
        {
            try
            {
                var resp = new ResponseMessage();

                UnityObjects.Data data = tiger.NewDataObject(DataObjectType.doProject);

                data.Delete(id);

                if (data.ErrorCode != 0)
                {
                    resp.Status = false;
                    resp.Message = "Silme hatası : Msg = " + data.ErrorDesc;
                }
                else
                {
                    resp.Status = true;
                    resp.Message = "Silme işlemi başarılı !";
                }

                return resp;
            }
            catch (Exception e)
            {
                return new ResponseMessage
                {
                    Message = "Hata oluştu msg :  " + e.Message,
                    Code = e.Message + " | " + e.StackTrace
                };
            }
        }


    }
}
