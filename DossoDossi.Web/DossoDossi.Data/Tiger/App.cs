﻿using DossoDossi.Data.DataAccess;
using System;
using UnityObjects;

namespace DossoDossi.Data.Tiger
{
    public class App
    {
        private static UnityApplication _instance = null;

        public static UnityApplication GetInstance()
        {
            if (_instance != null) return _instance;

            _instance = new UnityApplication();

            return _instance;
        }

        public UnityApplication tiger
        {
            get
            {
                try
                {
                    var app = GetInstance();
                    //app.ForWebUse = true;
                    try
                    {
                        if (app.Connected)
                        {
                            // app.Disconnect();
                        }
                    }
                    catch (System.Runtime.InteropServices.COMException ex)
                    {
                        _instance = null;
                        app = GetInstance();
                    }

                    if (!app.LoggedIn)
                    {
                        var canli =int.Parse(SettingDataAccess.GetValByCode("Logo.FirmNumber"));
                        var username = SettingDataAccess.GetValByCode("Logo.UserName");
                        var pass = SettingDataAccess.GetValByCode("Logo.Password");

                        var vry = app.Login(username, pass, canli);
                    }

                    return app;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
