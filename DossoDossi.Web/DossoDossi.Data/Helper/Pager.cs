﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Helper
{
    public class Pager
    {
        public int CurrentPage { get; set; }
        public string Filter { get; set; }
        public string Order { get; set; }
        public int TakeCount { get; set; }
    }
}
