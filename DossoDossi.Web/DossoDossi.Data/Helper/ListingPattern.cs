﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Helper
{
    public static class ListingPattern
    {
        public static List<TEntity> ToPager<TEntity, TKey>(this Table<TEntity> dbSet,
              Expression<Func<TEntity, bool>> predicate, Func<TEntity, TKey> keySelector, out int count, int currentPage, int pageCount) where TEntity : class
        {
            count = predicate == null ? dbSet.Count() : dbSet.Where(predicate).Count();
            int skipCount = count < pageCount ? 0 : currentPage * pageCount;

            if (predicate != null && skipCount > 0)
            {
                return dbSet.Where(predicate).OrderBy(keySelector).Skip(skipCount).Take(pageCount).ToList();
            }

            if (predicate != null && skipCount == 0)
            {
                return dbSet.Where(predicate).OrderBy(keySelector).Take(pageCount).ToList();
            }
            return skipCount > 0
              ? dbSet.OrderBy(keySelector).Skip(skipCount).Take(pageCount).ToList()
              : dbSet.OrderBy(keySelector).Take(pageCount).ToList();
        }

        public static List<TEntity> ToPagerByDesc<TEntity, TKey>(this Table<TEntity> dbSet,
            Expression<Func<TEntity, bool>> predicate, Func<TEntity, TKey> keySelector, out int count, int currentPage, int pageCount) where TEntity : class
        {
            count = predicate == null ? dbSet.Count() : dbSet.Where(predicate).Count();
            var skipCount = count < pageCount ? 0 : currentPage * pageCount;

            if (predicate != null && skipCount > 0)
            {
                return dbSet.Where(predicate).OrderByDescending(keySelector).Skip(skipCount).Take(pageCount).ToList();
            }
            else if (predicate != null && skipCount == 0)
            {
                return dbSet.Where(predicate).OrderByDescending(keySelector).Take(pageCount).ToList();
            }
            else
            {
                return skipCount > 0
                  ? dbSet.OrderByDescending(keySelector).Skip(skipCount).Take(pageCount).ToList()
                  : dbSet.OrderByDescending(keySelector).Take(pageCount).ToList();
            }
        }

        public static IQueryable<TEntity> Pager<TEntity>(this IQueryable<TEntity> dbSet, Expression<Func<TEntity, bool>> predicate, Pager pager, out int count) where TEntity : class
        {
            return PagerQuery(dbSet, predicate, pager.Order, out count, pager.CurrentPage, pager.TakeCount).AsQueryable();
        }
        private static IQueryable<TEntity> PagerQuery<TEntity>(IQueryable<TEntity> dbSet, Expression<Func<TEntity, bool>> predicate, string orderBy, out int count, int currentPage, int pageCount) where TEntity : class
        {
            var query = predicate != null ? dbSet.Where(predicate) : dbSet;

            count = query.Count();
            if (pageCount == 0)
                pageCount = 20;

            var skipCount = count < pageCount ? 0 : currentPage * pageCount;

            var order = new OrderBy(orderBy);

            var type = typeof(TEntity).GetProperty(order.Column).PropertyType;
            var isGeneric = type.IsGenericType;
            if (isGeneric)
            {
                type = Nullable.GetUnderlyingType(type);
            }
            switch (type.Name)
            {
                case "Int32":
                    query = isGeneric ? GetOrderBy<TEntity, int?>(query, order) : GetOrderBy<TEntity, int>(query, order);
                    break;

                case "Boolean":
                    query = isGeneric ? GetOrderBy<TEntity, bool?>(query, order) : GetOrderBy<TEntity, bool>(query, order);
                    break;

                case "DateTime":
                    query = isGeneric ? GetOrderBy<TEntity, DateTime?>(query, order) : GetOrderBy<TEntity, DateTime>(query, order);
                    break;

                case "String":
                    query = GetOrderBy<TEntity, string>(query, order);
                    break;
                case "Decimal":
                    query = isGeneric ? GetOrderBy<TEntity, decimal?>(query, order) : GetOrderBy<TEntity, decimal>(query, order);
                    break;

            }

            return skipCount >= 1 ? query.Skip(skipCount).Take(pageCount) : query.Take(pageCount);
        }

        public static IQueryable<TEntity> Pager<TEntity>(this Table<TEntity> dbSet, Expression<Func<TEntity, bool>> predicate, string orderBy, out int count, int currentPage, int pageCount) where TEntity : class
        {
            var query = predicate != null ? dbSet.Where(predicate) : dbSet;

            count = query.Count();
            if (pageCount == 0)
                pageCount = 20;

            var skipCount = count < pageCount ? 0 : currentPage * pageCount;

            var order = new OrderBy(orderBy);

            var prop = typeof(TEntity).GetProperty(order.Column);

            if (prop == null)
                return skipCount >= 1 ? query.Skip(skipCount).Take(pageCount) : query.Take(pageCount);

            var type = prop.PropertyType;

            var isGeneric = type.IsGenericType;
            if (isGeneric)
                type = Nullable.GetUnderlyingType(type);
            switch (type.Name)
            {
                case "Int32":
                    query = isGeneric ? GetOrderBy<TEntity, int?>(query, order) : GetOrderBy<TEntity, int>(query, order);
                    break;
                case "Int64":
                    query = isGeneric ? GetOrderBy<TEntity, long?>(query, order) : GetOrderBy<TEntity, long>(query, order);
                    break;

                case "Boolean":
                    query = isGeneric ? GetOrderBy<TEntity, bool?>(query, order) : GetOrderBy<TEntity, bool>(query, order);
                    break;

                case "DateTime":
                    query = isGeneric ? GetOrderBy<TEntity, DateTime?>(query, order) : GetOrderBy<TEntity, DateTime>(query, order);
                    break;

                case "String":
                    query = GetOrderBy<TEntity, string>(query, order);
                    break;
                case "Decimal":
                    query = isGeneric ? GetOrderBy<TEntity, decimal?>(query, order) : GetOrderBy<TEntity, decimal>(query, order);
                    break;
                case "Double":
                    query = isGeneric ? GetOrderBy<TEntity, double?>(query, order) : GetOrderBy<TEntity, double>(query, order);
                    break;

            }

            return skipCount >= 1 ? query.Skip(skipCount).Take(pageCount) : query.Take(pageCount);
        }

        private static IOrderedQueryable<TEntity> GetOrderBy<TEntity, TKey>(IQueryable<TEntity> query, OrderBy orderBy) where TEntity : class
        {
            var paramterExpression = Expression.Parameter(typeof(TEntity));
            if (orderBy.Method.Equals(Methods.ASC.ToString()))
            {
                return
                  query.OrderBy(
                    (Expression<Func<TEntity, TKey>>)
                      Expression.Lambda(Expression.PropertyOrField(paramterExpression, orderBy.Column), paramterExpression));
            }
            return
              query.OrderByDescending(
                (Expression<Func<TEntity, TKey>>)
                  Expression.Lambda(Expression.PropertyOrField(paramterExpression, orderBy.Column), paramterExpression));
        }

        public static PagerResult<T> PagerDynamic<T>(this Table<T> dbSet, Expression<Func<T, bool>> predicate, Pager pager) where T : class
        {
            int count;
            var list = dbSet.Pager(predicate, pager.Order, out count, pager.CurrentPage, pager.TakeCount).ToList();
            return new PagerResult<T>
            {
                Pager = pager,
                Total = count,
                Data = list
            };
        }
    }

    public class OrderBy
    {
        public OrderBy(string OrderBy)
        {
            var order = OrderBy.Split('|');

            Column = order[0];
            Method = order[1];
        }
        public string Column { get; set; }
        public string Method { get; set; }
    }
    [Flags]
    public enum Methods
    {
        ASC,
        DESC
    }
}
