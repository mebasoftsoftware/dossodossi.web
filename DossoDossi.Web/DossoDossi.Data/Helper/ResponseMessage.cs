﻿using DossoDossi.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Helper
{
    [Serializable]
    public class ResponseMessage
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public string Code { get; set; }
        public string ErrorMessage { get; set; }

        public string Token { get; set; }
        public string Type { get; set; }
        public object Data { get; set; }
        public List<OfferItem> Orders { get; set; }
    }

    [Serializable]
    public class ResponseMessage<T>
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public string Code { get; set; }
        public string ErrorMessage { get; set; }

        public string Token { get; set; }
        public string Type { get; set; }
        public T Data { get; set; }

    }
}
