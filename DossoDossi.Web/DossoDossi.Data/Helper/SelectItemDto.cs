﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Helper
{
    public class SelectItemDto
    {
        public string id { get; set; }
        public int ayint { get; set; }
        public string text { get; set; }
    }
}
