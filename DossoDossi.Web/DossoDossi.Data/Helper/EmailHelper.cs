﻿using DossoDossi.Data.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;

namespace DossoDossi.Data.Helper
{
    public class EmailHelper
    {
        public static ResponseMessage Send(List<string> to, List<string> cc, string subject, string body, List<string> bcc = null)
        {
            try
            {
                var APIKey = SettingDataAccess.GetValByCode("Email.Username");

                var SecretKey = SettingDataAccess.GetValByCode("Email.Password");

                var msg = new MailMessage();
                msg.IsBodyHtml = true;

                msg.From = new MailAddress(SettingDataAccess.GetValByCode("Email.From"), SettingDataAccess.GetValByCode("Email.Subject"));

                foreach (var email in to)
                {
                    if (email.Contains(";"))
                    {
                        var arr = email.Split(';');
                        foreach (var s in arr)
                        {
                            if (s.IsValidEmail())
                                msg.To.Add(new MailAddress(s));
                        }
                    }
                    else
                    {
                        if (email.IsValidEmail())
                            msg.To.Add(new MailAddress(email));
                    }
                }

                foreach (var email in cc)
                {
                    if (email.Contains(";"))
                    {
                        var arr = email.Split(';');
                        foreach (var s in arr)
                        {
                            if (s.IsValidEmail())
                                msg.CC.Add(new MailAddress(s));
                        }
                    }
                    else
                    {
                        if (email.IsValidEmail())
                            msg.CC.Add(new MailAddress(email));
                    }
                }

                if (bcc != null && bcc.Any())
                {
                    foreach (var email in bcc)
                        msg.Bcc.Add(new MailAddress(email));
                }

                msg.Subject = subject;
                msg.Body = body;
                msg.IsBodyHtml = true;

                var client = new SmtpClient(SettingDataAccess.GetValByCode("Email.Server"), int.Parse(SettingDataAccess.GetValByCode("Email.Port")));
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.EnableSsl = false;

                client.UseDefaultCredentials = true;
                client.Credentials = new NetworkCredential(APIKey, SecretKey);

                client.Send(msg);

                return new ResponseMessage { Status = true };
            }
            catch (Exception e)
            {
                return new ResponseMessage
                {
                    Message = "Error",
                    Code = e.StackTrace,
                };
            }
        }
    }
}
