﻿using System;
using System.IO;

namespace DossoDossi.Data.Helper
{

   public class Loging
    {

        public static string LogPath = System.Web.Configuration.WebConfigurationManager.AppSettings["LogFolder"];
       
        private static object mutex = new object();

        public static void AddLog(string msg, LogType type)
        {
            lock (mutex)
            {
                try
                {
                    if (!Directory.Exists(LogPath))
                        Directory.CreateDirectory(LogPath);

                    string filename =
                        $"{DateTime.Now.Year}-{DateTime.Now.Month.ToString().PadLeft(2, '0')}-{DateTime.Now.Day.ToString().PadLeft(2, '0')}.log";
                    string typeName = Enum.GetName(typeof(LogType), type);
                    File.AppendAllText(LogPath + filename, typeName + ": " + DateTime.Now + ": " + msg + Environment.NewLine);
                }
                catch
                {

                }
            }
        }
    }
}
