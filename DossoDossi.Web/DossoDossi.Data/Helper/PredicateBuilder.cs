﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Helper
{
    public static class PredicateBuilder
    {
        public static Expression<Func<T, bool>> True<T>() { return f => true; }
        public static Expression<Func<T, bool>> False<T>() { return f => false; }

        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> expressionOne, Expression<Func<T, bool>> expressionTwo)
        {
            var invokedExpr = Expression.Invoke(expressionTwo, expressionOne.Parameters);
            return Expression.Lambda<Func<T, bool>>(Expression.OrElse(expressionOne.Body, invokedExpr), expressionOne.Parameters);
        }
        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> expressionOne, Expression<Func<T, bool>> expressionTwo)
        {
            var invokedExpr = Expression.Invoke(expressionTwo, expressionOne.Parameters);
            return Expression.Lambda<Func<T, bool>>(Expression.AndAlso(expressionOne.Body, invokedExpr), expressionOne.Parameters);
        }

    }
}
