﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Helper
{
    public static class LangCodeModel
    {
        /// <summary>
        /// Lang Desc Method
        /// </summary>
        /// <param name="lang"></param>
        /// <param name="tr"></param>
        /// <param name="en"></param>
        /// <returns></returns>
        public static string LangDesc(string lang, string tr, string en)
        {
            if (lang.ToControl())
            {
                var ses = System.Web.HttpContext.Current.Session["language"];
                if (ses != null)
                {
                    lang = ses.ToString();
                }
            }

            return lang == null || lang == "tr-TR" ? tr : en;
        }
    }
}
