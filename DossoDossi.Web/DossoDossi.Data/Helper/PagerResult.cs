﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Helper
{
    public sealed class PagerResult<T>
    {
        public Pager Pager { get; set; }
        public int Total { get; set; }
        public List<T> Data { get; set; }

        public string ErrorMessage { get; set; }
        public string StackTrace{ get; set; }


    }
}
