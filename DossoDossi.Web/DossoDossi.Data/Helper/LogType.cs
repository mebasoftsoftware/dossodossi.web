﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DossoDossi.Data.Helper
{
    public enum LogType
    {
        INFO,
        WARNING,
        ERROR,
        DEBUG,
        SUCCESS
    }
}
