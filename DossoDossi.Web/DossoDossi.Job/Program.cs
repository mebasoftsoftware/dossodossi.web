﻿
namespace DossoDossi.Job
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //args = new string[]
            //{
            // "job.localmusteri"
            //};

            foreach (var arg in args)
            {
                switch (arg.ToLower())
                {
                    case "job.tedarikstok":
                        ProductCloudJob.Run();
                        break;
                    case "job.localaktar":
                        ProductLocalJob.Run();
                        break;
                    case "job.localmusteri":
                        MusteriCloudJob.Run();
                        break;
                }
            }
        }
    }
}
