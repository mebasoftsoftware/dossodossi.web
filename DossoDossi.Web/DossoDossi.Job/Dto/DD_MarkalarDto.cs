﻿
using System;

namespace DossoDossi.Job.Dto
{
   public class DD_MarkalarDto
    {
        public Guid MarkaId{ get; set; }
        public Guid TedarikciId{ get; set; }
        public string Kod{ get; set; }
        public string Ad{ get; set; }
        public string SistemKod{ get; set; }

    }
}
