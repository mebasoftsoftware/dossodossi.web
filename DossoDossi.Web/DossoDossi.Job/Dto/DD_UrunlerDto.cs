﻿using System;

namespace DossoDossi.Job.Dto
{
   public class DD_UrunlerDto
    {
        public Guid UrunId { get; set; }
        public Guid MarkaId{ get; set; }
        public string ModelKodu { get; set; }
        public string Ad { get; set; }
        public string Aciklama { get; set; }
        public string UrunDetay { get; set; }
        public decimal Fiyat { get; set; }
        public int SetAdet { get; set; }
        public DateTime CreatedDate { get; set; }
        public string AciklamaEN { get; set; }
        public string AdEN { get; set; }
        public string BKod { get; set; }
        public string KategoriAd { get; set; }
        public string KategoriAdEN { get; set; }

    }
}
