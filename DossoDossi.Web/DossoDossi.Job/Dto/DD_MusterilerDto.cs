﻿using System;

namespace DossoDossi.Job.Dto
{
    public class DD_MusterilerDto
    {
        public Guid  MusteriId{ get; set; }
        public string  Ad{ get; set; }
        public string  Soyad{ get; set; }
        public string  Ulke{ get; set; }
        public string  Sehir{ get; set; }
        public string  Telefon{ get; set; }
        public string  MusteriKodu{ get; set; }
        public int  nFirmaId{ get; set; }
        public DateTime  CreatedDate{ get; set; }
    }
}
