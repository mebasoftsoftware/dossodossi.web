﻿using Dapper;
using DossoDossi.Data.DataAccess;
using DossoDossi.Data.Entity;
using DossoDossi.Data.Helper;
using DossoDossi.Job.Dto;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DossoDossi.Job
{
   public class MusteriCloudJob
    {
        public static void Run()
        {
            //müşteriler güncellenir.
            new Thread(Start).Start();
        }

        public static void Start()
        {
            Log("Müşteri aktarımı başladı.", LogType.INFO);

            var musteriList = GetAllMusterilList();

            if (!musteriList.Any())
            {
                Log("Kayıt bulunamadı", LogType.INFO);
                return;
            }

            var list = musteriList.Select(a => new Musteriler
            {
                MusteriId = a.MusteriId,
                Code = a.MusteriKodu,
                SicilNo = a.MusteriKodu,
                Name = a.Ad + (!a.Soyad.ToControl() ? " " + a.Soyad : ""),
                UQ = a.MusteriId,
                City = a.Sehir,
                Country = a.Ulke,
                Phone = a.Telefon,
                Status = true,
                CreatedOn = a.CreatedDate
            }).ToList();

           var resp =  CustomerDataAccess.UpsertMusteriMultiple(list);

            Log("Müşteri aktarımı bitti.Adet :" + list.Count, LogType.INFO);
        }

        public static List<DD_MusterilerDto> GetAllMusterilList()
        {
            try
            {
                var key =-1 * int.Parse("MusteriAktarimSonKacDk".AppSettingsVal());

                DateTime date = DateTime.Now.AddHours(key);
                
                //using (var db = new SqlConnection(UtilityHelper.ConnectionStr("DataContext")))
                //{
                //    var lastItem = db.Query<Musteriler>("SELECT TOP 1  * from Musteriler order by CreatedOn DESC").FirstOrDefault();

                //    if (lastItem != null)
                //    {
                //        date = lastItem.CreatedOn.Value;
                //    }
                //}

                using (var db = new SqlConnection(UtilityHelper.ConnectionStr("CloudContext")))
                {
                    return db.Query<DD_MusterilerDto>("SELECT * FROM [dbo].[Musteriler] where IsDeleted = 0 AND CreatedDate >= @date ORDER BY CreatedDate ASC", new { date }).OrderBy(a => a.CreatedDate).ToList();
                }
            }
            catch (Exception ex)
            {
                Log("Müşteri aktarımı hata.Ex:" + ex.StackTrace, LogType.ERROR);
                return new List<DD_MusterilerDto>();
            }
        }

        protected static void Log(string mess, LogType type)
        {
            Loging.AddLog(mess, type);
            Console.WriteLine(mess);
        }

    }
}
