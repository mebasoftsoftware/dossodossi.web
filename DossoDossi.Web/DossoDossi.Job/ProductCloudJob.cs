﻿using DossoDossi.Data.DataAccess;
using DossoDossi.Data.Helper;
using DossoDossi.Job.Dto;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using Dapper;
using System.Threading.Tasks;
using DossoDossi.Data.Entity;

namespace DossoDossi.Job
{
    public class ProductCloudJob
    {
        public static void Run()
        {
            //tedarikçiler güncellenir.
            new Thread(StartTedarik).Start();
        }


        public static void StartTedarik()
        {
            var projelers = ProjeDataAccess.GetActriveProjelers();

            var markalars = TumMarkalar();

            foreach (var item in projelers)
            {
                var allTedarikciler = ProjeDataAccess.GetProjeTedarikciler(item.CODE);

                foreach (var marka in markalars)
                {
                    var firs = allTedarikciler.FirstOrDefault(a => a.MarkaId == marka.MarkaId);

                    //var cari = CustomerDataAccess.GetByIdParam("OzelKod2", marka.SistemKod);
                    //if (cari == null)
                    //{
                    //    continue;
                    //}

                    if (firs == null)
                    {
                        var add = new ProjeTedarikciler
                        {
                            Id = Guid.NewGuid(),
                            CariId = 0,
                            CariKodu = marka.SistemKod,
                            CariUnvan = marka.Ad.ToUpperCase(),
                            ProjeKodu = item.CODE,
                            KullaniciAdi = "-",
                            Sifre = "-",
                            MarkaId = marka.MarkaId
                        };

                        var resp = CustomerDataAccess.UpsertTedarikciPost(add);

                        UpsertItemProduct(marka, add.Id, item.CODE);
                    }
                    else
                    {
                        UpsertItemProduct(marka, firs.Id, item.CODE);

                        continue;
                    }
                }
            }
        }

        public static void UpsertItemProduct(DD_MarkalarDto marka, Guid? ptrId, string projeKodu)
        {
            var listUrunler = GetListAll(marka.MarkaId);

            Console.WriteLine(marka.Ad + " ürün aktarımı başladı.");

            //eskileri tümü false edildi.aktarımbaşlandı.
            CustomerDataAccess.StokAktarimUpdate(marka.MarkaId, false);

            foreach (var item in listUrunler)
            {
                var add = new ProjeTedarikciUrunler
                {
                    Id = Guid.NewGuid(),
                    Barkodu = item.BKod,
                    StokAdi = item.Ad,
                    StokKodu = item.BKod,
                    CariId = 0,
                    Doviz = "USD",
                    CreatedOn = item.CreatedDate,
                    SatisFiyat = (double)item.Fiyat,
                    SeriAdedi = item.SetAdet,
                    ProjeKodu = projeKodu,
                    StokId = 0,
                    Aciklama2 = item.Aciklama.SubStringCut(80),
                    GrupKodu = item.KategoriAd,
                    Marka = marka.Ad,
                    PTrId = ptrId.Value,
                };

                CustomerDataAccess.UpsertStockPost(add);
            }

            //olmayanlar silinecek.
            CustomerDataAccess.DeleteStokAktarim(marka.MarkaId, false);

            Console.WriteLine(marka.Ad + " aktarımı bitti.Toplam :" + listUrunler.Count);


        }

        public static List<DD_UrunlerDto> GetListAll(Guid markaId)
        {
            var list = new List<DD_UrunlerDto>();

            try
            {
                using (var db = new SqlConnection(UtilityHelper.ConnectionStr("CloudContext")))
                {
                    list = db.Query<DD_UrunlerDto>
                        ("SELECT u.UrunId, u.ModelKodu, u.Ad, u.Aciklama, u.UrunDetay, u.MarkaId, u.Fiyat, u.SetAdet, u.CreatedDate, u.AciklamaEN, u.AdEN," +
                        " u.BKod ," +
                        " k.Ad as KategoriAd, " +
                        "k.AdEN as KategoriAdEN " +
                        "FROM [dbo].[Urunler] as u " +
                        " LEFT JOIN [Kategoriler] as k on u.KategoriId = k.KategoriId " +
                        " WHERE  u.FuarYayin = 1 And u.IsDeleted = 0 AND u.MarkaId = @markaId", new { markaId }).ToList();
                }

            }
            catch (Exception ex)
            {


            }

            return list;

        }


        public static List<DD_MarkalarDto> TumMarkalar()
        {
            var list = new List<DD_MarkalarDto>();
            try
            {
                using (var db = new SqlConnection(UtilityHelper.ConnectionStr("CloudContext")))
                {
                    list = db.Query<DD_MarkalarDto>("SELECT * FROM [dbo].[Markalar] where IsDeleted = 0 And LEN(SistemKod) > 0 ").ToList();
                }
            }
            catch (Exception ex)
            {

            }

            return list;
        }
    }
}
