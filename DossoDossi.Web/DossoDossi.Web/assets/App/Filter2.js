﻿var Filter2 = {
    GetList: function (currentPage, filter, order, takeCount) {
        Erp.U.SaveFilterParamDifferent("2",currentPage, filter, order, takeCount, 'ID|DESC');
        var form = $("#FilterForm2");
        //  $.Block.Open({ target: ".portlet" });
        Erp.U.A.Req.F(form, function (data) {
            //console.log(JSON.stringify(data));
            if (data != null && data.Data != null && data.Data.length > 0) {
                //  alert(JSON.stringify(data.Data));
                var count = data.Pager.CurrentPage === 0 ? 1 : (data.Pager.CurrentPage * data.Pager.TakeCount) + 1;
                $.each(data.Data, function (i, item) {
                    item.Index = count;
                    
                    item.json = JSON.stringify(item);

                    count++;
                });
            }
            
            var innerHtml = Mustache.to_html($('#Template2').html(), { DATA: data.Data });
            $("#List2").empty().append(innerHtml);
            Erp.U.GetPager(data.Pager, data.Total, "#Pager2");
            $("#FilterResult_Total_Count2").val(data.Total);
            $("#countTabTotal2").text(data.Total);

            $('#Pager2 ul li a').click(function () {
                Filter2.GetList($(this).attr("data-page"), data.Pager.Filter, data.Pager.Order);
            });
            

            Erp.U.ActiveTooltip();
        });
    },
    Event: function () {

        Filter2.GetList(null, null, "ID|DESC", 10);

        $("#TakeNumberList2").on('change', function () {
            var t = $(this).val();
            $("#TakeCount2").val(t);
            Filter2.GetList();
        });

        $("#Refresh2").click(function () {
            $("#Filter2").val('');
            $("#FiltreInput2").val('');
            $(".clearprop2").val('');
            $(".clearpropselect2").val('-1');
            
            Filter2.GetList(null, null, null, 10);
            $("#TakeNumberList").val('10');
            $("#AppendedItemLiSt").empty();
            $(".addtobaskeymultiplebutton").hide();

        });
        $("#FiltreButton2").click(function () {

            $('#Filter2').val($('#FiltreInput2').val());
            var take = $("#TakeNumberList2").val();
            var page = $("#CurrentPage2").val();
            Filter2.GetList(page,null,null,take);
        });

        Erp.U.KeyPressActive('#FiltreInput2', '#FiltreButton2');

        Erp.U.SortOrderColumnDif('tablecontent2', function (orderResult) {
            Filter2.GetList(null, null, orderResult, 0);
        });
    }
    
}
