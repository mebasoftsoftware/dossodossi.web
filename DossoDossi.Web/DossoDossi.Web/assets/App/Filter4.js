﻿var Filter4 = {
    GetList: function (currentPage, filter, order, takeCount) {
        Erp.U.SaveFilterParamDifferent("4",currentPage, filter, order, takeCount, 'ID|DESC');
        var form = $("#FilterForm4");
        //  $.Block.Open({ target: ".portlet" });
        Erp.U.A.Req.F(form, function (data) {
            //console.log(JSON.stringify(data));
            if (data != null && data.Data != null && data.Data.length > 0) {
                //  alert(JSON.stringify(data.Data));
                var count = data.Pager.CurrentPage === 0 ? 1 : (data.Pager.CurrentPage * data.Pager.TakeCount) + 1;
                $.each(data.Data, function (i, item) {
                    item.Index = count;
                    
                    item.json = JSON.stringify(item);

                    count++;
                });
            }
            
            var innerHtml = Mustache.to_html($('#Template4').html(), { DATA: data.Data });
            $("#List4").empty().append(innerHtml);
            Erp.U.GetPager(data.Pager, data.Total, "#Pager4");
            $("#FilterResult_Total_Count4").val(data.Total);
            $("#countTabTotal4").text(data.Total);

            $('#Pager4 ul li a').click(function () {
                Filter4.GetList($(this).attr("data-page"), data.Pager.Filter, data.Pager.Order);
            });
            

            Erp.U.ActiveTooltip();
        });
    },
    Event: function () {

        Filter4.GetList(null, null, "ID|DESC", 10);

        $("#TakeNumberList4").on('change', function () {
            var t = $(this).val();
            $("#TakeCount4").val(t);
            Filter4.GetList();
        });

        $("#Refresh4").click(function () {
            $("#Filter4").val('');
            $("#FiltreInput4").val('');
            $(".clearprop4").val('');
            $(".clearpropselect4").val('-1');
            
            Filter4.GetList(null, null, null, 10);
            $("#TakeNumberList").val('10');
            $("#AppendedItemLiSt").empty();
            $(".addtobaskeymultiplebutton").hide();

        });
        $("#FiltreButton4").click(function () {

            $('#Filter4').val($('#FiltreInput4').val());
            var take = $("#TakeNumberList4").val();
            var page = $("#CurrentPage4").val();
            Filter4.GetList(page,null,null,take);
        });

        Erp.U.KeyPressActive('#FiltreInput4', '#FiltreButton4');

        Erp.U.SortOrderColumnDif('tablecontent4', function (orderResult) {
            Filter4.GetList(null, null, orderResult, 0);
        });
    }
    
}
