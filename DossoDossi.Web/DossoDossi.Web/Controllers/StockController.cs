﻿using DossoDossi.Data.DataAccess;
using DossoDossi.Data.Entity;
using DossoDossi.Data.Helper;
using DossoDossi.Data.Models;
using DossoDossi.Data.Tiger;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DossoDossi.Web.Controllers
{
    [LoginFilter]
    public class StockController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.OzelKodu1 = ProductDataAccess.GetGroupNamesSelect("OzelKodu");
            ViewBag.OzelKodu2 = ProductDataAccess.GetGroupNamesSelect("OzelKodu2");
            ViewBag.OzelKodu3 = ProductDataAccess.GetGroupNamesSelect("OzelKodu3");
            ViewBag.OzelKodu4 = ProductDataAccess.GetGroupNamesSelect("OzelKodu4");
            ViewBag.OzelKodu5 = ProductDataAccess.GetGroupNamesSelect("OzelKodu5");
            return View();
        }

        [HttpPost]
        public JsonResult GetList(Pager pager,string barkod,string stoklu,
            string ozelkod,
            string ozelkod2,
            string ozelkod3,
            string ozelkod4,
            string ozelkod5
            )
        {
            var res = ProductDataAccess.GetList(pager, barkod, stoklu,
                ozelkod,ozelkod2,ozelkod3,ozelkod4,ozelkod5);

            if(res!=null && res.Data!=null && res.Data.Any())
            {
                foreach(var item in res.Data)
                {
                    var reserveProc = OffersDataAccess.GetReserveStockCount(item.StokId);
                    item.Birim2 = reserveProc.ToString();
                    item.EmagazaKodu = ((item.MerkezStok ?? 0) - reserveProc - (item.BekleyenSiparisMiktar??0)).ToString();
                }
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Transaction(int id)
        {
            ViewBag.Data = ProductDataAccess.GetById(id);
            return View();
        }

        [HttpPost]
        public JsonResult GetListTransaction(Pager pager,int id)
        {
            var user = SessionHelper.User;

            var us = AccountDataAccess.GetById(user.UserId);
            
            var res = ProductDataAccess.GetListTransaction(pager,id,us.Isyeri);
            return Json(res, JsonRequestBehavior.AllowGet);
        }


        public PartialViewResult ReserveListPartial(int id)
        {
            var reserveProc = OffersDataAccess.GetReserveStockList(id);
            return PartialView("_ReserveListPartial",reserveProc);
        }

        public PartialViewResult OpenProduct()
        {
            return PartialView("_OpenProductPartial");
        }

        public JsonResult GetStockCalc(int stokId, int? ambarNo)
        {
            var list = ProductDataAccess.StokAmbarToplamlariGetir(stokId);

            var reserveProc = OffersDataAccess.GetReserveStockList(stokId);

            var s = list.Where(a => a.AmbarNo == ambarNo).Sum(a => a.StokMiktari??0);

            var r = reserveProc.Sum(a => a.Miktar);
          
            var sonuc = new
            {
                Stock = s,
                Reserve = r,
                NetStock = s - r,
                ReserveList = reserveProc,
                AmbarList = list,
                ClientCode  = ""
            };
            return Json(sonuc, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMalzemeFiyat(int stokId, string cariKodu)
        {
            var fiyat = ProductDataAccess.GetMalzemeFiyat(stokId, cariKodu);

            var stokKodu = ProductDataAccess.GetCariStokKoduEnSon(stokId, cariKodu);

            if (fiyat == null)
            {
                return Json(new { Fiyat = 0 , OzelKodu  = stokKodu}, JsonRequestBehavior.AllowGet);
            }

            fiyat.OzelKodu = stokKodu;

            return Json(fiyat, JsonRequestBehavior.AllowGet);
        }

        public ActionResult OnTheRoad()
        {
            return View();
        }


        [HttpPost]
        public JsonResult GetOnTheRoadList(Pager pager,string orderNo,string supplier,string po)
        {
            var res = ProductDataAccess.GetOnTheRoadList(pager,orderNo,supplier,po);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateOnTheRoad()
        {
            var user = SessionHelper.User;

            if (user == null)
                return RedirectToAction("Login", "Account");

            ViewBag.User = AccountDataAccess.GetById(user.UserId);
            ViewBag.TasiyiciKodlar = LogoDataAccess.TasiyiciKodlar();

            return View();
        }

        [HttpPost]
        public JsonResult CreateOnTheRoadData(CreateOnTheRoadDto model)
        {
            var resp =  ProductDataAccess.CreateOnTheRoadData(model);
            return Json(resp, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult OnTheRoadDetail(int id)
        {
            OnTheRoadProduct data = ProductDataAccess.GetOnTheRoadDetailById(id);
            return PartialView("_OnTheRoadDetailPartial", data);
        }

        public PartialViewResult OnTheRoadEditOpen(int id)
        {
            OnTheRoadProduct data = ProductDataAccess.GetOnTheRoadDetailById(id);
            return PartialView("_OnTheRoadEditPartial", data);
        }


        public PartialViewResult OnTheRoadListPartial(int id)
        {
            List<OnTheRoadProduct> list = ProductDataAccess.GetOnTheRoadListByStockId(id);
            return PartialView("_OnTheRoadListPartial",list);
        }

        [HttpPost]
        public JsonResult EditOnTheRoadData(OnTheRoadProduct model)
        {
            var resp = ProductDataAccess.EditOnTheRoadData(model);
            return Json(resp, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteOnTheRoad(int id)
        {
            var resp = ProductDataAccess.DeleteOnTheRoad(id);

            return Json(resp, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Wizard()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetWizardList(Pager pager)
        {
            var res = ProductDataAccess.GetWizardList(pager);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult WaitingOrderPartial(int id)
        {
            var user = SessionHelper.User;
            
            var us = AccountDataAccess.GetById(user.UserId);

            var list = OrderDataAccess.GetBekleyenSiparislerById(id, us.Isyeri);
            return PartialView("_WaitingOrderPartial", list);
        }

        public ActionResult OnTheRoadXls()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ReadOnTheRoadFile(HttpPostedFileBase file)
        {
            if (file == null)
            {
                ViewBag.Error = "Please select file.";
                return View("OnTheRoadXls");
            }

            try
            {
                var fileName = "ExcelFile";
                FileIoHelper.AddSync(file, fileName);

                DataTable list = FileIoHelper.Read(fileName);

                int trNo = 0;
                var nesne = new List<ExcelDynamicDto>();

                PrivtDataTable(list, trNo, nesne);

                var respList = new List<ResponseMessage>();

                foreach (var fat in nesne)
                {
                    ResponseMessage resp = ProductDataAccess.UpsertOnTheRoadData(fat);
                    respList.Add(resp);
                }

                ViewBag.Result = respList;

                return View("OnTheRoadXls");

            }
            catch (System.Exception ex)
            {
                ViewBag.Error = "An error occurred.Please control your xls file. Error Msg  : " + ex.Message;
                return View("OnTheRoadXls");
            }
        }

        private static void PrivtDataTable(DataTable list, int trNo, List<ExcelDynamicDto> nesne)
        {
            foreach (DataRow row in list.Rows)
            {
                int tdno = 0;

                var add = new ExcelDynamicDto { No = trNo.ToString(), List = new List<ExcelItemDto>() };

                foreach (var cell in row.ItemArray)
                {
                    DataColumn head = list.Columns[tdno];

                    var addItem = new ExcelItemDto
                    {
                        Baslik = head.Caption,
                        Deger = cell.ToString()
                    };

                    if (cell is DateTime)
                    {
                        addItem.Date = ((DateTime)cell).Date;
                    }

                    add.List.Add(addItem);

                    tdno++;
                }

                nesne.Add(add);

                trNo++;
            }
        }


        public ActionResult AmbarTransfer()
        {
            ViewBag.Ambarlar = LogoDataAccess.GetAmbarlar();

            return View();
        }


        [HttpPost]
        public JsonResult TransferAktar(AmbarTransferAktarimDto data)
        {
            var prIds = data.Satirlar.Select(a => int.Parse(a.StokId)).ToList();


            var products = ProductDataAccess.GetListByIds(prIds);
            foreach (var item in data.Satirlar)
            {
                var first = products.FirstOrDefault(a => a.StokId== int.Parse(item.StokId));
                item.UrunKodu = first.MalzemeKodu;
                item.Birim = first.BirimSetiKodu;
            }

            var user = SessionHelper.User;

            data.FirmNo = int.Parse(SettingDataAccess.GetValByCode("Logo.FirmNumber"));
            data.BelgeNo = "T - " + data.CekilecekAmbarAdi + "-" + data.HedefAmbarAdi;

            data.IslemiYapan = user.Name;

            var resp = new LogoOrderService().AmbarAktarimYap(data);

            return Json(resp, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult AmbarToplamlariOpen(int id)
        {
            var list = ProductDataAccess.StokAmbarToplamlariGetir(id);

            return PartialView("_AmbarToplamlariOpenPartial",list);
        }

    }
}