﻿using DossoDossi.Data.DataAccess;
using DossoDossi.Data.Entity;
using DossoDossi.Data.Helper;
using DossoDossi.Data.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DossoDossi.Web.Controllers
{
    [LoginFilter]
    public class ButceController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetList(Pager pager, int? yil)
        {
            var result = ButceDataAccess.GetList(pager, yil);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult Create()
        {
            ViewBag.MuhasebeKodlari = ButceDataAccess.GetMuhasebeKodlari();
            ViewBag.MasrafMerkezleri = ButceDataAccess.MasrafMerkeziList();
            ViewBag.Proje = ButceDataAccess.ProjeList();

            //ViewBag.Urun = ButceDataAccess.UrunList();

            var model = new Butceler
            {
                Yil = DateTime.Today.Year
            };
            return PartialView("_CreatePartial", model);
        }

        [HttpPost]
        public JsonResult Create(Butceler model)
        {
            return Json(ButceDataAccess.Create(model));
        }

        public PartialViewResult Edit(Guid id)
        {
            var data = ButceDataAccess.GetById(id);

            ViewBag.MuhasebeKodlari = ButceDataAccess.GetMuhasebeKodlari();
            ViewBag.MasrafMerkezleri = ButceDataAccess.MasrafMerkeziList();
            ViewBag.Proje = ButceDataAccess.ProjeList();
            //ViewBag.Urun = ButceDataAccess.UrunList();

            return PartialView("_EditPartial", data);
        }

        [HttpPost]
        public JsonResult Update(Butceler model)
        {
            return Json(ButceDataAccess.Update(model));
        }

        [HttpPost]
        public JsonResult Delete(Guid id)
        {
            return Json(ButceDataAccess.Delete(id));
        }


        public ActionResult Excel()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ExcelPost(HttpPostedFileBase file)
        {

            if (file == null)
            {
                ViewBag.Error = "Dosya seçimi zorunludur.";
                return View("Excel");
            }

            try
            {
                FileIoHelper.AddSyncFile(file, "Butce");

                var readedList = FileIoHelper.ReadFile("Butce");

                var list = readedList.Select(row => new ButcelerDto
                {
                    Yil = row.Field<string>("Yil"),
                    HesapKodu = row.Field<string>("HesapKodu"),
                    HesapAdi = row.Field<string>("HesapAdi"),
                    HesapAdiEn = row.Field<string>("HesapAdiEn"),
                    MasrafMerkeziKodu = row.Field<string>("MasrafMerkeziKodu"),
                    Proje = row.Field<string>("Proje"),
                    Urun = row.Field<string>("Urun"),
                    //Aciklama = row.Field<string>("Aciklama"),
                    Ocak = row.Field<string>("Ocak"),
                    Subat = row.Field<string>("Subat"),
                    Mart = row.Field<string>("Mart"),
                    Nisan = row.Field<string>("Nisan"),
                    Mayis = row.Field<string>("Mayis"),
                    Haziran = row.Field<string>("Haziran"),
                    Temmuz = row.Field<string>("Temmuz"),
                    Agustos = row.Field<string>("Agustos"),
                    Eylul = row.Field<string>("Eylul"),
                    Ekim = row.Field<string>("Ekim"),
                    Kasim = row.Field<string>("Kasim"),
                    Aralik = row.Field<string>("Aralik"),

                }).ToList();

                ViewBag.List = list;
            }
            catch (Exception e)
            {
                ViewBag.Error = "Yükleme esnasında hata oluştu.Lütfen örnek exceldeki sütunlar eksiksiz olmalıdır. Err = " + e.Message;
            }

            return View("Excel");
        }

        [HttpPost]
        public JsonResult UpsertJson(Butceler model)
        {
            var st = ButceDataAccess.Upsert(model);
            return Json(new { Status = st, Message = st ? "Başarılı" : "Hata oluştu" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MasrafMerkeziList()
        {
            return View();
        }

        [HttpPost]
        public JsonResult LogoMasrafMerkezGetList(Pager pager)
        {
            return Json(ButceDataAccess.LogoMasrafMerkezGetList(pager));
        }

        public ActionResult Rapor()
        {
            
            ViewBag.MasrafMerkezler = ButceDataAccess.GetAllByButcesiOlanList();
            ViewBag.MuhasebeKodlari = ButceDataAccess.GetMuhasebeKodlari();
            
            ViewBag.Proje = ButceDataAccess.ProjeList();
            //ViewBag.Urun = ButceDataAccess.UrunList();

            //ButceDataAccess.GetMuhasebeKodlari();
            return View();
        }

        public PartialViewResult RaporGetir(int yil, int? ay, string cost, string proje, string urun, string muhacCode)
        {
            ViewBag.Butceler = ButceDataAccess.ButceList(yil, cost, proje, urun, muhacCode);

            ViewBag.Gerceklesenler = ButceDataAccess.GerceklesenlerGetir(yil, ay, cost, proje, urun, muhacCode);

            return PartialView("_RaporDetailPartial");
        }

    }
}