﻿using DossoDossi.Data.DataAccess;
using DossoDossi.Data.Entity;
using DossoDossi.Data.Helper;
using DossoDossi.Data.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DossoDossi.Web.Controllers
{
    [LoginFilter]
    public class DemandController : Controller
    {
        // GET: Demand
        public ActionResult Index()
        {

            ViewBag.StatusList = OffersDataAccess.GetAllStatus();
            return View();
        }


        [HttpPost]
        public JsonResult GetList(OfferPagerDto pager)
        {
            var user = SessionHelper.User;

            var us = AccountDataAccess.GetById(user.UserId);
            pager.IsYeri = us.Isyeri;

            return Json(DemandDataAccess.GetList(pager));
        }

        public ActionResult Create()
        {
            var user = SessionHelper.User;

            if (user == null)
                return RedirectToAction("Login", "Account");

            ViewBag.User = AccountDataAccess.GetById(user.UserId);
            ViewBag.Isyeri = LogoDataAccess.GetIsyeri();
            ViewBag.Bolum = LogoDataAccess.GetBolumler();
            ViewBag.Ambarlar = LogoDataAccess.GetAmbarlar();
            ViewBag.VadeList = LogoDataAccess.VadeList();
            ViewBag.TasiyiciKodlar = LogoDataAccess.TasiyiciKodlar();
            ViewBag.User = AccountDataAccess.GetById(user.UserId);
            ViewBag.DefCurrency = SettingDataAccess.GetValByCode("Offer.DefaultCurrency");

            return View();
        }


        public ActionResult Edit(Guid id)
        {
            var user = SessionHelper.User;

            if (user == null)
                return RedirectToAction("Login", "Account");

            ViewBag.User = AccountDataAccess.GetById(user.UserId);
            ViewBag.Isyeri = LogoDataAccess.GetIsyeri();
            ViewBag.Bolum = LogoDataAccess.GetBolumler();
            ViewBag.Ambarlar = LogoDataAccess.GetAmbarlar();
            ViewBag.VadeList = LogoDataAccess.VadeList();
            ViewBag.TasiyiciKodlar = LogoDataAccess.TasiyiciKodlar();
            ViewBag.User = AccountDataAccess.GetById(user.UserId);
            ViewBag.DefCurrency = SettingDataAccess.GetValByCode("Offer.DefaultCurrency");

            var offer = DemandDataAccess.GetByUQ(id);
            ViewBag.Data = offer;

            var items = DemandDataAccess.GetItems(offer.Id);

            return View(items);
        }


        [HttpPost]
        public JsonResult Create(Demand offer, List<DemandItem> items)
        {
            var resp = DemandDataAccess.Create(offer, items);
            return Json(resp, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult Edit(Demand offer, List<DemandItem> items, List<int> deleteItems)
        {
            var resp = DemandDataAccess.Edit(offer, items, deleteItems);
            return Json(resp, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FileAddForItem(int id, string nos, string counts)
        {
            var files = Request.Files;

            if (files == null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            var arr = nos.Split(',').Where(x => !x.ToControl()).ToList();
            var cts = counts.Split(',').Where(x => !x.ToControl()).ToList();

            for (int i = 0; i < arr.Count; i++)
            {
                var no = arr[i];
                var fileCount = int.Parse(cts[i]);

                if (fileCount > 0)
                {
                    for (int j = 0; j < fileCount; j++)
                    {
                        var file = files[i];

                        var extpath = Path.GetExtension(file.FileName);

                        var fileNameOrigin = Path.GetFileNameWithoutExtension(file.FileName);

                        var fileName = fileNameOrigin.Replace(" ", "_").Replace(",", "").SetTurkishCharacterToEnglish() + "_" + DateTime.Today.ToString("yyyyMMdd") + extpath;

                        var localfolder = "/Files/demand/" + id + "/";

                        var directoryMapPath = Server.MapPath("~" + localfolder);
                        var path = Path.Combine(directoryMapPath, fileName);

                        var localpath = localfolder + fileName;

                        if (!Directory.Exists(directoryMapPath))
                        {
                            Directory.CreateDirectory(directoryMapPath);
                        }

                        file.SaveAs(path);

                        DemandDataAccess.UpdateFileDdemandItemByNo(id, no, localpath);
                    }
                }
            }

            return Json(true);
        }

        [HttpPost]
        public JsonResult Delete(int id)
        {
            var user = SessionHelper.User;

            var res = DemandDataAccess.Delete(id, user.UserId);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateStatus(int id, int status)
        {
            var user = SessionHelper.User;
            var res = DemandDataAccess.UpdateStatus(id, status);
            return Json(res, JsonRequestBehavior.AllowGet);
        }


        public PartialViewResult GetDetail(int id)
        {
            ViewBag.Data = DemandDataAccess.GetById(id);
            var items = DemandDataAccess.GetItems(id);

            return PartialView("_DemandDetailPartial", items);
        }

        public ActionResult CreateFromDemand(Guid id)
        {
            var user = SessionHelper.User;

            if (user == null)
                return RedirectToAction("Login", "Account");

            ViewBag.User = AccountDataAccess.GetById(user.UserId);
            ViewBag.Isyeri = LogoDataAccess.GetIsyeri();
            ViewBag.Bolum = LogoDataAccess.GetBolumler();
            ViewBag.Ambarlar = LogoDataAccess.GetAmbarlar();
            ViewBag.VadeList = LogoDataAccess.VadeList();
            ViewBag.TasiyiciKodlar = LogoDataAccess.TasiyiciKodlar();
            ViewBag.User = AccountDataAccess.GetById(user.UserId);
            ViewBag.DefCurrency = SettingDataAccess.GetValByCode("Offer.DefaultCurrency");

            var offer = DemandDataAccess.GetByUQ(id);
            ViewBag.Offer = offer;
            ViewBag.Items = DemandDataAccess.GetItems(offer.Id);

            return View();
        }
    }
}