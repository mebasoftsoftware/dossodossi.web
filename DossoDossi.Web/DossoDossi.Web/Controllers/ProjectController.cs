﻿using DossoDossi.Data.DataAccess;
using DossoDossi.Data.Entity;
using DossoDossi.Data.Helper;
using DossoDossi.Data.Models;
using DossoDossi.Data.Tiger;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DossoDossi.Web.Controllers
{
    [LoginFilter]
    public class ProjectController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetList(Pager pager)
        {
            return Json(ProjeDataAccess.GetList(pager), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(ProjectUpsertDto model)
        {
            var resp = new LogoProjectService().Create(model);

            if (!resp.Status)
            {
                ViewBag.Hata = resp.Message;
                ViewBag.RespJson = JsonConvert.SerializeObject(resp);

                return View(model);
            }

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            var model = ProjeDataAccess.GetById(id);

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(ProjectUpsertDto model)
        {
            var resp = new LogoProjectService().Edit(model);

            if (!resp.Status)
            {
                ViewBag.Hata = resp.Message;
                return View(model);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var resp = new LogoProjectService().Delete(id);
            return Json(resp, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProjeMusteriler(string code)
        {
            var list = ProjeDataAccess.GetProjeMusteriler(code);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProjeTedarikciler(string code)
        {
            var list = ProjeDataAccess.GetProjeTedarikciler(code);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ExcelMusteriPost(HttpPostedFileBase file)
        {
            if (file == null)
            {
                return Json(new ResponseMessage { Message = "Dosya zorunludur" }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                FileIoHelper.AddSyncFile(file, "Musteri");

                var readedList = FileIoHelper.ReadFile("Musteri");

                var list = readedList.Select(row => new Musteriler
                {
                    Name = row.Field<string>("MusteriAdiSoyadi"),
                    Email = row.Field<string>("Email"),
                    Phone = row.Field<string>("Telefon"),
                    Country = row.Field<string>("Ulke"),
                    City = row.Field<string>("Sehir"),
                    CompanyName = row.Field<string>("FirmaAdi"),
                    TaxNo = row.Field<string>("VergiNo"),
                    TaxOffice = row.Field<string>("VergiDaire"),
                    SicilNo = row.Field<string>("KartNo"),
                    Note = row.Field<string>("Aciklama"),
                }).ToList();

                return Json(new ResponseMessage { Message = "OK", Status = true, Data = list }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(new ResponseMessage { Message = "Dosya okuma hatası." + ex.Message, Code = ex.StackTrace }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UpsertMusteriPost(string code, Musteriler model)
        {
            if (model == null)
            {
                return Json(new ResponseMessage { Message = "Model zorunludur." }, JsonRequestBehavior.AllowGet);
            }

            var resp = CustomerDataAccess.UpsertMusteriPost(code, model);

            return Json(resp, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteAllMusteri(string code)
        {
            return Json(CustomerDataAccess.DeleteAllMusteri(code), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteAllTedarik(string code)
        {
            return Json(CustomerDataAccess.DeleteAllTedarik(code), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteLineMusteri(Guid id)
        {
            return Json(CustomerDataAccess.DeleteLineMusteri(id), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult DeleteLineTedarik(Guid id)
        {
            return Json(CustomerDataAccess.DeleteLineTedarik(id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteLineStok(Guid id)
        {
            return Json(CustomerDataAccess.DeleteLineStok(id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ExcelTedarikPost(HttpPostedFileBase file)
        {
            if (file == null)
            {
                return Json(new ResponseMessage { Message = "Dosya zorunludur" }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                FileIoHelper.AddSyncFile(file, "Tedarik");

                var readedList = FileIoHelper.ReadFile("Tedarik");

                var list = readedList.Select(row => new ProjeTedarikciler
                {
                    CariKodu= row.Field<string>("CariKodu"),
                    CariUnvan = row.Field<string>("CariUnvan"),
                    KullaniciAdi = row.Field<string>("KullaniciAdi"),
                    Sifre= row.Field<string>("Parola"),
                }).ToList();

                return Json(new ResponseMessage { Message = "OK", Status = true, Data = list }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(new ResponseMessage { Message = "Dosya okuma hatası." + ex.Message, Code = ex.StackTrace }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UpsertTedarikciPost(ProjeTedarikciler model)
        {
            if (model == null)
            {
                return Json(new ResponseMessage { Message = "Model zorunludur." }, JsonRequestBehavior.AllowGet);
            }

            var resp = CustomerDataAccess.UpsertTedarikciPost(model);

            return Json(resp, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult OpenTedarikUrunler(string code,Guid id)
        {
            var list = ProjeDataAccess.GetTedarikciUrunler(code, id);
            ViewBag.List = list;
            ViewBag.Code = code;
            ViewBag.Id = id;
            return PartialView("_TedarikciUrunlerPartial");
        }

        [HttpPost]
        public JsonResult ExcelMalzemePost(HttpPostedFileBase file)
        {
            if (file == null)
            {
                return Json(new ResponseMessage { Message = "Dosya zorunludur" }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                FileIoHelper.AddSyncFile(file, "Stok");

                var readedList = FileIoHelper.ReadFile("Stok");

                var list = readedList.Select(row => new ProjeTedarikciUrunler
                {
                    StokKodu = row.Field<string>("StokKodu"),
                    StokAdi= row.Field<string>("StokAdi"),
                    Aciklama2= row.Field<string>("Aciklama2"),
                    GrupKodu = row.Field<string>("GrupKodu"),
                    Marka = row.Field<string>("Marka"),
                    SatisFiyat =double.Parse(row.Field<string>("SatisFiyati")),
                    Barkodu = row.Field<string>("Barkodu"),
                    SeriAdedi=int.Parse(row.Field<string>("SeriAdedi")),
                    Doviz= row.Field<string>("Doviz")
                }).ToList();

                return Json(new ResponseMessage { Message = "OK", Status = true, Data = list }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(new ResponseMessage { Message = "Dosya okuma hatası." + ex.Message, Code = ex.StackTrace }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult UpsertStockPost(ProjeTedarikciUrunler model)
        {
            if (model == null)
            {
                return Json(new ResponseMessage { Message = "Model zorunludur." }, JsonRequestBehavior.AllowGet);
            }

            var resp = CustomerDataAccess.UpsertStockPost(model);
            return Json(resp, JsonRequestBehavior.AllowGet);
        }
    }
}