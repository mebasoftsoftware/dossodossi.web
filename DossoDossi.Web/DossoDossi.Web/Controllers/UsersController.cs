﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DossoDossi.Data.DataAccess;
using DossoDossi.Data.Entity;
using DossoDossi.Data.Helper;
using PagedList;

namespace DossoDossi.Web.Controllers
{
    [LoginFilter]
    public class UserController : Controller
    {
        // GET: Users
        public ActionResult Index(string filtre, int? page = 1)
        {

            var user = SessionHelper.User;
            if (user == null)
                return RedirectToAction("Login", "Account");

            if (!user.Roles.CheckRole("U1"))
            {
                return RedirectToAction("Index", "Home");
            }

            using (var db = new DBDataContext(UtilityHelper.Connection))
            {
                var sorgu = db.Accounts.AsQueryable();
                
                if (!string.IsNullOrEmpty(filtre))
                {
                    sorgu = sorgu.Where(a => a.Name.Contains(filtre)|| a.UserName.Contains(filtre));
                }

                var liste = sorgu.OrderBy(a => a.Name).ToPagedList(page.Value, 20);


                var mangdsIds = liste.ToList().Where(a => a.ParentId.HasValue).Select(a => a.ParentId.Value).Distinct().ToList();

                if (mangdsIds.Any())
                {
                    ViewBag.Parents = db.Accounts.Where(a => mangdsIds.Contains(a.Id)).ToList();
                }


                return View(liste);
            };
        }

        public PartialViewResult Detail(int id)
        {
            var list = AccountDataAccess.GetById(id);
            ViewBag.List = list;
            return PartialView("_AccountsDetailPartial");

        }

        public ActionResult Insert()
        {
            var parent = AccountDataAccess.All();
            ViewBag.Parent = parent;
            ViewBag.Isyeri = LogoDataAccess.GetIsyeri();
            ViewBag.Bolum = LogoDataAccess.GetBolumler();
            ViewBag.Roles = AccountDataAccess.GetAllRoles();

            ViewBag.Saticilar = LogoDataAccess.GetSaticiKodlar();

            ViewBag.Projeler = ProjeDataAccess.GetTumProjeler();

            return View();
        }

        [HttpPost]
        public ActionResult Insert(Account model,List<string> codes)
        {
            model.QU= Guid.NewGuid();
            model.CreateDatetime = DateTime.Now;
            model.Status = true;
            var data = AccountDataAccess.Insert(model);
            if (data)
            {
                return RedirectToAction("Index");
            }

            ViewBag.Hata = "An error occured please try again later.";

            var parent = AccountDataAccess.All();
            ViewBag.Parent = parent;
            ViewBag.Roles = AccountDataAccess.GetAllRoles();

            return View(model);
        }

        public ActionResult Update(int id)
        {
            var data = AccountDataAccess.GetById(id);
            var parent = AccountDataAccess.All();
            ViewBag.Parent = parent;
            ViewBag.Roles = AccountDataAccess.GetAllRoles();

            ViewBag.Isyeri = LogoDataAccess.GetIsyeri();
            ViewBag.Bolum = LogoDataAccess.GetBolumler();
            ViewBag.Saticilar = LogoDataAccess.GetSaticiKodlar();
            ViewBag.Projeler = ProjeDataAccess.GetTumProjeler();

            return View(data);
        }

        [HttpPost]
        public ActionResult Update(Account model)
        {
            var data = AccountDataAccess.Update(model);
            if (data)
            {
                return RedirectToAction("Index");
            }

            ViewBag.Hata = "An error occured please try again later.";
            var parent = AccountDataAccess.All();
            ViewBag.Parent = parent;
         

            return View();
        }

        public ActionResult Active(int id)
        {
            var data = AccountDataAccess.Active(id);
            if (data)
            {
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            var sonuc = AccountDataAccess.Delete(id);

            return RedirectToAction("Index");
        }

        public ActionResult Roles(int? id=null)
        {
            var user = SessionHelper.User;
            if (user == null)
                return RedirectToAction("Login", "Account");

            if (!user.Roles.CheckRole("U5"))
            {
                return RedirectToAction("Index", "Home");
            }

            ViewBag.All =  AccountDataAccess.All();

            if (id.HasValue)
            {
                ViewBag.User = AccountDataAccess.GetById(id.Value);
                ViewBag.OwnRoles = AccountDataAccess.GetRoleCodes(id.Value);
                ViewBag.Roles = AccountDataAccess.GetAllRoles();
            }

            return View();
        }

        [HttpPost]
        public JsonResult UpdateRoles(int id, List<string> arr)
        {
            AccountDataAccess.DeleteRoles(id);

            AccountDataAccess.InsertRoles(id, arr);

            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}