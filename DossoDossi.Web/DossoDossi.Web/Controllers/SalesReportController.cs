﻿using DossoDossi.Data.Entity;
using DossoDossi.Data.Helper;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace DossoDossi.Web.Controllerss
{
    public class SalesReportController : Controller
    {
        // GET: SalesReport
        public ActionResult Index(DateTime? start=null, DateTime? end=null)
        {
            var t = DateTime.Today;
            var first = new DateTime(t.Year, t.Month, 1);
            ViewBag.Start = start ?? first;
            ViewBag.End = end ?? first.AddMonths(1).AddDays(-1);

            Session["f_start"] = start ?? first;
            Session["f_end"] = end ?? first.AddMonths(1).AddDays(-1);

            return View();
        }

        //public PartialViewResult SalesReportGridPartial()
        //{
        //    var start = (DateTime)Session["f_start"];
        //    var end = (DateTime)Session["f_end"];
        //    using (var db = new DBDataContext(UtilityHelper.Connection))
        //    {
        //        var siparis = db.View_SiparisDetaylis.Where(a => a.FisTarih >= start && a.FisTarih <= end).ToList();
        //        return PartialView("_SalesReportGridPartial",siparis);
        //    }
        //}

    }
}