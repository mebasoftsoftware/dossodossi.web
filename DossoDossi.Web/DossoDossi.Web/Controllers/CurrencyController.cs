﻿using DossoDossi.Data.DataAccess;
using System.Web.Mvc;

namespace DossoDossi.Web.Controllers
{

    public class CurrencyController : Controller
    {
        // GET: Currency
        public PartialViewResult DailyExchange()
        {
            var list = CurrencyDataAccess.GetCurrency();
            return PartialView("_DailyExchangeListPartial", list);
        }
    }
}