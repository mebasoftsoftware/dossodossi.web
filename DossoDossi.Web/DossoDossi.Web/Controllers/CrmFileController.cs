﻿using DossoDossi.Data.DataAccess;
using DossoDossi.Data.Helper;
using DossoDossi.Data.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DossoDossi.Web.Controllers
{
    [LoginFilter]
    public class CrmFileController : BaseController
    {
        // GET: DossoDossiFile
        public PartialViewResult Open(int type, string recordId)
        {
            ViewBag.Type = type;
            ViewBag.RecordId = recordId;
            var list = FileDataAccess.GetFiles(type, recordId);
            return PartialView("_OpenPartial",list);
        }


        public PartialViewResult GetFiles(int type, string recordId)
        {
            var list = FileDataAccess.GetFiles(type, recordId);
            return PartialView("_ListPartial", list);
        }

        [HttpPost]
        public JsonResult Delete(Guid id)
        {
            return Json(FileDataAccess.Delete(id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FileAdd(int type,string recordId, HttpPostedFileBase[] file)
        {
            if (file == null || file[0] == null || file.Length == 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            foreach (var ifile in file)
            {
                var guid = Guid.NewGuid();
                var extpath = Path.GetExtension(ifile.FileName);

                var fileName = guid + extpath;
                var localfolder = "/Files/" + type + "/" + recordId+ "/";
                var directoryMapPath = Server.MapPath("~" + localfolder);
                var path = Path.Combine(directoryMapPath, fileName);

                var localpath = localfolder + fileName;

                if (!Directory.Exists(directoryMapPath))
                {
                    Directory.CreateDirectory(directoryMapPath);
                }

                ifile.SaveAs(path);

                var add = new DossoDossiFileDto
                {
                    RecordId = recordId,
                    TableId = type,
                    FilePath = localpath,
                    FileName = ifile.FileName,
                    Id = Guid.NewGuid(),
                    CreatedOn = DateTime.Now
                };

                FileDataAccess.Create(add);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }

    }
}