﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DossoDossi.Data.DataAccess;
using DossoDossi.Data.Helper;
using DossoDossi.Data.Models;
using DossoDossi.Data.Models.Report;


namespace DossoDossi.Web.Controllers
{
    [LoginFilter]
    public class FinanceReportController : Controller
    {
        // GET: FinanceReport
        public ActionResult Index()
        {

            ViewBag.AllFirm = RaporDataAccess.GetAllFirm();

            return View();
        }

        public PartialViewResult Level2Finans()
        {
            ViewBag.ToplamTL = RaporDataAccess.GetToplamBankaBakiyeTL("CARDTYPE = 1");

            ViewBag.ToplamUSD = RaporDataAccess.GetToplamBankaBakiyeDiger("CARDTYPE = 3 AND TRCURR = 1");

            ViewBag.ToplamEUR = RaporDataAccess.GetToplamBankaBakiyeDiger("CARDTYPE = 3 AND TRCURR = 20");

            ViewBag.KasaToplam = RaporDataAccess.GetToplamKasaBakiye();

            ViewBag.MusteriToplam = RaporDataAccess.GetToplamMusteri();

            ViewBag.TedarikciToplam = RaporDataAccess.GetToplamTedarikci();

            ViewBag.CekToplamTL = RaporDataAccess.GetToplamCekBakiye("TL");
            ViewBag.CekToplamUSD = RaporDataAccess.GetToplamCekBakiye("USD");
            ViewBag.CekToplamEUR = RaporDataAccess.GetToplamCekBakiye("EURO");

            ViewBag.SenetToplamTL = RaporDataAccess.GetToplamSenetBakiye("TL");
            ViewBag.SenetToplamUSD = RaporDataAccess.GetToplamSenetBakiye("USD");
            ViewBag.SenetToplamEUR = RaporDataAccess.GetToplamSenetBakiye("EURO");

            ViewBag.USDTR = "";
            ViewBag.EURTR = "";

            return PartialView("_Level2Finans");
        }

        public PartialViewResult DateReport(DateTime start,DateTime end,int firmNo)
        {
            var list = RaporDataAccess.GetDailyReport(start, end, firmNo);

            ViewBag.ToplamTL = RaporDataAccess.GetBankBalanceDynmaic("",firmNo);

            ViewBag.BankaDetailList = RaporDataAccess.GetBankBalanceDynmaicDetailList("", firmNo);


            ViewBag.BorcAlacakList = RaporDataAccess.BorcAlacakCariGrupList(firmNo);

            ViewBag.MasrafToplam = RaporDataAccess.AlinanHizmetMasrafToplam(start, end, firmNo);

            ViewBag.InventoryTotal = RaporDataAccess.InventoryTotal(firmNo);

            return PartialView("_DateReportPartial", list);
        }

        public PartialViewResult DateReportByCountry(DateTime start, DateTime end, int firmNo)
        {
            var list = RaporDataAccess.GetDailyReportByCountry(start, end, firmNo);

            ViewBag.ToplamTL = RaporDataAccess.GetBankBalanceDynmaic("", firmNo);

            ViewBag.BankaDetailList = RaporDataAccess.GetBankBalanceDynmaicDetailList("", firmNo);

            ViewBag.BorcAlacakList = RaporDataAccess.BorcAlacakCariGrupList(firmNo);

            ViewBag.MasrafToplam = RaporDataAccess.AlinanHizmetMasrafToplam(start, end, firmNo);

            ViewBag.InventoryTotal = RaporDataAccess.InventoryTotal(firmNo);
            
            ViewBag.Start = start;
            ViewBag.End = end;

            return PartialView("_DateReportByCountryPartial", list);
        }

        public PartialViewResult GetExpenseDetailListPartial(DateTime start, DateTime end, int firmNo)
        {
            var resp = RaporDataAccess.AlinanHizmetMasrafList(start, end, firmNo);

            return PartialView("_DateExpenseReportPartial", resp);
        }
        public PartialViewResult GetInventoryDetailListPartial(int firmNo)
        {
            var resp = RaporDataAccess.InventoryTotalDetailList(firmNo);

            return PartialView("_DateInventoryReportPartial", resp);
        }

        public PartialViewResult Level2MalzemeStok()
        {
            var list = RaporDataAccess.GetMalzemeStoklar();
            return PartialView("_Level2MalzemeStok", list);
        }

        public PartialViewResult Level2Cari()
        {
            return PartialView("_Level2Cari");
        }
        public PartialViewResult Level2Malzeme()
        {
            return PartialView("_Level2Malzeme");
        }
        public PartialViewResult Level2Satis()
        {
            ViewBag.UrunGrupToplam = RaporDataAccess.GetUrunGrupToplam();
            return PartialView("_Level2Satis");
        }

        public PartialViewResult Level2Alis()
        {
            return PartialView("_Level2Alis");
        }

        public PartialViewResult Level2Uretim()
        {
            return PartialView("_Level2Uretim");
        }

        public PartialViewResult Level3Banka(string id, string curr)
        {
            List<R_Bankalar> bankalar = RaporDataAccess.GetBankalar();
            ViewBag.Bankalar = bankalar;

            var list = new List<BankaBakiyeDto>();
            if (curr == "TL")
            {
                list = RaporDataAccess.GetBankaveBakiyelerGroupTL(id);
            }
            else
            {
                list = RaporDataAccess.GetBankaveBakiyelerGroupDiger(id);
            }

            ViewBag.Currency = curr;
            ViewBag.Sorgu = id;
            return PartialView("_Level3Banka", list);
        }
        public PartialViewResult Level3FinansKasa()
        {
            var list = RaporDataAccess.GetKasaBakiyeler();

            return PartialView("_Level3FinansKasa", list);
        }

        public PartialViewResult Level3BorcluCari()
        {
            var list = RaporDataAccess.GetBorcluCariler();

            return PartialView("_Level3BorcluCari", list);
        }

        public PartialViewResult Level3AlacakCari()
        {
            var list = RaporDataAccess.GetAlacakCariler();
            return PartialView("_Level3AlacakCari", list);
        }

        public PartialViewResult Level3Musteriler(string id)
        {
            var list = RaporDataAccess.GetMusterilerList(id);
            ViewBag.AllList = RaporDataAccess.GetMusterilerAllList(id);

            return PartialView("_Level3Musteriler", list);
        }


        public PartialViewResult Level3Cekler(string doviz)
        {
            //var list = RaporDataAccess.GetCekler();

            var list = RaporDataAccess.CekToplamByCsTuruGroupBy(doviz);

            ViewBag.GroupSum = list;
            ViewBag.Doviz = doviz;

            //ViewBag.MusteriCekiPort = RaporDataAccess.CekToplamByCsTuru("Musteri Ceki","Portfoyde");
            //ViewBag.MusteriCekiTah = RaporDataAccess.CekToplamByCsTuru("Musteri Ceki", "Tahsil Edildi");
            //
            //ViewBag.KendiCekimizPort = RaporDataAccess.CekToplamByCsTuru("Kendi Cekimiz","Portfoyde");
            //ViewBag.KendiCekimizTah = RaporDataAccess.CekToplamByCsTuru("Kendi Cekimiz", "Tahsile Edildi");

            return PartialView("_Level3Cekler");
        }

        public PartialViewResult Level5Senetler(string doviz, string csTuru, string durumu, string yil, string ay)
        {


            var list = RaporDataAccess.GetSenetler(doviz, yil, ay, csTuru, durumu);

            return PartialView("_Level5Senetler", list);
        }

        public PartialViewResult Level5GridSenet(string doviz, string csTuru, string durumu, string yil, string ay)
        {

            ViewBag.Doviz = doviz;

            var list = RaporDataAccess.GetSenetlerList(doviz, csTuru, durumu, yil, ay);
            return PartialView("_Level5GridSenet", list);
        }

        public PartialViewResult Level4SenetlerAyBazli(string doviz, string csTuru, string durumu)
        {
            ViewBag.Doviz = doviz;

            var list = RaporDataAccess.GetSenetlerAyBazli(doviz, csTuru, durumu);
            ViewBag.GroupSum = list;
            ViewBag.Doviz = doviz;
            ViewBag.CsTuru = csTuru;
            ViewBag.Durumu = durumu;

            return PartialView("_Level4SenetlerAyBazli");
        }

        public PartialViewResult Level3SenetlerGenel(string doviz)
        {
            ViewBag.Doviz = doviz;

            var list = RaporDataAccess.GetSenetlerGenel(doviz);
            ViewBag.GroupSum = list;

            return PartialView("_Level3SenetlerGenel");
        }

        public PartialViewResult Level3UrunGrup()
        {
            var list = RaporDataAccess.GetUrunGrup();
            return PartialView("_Level3UrunGrup", list);
        }

        public PartialViewResult Level3AyBazli()
        {
            var list = RaporDataAccess.GetAyBazli();

            return PartialView("_Level3AyBazli", list);
        }

        public PartialViewResult Level3Bolge()
        {
            var list = RaporDataAccess.GetBolge();

            return PartialView("_Level3Bolge", list);
        }

        public PartialViewResult Level3SatisEleman()
        {
            var list = RaporDataAccess.GetSatisEleman();

            return PartialView("_Level3SatisEleman", list);
        }
        public PartialViewResult Level3Cari()
        {
            var list = RaporDataAccess.GetCariBazli();

            return PartialView("_Level3CariBazli", list);
        }

        public PartialViewResult Level3TicariMal()
        {
            return PartialView("_Level3TicariMal");
        }

        public PartialViewResult Level3Mamul()
        {
            return PartialView("_Level3Mamul");
        }

        public PartialViewResult Level3Demirbas()
        {
            return PartialView("_Level3Demirbas");
        }

        public PartialViewResult Level3Siparis()
        {
            return PartialView("_Level3Siparis");
        }

        public PartialViewResult Level3Irsaliye()
        {
            return PartialView("_Level3Irsaliye");
        }

        public PartialViewResult Level3Fatura()
        {
            return PartialView("_Level3Fatura");
        }

        public PartialViewResult Level3AlisFatura()
        {
            return PartialView("_Level3AlisFatura");
        }

        public PartialViewResult Level3UretimEmir()
        {
            return PartialView("_Level3UretimEmir");
        }

        public PartialViewResult Level4GridBanka(string hesapkodu, string curr, string sorgu)
        {
            var list = RaporDataAccess.GetBankaveBakiyelerList(hesapkodu, sorgu);

            ViewBag.Currency = curr;

            return PartialView("_Level4GridBanka", list);
        }

        public PartialViewResult Level4GridBankaDateRange(DateTime start, DateTime end,int firmNo,string bankCode)
        {
            var list = RaporDataAccess.GetBankaveBakiyelerListDateRange(start,end,firmNo,bankCode);
            
            return PartialView("_DatePaymentReport", list);
        }

        public PartialViewResult Level4GridKasa(string kodu)
        {
            var list = RaporDataAccess.GetKasaBakiyelerList(kodu);
            ViewBag.Kodu = kodu;

            return PartialView("_Level4GridKasa", list);
        }

        public PartialViewResult Level4GridKasaTable(string kodu, DateTime? start, DateTime? end)
        {
            var list = RaporDataAccess.GetKasaBakiyelerList(kodu);
            if (start.HasValue)
            {
                list = list.Where(a => a.Tarih >= start).ToList();
            }

            if (end.HasValue)
            {
                list = list.Where(a => a.Tarih <= end).ToList();
            }

            return PartialView("_Level4GridKasaTable", list);
        }

        public PartialViewResult Level3MalzemeHareket(string kodu)
        {
            var list = RaporDataAccess.GetMalzemeHareket(kodu);
            ViewBag.Kodu = kodu;

            return PartialView("_Level3MalzemeHareket", list);
        }

        public PartialViewResult Level3MalzemeHareketTable(string kodu, DateTime? start, DateTime? end)
        {
            var list = RaporDataAccess.GetMalzemeHareket(kodu);
            if (start.HasValue)
            {
                list = list.Where(a => a.Tarih >= start).ToList();
            }

            if (end.HasValue)
            {
                list = list.Where(a => a.Tarih <= end).ToList();
            }

            return PartialView("_Level3MalzemeHareketTable", list);
        }

        public PartialViewResult Level4GridCariBorc(string kodu)
        {
            var list = RaporDataAccess.GetBorcluCarilerList(kodu);
            return PartialView("_Level4GridCariBorc", list);
        }

        public PartialViewResult Level4GridCariAlacak(string kodu)
        {
            var list = RaporDataAccess.GetAlacakCarilerList(kodu);
            return PartialView("_Level4GridCariAlacak", list);
        }

        public PartialViewResult Level4GridCek(string turu, string durum, string doviz, string yil, string ay)
        {
            var list = RaporDataAccess.GetCeklerList(turu, durum, doviz, yil, ay);
            ViewBag.Doviz = doviz;
            return PartialView("_Level4GridCek", list);
        }

        public PartialViewResult Level4GridCekAyBazli(string turu, string durum, string doviz)
        {
            ViewBag.Turu = turu;
            ViewBag.Durum = durum;
            ViewBag.Doviz = doviz;

            var list = RaporDataAccess.GetCeklerList(turu, durum, doviz);

            return PartialView("_Level4GridCekAyBazli", list);
        }


        public PartialViewResult Level4GridUrunGrup(string kodu)
        {
            var list = RaporDataAccess.GetUrunGrupList(kodu);
            ViewBag.Kodu = kodu;

            return PartialView("_Level4GridUrunGrup", list);
        }

        public PartialViewResult Level4GridUrunGrupTable(string kodu, DateTime? start, DateTime? end)
        {
            var list = RaporDataAccess.GetUrunGrupList(kodu);
            if (start.HasValue)
            {
                list = list.Where(a => a.Tarih >= start).ToList();
            }

            if (end.HasValue)
            {
                list = list.Where(a => a.Tarih <= end).ToList();
            }

            return PartialView("_Level4GridUrunGrupTable", list);
        }

        public PartialViewResult Level4GridAyBazli(int kodu)
        {
            var list = RaporDataAccess.GetAyBazliList(kodu);
            ViewBag.Kodu = kodu;

            return PartialView("_Level4GridAyBazli", list);
        }

        public PartialViewResult Level4GridAyBazliTable(int kodu, DateTime? start, DateTime? end)
        {
            var list = RaporDataAccess.GetAyBazliList(kodu);
            if (start.HasValue)
            {
                list = list.Where(a => a.Tarih >= start).ToList();
            }

            if (end.HasValue)
            {
                list = list.Where(a => a.Tarih <= end).ToList();
            }

            return PartialView("_Level4GridAyBazliTable", list);
        }

        public PartialViewResult Level4GridBolge(string kodu)
        {
            var list = RaporDataAccess.GetBolgeList(kodu);
            ViewBag.Kodu = kodu;

            return PartialView("_Level4GridBolge", list);
        }

        public PartialViewResult Level4GridBolgeTable(string kodu, DateTime? start, DateTime? end)
        {
            var list = RaporDataAccess.GetBolgeList(kodu);
            if (start.HasValue)
            {
                list = list.Where(a => a.Tarih >= start).ToList();
            }

            if (end.HasValue)
            {
                list = list.Where(a => a.Tarih <= end).ToList();
            }

            return PartialView("_Level4GridBolgeTable", list);
        }

        //public PartialViewResult Level4GridSatisEleman(string kodu)
        //{
        //    var list = RaporDataAccess.GetSatisElemanList(kodu);
        //    ViewBag.Kodu = kodu;

        //    return PartialView("_Level4GridSatisEleman", list);
        //}

        public PartialViewResult Level4SehirSatisEleman(string kodu)
        {
            var list = RaporDataAccess.GetSatisElemanBolgelerToplam(kodu);
            ViewBag.Kodu = kodu;

            return PartialView("_Level4SehirSatisEleman", list);
        }

        public PartialViewResult Level5SatisElemanSehirGrid(string satici, string sehir)
        {
            ViewBag.Satici = satici;
            ViewBag.Sehir = sehir;
            var list = RaporDataAccess.GetSatisElemanList(satici, sehir);

            return PartialView("_Level5SatisElemanSehirGrid", list);
        }

        public PartialViewResult Level5SatisElemanSehirGridTable(string sehir, string satici, DateTime? start, DateTime? end)
        {
            var list = RaporDataAccess.GetSatisElemanList(satici, sehir);
            if (start.HasValue)
            {
                list = list.Where(a => a.Tarih >= start).ToList();
            }

            if (end.HasValue)
            {
                list = list.Where(a => a.Tarih <= end).ToList();
            }

            return PartialView("_Level4GridSatisElemanTable", list);
        }
        public PartialViewResult Level4GridCariBazli(string kodu)
        {
            var list = RaporDataAccess.GetCariBazliList(kodu);
            ViewBag.Kodu = kodu;

            return PartialView("_Level4GridCariBazli", list);
        }

        public PartialViewResult Level4GridCariBazliTable(string kodu, DateTime? start, DateTime? end)
        {
            var list = RaporDataAccess.GetCariBazliList(kodu);
            if (start.HasValue)
            {
                list = list.Where(a => a.Tarih >= start).ToList();
            }

            if (end.HasValue)
            {
                list = list.Where(a => a.Tarih <= end).ToList();
            }

            return PartialView("_Level4GridCariBazliTable", list);
        }

        public ActionResult Level4GridBanka_V()
        {
            return View();
        }

        public PartialViewResult Level4GrupBankaDetay(string kodu, string curr, string sorgu)
        {
            var bankalar = RaporDataAccess.GetBankalar().Where(a => a.BankaAdi == kodu).ToList();
            ViewBag.Bankalar = bankalar;
            var list = new List<BankaBakiyeDto>();

            if (curr == "TL")
            {
                list = RaporDataAccess.GetBankaveBakiyelerTL(kodu, sorgu);
            }
            else
            {
                list = RaporDataAccess.GetBankaveBakiyelerDiger(kodu, sorgu);
            }


            ViewBag.Currency = curr;
            ViewBag.Sorgu = sorgu;
            return PartialView("_Level4GrupBankaDetay", list);
        }
    }
}