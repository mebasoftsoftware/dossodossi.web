﻿using DossoDossi.Data.Helper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace DossoDossi.Web.Controllers
{
    public class BaseController : Controller
    {

        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            base.OnActionExecuting(ctx);
            var l = ctx.HttpContext.Request.QueryString["l"];
            SetLang(l);
        }


        public BaseController()
        {
            SetLang(null);
        }

        public void SetLang(string lang)
        {
            string culture = "";

            try
            {
                if (!lang.ToControl())
                {
                    if (lang == "tr" || lang == "tr-TR")
                    {
                        culture = "tr-TR";
                    }
                    else if (lang == "en" || lang == "en-US")
                    {
                        culture = "en-US";
                    }
                    else
                    {
                        culture = "tr-TR";
                    }

                    Session["language"] = culture;
                }
                else
                {
                    culture = GetLang();
                }

                ViewBag.Lang = culture;

                Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(culture);
                Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(culture);
            }
            catch (Exception e)
            {
            }
        }

        public static string GetLang()
        {
            var cookie = System.Web.HttpContext.Current.Session["language"];

            if (cookie != null && cookie.ToString() != null)
            {
                return cookie.ToString();
            }
            else
            {

            }

            return "tr-TR";

        }
    }
}