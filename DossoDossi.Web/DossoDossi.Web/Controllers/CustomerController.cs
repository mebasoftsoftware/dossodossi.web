﻿using DossoDossi.Data.DataAccess;
using DossoDossi.Data.Helper;
using DossoDossi.Data.Models;
using DossoDossi.Data.Tiger;
using System.Web;
using System.Web.Mvc;

namespace DossoDossi.Web.Controllers
{
    [LoginFilter]
    public class CustomerController : Controller
    {
        // GET: Customer
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetList(Pager pager,string isall,string start)
        {
            var res = CustomerDataAccess.GetList(pager,isall,start);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetById(int id)
        {
            return Json(CustomerDataAccess.GetById(id), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Transaction(int id)
        {
            ViewBag.Data = CustomerDataAccess.GetById(id);
            return View();
        }

        [HttpPost]
        public JsonResult GetListTransaction(Pager pager, int id)
        {
            var res = CustomerDataAccess.GetListTransaction(pager, id);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult OpenSearch()
        {
            return PartialView("_OpenSearchPartial");
        }

        public PartialViewResult OpenAdres(string code)
        {
            var list = LogoDataAccess.GetCariSevkAdres(code);
            ViewBag.Cari = CustomerDataAccess.GetByCode(code);
            return PartialView("_OpenAddressPartial",list);
        }

        public ActionResult UpsertAddress(int id)
        {
            ViewBag.Country = LogoDataAccess.GetCountry();

            ViewBag.Cari = CustomerDataAccess.GetById(id);
            var list = LogoDataAccess.GetCariSevkAdres(id);
            return View(list);
        }

        public PartialViewResult CreateSevkAdres(int id)
        {
            ViewBag.CustId = id;
            return PartialView("_SevkAddressCreatePartial");
        }

        [HttpPost]
        public JsonResult CreateCustomerDeliveryAddress(LogoCustomerDeliveryAddressRequestDto model)
        {
            var resp = new LogoAdresService().CreateCustomerTigerDeliveryAddress(model);
            return Json(resp, JsonRequestBehavior.AllowGet); ;
        }
        public ActionResult CollectiveInsert()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CollectiveInsert(HttpPostedFileBase file)
        {
            return View();
        }
    }
}