﻿function RandomGet() {
    return Math.floor(Math.random() * 100000000);
}
function SatirEkle() {
    var no = RandomGet();

    var innerHtml = Mustache.to_html($('#Template').html(), { DATA: { No: no } });

    $("#List").append(innerHtml);

    OrderLineCal();
}

function OrderLineCal() {
    var no = 1;
    $(".ordernoline").each(function () {
        $(this).text(no);
        no++;
    });
}
function SatirSil(no) {


    bootbox.confirm("Satırı silmek istedipinize eminmisiniz?",
        function (result) {
            if (result === true) {

                setTimeout(function () {
                    $(".list_line_" + no).remove();
                    OrderLineCal();
                },
                    300);

            }
        });

}
function GetAllItems() {
    var res = [];

    var validLine = true;

    $(".allitemlistlines").each(function () {

        var no = $(this).attr("data-no");
        var add = {
            StandId: $("#standId").val(),
            ProductId: $("#productId_" + no).val(),
            SuplierId: $("#suplierId").val(),
            FairId: $("#fairId").val(),
            Qty: $("#qty_" + no).val(),
            Price: $("#price_" + no).val(),
            Currency: $("#currency_" + no).val(),
        };

        if (add.ProductId < 0 || add.ProductId == "" || add.ProductId == undefined || add.ProductId == null) {
            Erp.U.P.T.Warning("Warning !", 'Lütfen  en az 1 satır giriniz.');
            validLine = false;
        }
        if (add.Qty < 0 || add.Qty == "" || add.Qty == undefined || add.Qty == null) {
            Erp.U.P.T.Warning("Warning !", 'Lütfen  adet sayısı yazınız.');
            validLine = false;
        }

        if (validLine == true) {
            res.push(add);
        }
    });

    if (validLine == false) {
        return [];
    }

    return res;
}
function FormKaydet(id) {
    var items = GetAllItems();

    if (items.length == 0) {
        Erp.U.P.T.Warning("Warning !", 'Lütfen en az bir satır giriniz.');
        return false;
    }
    if (items.length < 0) {
        Erp.U.P.T.Warning("Warning !", 'Lütfen en az bir satır  giriniz.');
        return false;
    }
    bootbox.confirm("Kaydetmek istediğinie emin misiniz?",
        function (result) {
            if (result === true) {
                Erp.U.A.Req.C("POST",
                    "/Fair/StandInproductsInsert",
                    { items: items ,id:id},
                    function (resp) {

                        if (resp.Status == true) {

                            Erp.U.P.T.Success("success", resp.Message);
                            //setTimeout(function () {
                            //    window.location.reload();
                            //}, 2000);


                        } else {
                            $(".loadinPro").text(data.Message);
                            Erp.U.P.T.Warning("Warning ", data.Message);

                        }
                    });


            }

        });

}