﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DossoDossi.Web
{
    public class RouteConfig
    {
        //public static void RegisterRoutes(RouteCollection routes)
        //{
        //    routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

        //    routes.MapRoute(
        //        name: "Default",
        //        url: "{controller}/{action}/{id}",
        //        defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
        //    );
        //}

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional },
            //    new string[] { "BusinessPlan.Web.Controllers" }
            //);

            routes.MapRoute(
         //    name: "Default",
         //    url: "{controller}/{action}/{id}",
         //    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
         //);

         "Default", // Route name
         "{controller}/{action}/{id}", // URL with parameters
         new
         {
             controller = "Account",
             action = "Login",
             id = UrlParameter.Optional,
         },
         new string[] { "DossoDossi.Web.Controllers" }
     );


        }
    }
}
