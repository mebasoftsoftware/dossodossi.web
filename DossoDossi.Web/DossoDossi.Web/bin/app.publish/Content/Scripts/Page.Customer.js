﻿var Customer = {
    GetList: function (currentPage, filter, order, takeCount) {

        Model.U.SaveFilterParam(currentPage, filter, order, takeCount, 'FullName|ASC');
        var form = $("#FilterForm");
        //  $.Block.Open({ target: ".portlet" });
        Model.U.A.Req.F(form, function (data) {
            //console.log(JSON.stringify(data));
            if (data != null && data.Data != null && data.Data.length > 0) {
                //  alert(JSON.stringify(data.Data));
                var count = data.Pager.CurrentPage === 0 ? 1 : (data.Pager.CurrentPage * data.Pager.TakeCount) + 1;
                $.each(data.Data, function (i, item) {
                    item.Index = count;
                    // item.QUERY_DATE = Business.U.TimeFormatNew(item.QUERY_DATE);
                    count++;
                });
            }

            setTimeout(function () {
                //  Metronic.stopPageLoading();
            }, 200);

            var innerHtml = Mustache.to_html($('#Template').html(), { DATA: data.Data });
            $("#List").empty().append(innerHtml);
            Model.U.GetPager(data.Pager, data.Total, "#Pager");
            $("#FilterResult_Total_Count").val(data.Total);
            $('#Pager ul li a').click(function () {
                Customer.GetList($(this).attr("data-page"), data.Pager.Filter, data.Pager.Order);
            });

            //Business.U.ActiveTooltip();
        });
    },
    Event: function () {

        Customer.GetList(null, null, "FullName|ASC", 10);

        $("#TakeNumberList").on('change', function () {
            var t = $(this).val();
            $("#TakeCount").val(t);
            Customer.GetList();
        });
        $("#Refresh").click(function () {
            $("#Filter").val('');
            $("#FiltreInput").val('');
            $(".clearprop").val('');
            $(".clearpropselect").val('-1');

            Customer.GetList(null, null, null, 10);
            $("#TakeNumberList").val('10');
        });
        $("#FiltreButton").click(function () {

            $('#Filter').val($('#FiltreInput').val());
            Customer.GetList(null, null, null, 10);

        });

        Model.U.KeyPressActive('#FiltreInput', '#FiltreButton');
        Model.U.SortOrderColumn(function (orderResult) {
            Customer.GetList(null, null, orderResult, 0);
        });
    },
    Create: {
        Open: function () {
            Model.U.P.Modal.Open("/Customer/Create", {}, function () {

            }, null, null, null, true);

        },
        Save: function () {
            var name = $("#Name").val();
            if (name == null || name == undefined || name == "") {
                Business.U.P.T.Warning("Uyarı !  : ", "Müşteri adı zorunludur");
                return false;
            }
            var code = $("#Code").val();
            if (code == null || code == undefined || code == "") {
                Business.U.P.T.Warning("Uyarı !  : ", "Müşteri kodu zorunludur");
                return false;
            }
            var form = $("#frmCreate");
            //  $.Block.Open({ target: ".portlet" });
            Business.U.A.Req.F(form, function (data) {
                if (data.Status) {
                    Business.U.P.T.Success("İşlem Sonucu : ", data.Message);
                    Customer.GetList(null, null, "Id|DESC", 10);
                    $(".clsbuttoncustomer").click();
                } else {
                    Business.U.P.T.Warning("Uyarı !  : ", data.Message);
                }
            });
        }
    },
    Edit: {
        Open: function () {

        },

        Save: function () {
            var name = $("#Name").val();
            if (name == null || name == undefined || name == "") {
                Business.U.P.T.Warning("Uyarı !  : ", "Müşteri adı zorunludur");
                return false;
            }
            var code = $("#Code").val();
            if (code == null || code == undefined || code == "") {
                Business.U.P.T.Warning("Uyarı !  : ", "Müşteri kodu zorunludur");
                return false;
            }

            var form = $("#frmUpdateCustomer");
            //  $.Block.Open({ target: ".portlet" });
            Business.U.A.Req.F(form, function (data) {
                if (data.Status) {
                    Business.U.P.T.Success("İşlem Sonucu : ", data.Message);
                } else {
                    Business.U.P.T.Warning("Uyarı !  : ", data.Message);
                }
            });
        }
    },
    Delete: function (id) {

        bootbox.confirm("Silmek istediğinize emin misiniz ?",
            function (result) {
                if (result === true) {
                    Business.U.A.Req.C("POST", "/Customer/Delete", { id: id }, function (data) {

                        if (!data.Status) {
                            Business.U.P.T.Warning("Uyarı !  : ", data.Message);
                            return false;
                        }

                        Business.U.P.T.Success("İşlem Sonucu : ", data.Message);
                        Customer.GetList(null, null, "Id|DESC", 10);
                    });
                }
            });
    },
    User: {
        Create: {
            Open: function (id) {
                Business.U.P.Modal.Open("/CustomerUser/Create", { id: id }, function () {

                }, null, null, null, true);

            },
            Save: function () {

                var form = $("#frmUserCreate");
                //  $.Block.Open({ target: ".portlet" });
                Business.U.A.Req.F(form, function (data) {
                    if (data.Status) {
                        Business.U.P.T.Success("İşlem Sonucu : ", data.Message);
                        setTimeout(function () { location.reload(); }, 1000);
                    } else {
                        Business.U.P.T.Warning("Uyarı !  : ", data.Message);
                    }
                });

            }
        },
        Edit: {
            Open: function (id) {
                Business.U.P.Modal.Open("/CustomerUser/Edit", { id: id }, function () {

                }, null, null, null, true);
            },
            Save: function () {

                var form = $("#frmUserUpdate");

                //  $.Block.Open({ target: ".portlet" });
                Business.U.A.Req.F(form, function (data) {
                    if (data.Status) {
                        Business.U.P.T.Success("İşlem Sonucu : ", data.Message);
                        setTimeout(function () { location.reload(); }, 1000);
                    } else {
                        Business.U.P.T.Warning("Uyarı !  : ", data.Message);
                    }
                });
            }
        },
        Delete: function (id) {

            bootbox.confirm("Silmek istediğinize emin misiniz ?",
                function (result) {
                    if (result === true) {
                        Business.U.A.Req.C("POST", "/CustomerUser/Delete", { id: id }, function (data) {

                            if (!data.Status) {
                                Business.U.P.T.Warning("Uyarı !  : ", data.Message);
                                return false;
                            }

                            Business.U.P.T.Success("İşlem Sonucu : ", data.Message);
                            Customer.GetList(null, null, "Id|DESC", 10);
                        });
                    }
                });
        }
    }
}