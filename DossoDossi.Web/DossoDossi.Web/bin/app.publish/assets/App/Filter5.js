﻿var Filter5 = {
    GetList: function (currentPage, filter, order, takeCount) {
        Erp.U.SaveFilterParamDifferent("5",currentPage, filter, order, takeCount, 'ID|DESC');
        var form = $("#FilterForm5");
        //  $.Block.Open({ target: ".portlet" });
        Erp.U.A.Req.F(form, function (data) {
            //console.log(JSON.stringify(data));
            if (data != null && data.Data != null && data.Data.length > 0) {
                //  alert(JSON.stringify(data.Data));
                var count = data.Pager.CurrentPage === 0 ? 1 : (data.Pager.CurrentPage * data.Pager.TakeCount) + 1;
                $.each(data.Data, function (i, item) {
                    item.Index = count;
                    
                    item.json = JSON.stringify(item);
                    if (item.Tarih != null) {
                        item.Tarih = Erp.U.TimeFormat(item.Tarih);
                    }
                    count++;
                });
            }
            
            var innerHtml = Mustache.to_html($('#Template5').html(), { DATA: data.Data });
            $("#List5").empty().append(innerHtml);
            Erp.U.GetPager(data.Pager, data.Total, "#Pager5");
            $("#FilterResult_Total_Count5").val(data.Total);
            $("#countTabTotal5").text(data.Total);

            $('#Pager5 ul li a').click(function () {
                Filter5.GetList($(this).attr("data-page"), data.Pager.Filter, data.Pager.Order);
            });
            

            Erp.U.ActiveTooltip();
        });
    },
    Event: function () {

        Filter5.GetList(null, null, "ID|DESC", 10);

        $("#TakeNumberList5").on('change', function () {
            var t = $(this).val();
            $("#TakeCount5").val(t);
            Filter5.GetList();
        });

        $("#Refresh5").click(function () {
            $("#Filter5").val('');
            $("#FiltreInput5").val('');
            $(".clearprop5").val('');
            $(".clearpropselect5").val('-1');
            
            Filter5.GetList(null, null, null, 10);
            $("#TakeNumberList").val('10');
            $("#AppendedItemLiSt").empty();
            $(".addtobaskeymultiplebutton").hide();

        });
        $("#FiltreButton5").click(function () {

            $('#Filter5').val($('#FiltreInput5').val());
            var take = $("#TakeNumberList5").val();
            var page = $("#CurrentPage5").val();
            Filter5.GetList(page,null,null,take);
        });

        Erp.U.KeyPressActive('#FiltreInput5', '#FiltreButton5');

        Erp.U.SortOrderColumnDif('tablecontent5', function (orderResult) {
            Filter5.GetList(null, null, orderResult, 0);
        });
    }
    
}
